﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgiCorp.Models
{
    public class Institution
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PostCode { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string TelephoneCountryCode { get; set; }

        public string TelephoneStateCode { get; set; }

        public string TelephoneNumber { get; set; }
        public bool IsActive { get; set; }
    }
}