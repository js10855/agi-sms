﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class ClientHelper
    {
        public const string KeyFieldName = "ClientID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALCountry balCountry = new BALCountry();
            BALAgent balAgent = new BALAgent();
            BALResidence balResidence = new BALResidence();
            BALClientType balClientType = new BALClientType();
            BALEthnicity balEthnicity = new BALEthnicity();
            BALClientGender balClientGender = new BALClientGender();
            BALClientNationality balClientNationality = new BALClientNationality();
            BALClientVisaType balClientVisaType = new BALClientVisaType();

            var columns = new MVCxGridViewColumnCollection();
            columns.Add("StudentReference");
            columns.Add("FirstName");
            columns.Add("LastName");
            columns.Add("MiddleName").Visible = false;
            columns.Add("DateOfBirth").Visible = false;
            columns.Add("Photo").Visible = false;
            columns.Add("PassportNumber").Visible = false;
            columns.Add(c =>
            {
                c.FieldName = "ResidenceID";
                c.Caption = "Residence";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ResidenceID";
                    p.ValueType = typeof(int);
                    p.BindList(balResidence.BALResidence_Get_All());
                });
            });
            columns.Add(c =>
            {
                c.FieldName = "ClientTypeID";
                c.Caption = "Client Type";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ClientTypeID";
                    p.ValueType = typeof(int);
                    p.BindList(balClientType.BALClientType_Get_All());
                });
            });
            columns.Add("NSNNumber").Visible = false;
            columns.Add("WorkNumber").Visible = false;
            columns.Add("PublicTrustNumber").Visible = false;
            columns.Add("ExternalRef1").Visible = false;
            columns.Add("ExternalRef2").Visible = false;


            columns.Add("Address1");
            columns.Add("Address2").Visible = false;
            columns.Add("Address3").Visible = false;
            columns.Add("PostCode");
            columns.Add("City");
            columns.Add("State");

            columns.Add(c =>
            {
                c.FieldName = "CountryID";
                c.Caption = "Country";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Country";
                    p.ValueField = "CountryID";
                    p.ValueType = typeof(int);
                    p.BindList(balCountry.BALCountry_Get_All());
                });
            });
            columns.Add("Email").Visible = false;
            columns.Add("PhoneCountryCode").Visible = false;
            columns.Add("PhoneStateCode").Visible = false;
            columns.Add("Phone").Visible = false;
            columns.Add(c =>
            {
                c.FieldName = "EthnicityID";
                c.Caption = "Ethnicity";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "EthnicityID";
                    p.ValueType = typeof(int);
                    p.BindList(balEthnicity.BALEthnicity_Get_All());
                });
            });
            columns.Add("IwiAffiliation1").Visible = false;
            columns.Add("IwiAffiliation2").Visible = false;
            columns.Add("IwiAffiliation3").Visible = false;
            columns.Add("IwiAffiliation4").Visible = false;
            columns.Add("IwiAffiliation5").Visible = false;

            columns.Add(c =>
            {
                c.FieldName = "ClientGenderID";
                c.Caption = "Client Gender";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ClientGenderID";
                    p.ValueType = typeof(int);
                    p.BindList(balClientGender.BALClientGender_Get_All());
                });
            });

            columns.Add(c =>
            {
                c.FieldName = "ClientNationalityID";
                c.Caption = "Client Nationality";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ClientNationalityID";
                    p.ValueType = typeof(int);
                    p.BindList(balClientNationality.BALClientNationality_Get_All());
                });
            });

            columns.Add(c =>
            {
                c.FieldName = "ClientVisaTypeID";
                c.Caption = "Client VisaType";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ClientVisaTypeID";
                    p.ValueType = typeof(int);
                    p.BindList(balClientVisaType.BALClientVisaType_Get_All());
                });
            });

            columns.Add(c =>
            {
                c.FieldName = "AgentID";
                c.Caption = "Agent";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "EntityName";
                    p.ValueField = "AgentID";
                    p.ValueType = typeof(int);
                    p.BindList(balAgent.BALAgent_Get_All());
                });
            });

            
            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}