﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class CampusHelper
    {
        public const string KeyFieldName = "CampusID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALCountry balCountry = new BALCountry();
            var columns = new MVCxGridViewColumnCollection();           
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });
            columns.Add("Description");
            columns.Add("Address1");
            columns.Add("Address2").Visible = false;
            columns.Add("Address3").Visible = false;
            columns.Add("PostCode");
            columns.Add("City");
            columns.Add("State");

            columns.Add(c =>
            {
                c.FieldName = "CountryID";
                c.Caption = "Country";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Country";
                    p.ValueField = "CountryID";
                    p.ValueType = typeof(int);
                    p.BindList(balCountry.BALCountry_Get_All());
                });
            });
            columns.Add("Email").Visible = false;
            columns.Add("PhoneCountryCode").Visible = false;
            columns.Add("PhoneStateCode").Visible = false;
            columns.Add("Phone").Visible = false;
            columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);            
            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}