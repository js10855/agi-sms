﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Web.Mvc;
using AgiCorp.BLL;

namespace AgiCorp.Code.Helpers
{
    public class StaffHelper
    {
        public const string KeyFieldName = "StaffID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALCountry balCountry = new BALCountry();
            BALStaffType balStaffType = new BALStaffType();
            BALStaffStatus balStaffStatus = new BALStaffStatus();

            var columns = new MVCxGridViewColumnCollection();          
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });
            columns.Add("Salutation");
            columns.Add("FirstName");
            columns.Add("MiddleName");
            columns.Add("LastName");
            columns.Add("DateOfBirth");
            columns.Add("WorkEmail");
            columns.Add("HomeEmail");
            columns.Add("Phone");
            columns.Add("Mobile");
            columns.Add("Address1");
            columns.Add("Address2").Visible = false;
            columns.Add("Address3").Visible = false;           
            columns.Add("City");
            columns.Add("State");

            columns.Add(c =>
            {
                c.FieldName = "CountryID";
                c.Caption = "Country";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Country";
                    p.ValueField = "CountryID";
                    p.ValueType = typeof(int);
                    p.BindList(balCountry.BALCountry_Get_All());
                });
            });
            columns.Add(c =>
            {
                c.FieldName = "StaffTypeID";
                c.Caption = "Staff Type";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "StaffTypeID";
                    p.ValueType = typeof(int);
                    p.BindList(balStaffType.BALStaffType_Get_All());
                });
            });
            columns.Add(c =>
            {
                c.FieldName = "StatusID";
                c.Caption = "Staff Status";


                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "StatusID";
                    p.ValueType = typeof(int);
                    p.BindList(balStaffStatus.BALStaffStatus_Get_All());
                });
            });
                        
            columns.Add("CostPerHour").Visible = false;           
            columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);
         
            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}