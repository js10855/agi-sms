﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class ModuleHelper
    {
        public const string KeyFieldName = "ModuleID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {          
            var columns = new MVCxGridViewColumnCollection();
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });

            columns.Add("Description");
            columns.Add("Credits");
            columns.Add("HoursPerCredit","Hours Per Credit");
            columns.Add("TotalHoursPerModule", "Total Hours Per Module");
            columns.Add("F2FHours", "F2FHours");
            columns.Add("SDHours", "SDHours");
            columns.Add("ClassF2F", "ClassF2F");
            columns.Add("PlacementF2F", "PlacementF2F");
            columns.Add("MinPassPercentage","Min Pass Percentage");
            columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);


            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}