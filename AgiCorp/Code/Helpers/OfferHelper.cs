﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class OfferHelper
    {
        public const string KeyFieldName = "OfferID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALCountry balCountry = new BALCountry();
            BALEntityType balEntityType = new BALEntityType();

            var columns = new MVCxGridViewColumnCollection();
           
            columns.Add("ClientID", "Client");
            columns.Add("OfferType");
            columns.Add("ProgrammeID", "Programme");
            columns.Add("IntakeID", "Intake");
            columns.Add("OfferYearID", "Offer Year");
            columns.Add("OfferDate");
            columns.Add("OfferCreatedDate");
            columns.Add("OfferLastEditedDate");
            columns.Add("OfferCancelledDate");
            columns.Add("OfferWithdrawnDate");
            columns.Add("OfferStatusID", "Offer Status");           

            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}