﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class IntakeHelper
    {
        public const string KeyFieldName = "IntakeID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALCountry balCountry = new BALCountry();
            BALProgramme balProgramme = new BALProgramme();
            BALFees balFees = new BALFees();
            BALMonth balMonth = new BALMonth();
            BALYear balYear = new BALYear();

            var columns = new MVCxGridViewColumnCollection();
            columns.Add("Code");            
            columns.Add("Description");
            //columns.Add(c =>
            //{
            //    c.FieldName = "ProgrammeID";
            //    c.Caption = "Programme Code";

            //    c.EditorProperties().ComboBox(p =>
            //    {
            //        p.TextField = "Code";
            //        p.ValueField = "ProgrammeID";
            //        p.ValueType = typeof(int);
            //        p.BindList(balProgramme.BALProgramme_Get_All());
            //    });
            //});
            columns.Add(c =>
            {
                c.FieldName = "ProgrammeID";
                c.Caption = "Programme Description";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ProgrammeID";
                    p.ValueType = typeof(int);
                    p.BindList(balProgramme.BALProgramme_Get_All());
                });
            });
            columns.Add(c =>
            {
                c.FieldName = "FeeID";
                c.Caption = "Fees";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Code";
                    p.ValueField = "FeeID";
                    p.ValueType = typeof(int);
                    p.BindList(balFees.BALFees_Get_All());
                });
            });
            columns.Add("Capacity");
            columns.Add(c =>
            {
                c.FieldName = "Month";
                c.Caption = "Month";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "MonthID";
                    p.ValueType = typeof(int);
                    p.BindList(balMonth.BALMonth_Get_All());
                });
            });
            columns.Add(c =>
            {
                c.FieldName = "Year";
                c.Caption = "Year";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "YearID";
                    p.ValueType = typeof(int);
                    p.BindList(balYear.BALYear_Get_All());
                });
            });          
            columns.Add("StartDate").Visible = false;
            columns.Add("EndDate").Visible = false;
            columns.Add("BreakStartDate").Visible = false;
            columns.Add("BreakEndDate").Visible = false;            
            columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);
           
            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}