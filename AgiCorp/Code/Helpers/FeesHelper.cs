﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class FeesHelper
    {
        public const string KeyFieldName = "FeeID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALCountry balCountry = new BALCountry();
            BALProgramme balProgramme = new BALProgramme();

            var columns = new MVCxGridViewColumnCollection();           
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });
            columns.Add("Description");
            //columns.Add("Address1");
            columns.Add(c =>
            {
                c.FieldName = "ProgrammeID";
                c.Caption = "Programme";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ProgrammeID";
                    p.ValueType = typeof(int);
                    p.BindList(balProgramme.BALProgramme_Get_All());
                });
            });
            columns.Add(c =>
            {
                c.FieldName = "CountryID";
                c.Caption = "Country";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Country";
                    p.ValueField = "CountryID";
                    p.ValueType = typeof(int);
                    p.BindList(balCountry.BALCountry_Get_All());
                });
            });

            columns.Add("TutionFees").Visible = false;
            columns.Add("MaterialFees");
            columns.Add("Insurance");
            columns.Add("RegistrationFees");
            columns.Add("AdministrationFees").Visible = false;
            columns.Add("OtherFees").Visible = false;
            columns.Add("TotalFees").Visible = false;
            columns.Add("AirportPickupFees").Visible = false;
            columns.Add("HomeStayPlacementFees").Visible = false;
            columns.Add("HomeStayFeesPerWeek").Visible = false;
            columns.Add("StartDate");
            columns.Add("EndDate");
            columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);            
            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}