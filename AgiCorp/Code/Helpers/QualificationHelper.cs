﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class QualificationHelper
    {
        public const string KeyFieldName = "QualificationID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALQualificationType balQualificationType = new BALQualificationType();
            BALQualificationLevel balQualificationLevel = new BALQualificationLevel();
                    
            var columns = new MVCxGridViewColumnCollection();
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });
            columns.Add("NZQACode","NZQA Code");
            columns.Add("Description");
            columns.Add("Credits");
            columns.Add("F2FHours", "Face 2 Face Hours");
            columns.Add("SDHours", "Self Directed Hours");
            columns.Add(c =>
            {
                c.FieldName = "QualificationLevelID";
                c.Caption = "Qualification Level";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "QualificationLevelID";
                    p.ValueType = typeof(int);
                    p.BindList(balQualificationLevel.BALQualificationLevel_Get_All());
                });
            });
            columns.Add(c =>
            {
                c.FieldName = "QualificationTypeID";
                c.Caption = "Qualification Type";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "QualificationTypeID";
                    p.ValueType = typeof(int);
                    p.BindList(balQualificationType.BALQualificationType_Get_All());
                });
            });
            columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);
            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}