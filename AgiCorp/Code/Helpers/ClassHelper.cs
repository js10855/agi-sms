﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class ClassHelper
    {
        public const string KeyFieldName = "ClassID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALCountry balCountry = new BALCountry();
            BALBuilding balBuilding = new BALBuilding();
            var columns = new MVCxGridViewColumnCollection();           
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });
            columns.Add("Description");
            //columns.Add("Building");
            columns.Add(c =>
            {
                c.FieldName = "BuildingID";
                c.Caption = "Building";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "BuildingID";
                    p.ValueType = typeof(int);
                    p.BindList(balBuilding.BALBuilding_Get_All());
                });
            });
            columns.Add("StudentCapacity");            
            columns.Add("IsActive", MVCxGridViewColumnType.CheckBox);           
            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}