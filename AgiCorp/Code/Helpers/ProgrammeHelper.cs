﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class ProgrammeHelper
    {
        public const string KeyFieldName = "ProgrammeID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALProgrammeLevel balProgrammeLevel = new BALProgrammeLevel();
            var columns = new MVCxGridViewColumnCollection();
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });
            columns.Add("Description");
            columns.Add("NZQACode", "NZQA Code");
            columns.Add("Credits");
            columns.Add("F2FHours", "F2FHours");
            columns.Add("SDHours", "SDHours");
            columns.Add("ClassF2F", "ClassF2F");
            columns.Add("PlacementF2F", "PlacementF2F");

            columns.Add(c =>
            {
                c.FieldName = "ProgrammeLevelID";
                c.Caption = "Programme Level";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ProgrammeLevelID";
                    p.ValueType = typeof(int);
                    p.BindList(balProgrammeLevel.BALProgrammeLevel_Get_All());
                });
            });

            columns.Add("ProgrammeLength", "ProgrammeLength").Visible=false;
            columns.Add("SemesterBreakFrom", "SemesterBreakFrom").Visible = false;
            columns.Add("SemesterBreakTo", "SemesterBreakTo").Visible = false;
            columns.Add("SummerBreakFrom", "SummerBreakFrom").Visible = false;
            columns.Add("SummerBreakTo", "SummerBreakTo").Visible = false;
            columns.Add("ProgrammeRating", "ProgrammeRating").Visible=false;
            columns.Add("StartDate", "StartDate").Visible=false;
            columns.Add("EndDate", "EndDate").Visible=false;
            columns.Add("NotApplicable", "NotApplicable").Visible=false;


            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}