﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgiCorp.BLL;
using DevExpress.Web.Mvc;

namespace AgiCorp.Code.Helpers
{
    public class EquipmentHelper
    {
        public const string KeyFieldName = "EquipmentID";

        static MVCxGridViewColumnCollection exportedColumns;
        public static MVCxGridViewColumnCollection ExportedColumns
        {
            get
            {
                if (exportedColumns == null)
                    exportedColumns = CreateExportedColumns();
                return exportedColumns;
            }
        }

        static GridViewSettings exportGridSettings;
        public static GridViewSettings ExportGridSettings
        {
            get
            {
                if (exportGridSettings == null)
                    exportGridSettings = CreateExportGridSettings();
                return exportGridSettings;
            }
        }

        static MVCxGridViewColumnCollection CreateExportedColumns()
        {
            BALClass balClass = new BALClass();
            BALEquipmentType balEquipmentType = new BALEquipmentType();
            var columns = new MVCxGridViewColumnCollection();
            columns.Add(c => {
                c.FieldName = "Code";
                c.EditorProperties().TextBox(properties => {
                    properties.ClientSideEvents.ValueChanged = "onValueChanged";
                });
            });
            columns.Add("Description");
            columns.Add(c =>
            {
                c.FieldName = "ClassID";
                c.Caption = "Class Room";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "ClassID";
                    p.ValueType = typeof(int);
                    p.BindList(balClass.BALClass_Get_All());
                });
            });
            columns.Add("Quantity");
            columns.Add(c =>
            {
                c.FieldName = "EquipmentTypeID";
                c.Caption = "Equipment Type";

                c.EditorProperties().ComboBox(p =>
                {
                    p.TextField = "Description";
                    p.ValueField = "EquipmentTypeID";
                    p.ValueType = typeof(int);
                    p.BindList(balEquipmentType.BALEquipmentType_Get_All());
                });
            });

            return columns;
        }

        static GridViewSettings CreateExportGridSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = KeyFieldName;
            settings.Columns.Assign(ExportedColumns);
            return settings;
        }

    }
}