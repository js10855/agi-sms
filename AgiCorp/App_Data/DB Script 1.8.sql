-------------------Drop Constraint

if exists(select * from sysobjects where name  ='FK_tblModuleLO_tblLO')
Alter table tblModuleLO drop CONSTRAINT  FK_tblModuleLO_tblLO
GO

if exists(select * from sysobjects where name  ='FK_tblModuleLO_tblModule')
Alter table tblModuleLO drop CONSTRAINT  FK_tblModuleLO_tblModule
GO

if exists(select * from sysobjects where name  ='FK_tblModuleAssessment_tblModuleAssessmentFormat')
Alter table tblModuleAssessment drop CONSTRAINT  FK_tblModuleAssessment_tblModuleAssessmentFormat
GO

if exists(select * from sysobjects where name  ='FK_tblModuleAssessment_tblModuleAssessmentType')
Alter table tblModuleAssessment drop CONSTRAINT  FK_tblModuleAssessment_tblModuleAssessmentType
GO


if exists(select * from sysobjects where name  ='FK_tblGradeMarks_tblGrade')
Alter table tblGradeMarks drop CONSTRAINT  FK_tblGradeMarks_tblGrade
GO

if exists(select * from sysobjects where name  ='FK_tblProgrammeModule_tblModule')
Alter table tblProgrammeModule drop CONSTRAINT  FK_tblProgrammeModule_tblModule
GO


if exists(select * from sysobjects where name  ='FK_tblQualification_tblQualificationLevel')
Alter table tblQualification drop CONSTRAINT  FK_tblQualification_tblQualificationLevel
GO

if exists(select * from sysobjects where name  ='FK_tblQualification_tblQualificationType')
Alter table tblQualification drop CONSTRAINT  FK_tblQualification_tblQualificationType
GO


if exists(select * from sysobjects where name  ='FK_tblQualificationGPO_tblQualification')
Alter table tblQualificationGPO drop CONSTRAINT  FK_tblQualificationGPO_tblQualification
GO

if exists(select * from sysobjects where name  ='FK_tblQualificationGPO_tblGPO')
Alter table tblQualificationGPO drop CONSTRAINT  FK_tblQualificationGPO_tblGPO
GO


if exists(select * from sysobjects where name  ='FK_tblQualificationProgramme_tblQualification')
Alter table tblQualificationProgramme drop CONSTRAINT  FK_tblQualificationProgramme_tblQualification
GO

if exists(select * from sysobjects where name  ='FK_tblQualificationProgramme_tblProgramme')
Alter table tblQualificationProgramme drop CONSTRAINT  FK_tblQualificationProgramme_tblProgramme
GO

if exists(select * from sysobjects where name  ='FK_tblProgrammeModule_tblProgramme')
Alter table tblProgrammeModule drop CONSTRAINT  FK_tblProgrammeModule_tblProgramme
GO


if exists(select * from sysobjects where name  ='FK_tblStaffModule_tblModule')
Alter table tblStaffModule drop CONSTRAINT  FK_tblStaffModule_tblModule
GO

if exists(select * from sysobjects where name  ='FK_tblStaffModule_tblStaff')
Alter table tblStaffModule drop CONSTRAINT  FK_tblStaffModule_tblStaff
GO

if exists(select * from sysobjects where name  ='FK_tblBuilding_tblCampus')
Alter table tblBuilding drop CONSTRAINT  FK_tblBuilding_tblCampus
GO
if exists(select * from sysobjects where name  ='FK_tblClass_tblBuilding')
Alter table tblClass drop CONSTRAINT  FK_tblClass_tblBuilding
GO
if exists(select * from sysobjects where name  ='FK_tblDepartment_tblSchool')
Alter table tblDepartment drop CONSTRAINT  FK_tblDepartment_tblSchool
GO
if exists(select * from sysobjects where name  ='FK_tblEquipment_tblEquipmentType')
Alter table tblEquipment drop CONSTRAINT  FK_tblEquipment_tblEquipmentType
GO
if exists(select * from sysobjects where name  ='FK_tblEquipment_tblClass')
Alter table tblEquipment drop CONSTRAINT  FK_tblEquipment_tblClass
GO

---Drop  Constraints


if exists(select * from sysobjects where name  ='FK_tblGradeMarks_tblGrade')
Alter table tblGradeMarks drop CONSTRAINT  FK_tblGradeMarks_tblGrade
GO


if exists(select * from sysobjects where name  ='FK_tblInstituteSchool_tblInstitution')
Alter table tblInstituteSchool drop CONSTRAINT  FK_tblInstituteSchool_tblInstitution
GO
if exists(select * from sysobjects where name  ='FK_tblInstituteSchool_tblSchool')
Alter table tblInstituteSchool drop CONSTRAINT  FK_tblInstituteSchool_tblSchool
GO
if exists(select * from sysobjects where name  ='FK_SchoolCampus_tblCampus')
Alter table tblSchoolCampus drop CONSTRAINT  FK_SchoolCampus_tblCampus
GO

if exists(select * from sysobjects where name  ='FK_SchoolCampus_tblSchool')
Alter table tblSchoolCampus drop CONSTRAINT  FK_SchoolCampus_tblSchool
GO

if exists(select * from sysobjects where name  ='FK_tblCampusBuilding_tblBuilding')
Alter table tblCampusBuilding drop CONSTRAINT  FK_tblCampusBuilding_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblCampusBuilding_tblCampus')
Alter table tblCampusBuilding drop CONSTRAINT  FK_tblCampusBuilding_tblCampus
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingDepartment_tblBuilding')
Alter table tblBuildingDepartment drop CONSTRAINT  FK_tblBuildingDepartment_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingDepartment_tblDepartment')
Alter table tblBuildingDepartment drop CONSTRAINT  FK_tblBuildingDepartment_tblDepartment
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingClass_tblBuilding')
Alter table tblBuildingClass drop CONSTRAINT  FK_tblBuildingClass_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingClass_tblClass')
Alter table tblBuildingClass drop CONSTRAINT  FK_tblBuildingClass_tblClass
GO

if exists(select * from sysobjects where name  ='FK_tblClassEquipment_tblEquipment')
Alter table tblClassEquipment drop CONSTRAINT  FK_tblClassEquipment_tblEquipment
GO

if exists(select * from sysobjects where name  ='FK_tblClassEquipment_tblClass')
Alter table tblClassEquipment drop CONSTRAINT  FK_tblClassEquipment_tblClass
GO

if exists(select * from sysobjects where name  ='FK_tblDepartmentBuilding_tblBuilding')
Alter table tblDepartmentBuilding drop CONSTRAINT  FK_tblDepartmentBuilding_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblDepartmentBuilding_tblDepartment')
Alter table tblDepartmentBuilding drop CONSTRAINT  FK_tblDepartmentBuilding_tblDepartment
GO

if exists(select * from sysobjects where name  ='FK_tblAgentCommission_tblAgent')
Alter table tblAgentCommission drop CONSTRAINT  FK_tblAgentCommission_tblAgent
GO

if exists(select * from sysobjects where name  ='FK_tblAgentContact_tblAgent')
Alter table tblAgentContact drop CONSTRAINT  FK_tblAgentContact_tblAgent
GO

if exists(select * from sysobjects where name  ='FK_tblAgentCountry_tblAgent')
Alter table tblAgentCountry drop CONSTRAINT  FK_tblAgentCountry_tblAgent
GO

if exists(select * from sysobjects where name  ='FK_tblAgentCountry_tblCountry')
Alter table tblAgentCountry drop CONSTRAINT  FK_tblAgentCountry_tblCountry
GO

if exists(select * from sysobjects where name  ='FK_tblAgentDocument_tblAgent')
Alter table tblAgentDocument drop CONSTRAINT  FK_tblAgentDocument_tblAgent
GO


if exists(select * from sysobjects where name  ='FK_tblClassDepartment_tblDepartment')
Alter table tblClassDepartment drop CONSTRAINT  FK_tblClassDepartment_tblDepartment
GO

if exists(select * from sysobjects where name  ='FK_tblClassDepartment_tblClass')
Alter table tblClassDepartment drop CONSTRAINT  FK_tblClassDepartment_tblClass
GO


Truncate table tblStaffModule
GO

Truncate table tblModuleLO
GO


Truncate table tblBuilding
Go
Truncate table tblClass
Go

Truncate table tblDepartment
Go

Truncate table tblEquipment
Go

Truncate table tblInstituteSchool
Go

Truncate table tblSchoolCampus
Go

Truncate table tblCampusBuilding
Go

Truncate table tblBuildingDepartment
Go

Truncate table tblBuildingClass
Go


Truncate table tblClassEquipment
Go

Truncate table tblDepartmentBuilding
Go

Truncate table tblAgentCommission
Go

Truncate table tblAgentContact
Go

Truncate table tblAgentCountry
Go

Truncate table tblAgentDocument
Go


Truncate table tblClassDepartment
Go


 
--------------1.8 SCRIPT START



Truncate table tblQualificationProgramme
Go
Truncate table tblProgramme
GO

Truncate table tblQualification
Go

Truncate table tblQualificationGPO
Go
Truncate table tblGPO
GO

Truncate table tblQualificationType
GO
Truncate table tblQualificationLevel
GO

Truncate table tblProgrammeModule
GO
Truncate table tblModule
GO
Truncate table tblGradeMarks
GO
Truncate table tblGrade
GO


Truncate table tblModuleAssessment
GO

Truncate table tblModuleAssessmentType
GO

Truncate table tblModuleAssessmentFormat
GO

If exists(select 1 from syscolumns where name ='FormatID' and id in (select id from sysobjects where name ='tblModuleAssessment'))
Alter table tblModuleAssessment drop column FormatID
Go

If exists(select 1 from syscolumns where name ='TypeID' and id in (select id from sysobjects where name ='tblModuleAssessment'))
Alter table tblModuleAssessment drop column TypeID
Go

If Not exists(select 1 from syscolumns where name ='ModuleAssessmentFormatID' and id in (select id from sysobjects where name ='tblModuleAssessment'))
Alter table tblModuleAssessment Add ModuleAssessmentFormatID int
Go

If Not exists(select 1 from syscolumns where name ='ModuleAssessmentTypeID' and id in (select id from sysobjects where name ='tblModuleAssessment'))
Alter table tblModuleAssessment Add ModuleAssessmentTypeID int
Go




ALTER TABLE [dbo].[tblQualificationProgramme]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationProgramme_tblProgramme] FOREIGN KEY([ProgrammeID])
REFERENCES [dbo].[tblProgramme] ([ProgrammeID])
GO

ALTER TABLE [dbo].[tblQualificationProgramme] CHECK CONSTRAINT [FK_tblQualificationProgramme_tblProgramme]
GO




ALTER TABLE [dbo].[tblQualificationProgramme]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationProgramme_tblQualification] FOREIGN KEY([QualificationID])
REFERENCES [dbo].[tblQualification] ([QualificationID])
GO

ALTER TABLE [dbo].[tblQualificationProgramme] CHECK CONSTRAINT [FK_tblQualificationProgramme_tblQualification]
GO







ALTER TABLE [dbo].[tblQualificationGPO]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationGPO_tblGPO] FOREIGN KEY([GPOID])
REFERENCES [dbo].[tblGPO] ([GPOID])
GO

ALTER TABLE [dbo].[tblQualificationGPO] CHECK CONSTRAINT [FK_tblQualificationGPO_tblGPO]
GO




ALTER TABLE [dbo].[tblQualificationGPO]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationGPO_tblQualification] FOREIGN KEY([QualificationID])
REFERENCES [dbo].[tblQualification] ([QualificationID])
GO

ALTER TABLE [dbo].[tblQualificationGPO] CHECK CONSTRAINT [FK_tblQualificationGPO_tblQualification]
GO



ALTER TABLE [dbo].[tblQualification]  WITH CHECK ADD  CONSTRAINT [FK_tblQualification_tblQualificationType] FOREIGN KEY([QualificationTypeID])
REFERENCES [dbo].[tblQualificationType] ([QualificationTypeID])
GO

ALTER TABLE [dbo].[tblQualification] CHECK CONSTRAINT [FK_tblQualification_tblQualificationType]
GO




------verify if Qualification table has data then you have to remove data from other relationship tables associated with Qualification then only you can add this relationship


ALTER TABLE [dbo].[tblQualification]  WITH CHECK ADD  CONSTRAINT [FK_tblQualification_tblQualificationLevel] FOREIGN KEY([QualificationLevelID])
REFERENCES [dbo].[tblQualificationLevel] ([QualificationLevelID])
GO

ALTER TABLE [dbo].[tblQualification] CHECK CONSTRAINT [FK_tblQualification_tblQualificationLevel]
GO



ALTER TABLE [dbo].[tblProgrammeModule]  WITH CHECK ADD  CONSTRAINT [FK_tblProgrammeModule_tblModule] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[tblModule] ([ModuleID])
GO

ALTER TABLE [dbo].[tblProgrammeModule] CHECK CONSTRAINT [FK_tblProgrammeModule_tblModule]
GO





-----tblGradeMarks with tblGrade



ALTER TABLE [dbo].[tblGradeMarks]  WITH CHECK ADD  CONSTRAINT [FK_tblGradeMarks_tblGrade] FOREIGN KEY([GradeID])
REFERENCES [dbo].[tblGrade] ([GradeID])
GO

ALTER TABLE [dbo].[tblGradeMarks] CHECK CONSTRAINT [FK_tblGradeMarks_tblGrade]
GO




IF Exists(select 1 from sysobjects where name like 'SCP_DELETE_BY_GradeID_tblGrade')
Drop Procedure SCP_DELETE_BY_GradeID_tblGrade

GO

CREATE PROCEDURE [dbo].[SCP_DELETE_BY_GradeID_tblGrade]
(
	 	 @GradeID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
		 
		 Delete from tblGradeMarks where GradeID=@GradeID


	 	 DELETE FROM tblGrade
	 	 WHERE GradeID = @GradeID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO


		 
		 
		 
		 
---------------TBLModuleAssessment with TBLModuleAssessmentType



ALTER TABLE [dbo].[tblModuleAssessment]  WITH CHECK ADD  CONSTRAINT [FK_tblModuleAssessment_tblModuleAssessmentType] FOREIGN KEY([ModuleAssessmentTypeID])
REFERENCES [dbo].[tblModuleAssessmentType] ([ModuleAssessmentTypeID])
GO

ALTER TABLE [dbo].[tblModuleAssessment] CHECK CONSTRAINT [FK_tblModuleAssessment_tblModuleAssessmentType]
GO

---------------TBLModuleAssessment with TBLModuleAssessmentFormat

ALTER TABLE [dbo].[tblModuleAssessment]  WITH CHECK ADD  CONSTRAINT [FK_tblModuleAssessment_tblModuleAssessmentFormat] FOREIGN KEY([ModuleAssessmentFormatID])
REFERENCES [dbo].[tblModuleAssessmentFormat] ([ModuleAssessmentFormatID])
GO

ALTER TABLE [dbo].[tblModuleAssessment] CHECK CONSTRAINT [FK_tblModuleAssessment_tblModuleAssessmentFormat]
GO 
		 
		 
		 
		 
IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ModuleID_tblModuleAssessment')
Drop Procedure SCP_GET_BY_ModuleID_tblModuleAssessment

GO



---------------
		 
CREATE PROCEDURE [dbo].[SCP_GET_BY_ModuleID_tblModuleAssessment]
(
	 	 @ModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 Number
,	 	 	 	 Name
,	 	 	 	 ModuleAssessmentTypeID
,	 	 	 	 ModuleAssessmentFormatID
,	 	 	 	 LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 WHERE ModuleID = @ModuleID
	 	 END 


		 
		 
		 
		 
		 
		 
		 
		 
		 
If not exists(select 1 from tblModuleAssessmentType where code ='01')
BEGIN
	Insert into tblModuleAssessmentType (Code,Description) values ('01','Individual');
	Insert into tblModuleAssessmentType (Code,Description) values ('02','Group');
	Insert into tblModuleAssessmentType (Code,Description) values ('03','Research');
	Insert into tblModuleAssessmentType (Code,Description) values ('04','Project');
	Insert into tblModuleAssessmentType (Code,Description) values ('05','Exam');
END


If not exists(select 1 from tblModuleAssessmentFormat where code ='01')
BEGIN
	Insert into tblModuleAssessmentFormat (Code,Description) values ('01','Written');
	Insert into tblModuleAssessmentFormat (Code,Description) values ('02','Presentation');
	Insert into tblModuleAssessmentFormat (Code,Description) values ('03','Research Project');
END








GO

IF Exists(select 1 from sysobjects where name like 'SCP_SET_tblModuleAssessment')
Drop Procedure SCP_SET_tblModuleAssessment
GO


CREATE PROCEDURE [dbo].[SCP_SET_tblModuleAssessment]
(
	 	 	 	 @Number varchar(50)
	 	 	 	 ,@Name varchar(50)
	 	 	 	 ,@ModuleAssessmentTypeID int
	 	 	 	 ,@ModuleAssessmentFormatID int
	 	 	 	 ,@LOCovered varchar(500)
	 	 	 	 ,@TotalMarks int
	 	 	 	 ,@MinPassPercentage int
	 	 	 	 ,@ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblModuleAssessment
	 	 	 	 ( 
	 	 	 	 Number
	 	 	 	 ,Name
	 	 	 	 ,ModuleAssessmentTypeID
	 	 	 	 ,ModuleAssessmentFormatID
	 	 	 	 ,LOCovered
	 	 	 	 ,TotalMarks
	 	 	 	 ,MinPassPercentage
	 	 	 	 ,ModuleID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Number
	 	 	 	 ,@Name
	 	 	 	 ,@ModuleAssessmentTypeID
	 	 	 	 ,@ModuleAssessmentFormatID
	 	 	 	 ,@LOCovered
	 	 	 	 ,@TotalMarks
	 	 	 	 ,@MinPassPercentage
	 	 	 	 ,@ModuleID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 


		 /****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ModuleAssessmentID_tblModuleAssessment]    Script Date: 3/21/2018 4:40:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentID_tblModuleAssessment]
(
	 	 @ModuleAssessmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 Number
,	 	 	 	 Name
,	 	 	 	 ModuleAssessmentTypeID
,	 	 	 	 ModuleAssessmentFormatID
,	 	 	 	 LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 WHERE ModuleAssessmentID = @ModuleAssessmentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblModuleAssessment]    Script Date: 3/21/2018 4:40:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblModuleAssessment]
(
	 	  @ModuleAssessmentID int
	 	 , @Number varchar(50)
	 	 , @Name varchar(50)
	 	 , @ModuleAssessmentTypeID int
	 	 , @ModuleAssessmentFormatID int
	 	 , @LOCovered varchar(500)
	 	 , @TotalMarks int
	 	 , @MinPassPercentage int
	 	 , @ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblModuleAssessment
	 	 SET 
	 	 	 	 	 Number = @Number
	 	 	 	 	 ,Name = @Name
	 	 	 	 	 ,ModuleAssessmentTypeID = @ModuleAssessmentTypeID
	 	 	 	 	 ,ModuleAssessmentFormatID = @ModuleAssessmentFormatID
	 	 	 	 	 ,LOCovered = @LOCovered
	 	 	 	 	 ,TotalMarks = @TotalMarks
	 	 	 	 	 ,MinPassPercentage = @MinPassPercentage
	 	 	 	 	 ,ModuleID = @ModuleID
	 	 	 	 	 
	 	 	 	 	 where ModuleAssessmentID = @ModuleAssessmentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblModuleAssessment]    Script Date: 3/21/2018 4:37:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 Number
,	 	 	 	 Name
,	 	 	 	 ModuleAssessmentTypeID
,	 	 	 	 ModuleAssessmentFormatID
,	 	 	 	 LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 END 
GO

---------------1.8 SCRIPT END
























--------ADD cONSTRAINTS


GO


ALTER TABLE [dbo].[tblDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblDepartment_tblSchool] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[tblSchool] ([SchoolID])
GO

ALTER TABLE [dbo].[tblDepartment] CHECK CONSTRAINT [FK_tblDepartment_tblSchool]
GO




ALTER TABLE [dbo].[tblEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblEquipment_tblEquipmentType] FOREIGN KEY([EquipmentTypeID])
REFERENCES [dbo].[tblEquipmentType] ([EquipmentTypeID])
GO

ALTER TABLE [dbo].[tblEquipment] CHECK CONSTRAINT [FK_tblEquipment_tblEquipmentType]
GO



ALTER TABLE [dbo].[tblEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblEquipment_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblEquipment] CHECK CONSTRAINT [FK_tblEquipment_tblClass]
GO


ALTER TABLE [dbo].[tblInstituteSchool]  WITH CHECK ADD  CONSTRAINT [FK_tblInstituteSchool_tblInstitution] FOREIGN KEY([InstituteID])
REFERENCES [dbo].[tblInstitution] ([InstitutionID])
GO

ALTER TABLE [dbo].[tblInstituteSchool] CHECK CONSTRAINT [FK_tblInstituteSchool_tblInstitution]
GO

ALTER TABLE [dbo].[tblInstituteSchool]  WITH CHECK ADD  CONSTRAINT [FK_tblInstituteSchool_tblSchool] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[tblSchool] ([SchoolID])
GO

ALTER TABLE [dbo].[tblInstituteSchool] CHECK CONSTRAINT [FK_tblInstituteSchool_tblSchool]
GO




ALTER TABLE [dbo].[tblSchoolCampus]  WITH CHECK ADD  CONSTRAINT [FK_SchoolCampus_tblCampus] FOREIGN KEY([CampusID])
REFERENCES [dbo].[tblCampus] ([CampusID])
GO

ALTER TABLE [dbo].[tblSchoolCampus] CHECK CONSTRAINT [FK_SchoolCampus_tblCampus]
GO



ALTER TABLE [dbo].[tblSchoolCampus]  WITH CHECK ADD  CONSTRAINT [FK_SchoolCampus_tblSchool] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[tblSchool] ([SchoolID])
GO

ALTER TABLE [dbo].[tblSchoolCampus] CHECK CONSTRAINT [FK_SchoolCampus_tblSchool]
GO


ALTER TABLE [dbo].[tblCampusBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblCampusBuilding_tblCampus] FOREIGN KEY([CampusID])
REFERENCES [dbo].[tblCampus] ([CampusID])
GO

ALTER TABLE [dbo].[tblCampusBuilding] CHECK CONSTRAINT [FK_tblCampusBuilding_tblCampus]
GO

ALTER TABLE [dbo].[tblCampusBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblCampusBuilding_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblCampusBuilding] CHECK CONSTRAINT [FK_tblCampusBuilding_tblBuilding]
GO





ALTER TABLE [dbo].[tblClass]  WITH CHECK ADD  CONSTRAINT [FK_tblClass_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblClass] CHECK CONSTRAINT [FK_tblClass_tblBuilding]
GO


ALTER TABLE [dbo].[tblBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblBuilding_tblCampus] FOREIGN KEY([CampusID])
REFERENCES [dbo].[tblCampus] ([CampusID])
GO

ALTER TABLE [dbo].[tblBuilding] CHECK CONSTRAINT [FK_tblBuilding_tblCampus]
GO



ALTER TABLE [dbo].[tblProgrammeModule]  WITH CHECK ADD  CONSTRAINT [FK_tblProgrammeModule_tblProgramme] FOREIGN KEY([ProgrammeID])
REFERENCES [dbo].[tblProgramme] ([ProgrammeID])
GO

ALTER TABLE [dbo].[tblProgrammeModule] CHECK CONSTRAINT [FK_tblProgrammeModule_tblProgramme]
GO


ALTER TABLE [dbo].[tblStaffModule]  WITH CHECK ADD  CONSTRAINT [FK_tblStaffModule_tblModule] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[tblModule] ([ModuleID])
GO

ALTER TABLE [dbo].[tblStaffModule] CHECK CONSTRAINT [FK_tblStaffModule_tblModule]
GO




ALTER TABLE [dbo].[tblStaffModule]  WITH CHECK ADD  CONSTRAINT [FK_tblStaffModule_tblStaff] FOREIGN KEY([StaffID])
REFERENCES [dbo].[tblStaff] ([StaffID])
GO

ALTER TABLE [dbo].[tblStaffModule] CHECK CONSTRAINT [FK_tblStaffModule_tblStaff]
GO


ALTER TABLE [dbo].[tblBuildingDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingDepartment_tblDepartment] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[tblDepartment] ([DepartmentID])
GO

ALTER TABLE [dbo].[tblBuildingDepartment] CHECK CONSTRAINT [FK_tblBuildingDepartment_tblDepartment]
GO

ALTER TABLE [dbo].[tblBuildingDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingDepartment_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblBuildingDepartment] CHECK CONSTRAINT [FK_tblBuildingDepartment_tblBuilding]
GO


ALTER TABLE [dbo].[tblBuildingClass]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingClass_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblBuildingClass] CHECK CONSTRAINT [FK_tblBuildingClass_tblClass]
GO

ALTER TABLE [dbo].[tblBuildingClass]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingClass_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblBuildingClass] CHECK CONSTRAINT [FK_tblBuildingClass_tblBuilding]
GO


ALTER TABLE [dbo].[tblClassEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassEquipment_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblClassEquipment] CHECK CONSTRAINT [FK_tblClassEquipment_tblClass]
GO

ALTER TABLE [dbo].[tblClassEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassEquipment_tblEquipment] FOREIGN KEY([EquipmentID])
REFERENCES [dbo].[tblEquipment] ([EquipmentID])
GO

ALTER TABLE [dbo].[tblClassEquipment] CHECK CONSTRAINT [FK_tblClassEquipment_tblEquipment]
GO


ALTER TABLE [dbo].[tblDepartmentBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblDepartmentBuilding_tblDepartment] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[tblDepartment] ([DepartmentID])
GO

ALTER TABLE [dbo].[tblDepartmentBuilding] CHECK CONSTRAINT [FK_tblDepartmentBuilding_tblDepartment]
GO

ALTER TABLE [dbo].[tblDepartmentBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblDepartmentBuilding_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblDepartmentBuilding] CHECK CONSTRAINT [FK_tblDepartmentBuilding_tblBuilding]
GO

ALTER TABLE [dbo].[tblAgentCommission]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentCommission_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentCommission] CHECK CONSTRAINT [FK_tblAgentCommission_tblAgent]
GO


ALTER TABLE [dbo].[tblAgentContact]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentContact_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentContact] CHECK CONSTRAINT [FK_tblAgentContact_tblAgent]
GO






ALTER TABLE [dbo].[tblAgentCountry]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentCountry_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentCountry] CHECK CONSTRAINT [FK_tblAgentCountry_tblAgent]
GO




ALTER TABLE [dbo].[tblAgentCountry]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentCountry_tblCountry] FOREIGN KEY([CountryID])
REFERENCES [dbo].[tblCountry] ([CountryID])
GO

ALTER TABLE [dbo].[tblAgentCountry] CHECK CONSTRAINT [FK_tblAgentCountry_tblCountry]
GO



ALTER TABLE [dbo].[tblAgentDocument]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentDocument_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentDocument] CHECK CONSTRAINT [FK_tblAgentDocument_tblAgent]
GO



ALTER TABLE [dbo].[tblClassDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassDepartment_tblDepartment] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[tblDepartment] ([DepartmentID])
GO

ALTER TABLE [dbo].[tblClassDepartment] CHECK CONSTRAINT [FK_tblClassDepartment_tblDepartment]
GO


ALTER TABLE [dbo].[tblClassDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassDepartment_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblClassDepartment] CHECK CONSTRAINT [FK_tblClassDepartment_tblClass]
GO 
 
ALTER TABLE [dbo].[tblModuleLo]  WITH CHECK ADD  CONSTRAINT [FK_tblModuleLO_tblLO] FOREIGN KEY([LOID])
REFERENCES [dbo].[tblLO] ([LOID])
GO

ALTER TABLE [dbo].[tblModuleLO] CHECK CONSTRAINT [FK_tblModuleLO_tblLO]
GO 

 
ALTER TABLE [dbo].[tblModuleLo]  WITH CHECK ADD  CONSTRAINT [FK_tblModuleLO_tblModule] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[tblModule] ([ModuleID])
GO

ALTER TABLE [dbo].[tblModuleLO] CHECK CONSTRAINT [FK_tblModuleLO_tblModule]
GO 

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblClient]    Script Date: 3/23/2018 8:15:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClient]
(
	 	  @ClientID int
	 	 , @StudentReference varchar(10)
	 	 , @FirstName varchar(50)
	 	 , @MiddleName varchar(50)
	 	 , @LastName varchar(50)
	 	 , @DateOfBirth datetime
	 	 , @Photo varbinary(max)
	 	 , @PassportNumber varchar(20)
	 	 , @ResidenceID int
	 	 , @ClientTypeID int
	 	 , @NSNNumber varchar(50)
	 	 , @WorkNumber varchar(50)
	 	 , @PublicTrustNumber varchar(50)
	 	 , @ExternalRef1 varchar(50)
	 	 , @ExternalRef2 varchar(50)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @EthnicityID int
	 	 , @IwiAffiliation1 varchar(50)
	 	 , @IwiAffiliation2 varchar(50)
	 	 , @IwiAffiliation3 varchar(50)
	 	 , @IwiAffiliation4 varchar(50)
	 	 , @IwiAffiliation5 varchar(50)
	 	 , @AgentID int
		 ,@PhotoFileName varchar(50)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClient
	 	 SET 
	 	 	 	 	 StudentReference = @StudentReference
	 	 	 	 	 ,FirstName = @FirstName
	 	 	 	 	 ,MiddleName = @MiddleName
	 	 	 	 	 ,LastName = @LastName
	 	 	 	 	 ,DateOfBirth = @DateOfBirth
	 	 	 	 	 ,Photo = @Photo
	 	 	 	 	 ,PassportNumber = @PassportNumber
	 	 	 	 	 ,ResidenceID = @ResidenceID
	 	 	 	 	 ,ClientTypeID = @ClientTypeID
	 	 	 	 	 ,NSNNumber = @NSNNumber
	 	 	 	 	 ,WorkNumber = @WorkNumber
	 	 	 	 	 ,PublicTrustNumber = @PublicTrustNumber
	 	 	 	 	 ,ExternalRef1 = @ExternalRef1
	 	 	 	 	 ,ExternalRef2 = @ExternalRef2
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,EthnicityID = @EthnicityID
	 	 	 	 	 ,IwiAffiliation1 = @IwiAffiliation1
	 	 	 	 	 ,IwiAffiliation2 = @IwiAffiliation2
	 	 	 	 	 ,IwiAffiliation3 = @IwiAffiliation3
	 	 	 	 	 ,IwiAffiliation4 = @IwiAffiliation4
	 	 	 	 	 ,IwiAffiliation5 = @IwiAffiliation5
	 	 	 	 	 ,AgentID = @AgentID
					 ,PhotoFileName =@PhotoFileName
	 	 	 	 	 
	 	 	 	 	 where ClientID = @ClientID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClient]    Script Date: 3/23/2018 10:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClient]
(
	 	 	 	 @StudentReference varchar(10)
	 	 	 	 ,@FirstName varchar(50)
	 	 	 	 ,@MiddleName varchar(50)
	 	 	 	 ,@LastName varchar(50)
	 	 	 	 ,@DateOfBirth datetime
	 	 	 	 ,@Photo varbinary(max)
	 	 	 	 ,@PassportNumber varchar(20)
	 	 	 	 ,@ResidenceID int
	 	 	 	 ,@ClientTypeID int
	 	 	 	 ,@NSNNumber varchar(50)
	 	 	 	 ,@WorkNumber varchar(50)
	 	 	 	 ,@PublicTrustNumber varchar(50)
	 	 	 	 ,@ExternalRef1 varchar(50)
	 	 	 	 ,@ExternalRef2 varchar(50)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@EthnicityID int
	 	 	 	 ,@IwiAffiliation1 varchar(50)
	 	 	 	 ,@IwiAffiliation2 varchar(50)
	 	 	 	 ,@IwiAffiliation3 varchar(50)
	 	 	 	 ,@IwiAffiliation4 varchar(50)
	 	 	 	 ,@IwiAffiliation5 varchar(50)
	 	 	 	 ,@AgentID int
				 ,@PhotoFileName varchar(50)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClient
	 	 	 	 ( 
	 	 	 	 StudentReference
	 	 	 	 ,FirstName
	 	 	 	 ,MiddleName
	 	 	 	 ,LastName
	 	 	 	 ,DateOfBirth
	 	 	 	 ,Photo
	 	 	 	 ,PassportNumber
	 	 	 	 ,ResidenceID
	 	 	 	 ,ClientTypeID
	 	 	 	 ,NSNNumber
	 	 	 	 ,WorkNumber
	 	 	 	 ,PublicTrustNumber
	 	 	 	 ,ExternalRef1
	 	 	 	 ,ExternalRef2
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,EthnicityID
	 	 	 	 ,IwiAffiliation1
	 	 	 	 ,IwiAffiliation2
	 	 	 	 ,IwiAffiliation3
	 	 	 	 ,IwiAffiliation4
	 	 	 	 ,IwiAffiliation5
	 	 	 	 ,AgentID
				 ,PhotoFileName
	 	 	 	 ) 
 SELECT 
	 	 	 	 @StudentReference
	 	 	 	 ,@FirstName
	 	 	 	 ,@MiddleName
	 	 	 	 ,@LastName
	 	 	 	 ,@DateOfBirth
	 	 	 	 ,@Photo
	 	 	 	 ,@PassportNumber
	 	 	 	 ,@ResidenceID
	 	 	 	 ,@ClientTypeID
	 	 	 	 ,@NSNNumber
	 	 	 	 ,@WorkNumber
	 	 	 	 ,@PublicTrustNumber
	 	 	 	 ,@ExternalRef1
	 	 	 	 ,@ExternalRef2
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@EthnicityID
	 	 	 	 ,@IwiAffiliation1
	 	 	 	 ,@IwiAffiliation2
	 	 	 	 ,@IwiAffiliation3
	 	 	 	 ,@IwiAffiliation4
	 	 	 	 ,@IwiAffiliation5
	 	 	 	 ,@AgentID
				 ,@PhotoFileName

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO