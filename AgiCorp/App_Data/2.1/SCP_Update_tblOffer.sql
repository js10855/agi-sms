USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_UPDATE_tblOffer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_UPDATE_tblOffer]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblOffer]
(
	 	  @OfferID int
	 	 , @ClientID int
	 	 , @OfferType varchar(15)
	 	 , @ProgrammeID int
	 	 , @IntakeID int
	 	 , @OfferYearID int
	 	 , @OfferDate date
	 	 , @OfferCreatedDate date
	 	 , @OfferLastEditedDate date
	 	 , @OfferCancelledDate date
	 	 , @OfferWithdrawnDate date
	 	 , @OfferStatusID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblOffer
	 	 SET 
	 	 	 	 	 ClientID = @ClientID
	 	 	 	 	 ,OfferType = @OfferType
	 	 	 	 	 ,ProgrammeID = @ProgrammeID
	 	 	 	 	 ,IntakeID = @IntakeID
	 	 	 	 	 ,OfferYearID = @OfferYearID
	 	 	 	 	 ,OfferDate = @OfferDate
	 	 	 	 	 ,OfferCreatedDate = @OfferCreatedDate
	 	 	 	 	 ,OfferLastEditedDate = @OfferLastEditedDate
	 	 	 	 	 ,OfferCancelledDate = @OfferCancelledDate
	 	 	 	 	 ,OfferWithdrawnDate = @OfferWithdrawnDate
	 	 	 	 	 ,OfferStatusID = @OfferStatusID
	 	 	 	 	 
	 	 	 	 	 where OfferID = @OfferID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
