USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ClientNationalityID_tblClientNationality]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ClientNationalityID_tblClientNationality]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientNationalityID_tblClientNationality]
(
	 	 @ClientNationalityID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientNationalityID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientNationality
	 	 WHERE ClientNationalityID = @ClientNationalityID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblClientNationality]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblClientNationality]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblClientNationality]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientNationalityID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientNationality
	 	 END 
GO 
