USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ProgrammeRatingID_tblProgrammeRating]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ProgrammeRatingID_tblProgrammeRating]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ProgrammeRatingID_tblProgrammeRating]
(
	 	 @ProgrammeRatingID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeRatingID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblProgrammeRating
	 	 WHERE ProgrammeRatingID = @ProgrammeRatingID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblProgrammeRating]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblProgrammeRating]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblProgrammeRating]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeRatingID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblProgrammeRating
	 	 END 
GO 
