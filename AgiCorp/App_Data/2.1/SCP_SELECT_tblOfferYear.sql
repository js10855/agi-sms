USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_OfferYearID_tblOfferYear]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_OfferYearID_tblOfferYear]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_OfferYearID_tblOfferYear]
(
	 	 @OfferYearID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferYearID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblOfferYear
	 	 WHERE OfferYearID = @OfferYearID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblOfferYear]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblOfferYear]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblOfferYear]
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferYearID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblOfferYear
	 	 END 
GO 
