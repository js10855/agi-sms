USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ClientGenderID_tblClientGender]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ClientGenderID_tblClientGender]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientGenderID_tblClientGender]
(
	 	 @ClientGenderID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientGenderID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientGender
	 	 WHERE ClientGenderID = @ClientGenderID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblClientGender]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblClientGender]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblClientGender]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientGenderID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientGender
	 	 END 
GO 
