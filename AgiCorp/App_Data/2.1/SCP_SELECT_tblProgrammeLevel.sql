USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ProgrammeLevelID_tblProgrammeLevel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ProgrammeLevelID_tblProgrammeLevel]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ProgrammeLevelID_tblProgrammeLevel]
(
	 	 @ProgrammeLevelID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeLevelID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblProgrammeLevel
	 	 WHERE ProgrammeLevelID = @ProgrammeLevelID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblProgrammeLevel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblProgrammeLevel]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblProgrammeLevel]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeLevelID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblProgrammeLevel
	 	 END 
GO 
