USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_SET_tblOffer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_SET_tblOffer]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_SET_tblOffer]
(
	 	 	 	 @ClientID int
	 	 	 	 ,@OfferType varchar(15)
	 	 	 	 ,@ProgrammeID int
	 	 	 	 ,@IntakeID int
	 	 	 	 ,@OfferYearID int
	 	 	 	 ,@OfferDate date
	 	 	 	 ,@OfferCreatedDate date
	 	 	 	 ,@OfferLastEditedDate date
	 	 	 	 ,@OfferCancelledDate date
	 	 	 	 ,@OfferWithdrawnDate date
	 	 	 	 ,@OfferStatusID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblOffer
	 	 	 	 ( 
	 	 	 	 ClientID
	 	 	 	 ,OfferType
	 	 	 	 ,ProgrammeID
	 	 	 	 ,IntakeID
	 	 	 	 ,OfferYearID
	 	 	 	 ,OfferDate
	 	 	 	 ,OfferCreatedDate
	 	 	 	 ,OfferLastEditedDate
	 	 	 	 ,OfferCancelledDate
	 	 	 	 ,OfferWithdrawnDate
	 	 	 	 ,OfferStatusID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClientID
	 	 	 	 ,@OfferType
	 	 	 	 ,@ProgrammeID
	 	 	 	 ,@IntakeID
	 	 	 	 ,@OfferYearID
	 	 	 	 ,@OfferDate
	 	 	 	 ,@OfferCreatedDate
	 	 	 	 ,@OfferLastEditedDate
	 	 	 	 ,@OfferCancelledDate
	 	 	 	 ,@OfferWithdrawnDate
	 	 	 	 ,@OfferStatusID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
