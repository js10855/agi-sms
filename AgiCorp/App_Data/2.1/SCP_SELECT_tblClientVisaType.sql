USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ClientVisaTypeID_tblClientVisaType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ClientVisaTypeID_tblClientVisaType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientVisaTypeID_tblClientVisaType]
(
	 	 @ClientVisaTypeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientVisaTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientVisaType
	 	 WHERE ClientVisaTypeID = @ClientVisaTypeID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblClientVisaType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblClientVisaType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblClientVisaType]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientVisaTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientVisaType
	 	 END 
GO 
