USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_OfferID_tblOffer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_OfferID_tblOffer]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_OfferID_tblOffer]
(
	 	 @OfferID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
	 	 FROM tblOffer
	 	 WHERE OfferID = @OfferID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblOffer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblOffer]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblOffer]
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
	 	 FROM tblOffer
	 	 END 
GO 
