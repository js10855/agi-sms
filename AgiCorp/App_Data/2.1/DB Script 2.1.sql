﻿Alter table tblProgramme add ProgrammeLevelID int,
ProgrammeLength int,
StartDate date,
EndDate date,
SemesterBreakFromDate date,
SemesterBreakToDate date,
IsSemesterBreakApplicable bit,
SummerBreakFromDate date,
SummerBreakToDate date,
ProgrammeRatingID int;
GO

Alter table tblClient add ClientGenderID int,
ClientNationalityID int,
ClientVisaTypeID int,
ClientGenderID int;
GO

Alter table tblFees add AirportPickupFees float,
HomeStayPlacementFees float,
HomeStayFeesPerWeek float;
GO 

Alter table tblInstitution add AccountName varchar(200),
AccountNumber varchar(200),
BankName varchar(200),
SwiftCode varchar(200),
SPOCEmail varchar(200);
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblClientVisaType]    Script Date: 5/4/2018 4:16:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblClientVisaType](
	[ClientVisaTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblClientVisaType] PRIMARY KEY CLUSTERED 
(
	[ClientVisaTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblClientNationality]    Script Date: 5/4/2018 4:16:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblClientNationality](
	[ClientNationalityID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblClientNationality] PRIMARY KEY CLUSTERED 
(
	[ClientNationalityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblProgrammeRating]    Script Date: 5/4/2018 4:16:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblProgrammeRating](
	[ProgrammeRatingID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblProgrammeRating] PRIMARY KEY CLUSTERED 
(
	[ProgrammeRatingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblProgrammeLevel]    Script Date: 5/4/2018 4:16:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblProgrammeLevel](
	[ProgrammeLevelID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblProgrammeLevel] PRIMARY KEY CLUSTERED 
(
	[ProgrammeLevelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblOfferYear]    Script Date: 5/4/2018 4:16:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblOfferYear](
	[OfferYearID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblOfferYear] PRIMARY KEY CLUSTERED 
(
	[OfferYearID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblOffer]    Script Date: 5/4/2018 4:57:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblOffer](
	[OfferID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NULL,
	[OfferType] [varchar](15) NULL,
	[ProgrammeID] [int] NULL,
	[IntakeID] [int] NULL,
	[OfferYearID] [int] NULL,
	[OfferDate] [date] NULL,
	[OfferCreatedDate] [date] NULL,
	[OfferLastEditedDate] [date] NULL,
	[OfferCancelledDate] [date] NULL,
	[OfferWithdrawnDate] [date] NULL,
	[OfferStatusID] [int] NULL,
 CONSTRAINT [PK_tblOffer] PRIMARY KEY CLUSTERED 
(
	[OfferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tblClientGender]    Script Date: 5/9/2018 4:22:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblClientGender](
	[ClientGenderID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblClientGender] PRIMARY KEY CLUSTERED 
(
	[ClientGenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblClient]    Script Date: 5/9/2018 4:48:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClient]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
,				 ClientGenderID
,				 ClientNationalityID
,				 ClientVisaTypeID
	 	 FROM tblClient
	 	 WHERE ClientID = @ClientID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ProgrammeID_tblProgramme]    Script Date: 5/9/2018 10:37:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ProgrammeID_tblProgramme]
(
	 	 @ProgrammeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F
,				 ProgrammeLevelID
,				 ProgrammeLength
,				 StartDate
,				 EndDate
,				 SemesterBreakFromDate
,				 SemesterBreakToDate
,				 IsSemesterBreakApplicable
,				 SummerBreakFromDate
,				 SummerBreakToDate
,				 ProgrammeRatingID
	 	 FROM tblProgramme
	 	 WHERE ProgrammeID = @ProgrammeID
	 	 END 
GO

ALTER PROCEDURE [dbo].[SCP_GET_BY_FeeID_tblFees]
(
	 	 @FeeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 FeeID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 ProgrammeID
,	 	 	 	 CountryID
,	 	 	 	 IsActive
,	 	 	 	 TutionFees
,	 	 	 	 MaterialFees
,	 	 	 	 Insurance
,	 	 	 	 RegistrationFees
,	 	 	 	 AdministrationFees
,	 	 	 	 OtherFees
,				 TotalFees
,				 AirportPickupFees
,				 HomeStayPlacementFees
,				 HomeStayFeesPerWeek
,	 	 	 	 StartDate
,	 	 	 	 EndDate
	 	 FROM tblFees
	 	 WHERE FeeID = @FeeID
	 	 END 

GO




ALTER PROCEDURE [dbo].[SCP_GET_BY_InstitutionID_tblInstitution]
(
	 	 @InstitutionID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 InstitutionID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 IsActive
,	 	 	 	 AccountName
,	 	 	 	 AccountNumber
,	 	 	 	 BankName
,	 	 	 	 SwiftCode
,	 	 	 	 SPOCEmail
	 	 FROM tblInstitution
	 	 WHERE InstitutionID = @InstitutionID
	 	 END 

GO