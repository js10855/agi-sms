﻿USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ProgrammeID_tblFees]    Script Date: 5/10/2018 5:05:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ProgrammeID_tblFees]
(
	 	 @ProgrammeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 FeeID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 ProgrammeID
,	 	 	 	 CountryID
,	 	 	 	 IsActive
,	 	 	 	 TutionFees
,	 	 	 	 MaterialFees
,	 	 	 	 Insurance
,	 	 	 	 RegistrationFees
,	 	 	 	 AdministrationFees
,	 	 	 	 OtherFees
,				 TotalFees
,				 AirportPickupFees
,				 HomeStayPlacementFees
,				 HomeStayFeesPerWeek
,	 	 	 	 StartDate
,	 	 	 	 EndDate
	 	 FROM tblFees
	 	 WHERE ProgrammeID = @ProgrammeID
	 	 END 
GO


ALTER PROCEDURE [dbo].[SCP_SET_tblClient]
(
	 	 	 	 @StudentReference varchar(10)
	 	 	 	 ,@FirstName varchar(50)
	 	 	 	 ,@MiddleName varchar(50)
	 	 	 	 ,@LastName varchar(50)
	 	 	 	 ,@DateOfBirth datetime
	 	 	 	 ,@Photo varbinary(max)
	 	 	 	 ,@PassportNumber varchar(20)
	 	 	 	 ,@ResidenceID int
	 	 	 	 ,@ClientTypeID int
	 	 	 	 ,@NSNNumber varchar(50)
	 	 	 	 ,@WorkNumber varchar(50)
	 	 	 	 ,@PublicTrustNumber varchar(50)
	 	 	 	 ,@ExternalRef1 varchar(50)
	 	 	 	 ,@ExternalRef2 varchar(50)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@EthnicityID int
	 	 	 	 ,@IwiAffiliation1 varchar(50)
	 	 	 	 ,@IwiAffiliation2 varchar(50)
	 	 	 	 ,@IwiAffiliation3 varchar(50)
	 	 	 	 ,@IwiAffiliation4 varchar(50)
	 	 	 	 ,@IwiAffiliation5 varchar(50)
	 	 	 	 ,@AgentID int
				 ,@PhotoFileName varchar(50)
				 ,@ClientGenderID int
				 ,@ClientNationalityID int
				 ,@ClientVisaTypeID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClient
	 	 	 	 ( 
	 	 	 	 StudentReference
	 	 	 	 ,FirstName
	 	 	 	 ,MiddleName
	 	 	 	 ,LastName
	 	 	 	 ,DateOfBirth
	 	 	 	 ,Photo
	 	 	 	 ,PassportNumber
	 	 	 	 ,ResidenceID
	 	 	 	 ,ClientTypeID
	 	 	 	 ,NSNNumber
	 	 	 	 ,WorkNumber
	 	 	 	 ,PublicTrustNumber
	 	 	 	 ,ExternalRef1
	 	 	 	 ,ExternalRef2
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,EthnicityID
	 	 	 	 ,IwiAffiliation1
	 	 	 	 ,IwiAffiliation2
	 	 	 	 ,IwiAffiliation3
	 	 	 	 ,IwiAffiliation4
	 	 	 	 ,IwiAffiliation5
	 	 	 	 ,AgentID
				 ,PhotoFileName
				 ,ClientGenderID 
				 ,ClientNationalityID 
				 ,ClientVisaTypeID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @StudentReference
	 	 	 	 ,@FirstName
	 	 	 	 ,@MiddleName
	 	 	 	 ,@LastName
	 	 	 	 ,@DateOfBirth
	 	 	 	 ,@Photo
	 	 	 	 ,@PassportNumber
	 	 	 	 ,@ResidenceID
	 	 	 	 ,@ClientTypeID
	 	 	 	 ,@NSNNumber
	 	 	 	 ,@WorkNumber
	 	 	 	 ,@PublicTrustNumber
	 	 	 	 ,@ExternalRef1
	 	 	 	 ,@ExternalRef2
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@EthnicityID
	 	 	 	 ,@IwiAffiliation1
	 	 	 	 ,@IwiAffiliation2
	 	 	 	 ,@IwiAffiliation3
	 	 	 	 ,@IwiAffiliation4
	 	 	 	 ,@IwiAffiliation5
	 	 	 	 ,@AgentID
				 ,@PhotoFileName
				 ,@ClientGenderID 
				 ,@ClientNationalityID 
				 ,@ClientVisaTypeID
				 

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

		 GO
		 
		 
		 
		 
		 ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClient]
(
	 	  @ClientID int
	 	 , @StudentReference varchar(10)
	 	 , @FirstName varchar(50)
	 	 , @MiddleName varchar(50)
	 	 , @LastName varchar(50)
	 	 , @DateOfBirth datetime
	 	 , @Photo varbinary(max)
	 	 , @PassportNumber varchar(20)
	 	 , @ResidenceID int
	 	 , @ClientTypeID int
	 	 , @NSNNumber varchar(50)
	 	 , @WorkNumber varchar(50)
	 	 , @PublicTrustNumber varchar(50)
	 	 , @ExternalRef1 varchar(50)
	 	 , @ExternalRef2 varchar(50)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @EthnicityID int
	 	 , @IwiAffiliation1 varchar(50)
	 	 , @IwiAffiliation2 varchar(50)
	 	 , @IwiAffiliation3 varchar(50)
	 	 , @IwiAffiliation4 varchar(50)
	 	 , @IwiAffiliation5 varchar(50)
	 	 , @AgentID int
		 , @PhotoFileName varchar(50)
		 , @ClientGenderID int
		 , @ClientNationalityID int
		 , @ClientVisaTypeID int
	 	 , @RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClient
	 	 SET 
	 	 	 	 	 StudentReference = @StudentReference
	 	 	 	 	 ,FirstName = @FirstName
	 	 	 	 	 ,MiddleName = @MiddleName
	 	 	 	 	 ,LastName = @LastName
	 	 	 	 	 ,DateOfBirth = @DateOfBirth
	 	 	 	 	 ,Photo = @Photo
	 	 	 	 	 ,PassportNumber = @PassportNumber
	 	 	 	 	 ,ResidenceID = @ResidenceID
	 	 	 	 	 ,ClientTypeID = @ClientTypeID
	 	 	 	 	 ,NSNNumber = @NSNNumber
	 	 	 	 	 ,WorkNumber = @WorkNumber
	 	 	 	 	 ,PublicTrustNumber = @PublicTrustNumber
	 	 	 	 	 ,ExternalRef1 = @ExternalRef1
	 	 	 	 	 ,ExternalRef2 = @ExternalRef2
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,EthnicityID = @EthnicityID
	 	 	 	 	 ,IwiAffiliation1 = @IwiAffiliation1
	 	 	 	 	 ,IwiAffiliation2 = @IwiAffiliation2
	 	 	 	 	 ,IwiAffiliation3 = @IwiAffiliation3
	 	 	 	 	 ,IwiAffiliation4 = @IwiAffiliation4
	 	 	 	 	 ,IwiAffiliation5 = @IwiAffiliation5
	 	 	 	 	 ,AgentID = @AgentID
					 ,PhotoFileName =@PhotoFileName
					 ,ClientGenderID = @ClientGenderID 
				     ,ClientNationalityID = @ClientNationalityID 
				     ,ClientVisaTypeID = @ClientVisaTypeID
	 	 	 	 	 
	 	 	 	 	 where ClientID = @ClientID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO





ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblClient]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
,				 ClientGenderID 
,				 ClientNationalityID 
,				 ClientVisaTypeID
	 	 FROM tblClient
	 	 END 

GO



ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClient]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
,				 ClientGenderID
,				 ClientNationalityID
,				 ClientVisaTypeID
	 	 FROM tblClient
	 	 WHERE ClientID = @ClientID
	 	 END 

GO


ALTER PROCEDURE [dbo].[SCP_UPDate_tblProgramme]
(
	 	  @ProgrammeID int
	 	 , @Code varchar(10)
	 	 , @NZQACode int
	 	 , @Description varchar(200)
	 	 , @Credits int
	 	 , @F2FHours int
	 	 , @SDHours int
		 , @ClassF2F int
         , @PlacementF2F int
		 , @ProgrammeLevelID int
		 , @ProgrammeLength int
		 , @SemesterBreakFromDate Date
		 , @SemesterBreakToDate Date
		 , @SummerBreakFromDate Date
		 , @SummerBreakToDate Date
		 , @ProgrammeRatingID int
		 , @StartDate Date
		 , @EndDate Date
		 , @IsSemesterBreakApplicable bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDate tblProgramme
	 	 SET 
	 	Code = @Code
	 	,NZQACode = @NZQACode
	 	,Description = @Description
	 	,Credits = @Credits
	 	,F2FHours = @F2FHours
	 	,SDHours = @SDHours
	 	,ClassF2F=@ClassF2F
		,PlacementF2F=@PlacementF2F
		,ProgrammeLength = @ProgrammeLength
		,ProgrammeLevelID=@ProgrammeLevelID
		, SemesterBreakFromDate = @SemesterBreakFromDate
		, SemesterBreakToDate = @SemesterBreakToDate
		, SummerBreakFromDate = @SummerBreakFromDate
		, SummerBreakToDate = @SummerBreakToDate
		, ProgrammeRatingID = @ProgrammeRatingID
		, StartDate = @StartDate
		, EndDate = @EndDate
		, IsSemesterBreakApplicable = @IsSemesterBreakApplicable
	 	where ProgrammeID = @ProgrammeID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO




ALTER PROCEDURE [dbo].[SCP_SET_tblProgramme]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@NZQACode int
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Credits int
	 	 	 	 ,@F2FHours int
	 	 	 	 ,@SDHours int
				 ,@ClassF2F int
				 ,@PlacementF2F int
				 , @ProgrammeLevelID int
				 , @ProgrammeLength int
				 , @SemesterBreakFromDate Date
				 , @SemesterBreakToDate Date
				 , @SummerBreakFromDate Date
				 , @SummerBreakToDate Date
				 , @ProgrammeRatingID int
				 , @StartDate Date
				 , @EndDate Date
				 , @IsSemesterBreakApplicable bit
	 	         ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblProgramme
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,NZQACode
	 	 	 	 ,Description
	 	 	 	 ,Credits
	 	 	 	 ,F2FHours
	 	 	 	 ,SDHours
				 ,ClassF2F
				 ,PlacementF2F
				 ,[ProgrammeLevelID]
				, ProgrammeLength 
				 , SemesterBreakFromDate 
				 , SemesterBreakToDate 
				 , SummerBreakFromDate 
				 , SummerBreakToDate 
				 , ProgrammeRatingID 
				 , StartDate 
				 , EndDate 
				 , IsSemesterBreakApplicable 
				  
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@NZQACode
	 	 	 	 ,@Description
	 	 	 	 ,@Credits
	 	 	 	 ,@F2FHours
	 	 	 	 ,@SDHours
				 ,@ClassF2F
				 ,@PlacementF2F
				 ,@ProgrammeLevelID
				 , @ProgrammeLength 
				 , @SemesterBreakFromDate 
				 , @SemesterBreakToDate 
				 , @SummerBreakFromDate 
				 , @SummerBreakToDate 
				 , @ProgrammeRatingID 
				 , @StartDate 
				 , @EndDate 
				 , @IsSemesterBreakApplicable 

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 



GO



ALTER PROCEDURE [dbo].[SCP_GET_BY_ProgrammeID_tblProgramme]
(
	 	 @ProgrammeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F
,				 ProgrammeLevelID
,				 ProgrammeLength
,				 SemesterBreakFromDate
,				 SemesterBreakToDate
,				 SummerBreakFromDate
,				 SummerBreakToDate
,				 ProgrammeRatingID
,				 StartDate
,				 EndDate
,				 IsSemesterBreakApplicable
	 	 FROM tblProgramme
	 	 WHERE ProgrammeID = @ProgrammeID
	 	 END 
go





ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblProgramme]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F 
,				 ProgrammeLevelID
,				 ProgrammeLength
,				 SemesterBreakFromDate
,				 SemesterBreakToDate
,				 SummerBreakFromDate
,				 SummerBreakToDate
,				 ProgrammeRatingID
,				 StartDate
,				 EndDate
,				 IsSemesterBreakApplicable
	 	 FROM tblProgramme
	 	 END 


GO

Alter table tblOffer add 
DocsFileName varchar(50),
[File] varbinary(max);
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblOffer]    Script Date: 5/17/2018 4:13:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblOffer]
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
	 	 FROM tblOffer
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_OfferID_tblOffer]    Script Date: 5/17/2018 4:16:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_OfferID_tblOffer]
(
	 	 @OfferID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
	 	 FROM tblOffer
	 	 WHERE OfferID = @OfferID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblOffer]    Script Date: 5/17/2018 4:19:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblOffer]
(
	 	  @OfferID int
	 	 , @ClientID int
	 	 , @OfferType varchar(15)
	 	 , @ProgrammeID int
	 	 , @IntakeID int
	 	 , @OfferYearID int
	 	 , @OfferDate date
	 	 , @OfferCreatedDate date
	 	 , @OfferLastEditedDate date
	 	 , @OfferCancelledDate date
	 	 , @OfferWithdrawnDate date
	 	 , @OfferStatusID int
		  ,@DocsFileName varchar(50)
		  ,@File varbinary(max)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblOffer
	 	 SET 
	 	 	 	 	 ClientID = @ClientID
	 	 	 	 	 ,OfferType = @OfferType
	 	 	 	 	 ,ProgrammeID = @ProgrammeID
	 	 	 	 	 ,IntakeID = @IntakeID
	 	 	 	 	 ,OfferYearID = @OfferYearID
	 	 	 	 	 ,OfferDate = @OfferDate
	 	 	 	 	 ,OfferCreatedDate = @OfferCreatedDate
	 	 	 	 	 ,OfferLastEditedDate = @OfferLastEditedDate
	 	 	 	 	 ,OfferCancelledDate = @OfferCancelledDate
	 	 	 	 	 ,OfferWithdrawnDate = @OfferWithdrawnDate
	 	 	 	 	 ,OfferStatusID = @OfferStatusID
	 	 	 	 	 ,DocsFileName= @DocsFileName
					 ,[File]= @File

	 	 	 	 	 where OfferID = @OfferID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblOffer]    Script Date: 5/17/2018 4:16:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblOffer]
(
	 	 	 	 @ClientID int
	 	 	 	 ,@OfferType varchar(15)
	 	 	 	 ,@ProgrammeID int
	 	 	 	 ,@IntakeID int
	 	 	 	 ,@OfferYearID int
	 	 	 	 ,@OfferDate date
	 	 	 	 ,@OfferCreatedDate date
	 	 	 	 ,@OfferLastEditedDate date
	 	 	 	 ,@OfferCancelledDate date
	 	 	 	 ,@OfferWithdrawnDate date
	 	 	 	 ,@OfferStatusID int
				 ,@DocsFileName varchar(50)
				 ,@File varbinary(max)
	 			 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblOffer
	 	 	 	 ( 
	 	 	 	 ClientID
	 	 	 	 ,OfferType
	 	 	 	 ,ProgrammeID
	 	 	 	 ,IntakeID
	 	 	 	 ,OfferYearID
	 	 	 	 ,OfferDate
	 	 	 	 ,OfferCreatedDate
	 	 	 	 ,OfferLastEditedDate
	 	 	 	 ,OfferCancelledDate
	 	 	 	 ,OfferWithdrawnDate
	 	 	 	 ,OfferStatusID
				 ,DocsFileName 
	 	 	 	 ,[File]
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClientID
	 	 	 	 ,@OfferType
	 	 	 	 ,@ProgrammeID
	 	 	 	 ,@IntakeID
	 	 	 	 ,@OfferYearID
	 	 	 	 ,@OfferDate
	 	 	 	 ,@OfferCreatedDate
	 	 	 	 ,@OfferLastEditedDate
	 	 	 	 ,@OfferCancelledDate
	 	 	 	 ,@OfferWithdrawnDate
	 	 	 	 ,@OfferStatusID
				 ,@DocsFileName 
	 	 	 	 ,@File

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_DoWithdraw_tblOffer]    Script Date: 5/18/2018 6:36:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_DoWithdraw_tblOffer]
(
	 	  @OfferID int	 	
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblOffer
	 	 SET 	 	 	 	 	
	 	 	 	 	 --OfferLastEditedDate =GETDATE()
	 	 	 	 	 OfferWithdrawnDate = GETDATE()	 	 	 	 	
	 	 	 	 	 ,OfferStatusID = 3
	 	 	 	 	 where OfferID = @OfferID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_DoCancel_tblOffer]    Script Date: 5/18/2018 6:36:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_DoCancel_tblOffer]
(
	 	  @OfferID int	 	
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblOffer
	 	 SET 	 	 	 	 	
	 	 	 	 	 --OfferLastEditedDate =GETDATE()
	 	 	 	 	 OfferCancelledDate = GETDATE()	 	 	 	 	
	 	 	 	 	 ,OfferStatusID = 2
	 	 	 	 	 where OfferID = @OfferID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO


Alter table tblSchool add AccountName varchar(200),
AccountNumber varchar(200),
BankName varchar(200),
SwiftCode varchar(200),
SPOCEmail varchar(200);
GO



ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblSchool]
AS
BEGIN
	 	 SELECT 
	 	 	 	 SchoolID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 IsActive
,	 	 	 	 AccountName
,	 	 	 	 AccountNumber
,	 	 	 	 BankName
,	 	 	 	 SwiftCode
,	 	 	 	 SPOCEmail
	 	 FROM tblSchool
	 	 END 

	GO

	ALTER  PROCEDURE [dbo].[SCP_GET_BY_SchoolID_tblSchool]
(
	 	 @SchoolID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 SchoolID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 IsActive
,	 	 	 	 AccountName
,	 	 	 	 AccountNumber
,	 	 	 	 BankName
,	 	 	 	 SwiftCode
,	 	 	 	 SPOCEmail
	 	 FROM tblSchool
	 	 WHERE SchoolID = @SchoolID
	 	 END 

GO




ALTER  PROCEDURE [dbo].[SCP_SET_tblSchool]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@IsActive bit
				 ,@AccountName varchar(200)
				 ,@AccountNumber  varchar(200)
				 ,@BankName  varchar(200)
				 ,@SwiftCode  varchar(200)
				 ,@SPOCEmail  varchar(200)

	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblSchool
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,IsActive
				,AccountName
				 ,AccountNumber
				 ,BankName
				 ,SwiftCode
				 ,SPOCEmail

	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@IsActive
				 ,@AccountName
				 ,@AccountNumber
				 ,@BankName
				 ,@SwiftCode
				 ,@SPOCEmail
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO



ALTER  PROCEDURE [dbo].[SCP_UPDATE_tblSchool]
(
	 	  @SchoolID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @IsActive bit
		,@AccountName varchar(200)
		,@AccountNumber  varchar(200)
		,@BankName  varchar(200)
		,@SwiftCode  varchar(200)
		,@SPOCEmail  varchar(200)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblSchool
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 	,AccountName = @AccountName
					,AccountNumber = @AccountNumber
					,BankName = @BankName 
					,SwiftCode  = @SwiftCode 
					,SPOCEmail = @SPOCEmail 
	 	 	 	 	 where SchoolID = @SchoolID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblProgramme]    Script Date: 5/22/2018 5:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblProgramme]
(
	 	  @ProgrammeID int
	 	 , @Code varchar(10)
	 	 , @NZQACode int
	 	 , @Description varchar(200)
	 	 , @Credits int
	 	 , @F2FHours int
	 	 , @SDHours int
		 , @ClassF2F int
         , @PlacementF2F int
		 , @ProgrammeLevelID int
		 , @ProgrammeLength int
		 --, @SemesterBreakFromDate Date
		 --, @SemesterBreakToDate Date
		 --, @SummerBreakFromDate Date
		 --, @SummerBreakToDate Date
		 , @ProgrammeRatingID int
		 --, @StartDate Date
		 --, @EndDate Date
		 --, @IsSemesterBreakApplicable bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDate tblProgramme
	 	 SET 
	 	Code = @Code
	 	,NZQACode = @NZQACode
	 	,Description = @Description
	 	,Credits = @Credits
	 	,F2FHours = @F2FHours
	 	,SDHours = @SDHours
	 	,ClassF2F=@ClassF2F
		,PlacementF2F=@PlacementF2F
		,ProgrammeLevelID=@ProgrammeLevelID
		,ProgrammeLength = @ProgrammeLength
		--, SemesterBreakFromDate = @SemesterBreakFromDate
		--, SemesterBreakToDate = @SemesterBreakToDate
		--, SummerBreakFromDate = @SummerBreakFromDate
		--, SummerBreakToDate = @SummerBreakToDate
		, ProgrammeRatingID = @ProgrammeRatingID
		--, StartDate = @StartDate
		--, EndDate = @EndDate
		--, IsSemesterBreakApplicable = @IsSemesterBreakApplicable
	 	where ProgrammeID = @ProgrammeID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblProgramme]    Script Date: 5/22/2018 5:23:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SCP_SET_tblProgramme]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@NZQACode int
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Credits int
	 	 	 	 ,@F2FHours int
	 	 	 	 ,@SDHours int
				 ,@ClassF2F int
				 ,@PlacementF2F int
				 , @ProgrammeLevelID int
				 , @ProgrammeLength int
				 --, @SemesterBreakFromDate Date
				 --, @SemesterBreakToDate Date
				 --, @SummerBreakFromDate Date
				 --, @SummerBreakToDate Date
				 , @ProgrammeRatingID int
				 --, @StartDate Date
				 --, @EndDate Date
				 --, @IsSemesterBreakApplicable bit
	 	         ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblProgramme
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,NZQACode
	 	 	 	 ,Description
	 	 	 	 ,Credits
	 	 	 	 ,F2FHours
	 	 	 	 ,SDHours
				 ,ClassF2F
				 ,PlacementF2F
				 ,[ProgrammeLevelID]
				, ProgrammeLength 
				 --, SemesterBreakFromDate 
				 --, SemesterBreakToDate 
				 --, SummerBreakFromDate 
				 --, SummerBreakToDate 
				 , ProgrammeRatingID 
				 --, StartDate 
				 --, EndDate 
				 --, IsSemesterBreakApplicable 
				  
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@NZQACode
	 	 	 	 ,@Description
	 	 	 	 ,@Credits
	 	 	 	 ,@F2FHours
	 	 	 	 ,@SDHours
				 ,@ClassF2F
				 ,@PlacementF2F
				 ,@ProgrammeLevelID
				 , @ProgrammeLength 
				 --, @SemesterBreakFromDate 
				 --, @SemesterBreakToDate 
				 --, @SummerBreakFromDate 
				 --, @SummerBreakToDate 
				 , @ProgrammeRatingID 
				 --, @StartDate 
				 --, @EndDate 
				 --, @IsSemesterBreakApplicable 

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblProgramme]    Script Date: 5/22/2018 5:22:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblProgramme]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F 
,				 ProgrammeLevelID
,				 ProgrammeLength
--,				 SemesterBreakFromDate
--,				 SemesterBreakToDate
--,				 SummerBreakFromDate
--,				 SummerBreakToDate
,				 ProgrammeRatingID
--,				 StartDate
--,				 EndDate
--,				 IsSemesterBreakApplicable
	 	 FROM tblProgramme
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ProgrammeID_tblProgramme]    Script Date: 5/22/2018 5:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ProgrammeID_tblProgramme]
(
	 	 @ProgrammeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F
,				 ProgrammeLevelID
,				 ProgrammeLength
--,				 SemesterBreakFromDate
--,				 SemesterBreakToDate
--,				 SummerBreakFromDate
--,				 SummerBreakToDate
,				 ProgrammeRatingID
--,				 StartDate
--,				 EndDate
--,				 IsSemesterBreakApplicable
	 	 FROM tblProgramme
	 	 WHERE ProgrammeID = @ProgrammeID
	 	 END 
GO


Alter table tblIntake add 
SemesterBreakFromDate date,
SemesterBreakToDate date,
IsSemesterBreakApplicable bit,
SummerBreakFromDate date,
SummerBreakToDate date,
IsSummerBreakApplicable bit
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblIntake]    Script Date: 5/23/2018 3:53:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblIntake]
AS
BEGIN
	 	 SELECT 
	 	 	 	 IntakeID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 ProgrammeID
,	 	 	 	 FeeID
,	 	 	 	 Capacity
,	 	 	 	 Month
,	 	 	 	 Year
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,				 SemesterBreakFromDate
,				 SemesterBreakToDate
,				 SummerBreakFromDate
,				 SummerBreakToDate
,				 IsSemesterBreakApplicable
,				 IsSummerBreakApplicable
,	 	 	 	 IsActive
	 	 FROM tblIntake
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_IntakeID_tblIntake]    Script Date: 5/23/2018 3:53:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_IntakeID_tblIntake]
(
	 	 @IntakeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 IntakeID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 ProgrammeID
,	 	 	 	 FeeID
,	 	 	 	 Capacity
,	 	 	 	 Month
,	 	 	 	 Year
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,				 SemesterBreakFromDate
,				 SemesterBreakToDate
,				 SummerBreakFromDate
,				 SummerBreakToDate
,				 IsSemesterBreakApplicable
,				 IsSummerBreakApplicable
,	 	 	 	 IsActive
	 	 FROM tblIntake
	 	 WHERE IntakeID = @IntakeID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblIntake]    Script Date: 5/23/2018 3:53:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblIntake]
(
	 	  @IntakeID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(50)
	 	 , @ProgrammeID int
	 	 , @FeeID int
	 	 , @Capacity int
	 	 , @Month int
	 	 , @Year int
	 	 , @StartDate datetime
	 	 , @EndDate datetime
	 	 , @SemesterBreakFromDate Date
		 , @SemesterBreakToDate Date
		 , @SummerBreakFromDate Date
		 , @SummerBreakToDate Date
		 , @IsSemesterBreakApplicable bit
		 , @IsSummerBreakApplicable bit
	 	 , @IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblIntake
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,ProgrammeID = @ProgrammeID
	 	 	 	 	 ,FeeID = @FeeID
	 	 	 	 	 ,Capacity = @Capacity
	 	 	 	 	 ,Month = @Month
	 	 	 	 	 ,Year = @Year
	 	 	 	 	 ,StartDate = @StartDate
	 	 	 	 	 ,EndDate = @EndDate
	 	 	 	 	 ,SemesterBreakFromDate=@SemesterBreakFromDate
,				 SemesterBreakToDate=@SemesterBreakToDate
,				 SummerBreakFromDate=@SummerBreakFromDate
,				 SummerBreakToDate=@SummerBreakToDate
,				 IsSemesterBreakApplicable=@IsSemesterBreakApplicable
,				 IsSummerBreakApplicable=@IsSummerBreakApplicable
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 
	 	 	 	 	 where IntakeID = @IntakeID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblIntake]    Script Date: 5/23/2018 3:53:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblIntake]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(50)
	 	 	 	 ,@ProgrammeID int
	 	 	 	 ,@FeeID int
	 	 	 	 ,@Capacity int
	 	 	 	 ,@Month int
	 	 	 	 ,@Year int
	 	 	 	 ,@StartDate datetime
	 	 	 	 ,@EndDate datetime
	 	 	 	 ,@SemesterBreakFromDate Date
				 ,@SemesterBreakToDate Date
				 ,@SummerBreakFromDate Date
				 ,@SummerBreakToDate Date
				 ,@IsSemesterBreakApplicable bit
				 ,@IsSummerBreakApplicable bit
	 	 	 	 ,@IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblIntake
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,ProgrammeID
	 	 	 	 ,FeeID
	 	 	 	 ,Capacity
	 	 	 	 ,Month
	 	 	 	 ,Year
	 	 	 	 ,StartDate
	 	 	 	 ,EndDate
	 	 		 ,SemesterBreakFromDate
,				 SemesterBreakToDate
,				 SummerBreakFromDate
,				 SummerBreakToDate
,				 IsSemesterBreakApplicable
,				 IsSummerBreakApplicable
	 	 	 	 ,IsActive
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@ProgrammeID
	 	 	 	 ,@FeeID
	 	 	 	 ,@Capacity
	 	 	 	 ,@Month
	 	 	 	 ,@Year
	 	 	 	 ,@StartDate
	 	 	 	 ,@EndDate
	 	 	     ,@SemesterBreakFromDate
,				 @SemesterBreakToDate
,				 @SummerBreakFromDate
,				 @SummerBreakToDate
,				 @IsSemesterBreakApplicable
,				 @IsSummerBreakApplicable
	 	 	 	 ,@IsActive

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

Create Procedure SCP_Get_StudentType
As
BEGIN
Select 'Domestic' as StudentType 
union 
Select 'International' as StudentType 

End
Go

alter table tblclient add StudentType varchar(20) 
Go

Alter PROCEDURE [dbo].[SCP_GET_ALL_tblClient]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
,				 ClientGenderID 
,				 ClientNationalityID 
,				 ClientVisaTypeID
,				 StudentType
	 	 FROM tblClient
	 	 END 

GO





ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClient]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
,				 ClientGenderID
,				 ClientNationalityID
,				 ClientVisaTypeID
,				StudentType
	 	 FROM tblClient
	 	 WHERE ClientID = @ClientID
	 	 END 


GO


ALTER PROCEDURE [dbo].[SCP_SET_tblClient]
(
	 	 	 	 @StudentReference varchar(10)
	 	 	 	 ,@FirstName varchar(50)
	 	 	 	 ,@MiddleName varchar(50)
	 	 	 	 ,@LastName varchar(50)
	 	 	 	 ,@DateOfBirth datetime
	 	 	 	 ,@Photo varbinary(max)
	 	 	 	 ,@PassportNumber varchar(20)
	 	 	 	 ,@ResidenceID int
	 	 	 	 ,@ClientTypeID int
	 	 	 	 ,@NSNNumber varchar(50)
	 	 	 	 ,@WorkNumber varchar(50)
	 	 	 	 ,@PublicTrustNumber varchar(50)
	 	 	 	 ,@ExternalRef1 varchar(50)
	 	 	 	 ,@ExternalRef2 varchar(50)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@EthnicityID int
	 	 	 	 ,@IwiAffiliation1 varchar(50)
	 	 	 	 ,@IwiAffiliation2 varchar(50)
	 	 	 	 ,@IwiAffiliation3 varchar(50)
	 	 	 	 ,@IwiAffiliation4 varchar(50)
	 	 	 	 ,@IwiAffiliation5 varchar(50)
	 	 	 	 ,@AgentID int
				 ,@PhotoFileName varchar(50)
				 ,@ClientGenderID int
				 ,@ClientNationalityID int
				 ,@ClientvisatypeID int
				 ,@StudentType varchar(20)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClient
	 	 	 	 ( 
	 	 	 	 StudentReference
	 	 	 	 ,FirstName
	 	 	 	 ,MiddleName
	 	 	 	 ,LastName
	 	 	 	 ,DateOfBirth
	 	 	 	 ,Photo
	 	 	 	 ,PassportNumber
	 	 	 	 ,ResidenceID
	 	 	 	 ,ClientTypeID
	 	 	 	 ,NSNNumber
	 	 	 	 ,WorkNumber
	 	 	 	 ,PublicTrustNumber
	 	 	 	 ,ExternalRef1
	 	 	 	 ,ExternalRef2
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,EthnicityID
	 	 	 	 ,IwiAffiliation1
	 	 	 	 ,IwiAffiliation2
	 	 	 	 ,IwiAffiliation3
	 	 	 	 ,IwiAffiliation4
	 	 	 	 ,IwiAffiliation5
	 	 	 	 ,AgentID
				 ,PhotoFileName
				 ,ClientGenderID 
				 ,ClientNationalityID 
				 ,ClientvisatypeID
				 ,StudentType
	 	 	 	 ) 
 SELECT 
	 	 	 	 @StudentReference
	 	 	 	 ,@FirstName
	 	 	 	 ,@MiddleName
	 	 	 	 ,@LastName
	 	 	 	 ,@DateOfBirth
	 	 	 	 ,@Photo
	 	 	 	 ,@PassportNumber
	 	 	 	 ,@ResidenceID
	 	 	 	 ,@ClientTypeID
	 	 	 	 ,@NSNNumber
	 	 	 	 ,@WorkNumber
	 	 	 	 ,@PublicTrustNumber
	 	 	 	 ,@ExternalRef1
	 	 	 	 ,@ExternalRef2
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@EthnicityID
	 	 	 	 ,@IwiAffiliation1
	 	 	 	 ,@IwiAffiliation2
	 	 	 	 ,@IwiAffiliation3
	 	 	 	 ,@IwiAffiliation4
	 	 	 	 ,@IwiAffiliation5
	 	 	 	 ,@AgentID
				 ,@PhotoFileName
				 ,@ClientGenderID 
				 ,@ClientNationalityID 
				 ,@ClientvisatypeID
				 ,@StudentType

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO





ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClient]
(
	 	  @ClientID int
	 	 , @StudentReference varchar(10)
	 	 , @FirstName varchar(50)
	 	 , @MiddleName varchar(50)
	 	 , @LastName varchar(50)
	 	 , @DateOfBirth datetime
	 	 , @Photo varbinary(max)
	 	 , @PassportNumber varchar(20)
	 	 , @ResidenceID int
	 	 , @ClientTypeID int
	 	 , @NSNNumber varchar(50)
	 	 , @WorkNumber varchar(50)
	 	 , @PublicTrustNumber varchar(50)
	 	 , @ExternalRef1 varchar(50)
	 	 , @ExternalRef2 varchar(50)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @EthnicityID int
	 	 , @IwiAffiliation1 varchar(50)
	 	 , @IwiAffiliation2 varchar(50)
	 	 , @IwiAffiliation3 varchar(50)
	 	 , @IwiAffiliation4 varchar(50)
	 	 , @IwiAffiliation5 varchar(50)
	 	 , @AgentID int
		 , @PhotoFileName varchar(50)
		 , @ClientGenderID int
		 , @ClientNationalityID int
		 , @ClientvisatypeID int
		 , @StudentType varchar(20)
	 	 , @RETURN_VALUE  INT OUTPUT

)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClient
	 	 SET 
	 	 	 	 	 StudentReference = @StudentReference
	 	 	 	 	 ,FirstName = @FirstName
	 	 	 	 	 ,MiddleName = @MiddleName
	 	 	 	 	 ,LastName = @LastName
	 	 	 	 	 ,DateOfBirth = @DateOfBirth
	 	 	 	 	 ,Photo = @Photo
	 	 	 	 	 ,PassportNumber = @PassportNumber
	 	 	 	 	 ,ResidenceID = @ResidenceID
	 	 	 	 	 ,ClientTypeID = @ClientTypeID
	 	 	 	 	 ,NSNNumber = @NSNNumber
	 	 	 	 	 ,WorkNumber = @WorkNumber
	 	 	 	 	 ,PublicTrustNumber = @PublicTrustNumber
	 	 	 	 	 ,ExternalRef1 = @ExternalRef1
	 	 	 	 	 ,ExternalRef2 = @ExternalRef2
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,EthnicityID = @EthnicityID
	 	 	 	 	 ,IwiAffiliation1 = @IwiAffiliation1
	 	 	 	 	 ,IwiAffiliation2 = @IwiAffiliation2
	 	 	 	 	 ,IwiAffiliation3 = @IwiAffiliation3
	 	 	 	 	 ,IwiAffiliation4 = @IwiAffiliation4
	 	 	 	 	 ,IwiAffiliation5 = @IwiAffiliation5
	 	 	 	 	 ,AgentID = @AgentID
					 ,PhotoFileName =@PhotoFileName
					 ,ClientGenderID = @ClientGenderID 
				     ,ClientNationalityID = @ClientNationalityID 
				     ,ClientvisatypeID = @ClientvisatypeID
					 ,StudentType = @StudentType
	 	 	 	 	 
	 	 	 	 	 where ClientID = @ClientID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO


CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblOffer]
(
	 	 @ClientID int
)
AS
BEGIN
	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID,
	P.Description as Programme,
	O.IntakeID,
	I.Description as Intake,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName
	From TblOffer O
	inner join tblProgramme P on P.ProgrammeID =  O.ProgrammeID
	inner join tblIntake I on I.IntakeID = O.IntakeID
	inner join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	Where ClientID = @ClientID		
END
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].["SCP_GET_BY_IntakeID_tblOffer]    Script Date: 5/29/2018 6:52:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_IntakeID_tblOffer]
(
	 	 @IntakeID int
)
AS
BEGIN
	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID,
	P.Description as Programme,
	O.IntakeID,
	I.Description as Intake,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName
	From TblOffer O
	inner join tblProgramme P on P.ProgrammeID =  O.ProgrammeID
	inner join tblIntake I on I.IntakeID = O.IntakeID
	inner join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	Where O.IntakeID = @IntakeID		
END 
GO

Alter table tblOffer add SchoolID int;
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblOffer]    Script Date: 6/1/2018 7:17:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblOffer]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 

	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID,
	P.Description as Programme,
	O.IntakeID,
	I.Description as Intake,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID


	From TblOffer O
	inner join tblProgramme P on P.ProgrammeID =  O.ProgrammeID
	inner join tblIntake I on I.IntakeID = O.IntakeID
	inner join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	inner join tblSchool C on C.SchoolID=O.SchoolID

	Where ClientID = @ClientID
	 	
		
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_IntakeID_tblOffer]    Script Date: 6/1/2018 7:19:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_IntakeID_tblOffer]
(
	 	 @IntakeID int
)
AS
BEGIN
	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID,
	P.Description as Programme,
	O.IntakeID,
	I.Description as Intake,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID

	From TblOffer O
	inner join tblProgramme P on P.ProgrammeID =  O.ProgrammeID
	inner join tblIntake I on I.IntakeID = O.IntakeID
	inner join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	inner join tblSchool C on C.SchoolID=O.SchoolID
	Where O.IntakeID = @IntakeID		
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblOffer]    Script Date: 6/1/2018 7:16:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblOffer]
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
,				 SchoolID
	 	 FROM tblOffer
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_OfferID_tblOffer]    Script Date: 6/1/2018 7:21:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_OfferID_tblOffer]
(
	 	 @OfferID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
,				 SchoolID
	 	 FROM tblOffer
	 	 WHERE OfferID = @OfferID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblOffer]    Script Date: 6/1/2018 7:22:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblOffer]
(
	 	 	 	 @ClientID int
	 	 	 	 ,@OfferType varchar(15)
	 	 	 	 ,@ProgrammeID int
	 	 	 	 ,@IntakeID int
	 	 	 	 ,@OfferYearID int
	 	 	 	 ,@OfferDate date
	 	 	 	 ,@OfferCreatedDate date
	 	 	 	 ,@OfferLastEditedDate date
	 	 	 	 ,@OfferCancelledDate date
	 	 	 	 ,@OfferWithdrawnDate date
	 	 	 	 ,@OfferStatusID int
				 ,@DocsFileName varchar(50)
				 ,@File varbinary(max)
				 ,@SchoolID int
	 			 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblOffer
	 	 	 	 ( 
	 	 	 	 ClientID
	 	 	 	 ,OfferType
	 	 	 	 ,ProgrammeID
	 	 	 	 ,IntakeID
	 	 	 	 ,OfferYearID
	 	 	 	 ,OfferDate
	 	 	 	 ,OfferCreatedDate
	 	 	 	 ,OfferLastEditedDate
	 	 	 	 ,OfferCancelledDate
	 	 	 	 ,OfferWithdrawnDate
	 	 	 	 ,OfferStatusID
				 ,DocsFileName 
	 	 	 	 ,[File]
				 ,SchoolID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClientID
	 	 	 	 ,@OfferType
	 	 	 	 ,@ProgrammeID
	 	 	 	 ,@IntakeID
	 	 	 	 ,@OfferYearID
	 	 	 	 ,@OfferDate
	 	 	 	 ,@OfferCreatedDate
	 	 	 	 ,@OfferLastEditedDate
	 	 	 	 ,@OfferCancelledDate
	 	 	 	 ,@OfferWithdrawnDate
	 	 	 	 ,@OfferStatusID
				 ,@DocsFileName 
	 	 	 	 ,@File
				 ,@SchoolID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblOffer]    Script Date: 6/1/2018 7:23:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblOffer]
(
	 	  @OfferID int
	 	 , @ClientID int
	 	 , @OfferType varchar(15)
	 	 , @ProgrammeID int
	 	 , @IntakeID int
	 	 , @OfferYearID int
	 	 , @OfferDate date
	 	 , @OfferCreatedDate date
	 	 , @OfferLastEditedDate date
	 	 , @OfferCancelledDate date
	 	 , @OfferWithdrawnDate date
	 	 , @OfferStatusID int
		  ,@DocsFileName varchar(50)
		  ,@File varbinary(max)
		  ,@SchoolID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblOffer
	 	 SET 
	 	 	 	 	 ClientID = @ClientID
	 	 	 	 	 ,OfferType = @OfferType
	 	 	 	 	 ,ProgrammeID = @ProgrammeID
	 	 	 	 	 ,IntakeID = @IntakeID
	 	 	 	 	 ,OfferYearID = @OfferYearID
	 	 	 	 	 ,OfferDate = @OfferDate
	 	 	 	 	 ,OfferCreatedDate = @OfferCreatedDate
	 	 	 	 	 ,OfferLastEditedDate = @OfferLastEditedDate
	 	 	 	 	 ,OfferCancelledDate = @OfferCancelledDate
	 	 	 	 	 ,OfferWithdrawnDate = @OfferWithdrawnDate
	 	 	 	 	 ,OfferStatusID = @OfferStatusID
	 	 	 	 	 ,DocsFileName= @DocsFileName
					 ,[File]= @File
					 ,SchoolID=@SchoolID

	 	 	 	 	 where OfferID = @OfferID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblOffer]    Script Date: 6/5/2018 4:52:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblOffer]
(
	 	  @OfferID int
	 	 --, @ClientID int
	 	 --, @OfferType varchar(15)
	 	 --, @ProgrammeID int
	 	 --, @IntakeID int
	 	 --, @OfferYearID int
	 	 --, @OfferDate date
	 	 --, @OfferCreatedDate date
	 	 --, @OfferLastEditedDate date
	 	 --, @OfferCancelledDate date
	 	 --, @OfferWithdrawnDate date
	 	 , @OfferStatusID int
		  --,@DocsFileName varchar(50)
		  --,@File varbinary(max)
		  --,@SchoolID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
		 IF @OfferStatusID = 2
			 UPDATE tblOffer
	 				SET 	 	 	 	 	
	 	 	 	 	 --OfferLastEditedDate =GETDATE()
	 	 	 	 	 OfferCancelledDate = GETDATE()	 	 	 	 	
	 	 	 	 	 ,OfferStatusID = @OfferStatusID
	 	 	 	 	 where OfferID = @OfferID
		ELSE IF @OfferStatusID = 3 OR  @OfferStatusID = 4
			 UPDATE tblOffer
	 				SET 	 	 	 	 	
	 	 	 	 	 --OfferLastEditedDate =GETDATE()
	 	 	 	 	 OfferWithdrawnDate = GETDATE()	 	 	 	 	
	 	 	 	 	 ,OfferStatusID =  @OfferStatusID
	 	 	 	 	 where OfferID = @OfferID		
		

	 	 --UPDATE tblOffer
	 	 --SET 
	 	 --	 	 	 --ClientID = @ClientID
	 	 --	 	 	 --,OfferType = @OfferType
	 	 --	 	 	 --,ProgrammeID = @ProgrammeID
	 	 --	 	 	 --,IntakeID = @IntakeID
	 	 --	 	 	 --,OfferYearID = @OfferYearID
	 	 --	 	 	 --,OfferDate = @OfferDate
	 	 --	 	 	 --,OfferCreatedDate = @OfferCreatedDate
	 	 --	 	 	  OfferLastEditedDate = GETDATE()	
	 	 --	 	 	 ,OfferCancelledDate = @OfferCancelledDate
	 	 --	 	 	 ,OfferWithdrawnDate = @OfferWithdrawnDate
	 	 --	 	 	 ,OfferStatusID = @OfferStatusID
	 	 --	 --	 	 ,DocsFileName= @DocsFileName
				--	 --,[File]= @File
				--	 --,SchoolID=@SchoolID

	 	 --	 	 	 where OfferID = @OfferID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblOffer]    Script Date: 6/6/2018 7:52:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblOffer]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 

	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID,
	P.Description as Programme,
	O.IntakeID,
	I.Description as Intake,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID


	From TblOffer O
	inner join tblProgramme P on P.ProgrammeID =  O.ProgrammeID
	inner join tblIntake I on I.IntakeID = O.IntakeID
	left join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	inner join tblSchool C on C.SchoolID=O.SchoolID

	Where ClientID = @ClientID		
END  
GO

INSERT [dbo].[tblCountry] ([Country]) VALUES (N'All Other Countries');
GO

Alter table tblClient add IRDNumber varchar(50), NZPermanantResidence bit;
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblClient]    Script Date: 6/20/2018 7:49:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClient]
(
	 	  @ClientID int
	 	 , @StudentReference varchar(10)
	 	 , @FirstName varchar(50)
	 	 , @MiddleName varchar(50)
	 	 , @LastName varchar(50)
	 	 , @DateOfBirth datetime
	 	 , @Photo varbinary(max)
	 	 , @PassportNumber varchar(20)
	 	 , @ResidenceID int
	 	 , @ClientTypeID int
	 	 , @NSNNumber varchar(50)
	 	 , @WorkNumber varchar(50)
	 	 , @PublicTrustNumber varchar(50)
	 	 , @ExternalRef1 varchar(50)
	 	 , @ExternalRef2 varchar(50)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @EthnicityID int
	 	 , @IwiAffiliation1 varchar(50)
	 	 , @IwiAffiliation2 varchar(50)
	 	 , @IwiAffiliation3 varchar(50)
	 	 , @IwiAffiliation4 varchar(50)
	 	 , @IwiAffiliation5 varchar(50)
	 	 , @AgentID int
		 , @PhotoFileName varchar(50)
		 , @ClientGenderID int
		 , @ClientNationalityID int
		 , @ClientvisatypeID int
		 , @StudentType varchar(20)
		 ,@IRDNumber varchar(50)
		 ,@NZPermanantResidence bit
	 	 , @RETURN_VALUE  INT OUTPUT

)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClient
	 	 SET 
	 	 	 	 	 StudentReference = @StudentReference
	 	 	 	 	 ,FirstName = @FirstName
	 	 	 	 	 ,MiddleName = @MiddleName
	 	 	 	 	 ,LastName = @LastName
	 	 	 	 	 ,DateOfBirth = @DateOfBirth
	 	 	 	 	 ,Photo = @Photo
	 	 	 	 	 ,PassportNumber = @PassportNumber
	 	 	 	 	 ,ResidenceID = @ResidenceID
	 	 	 	 	 ,ClientTypeID = @ClientTypeID
	 	 	 	 	 ,NSNNumber = @NSNNumber
	 	 	 	 	 ,WorkNumber = @WorkNumber
	 	 	 	 	 ,PublicTrustNumber = @PublicTrustNumber
	 	 	 	 	 ,ExternalRef1 = @ExternalRef1
	 	 	 	 	 ,ExternalRef2 = @ExternalRef2
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,EthnicityID = @EthnicityID
	 	 	 	 	 ,IwiAffiliation1 = @IwiAffiliation1
	 	 	 	 	 ,IwiAffiliation2 = @IwiAffiliation2
	 	 	 	 	 ,IwiAffiliation3 = @IwiAffiliation3
	 	 	 	 	 ,IwiAffiliation4 = @IwiAffiliation4
	 	 	 	 	 ,IwiAffiliation5 = @IwiAffiliation5
	 	 	 	 	 ,AgentID = @AgentID
					 ,PhotoFileName =@PhotoFileName
					 ,ClientGenderID = @ClientGenderID 
				     ,ClientNationalityID = @ClientNationalityID 
				     ,ClientvisatypeID = @ClientvisatypeID
					 ,StudentType = @StudentType
	 	 	 	 	 ,IRDNumber=@IRDNumber
				     ,NZPermanantResidence=@NZPermanantResidence
	 	 	 	 	 where ClientID = @ClientID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClient]    Script Date: 6/20/2018 7:47:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SCP_SET_tblClient]
(
	 	 	 	 @StudentReference varchar(10)
	 	 	 	 ,@FirstName varchar(50)
	 	 	 	 ,@MiddleName varchar(50)
	 	 	 	 ,@LastName varchar(50)
	 	 	 	 ,@DateOfBirth datetime
	 	 	 	 ,@Photo varbinary(max)
	 	 	 	 ,@PassportNumber varchar(20)
	 	 	 	 ,@ResidenceID int
	 	 	 	 ,@ClientTypeID int
	 	 	 	 ,@NSNNumber varchar(50)
	 	 	 	 ,@WorkNumber varchar(50)
	 	 	 	 ,@PublicTrustNumber varchar(50)
	 	 	 	 ,@ExternalRef1 varchar(50)
	 	 	 	 ,@ExternalRef2 varchar(50)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@EthnicityID int
	 	 	 	 ,@IwiAffiliation1 varchar(50)
	 	 	 	 ,@IwiAffiliation2 varchar(50)
	 	 	 	 ,@IwiAffiliation3 varchar(50)
	 	 	 	 ,@IwiAffiliation4 varchar(50)
	 	 	 	 ,@IwiAffiliation5 varchar(50)
	 	 	 	 ,@AgentID int
				 ,@PhotoFileName varchar(50)
				 ,@ClientGenderID int
				 ,@ClientNationalityID int
				 ,@ClientvisatypeID int
				 ,@StudentType varchar(20)
				 ,@IRDNumber varchar(50)
				 ,@NZPermanantResidence bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClient
	 	 	 	 ( 
	 	 	 	 StudentReference
	 	 	 	 ,FirstName
	 	 	 	 ,MiddleName
	 	 	 	 ,LastName
	 	 	 	 ,DateOfBirth
	 	 	 	 ,Photo
	 	 	 	 ,PassportNumber
	 	 	 	 ,ResidenceID
	 	 	 	 ,ClientTypeID
	 	 	 	 ,NSNNumber
	 	 	 	 ,WorkNumber
	 	 	 	 ,PublicTrustNumber
	 	 	 	 ,ExternalRef1
	 	 	 	 ,ExternalRef2
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,EthnicityID
	 	 	 	 ,IwiAffiliation1
	 	 	 	 ,IwiAffiliation2
	 	 	 	 ,IwiAffiliation3
	 	 	 	 ,IwiAffiliation4
	 	 	 	 ,IwiAffiliation5
	 	 	 	 ,AgentID
				 ,PhotoFileName
				 ,ClientGenderID 
				 ,ClientNationalityID 
				 ,ClientvisatypeID
				 ,StudentType
				 ,IRDNumber
				 ,NZPermanantResidence
	 	 	 	 ) 
 SELECT 
	 	 	 	 @StudentReference
	 	 	 	 ,@FirstName
	 	 	 	 ,@MiddleName
	 	 	 	 ,@LastName
	 	 	 	 ,@DateOfBirth
	 	 	 	 ,@Photo
	 	 	 	 ,@PassportNumber
	 	 	 	 ,@ResidenceID
	 	 	 	 ,@ClientTypeID
	 	 	 	 ,@NSNNumber
	 	 	 	 ,@WorkNumber
	 	 	 	 ,@PublicTrustNumber
	 	 	 	 ,@ExternalRef1
	 	 	 	 ,@ExternalRef2
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@EthnicityID
	 	 	 	 ,@IwiAffiliation1
	 	 	 	 ,@IwiAffiliation2
	 	 	 	 ,@IwiAffiliation3
	 	 	 	 ,@IwiAffiliation4
	 	 	 	 ,@IwiAffiliation5
	 	 	 	 ,@AgentID
				 ,@PhotoFileName
				 ,@ClientGenderID 
				 ,@ClientNationalityID 
				 ,@ClientvisatypeID
				 ,@StudentType
				 ,@IRDNumber
				 ,@NZPermanantResidence

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblClient]    Script Date: 6/20/2018 7:46:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblClient]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
,				 ClientGenderID 
,				 ClientNationalityID 
,				 ClientVisaTypeID
,				 StudentType
,				 IRDNumber
,				 NZPermanantResidence
	 	 FROM tblClient
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblClient]    Script Date: 6/20/2018 7:47:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClient]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
,				 ClientGenderID
,				 ClientNationalityID
,				 ClientVisaTypeID
,				StudentType
,				 IRDNumber
,				 NZPermanantResidence
	 	 FROM tblClient
	 	 WHERE ClientID = @ClientID
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_Fee_Desc_Country_tblFees]    Script Date: 6/27/2018 7:03:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SCP_GET_Fee_Desc_Country_tblFees]
AS
BEGIN
	 	 SELECT 
	 	 	 	 FeeID
,	 	 	 	 Description
,	 	 	 	 tblFees.CountryID
,				 Country
	 	 FROM tblFees left join tblCountry on tblFees.CountryID=tblCountry.CountryID
	 	 END 
GO

sp_RENAME 'tblOffer.ProgrammeID' , 'ProgrammeID1','COLUMN';
GO

Alter table tblOffer Add ProgrammeID2 int;
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblOffer]    Script Date: 7/2/2018 7:33:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblOffer]
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID1
,	 	 	 	 ProgrammeID2
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
,				 SchoolID
	 	 FROM tblOffer
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_OfferID_tblOffer]    Script Date: 7/2/2018 7:44:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_OfferID_tblOffer]
(
	 	 @OfferID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID1
,				 ProgrammeID2
,	 	 	 	 IntakeID
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
,				 SchoolID
	 	 FROM tblOffer
	 	 WHERE OfferID = @OfferID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_IntakeID_tblOffer]    Script Date: 7/2/2018 7:43:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_IntakeID_tblOffer]
(
	 	 @IntakeID int
)
AS
BEGIN
	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID1,
	P.Description as Programme,
	O.IntakeID,
	I.Description as Intake,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID

	From TblOffer O
	inner join tblProgramme P on P.ProgrammeID =  O.ProgrammeID1
	inner join tblIntake I on I.IntakeID = O.IntakeID
	inner join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	inner join tblSchool C on C.SchoolID=O.SchoolID
	Where O.IntakeID = @IntakeID		
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblOffer]    Script Date: 7/2/2018 7:42:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblOffer]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 

	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID1,
	P.Description as Programme,
	O.IntakeID,
	I.Description as Intake,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID


	From TblOffer O
	inner join tblProgramme P on P.ProgrammeID =  O.ProgrammeID1
	inner join tblIntake I on I.IntakeID = O.IntakeID
	left join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	inner join tblSchool C on C.SchoolID=O.SchoolID

	Where ClientID = @ClientID
	 	
		
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblOffer]    Script Date: 7/2/2018 7:45:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblOffer]
(
	 	 	 	 @ClientID int
	 	 	 	 ,@OfferType varchar(15)
	 	 	 	 ,@ProgrammeID1 int
				 ,@ProgrammeID2 int
	 	 	 	 ,@IntakeID int
	 	 	 	 ,@OfferYearID int
	 	 	 	 ,@OfferDate date
	 	 	 	 ,@OfferCreatedDate date
	 	 	 	 ,@OfferLastEditedDate date
	 	 	 	 ,@OfferCancelledDate date
	 	 	 	 ,@OfferWithdrawnDate date
	 	 	 	 ,@OfferStatusID int
				 ,@DocsFileName varchar(50)
				 ,@File varbinary(max)
				 ,@SchoolID int
	 			 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblOffer
	 	 	 	 ( 
	 	 	 	 ClientID
	 	 	 	 ,OfferType
	 	 	 	 ,ProgrammeID1
				 ,ProgrammeID2
	 	 	 	 ,IntakeID
	 	 	 	 ,OfferYearID
	 	 	 	 ,OfferDate
	 	 	 	 ,OfferCreatedDate
	 	 	 	 ,OfferLastEditedDate
	 	 	 	 ,OfferCancelledDate
	 	 	 	 ,OfferWithdrawnDate
	 	 	 	 ,OfferStatusID
				 ,DocsFileName 
	 	 	 	 ,[File]
				 ,SchoolID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClientID
	 	 	 	 ,@OfferType
	 	 	 	 ,@ProgrammeID1
				 ,@ProgrammeID2
	 	 	 	 ,@IntakeID
	 	 	 	 ,@OfferYearID
	 	 	 	 ,@OfferDate
	 	 	 	 ,@OfferCreatedDate
	 	 	 	 ,@OfferLastEditedDate
	 	 	 	 ,@OfferCancelledDate
	 	 	 	 ,@OfferWithdrawnDate
	 	 	 	 ,@OfferStatusID
				 ,@DocsFileName 
	 	 	 	 ,@File
				 ,@SchoolID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ProgrammeID_CountryID_tblFees]    Script Date: 7/4/2018 4:44:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ProgrammeID_CountryID_tblFees]
(
	 	 @ProgrammeID int,
		 @CountryID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 FeeID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 ProgrammeID
,	 	 	 	 CountryID
,	 	 	 	 IsActive
,	 	 	 	 TutionFees
,	 	 	 	 MaterialFees
,	 	 	 	 Insurance
,	 	 	 	 RegistrationFees
,	 	 	 	 AdministrationFees
,	 	 	 	 OtherFees
,				 TotalFees
,				 AirportPickupFees
,				 HomeStayPlacementFees
,				 HomeStayFeesPerWeek
,	 	 	 	 StartDate
,	 	 	 	 EndDate
	 	 FROM tblFees
	 	 WHERE ProgrammeID = @ProgrammeID
		 AND CountryID=@CountryID
	 	 END 
GO

sp_RENAME 'tblOffer.IntakeID' , 'IntakeID1','COLUMN';
GO

Alter table tblOffer Add IntakeID2 int;
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblOffer]    Script Date: 7/4/2018 5:38:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblOffer]
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID1
,	 	 	 	 ProgrammeID2
,	 	 	 	 IntakeID1
,	 	 	 	 IntakeID2
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
,				 SchoolID
	 	 FROM tblOffer
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_OfferID_tblOffer]    Script Date: 7/4/2018 5:43:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_OfferID_tblOffer]
(
	 	 @OfferID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 OfferID
,	 	 	 	 ClientID
,	 	 	 	 OfferType
,	 	 	 	 ProgrammeID1
,				 ProgrammeID2
,	 	 	 	 IntakeID1
,	 	 	 	 IntakeID2
,	 	 	 	 OfferYearID
,	 	 	 	 OfferDate
,	 	 	 	 OfferCreatedDate
,	 	 	 	 OfferLastEditedDate
,	 	 	 	 OfferCancelledDate
,	 	 	 	 OfferWithdrawnDate
,	 	 	 	 OfferStatusID
,				 DocsFileName
,				 [File]
,				 SchoolID
	 	 FROM tblOffer
	 	 WHERE OfferID = @OfferID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblOffer]    Script Date: 7/4/2018 5:44:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblOffer]
(
	 	 	 	 @ClientID int
	 	 	 	 ,@OfferType varchar(15)
	 	 	 	 ,@ProgrammeID1 int
				 ,@ProgrammeID2 int
	 	 	 	 ,@IntakeID1 int
				 ,@IntakeID2 int
	 	 	 	 ,@OfferYearID int
	 	 	 	 ,@OfferDate date
	 	 	 	 ,@OfferCreatedDate date
	 	 	 	 ,@OfferLastEditedDate date
	 	 	 	 ,@OfferCancelledDate date
	 	 	 	 ,@OfferWithdrawnDate date
	 	 	 	 ,@OfferStatusID int
				 ,@DocsFileName varchar(50)
				 ,@File varbinary(max)
				 ,@SchoolID int
	 			 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblOffer
	 	 	 	 ( 
	 	 	 	 ClientID
	 	 	 	 ,OfferType
	 	 	 	 ,ProgrammeID1
				 ,ProgrammeID2
	 	 	 	 ,IntakeID1
				 ,IntakeID2
	 	 	 	 ,OfferYearID
	 	 	 	 ,OfferDate
	 	 	 	 ,OfferCreatedDate
	 	 	 	 ,OfferLastEditedDate
	 	 	 	 ,OfferCancelledDate
	 	 	 	 ,OfferWithdrawnDate
	 	 	 	 ,OfferStatusID
				 ,DocsFileName 
	 	 	 	 ,[File]
				 ,SchoolID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClientID
	 	 	 	 ,@OfferType
	 	 	 	 ,@ProgrammeID1
				 ,@ProgrammeID2
	 	 	 	 ,@IntakeID1
				 ,@IntakeID2
	 	 	 	 ,@OfferYearID
	 	 	 	 ,@OfferDate
	 	 	 	 ,@OfferCreatedDate
	 	 	 	 ,@OfferLastEditedDate
	 	 	 	 ,@OfferCancelledDate
	 	 	 	 ,@OfferWithdrawnDate
	 	 	 	 ,@OfferStatusID
				 ,@DocsFileName 
	 	 	 	 ,@File
				 ,@SchoolID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO


USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_IntakeID_tblOffer]    Script Date: 7/4/2018 5:44:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_IntakeID_tblOffer]
(
	 	 @IntakeID int
)
AS
BEGIN
	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID1,
	P1.Description as Programme1,
	O.IntakeID1,
	I1.Description as Intake1,
	O.ProgrammeID2,
	P2.Description as Programme2,
	O.IntakeID2,
	I2.Description as Intake2,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID

	From TblOffer O
	inner join tblProgramme P1 on P1.ProgrammeID =  O.ProgrammeID1
	inner join tblIntake I1 on I1.IntakeID = O.IntakeID1
	inner join tblProgramme P2 on P2.ProgrammeID =  O.ProgrammeID2
	inner join tblIntake I2 on I1.IntakeID = O.IntakeID2
	inner join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	inner join tblSchool C on C.SchoolID=O.SchoolID
	Where O.IntakeID1 = @IntakeID Or  O.IntakeID2 = @IntakeID			
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblOffer]    Script Date: 7/4/2018 5:40:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblOffer]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 

	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID1,
	P1.Description as Programme1,
	O.IntakeID1,
	I1.Description as Intake1,
	O.ProgrammeID2,
	P2.Description as Programme2,
	O.IntakeID2,
	I2.Description as Intake2,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID

	From TblOffer O
	inner join tblProgramme P1 on P1.ProgrammeID =  O.ProgrammeID1
	inner join tblIntake I1 on I1.IntakeID = O.IntakeID1
	inner join tblProgramme P2 on P2.ProgrammeID =  O.ProgrammeID2
	inner join tblIntake I2 on I1.IntakeID = O.IntakeID2
	inner join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	inner join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	inner join tblSchool C on C.SchoolID=O.SchoolID
	--Where O.IntakeID1 = @IntakeID Or  O.IntakeID2 = @IntakeID	

	Where ClientID = @ClientID
	 	
		
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_IntakeID_tblOffer]    Script Date: 7/5/2018 7:52:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_IntakeID_tblOffer]
(
	 	 @IntakeID int
)
AS
BEGIN
	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID1,
	P1.Description as Programme1,
	O.IntakeID1,
	I1.Description as Intake1,
	O.ProgrammeID2,
	P2.Description as Programme2,
	O.IntakeID2,
	I2.Description as Intake2,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID

	From TblOffer O
	left join tblProgramme P1 on P1.ProgrammeID =  O.ProgrammeID1
	left join tblIntake I1 on I1.IntakeID = O.IntakeID1
	left join tblProgramme P2 on P2.ProgrammeID =  O.ProgrammeID2
	left join tblIntake I2 on I1.IntakeID = O.IntakeID2
	left join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	left join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	left join tblSchool C on C.SchoolID=O.SchoolID
	Where O.IntakeID1 = @IntakeID Or  O.IntakeID2 = @IntakeID			
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblOffer]    Script Date: 7/5/2018 7:47:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblOffer]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 

	Select 
	OfferID,
	ClientID,
	OfferType,
	O.ProgrammeID1,
	P1.Description as Programme1,
	O.IntakeID1,
	I1.Description as Intake1,
	O.ProgrammeID2,
	P2.Description as Programme2,
	O.IntakeID2,
	I2.Description as Intake2,
	Y.OfferYearID,
	Y.Description as OfferYear,
	OfferDate,
	OfferCreatedDate,
	OfferLastEditedDate,
	OfferCancelledDate,
	OfferWithdrawnDate,
	S.OfferStatusID,
	S.Description as OfferStatus,
	DocsFileName,
	O.SchoolID

	From TblOffer O
	left join tblProgramme P1 on P1.ProgrammeID =  O.ProgrammeID1
	left join tblIntake I1 on I1.IntakeID = O.IntakeID1
	left join tblProgramme P2 on P2.ProgrammeID =  O.ProgrammeID2
	left join tblIntake I2 on I1.IntakeID = O.IntakeID2
	left join tblOfferYear Y on Y.OfferYearID = O.OfferYearID
	left join tblOfferStatus S on S.OfferStatusID = O.OfferStatusID
	left join tblSchool C on C.SchoolID=O.SchoolID
	--Where O.IntakeID1 = @IntakeID Or  O.IntakeID2 = @IntakeID	

	Where ClientID = @ClientID
	 	
		
END 
GO