/****** Object:  Table [dbo].[tblGPOLO]    Script Date: 3/23/2018 5:28:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblGPOLO](
	[GPOLOID] [int] IDENTITY(1,1) NOT NULL,
	[GPOID] [int] NULL,
	[LOID] [int] NULL,
 CONSTRAINT [PK_tblGPOLO] PRIMARY KEY CLUSTERED 
(
	[GPOLOID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblGPOLO]    Script Date: 3/23/2018 6:56:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblGPOLO]
(
	 	 	 	 @GPOID int
	 	 	 	 ,@LOID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(GPOLOID) from tblGPOLO where
		GPOID=@GPOID and LOID=@LOID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblGPOLO
	 	 	 	 ( 
	 	 	 	 GPOID
	 	 	 	 ,LOID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @GPOID
	 	 	 	 ,@LOID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_GPOID_tblGPOLO]    Script Date: 3/23/2018 6:58:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_GPOID_tblGPOLO]
(
	 	 @GPOID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblGPOLO.GPOLOID
,	 	 	 	 tblGPOLO.GPOID
,	 	 	 	 tblGPOLO.LOID
,				 tblLO.Code as [LO Code]
,				 tblLO.Description as [LO Description]
,				 tblGPO.Code [GPO Code]
,				 tblGPO.Description [GPO Description]
	 	 FROM 
		 tblGPOLO left join tblGPO on 
			tblGPOLO.GPOID=tblGPO.GPOID
		 left join tblLO on
			tblGPOLO.LOID= tblLO.LOID
	 	 WHERE tblGPOLO.GPOID = @GPOID
	 	 END 
GO


