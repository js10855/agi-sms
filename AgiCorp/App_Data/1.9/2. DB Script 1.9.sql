	 
IF Exists(Select 1 from syscolumns where name = 'AssessmentID' and Id in (select ID from sysobjects where name = 'tblClientAssessments'))
	Alter table tblClientAssessments drop column AssessmentID 
GO
IF Not Exists(Select 1 from syscolumns where name = 'ModuleAssessmentID' and Id in (select ID from sysobjects where name = 'tblClientAssessments'))
Alter table tblClientAssessments add ModuleAssessmentID int
GO


IF Exists(Select 1 from syscolumns where name = 'DocumentTypeID' and Id in (select ID from sysobjects where name = 'tblClientOfferOfPlace'))
	Alter table tblClientOfferOfPlace drop column DocumentTypeID 
GO
IF Not Exists(Select 1 from syscolumns where name = 'ClientDocumentTypeID' and Id in (select ID from sysobjects where name = 'tblClientOfferOfPlace'))
Alter table tblClientOfferOfPlace add ClientDocumentTypeID int
GO







IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientEnglishProficiency')
Drop Procedure SCP_GET_BY_ClientID_tblClientEnglishProficiency
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientEnglishProficiency]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientEnglishProficiencyID
,	 	 	 	 EnglishProficiencyType
,	 	 	 	 EnglishProficiencyExamNumber
,	 	 	 	 EnglishProficiencyExamDate
,	 	 	 	 EnglishProficiencyExpiryDate
,	 	 	 	 EnglishProficiencyOverallBand
,	 	 	 	 IELTSReading
,	 	 	 	 IELTSWriting
,	 	 	 	 IELTSSpeaking
,	 	 	 	 IELTSListening
,	 	 	 	 ClientID
	 	 FROM tblClientEnglishProficiency
	 	 WHERE ClientID = @ClientID
	 	 END 

GO
		 
		 

IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientAddress')
Drop Procedure SCP_GET_BY_ClientID_tblClientAddress
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientAddress]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	 ClientAddressID
		,Address1
		,Address2
		,Address3
		,PostCode
		,City
		,State
		,CountryID
		,Email
		,PhoneCountryCode
		,PhoneStateCode
		,Phone
		,ClientID
	 	 FROM tblClientAddress
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 

		 

IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientAssessments')
Drop Procedure SCP_GET_BY_ClientID_tblClientAssessments
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientAssessments]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	 ClientAssessmentID
		,ClassID
		,ModuleID
		,ModuleAssessmentID
		,TotalMarks
		,MarksObtained
		,ClientID
	 	 FROM tblClientAssessments
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 
IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientAttendance')
Drop Procedure SCP_GET_BY_ClientID_tblClientAttendance
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientAttendance]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientAttendanceID
		,ModuleID
		,AttendancePercentage
		,OverallAttendance
		,ClientID
	 	 FROM tblClientAttendance
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 
		 
		 
		 
		 
IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientDocument')
Drop Procedure SCP_GET_BY_ClientID_tblClientDocument
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientDocument]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientDocumentID
		,Code
		,DocumentDescription
		,FileType
		,FileSize
		,ClientID
	 	 FROM tblClientDocument
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 


IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientGrade')
Drop Procedure SCP_GET_BY_ClientID_tblClientGrade
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientGrade]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientGradeID
		,ClassID
		,ModuleID
		,GradeID
		,ClientID
	 	 FROM tblClientGrade
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 

IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientOfferOfPlace')
Drop Procedure SCP_GET_BY_ClientID_tblClientOfferOfPlace
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientOfferOfPlace]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientOfferOfPlaceID
		,Code
		,DocumentDescription
		,ClientDocumentTypeID
		,ClientID
	 	 FROM tblClientOfferOfPlace
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 
		 
		 
IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientPlacement')
Drop Procedure SCP_GET_BY_ClientID_tblClientPlacement
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientPlacement]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientPlacementID
		,Code
		,FacilityName
		,CurrentlyPlaced
		,FromDate
		,ToDate
		,ClientID
	 	 FROM tblClientPlacement
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 
		 
IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientWorkExperience')
Drop Procedure SCP_GET_BY_ClientID_tblClientWorkExperience
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientWorkExperience]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 		 ClientWorkExperienceID
		,Code
		,EmployerName
		,EmployerEmail
		,EmployerWebsite
		,PhoneCountryCode
		,PhoneStateCode
		,Phone
		,JobTitle
		,CurrentlyWorking
		,FromDate
		,ToDate
		,PostCode
		,City
		,State
		,CountryID
		,ClientID

	 	 FROM tblClientWorkExperience
	 	 WHERE ClientID = @ClientID
	 	 END 

	
	GO
		 
		 
		 

IF Exists(select 1 from sysobjects where name like 'SCP_GET_ALL_tblModuleAssessment')
Drop Procedure SCP_GET_ALL_tblModuleAssessment
GO		 
CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 Number
,	 	 	 	 Name
,				 ModuleAssessmentTypeID
,				 ModuleAssessmentFormatID	 	 	 	 
,				LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 END 

GO




IF Exists(select 1 from sysobjects where name like 'SCP_UPDATE_tblClientAssessments')
Drop Procedure SCP_UPDATE_tblClientAssessments
GO	

CREATE PROCEDURE [dbo].[SCP_UPDATE_tblClientAssessments]
(
	 	  @ClientAssessmentID int
	 	 , @ClassID int
	 	 , @ModuleID int
	 	 , @ModuleAssessmentID int
	 	 , @TotalMarks int
	 	 , @MarksObtained float
	 	 , @ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClientAssessments
	 	 SET 
	 	 	 	 	 ClassID = @ClassID
	 	 	 	 	 ,ModuleID = @ModuleID
	 	 	 	 	 ,ModuleAssessmentID = @ModuleAssessmentID
	 	 	 	 	 ,TotalMarks = @TotalMarks
	 	 	 	 	 ,MarksObtained = @MarksObtained
	 	 	 	 	 ,ClientID = @ClientID
	 	 	 	 	 
	 	 	 	 	 where ClientAssessmentID = @ClientAssessmentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO




IF Exists(select 1 from sysobjects where name like 'SCP_SET_tblClientAssessments')
Drop Procedure SCP_SET_tblClientAssessments
GO
CREATE PROCEDURE [dbo].[SCP_SET_tblClientAssessments]
(
	 	 	 	 @ClassID int
	 	 	 	 ,@ModuleID int
	 	 	 	 ,@ModuleAssessmentID int
	 	 	 	 ,@TotalMarks int
	 	 	 	 ,@MarksObtained float
	 	 	 	 ,@ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClientAssessments
	 	 	 	 ( 
	 	 	 	 ClassID
	 	 	 	 ,ModuleID
	 	 	 	 ,ModuleAssessmentID
	 	 	 	 ,TotalMarks
	 	 	 	 ,MarksObtained
	 	 	 	 ,ClientID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClassID
	 	 	 	 ,@ModuleID
	 	 	 	 ,@ModuleAssessmentID
	 	 	 	 ,@TotalMarks
	 	 	 	 ,@MarksObtained
	 	 	 	 ,@ClientID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO




IF Exists(select 1 from sysobjects where name like 'SCP_GET_ALL_tblClientOfferOfPlace')
Drop Procedure SCP_GET_ALL_tblClientOfferOfPlace
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblClientOfferOfPlace]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientOfferOfPlaceID
,	 	 	 	 Code
,	 	 	 	 DocumentDescription
,	 	 	 	 ClientDocumentTypeID
,	 	 	 	 ClientID
	 	 FROM tblClientOfferOfPlace
	 	 END 

GO



IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientID_tblClientOfferOfPlace')
Drop Procedure SCP_GET_BY_ClientID_tblClientOfferOfPlace
GO


CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientOfferOfPlace]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientOfferOfPlaceID
		,Code
		,DocumentDescription
		,ClientDocumentTypeID
		,ClientID
	 	 FROM tblClientOfferOfPlace
	 	 WHERE ClientID = @ClientID
	 	 END 

	GO
	
IF Exists(select 1 from sysobjects where name like 'SCP_GET_BY_ClientOfferOfPlaceID_tblClientOfferOfPlace')
Drop Procedure SCP_GET_BY_ClientOfferOfPlaceID_tblClientOfferOfPlace
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientOfferOfPlaceID_tblClientOfferOfPlace]
(
	 	 @ClientOfferOfPlaceID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientOfferOfPlaceID
,	 	 	 	 Code
,	 	 	 	 DocumentDescription
,	 	 	 	 ClientDocumentTypeID
,	 	 	 	 ClientID
	 	 FROM tblClientOfferOfPlace
	 	 WHERE ClientOfferOfPlaceID = @ClientOfferOfPlaceID
	 	 END 

GO


	
	
	
IF Exists(select 1 from sysobjects where name like 'SCP_UPDATE_tblClientOfferOfPlace')
Drop Procedure SCP_UPDATE_tblClientOfferOfPlace
GO	
	
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblClientOfferOfPlace]
(
	 	  @ClientOfferOfPlaceID int
	 	 , @Code varchar(10)
	 	 , @DocumentDescription varchar(500)
	 	 , @ClientDocumentTypeID int
	 	 , @ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClientOfferOfPlace
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,DocumentDescription = @DocumentDescription
	 	 	 	 	 ,ClientDocumentTypeID = @ClientDocumentTypeID
	 	 	 	 	 ,ClientID = @ClientID
	 	 	 	 	 
	 	 	 	 	 where ClientOfferOfPlaceID = @ClientOfferOfPlaceID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

		 GO
		 
		 
		 

IF Exists(select 1 from sysobjects where name like 'SCP_SET_tblClientOfferOfPlace')
Drop Procedure SCP_SET_tblClientOfferOfPlace
GO	
	CREATE PROCEDURE [dbo].[SCP_SET_tblClientOfferOfPlace]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@DocumentDescription varchar(500)
	 	 	 	 ,@ClientDocumentTypeID int
	 	 	 	 ,@ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClientOfferOfPlace
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,DocumentDescription
	 	 	 	 ,ClientDocumentTypeID
	 	 	 	 ,ClientID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@DocumentDescription
	 	 	 	 ,@ClientDocumentTypeID
	 	 	 	 ,@ClientID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 

GO