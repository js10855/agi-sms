USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ClientDocumentTypeID_tblClientDocumentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ClientDocumentTypeID_tblClientDocumentType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientDocumentTypeID_tblClientDocumentType]
(
	 	 @ClientDocumentTypeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientDocumentTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientDocumentType
	 	 WHERE ClientDocumentTypeID = @ClientDocumentTypeID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblClientDocumentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblClientDocumentType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblClientDocumentType]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientDocumentTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblClientDocumentType
	 	 END 
GO 
