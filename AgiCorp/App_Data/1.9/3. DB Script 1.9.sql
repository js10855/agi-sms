﻿ Alter table tblModuleAssessment drop column Name;
 Alter table tblModuleAssessment drop column Number;

 Alter table tblModuleAssessment add AssessmentName varchar(50);
 Alter table tblModuleAssessment add AssessmentNumber varchar(50);

 /****** Object:  StoredProcedure [dbo].[SCP_SET_tblModuleAssessment]    Script Date: 3/25/2018 7:26:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SCP_SET_tblModuleAssessment]
(
	 	 	 	 @AssessmentNumber varchar(50)
	 	 	 	 ,@AssessmentName varchar(50)
	 	 	 	 ,@ModuleAssessmentTypeID int
	 	 	 	 ,@ModuleAssessmentFormatID int
	 	 	 	 ,@LOCovered varchar(500)
	 	 	 	 ,@TotalMarks int
	 	 	 	 ,@MinPassPercentage int
	 	 	 	 ,@ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblModuleAssessment
	 	 	 	 ( 
	 	 	 	 AssessmentNumber
	 	 	 	 ,AssessmentName
	 	 	 	 ,ModuleAssessmentTypeID
	 	 	 	 ,ModuleAssessmentFormatID
	 	 	 	 ,LOCovered
	 	 	 	 ,TotalMarks
	 	 	 	 ,MinPassPercentage
	 	 	 	 ,ModuleID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @AssessmentNumber
	 	 	 	 ,@AssessmentName
	 	 	 	 ,@ModuleAssessmentTypeID
	 	 	 	 ,@ModuleAssessmentFormatID
	 	 	 	 ,@LOCovered
	 	 	 	 ,@TotalMarks
	 	 	 	 ,@MinPassPercentage
	 	 	 	 ,@ModuleID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblModuleAssessment]    Script Date: 3/25/2018 7:25:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 AssessmentNumber
,	 	 	 	 AssessmentName
,				 ModuleAssessmentTypeID
,				 ModuleAssessmentFormatID	 	 	 	 
,				LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ModuleAssessmentID_tblModuleAssessment]    Script Date: 3/25/2018 7:25:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentID_tblModuleAssessment]
(
	 	 @ModuleAssessmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 AssessmentNumber
,	 	 	 	 AssessmentName
,	 	 	 	 ModuleAssessmentTypeID
,	 	 	 	 ModuleAssessmentFormatID
,	 	 	 	 LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 WHERE ModuleAssessmentID = @ModuleAssessmentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ModuleID_tblModuleAssessment]    Script Date: 3/25/2018 7:26:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		 
ALTER PROCEDURE [dbo].[SCP_GET_BY_ModuleID_tblModuleAssessment]
(
	 	 @ModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 AssessmentNumber
,	 	 	 	 AssessmentName
,	 	 	 	 ModuleAssessmentTypeID
,	 	 	 	 ModuleAssessmentFormatID
,	 	 	 	 LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 WHERE ModuleID = @ModuleID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblModuleAssessment]    Script Date: 3/25/2018 7:31:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblModuleAssessment]
(
	 	  @ModuleAssessmentID int
	 	 , @AssessmentNumber varchar(50)
	 	 , @AssessmentName varchar(50)
	 	 , @ModuleAssessmentTypeID int
	 	 , @ModuleAssessmentFormatID int
	 	 , @LOCovered varchar(500)
	 	 , @TotalMarks int
	 	 , @MinPassPercentage int
	 	 , @ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblModuleAssessment
	 	 SET 
	 	 	 	 	 AssessmentNumber = @AssessmentNumber
	 	 	 	 	 ,AssessmentName = @AssessmentName
	 	 	 	 	 ,ModuleAssessmentTypeID = @ModuleAssessmentTypeID
	 	 	 	 	 ,ModuleAssessmentFormatID = @ModuleAssessmentFormatID
	 	 	 	 	 ,LOCovered = @LOCovered
	 	 	 	 	 ,TotalMarks = @TotalMarks
	 	 	 	 	 ,MinPassPercentage = @MinPassPercentage
	 	 	 	 	 ,ModuleID = @ModuleID
	 	 	 	 	 
	 	 	 	 	 where ModuleAssessmentID = @ModuleAssessmentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO
 		 
		 
		 
		 
		 
		 
		 
		 

































