USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_QualificationGPOID_tblQualificationGPO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_QualificationGPOID_tblQualificationGPO]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_QualificationGPOID_tblQualificationGPO]
(
	 	 @QualificationGPOID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationGPOID
,	 	 	 	 QualificationID
,	 	 	 	 GPOID
	 	 FROM tblQualificationGPO
	 	 WHERE QualificationGPOID = @QualificationGPOID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblQualificationGPO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblQualificationGPO]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblQualificationGPO]
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationGPOID
,	 	 	 	 QualificationID
,	 	 	 	 GPOID
	 	 FROM tblQualificationGPO
	 	 END 
GO 
