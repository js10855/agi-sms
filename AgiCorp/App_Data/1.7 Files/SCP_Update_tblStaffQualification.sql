USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_UPDATE_tblStaffQualification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_UPDATE_tblStaffQualification]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblStaffQualification]
(
	 	  @StaffQualificationID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @StaffQualificationLevelID int
	 	 , @StaffQualificationTypeID int
	 	 , @StaffID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblStaffQualification
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,StaffQualificationLevelID = @StaffQualificationLevelID
	 	 	 	 	 ,StaffQualificationTypeID = @StaffQualificationTypeID
	 	 	 	 	 ,StaffID = @StaffID
	 	 	 	 	 
	 	 	 	 	 where StaffQualificationID = @StaffQualificationID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
