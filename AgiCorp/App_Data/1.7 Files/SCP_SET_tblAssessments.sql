USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_SET_tblAssessments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_SET_tblAssessments]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_SET_tblAssessments]
(
	 	 	 	 @Name varchar(50)
	 	 	 	 ,@Type varchar(50)
	 	 	 	 ,@Format varchar(50)
	 	 	 	 ,@LastName varchar(50)
	 	 	 	 ,@TotalMarks int
	 	 	 	 ,@MinPassPercentage int
	 	 	 	 ,@ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblAssessments
	 	 	 	 ( 
	 	 	 	 Name
	 	 	 	 ,Type
	 	 	 	 ,Format
	 	 	 	 ,LastName
	 	 	 	 ,TotalMarks
	 	 	 	 ,MinPassPercentage
	 	 	 	 ,ModuleID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Name
	 	 	 	 ,@Type
	 	 	 	 ,@Format
	 	 	 	 ,@LastName
	 	 	 	 ,@TotalMarks
	 	 	 	 ,@MinPassPercentage
	 	 	 	 ,@ModuleID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
