USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ModuleAssessmentTypeID_tblModuleAssessmentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentTypeID_tblModuleAssessmentType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentTypeID_tblModuleAssessmentType]
(
	 	 @ModuleAssessmentTypeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblModuleAssessmentType
	 	 WHERE ModuleAssessmentTypeID = @ModuleAssessmentTypeID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblModuleAssessmentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessmentType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessmentType]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblModuleAssessmentType
	 	 END 
GO 
