USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_SET_tblModuleAssessment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_SET_tblModuleAssessment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_SET_tblModuleAssessment]
(
	 	 	 	 @Number varchar(50)
	 	 	 	 ,@Name varchar(50)
	 	 	 	 ,@TypeID int
	 	 	 	 ,@FormatID int
	 	 	 	 ,@LOCovered varchar(500)
	 	 	 	 ,@TotalMarks int
	 	 	 	 ,@MinPassPercentage int
	 	 	 	 ,@ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblModuleAssessment
	 	 	 	 ( 
	 	 	 	 Number
	 	 	 	 ,Name
	 	 	 	 ,TypeID
	 	 	 	 ,FormatID
	 	 	 	 ,LOCovered
	 	 	 	 ,TotalMarks
	 	 	 	 ,MinPassPercentage
	 	 	 	 ,ModuleID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Number
	 	 	 	 ,@Name
	 	 	 	 ,@TypeID
	 	 	 	 ,@FormatID
	 	 	 	 ,@LOCovered
	 	 	 	 ,@TotalMarks
	 	 	 	 ,@MinPassPercentage
	 	 	 	 ,@ModuleID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
