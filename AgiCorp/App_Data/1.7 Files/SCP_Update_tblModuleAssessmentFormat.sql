USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_UPDATE_tblModuleAssessmentFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_UPDATE_tblModuleAssessmentFormat]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblModuleAssessmentFormat]
(
	 	  @ModuleAssessmentFormatID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblModuleAssessmentFormat
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 
	 	 	 	 	 where ModuleAssessmentFormatID = @ModuleAssessmentFormatID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
