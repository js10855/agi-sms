USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_StaffQualificationLevelID_tblStaffQualificationLevel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_StaffQualificationLevelID_tblStaffQualificationLevel]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_StaffQualificationLevelID_tblStaffQualificationLevel]
(
	 	 @StaffQualificationLevelID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffQualificationLevelID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblStaffQualificationLevel
	 	 WHERE StaffQualificationLevelID = @StaffQualificationLevelID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblStaffQualificationLevel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblStaffQualificationLevel]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblStaffQualificationLevel]
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffQualificationLevelID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblStaffQualificationLevel
	 	 END 
GO 
