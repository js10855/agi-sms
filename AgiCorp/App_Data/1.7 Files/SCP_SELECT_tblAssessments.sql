USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_AssessmentsID_tblAssessments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_AssessmentsID_tblAssessments]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AssessmentsID_tblAssessments]
(
	 	 @AssessmentsID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AssessmentsID
,	 	 	 	 Name
,	 	 	 	 Type
,	 	 	 	 Format
,	 	 	 	 LastName
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblAssessments
	 	 WHERE AssessmentsID = @AssessmentsID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblAssessments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblAssessments]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblAssessments]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AssessmentsID
,	 	 	 	 Name
,	 	 	 	 Type
,	 	 	 	 Format
,	 	 	 	 LastName
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblAssessments
	 	 END 
GO 
