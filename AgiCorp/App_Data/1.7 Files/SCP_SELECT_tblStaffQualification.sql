USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_StaffQualificationID_tblStaffQualification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_StaffQualificationID_tblStaffQualification]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_StaffQualificationID_tblStaffQualification]
(
	 	 @StaffQualificationID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffQualificationID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 StaffQualificationLevelID
,	 	 	 	 StaffQualificationTypeID
,	 	 	 	 StaffID
	 	 FROM tblStaffQualification
	 	 WHERE StaffQualificationID = @StaffQualificationID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblStaffQualification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblStaffQualification]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblStaffQualification]
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffQualificationID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 StaffQualificationLevelID
,	 	 	 	 StaffQualificationTypeID
,	 	 	 	 StaffID
	 	 FROM tblStaffQualification
	 	 END 
GO 
