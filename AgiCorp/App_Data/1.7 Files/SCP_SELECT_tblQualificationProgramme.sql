USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_QualificationProgrammeID_tblQualificationProgramme]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_QualificationProgrammeID_tblQualificationProgramme]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_QualificationProgrammeID_tblQualificationProgramme]
(
	 	 @QualificationProgrammeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationProgrammeID
,	 	 	 	 QualificationID
,	 	 	 	 ProgrammeID
	 	 FROM tblQualificationProgramme
	 	 WHERE QualificationProgrammeID = @QualificationProgrammeID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblQualificationProgramme]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblQualificationProgramme]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblQualificationProgramme]
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationProgrammeID
,	 	 	 	 QualificationID
,	 	 	 	 ProgrammeID
	 	 FROM tblQualificationProgramme
	 	 END 
GO 
