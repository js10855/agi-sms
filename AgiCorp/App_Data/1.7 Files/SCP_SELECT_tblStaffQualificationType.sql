USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_StaffQualificationTypeID_tblStaffQualificationType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_StaffQualificationTypeID_tblStaffQualificationType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_StaffQualificationTypeID_tblStaffQualificationType]
(
	 	 @StaffQualificationTypeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffQualificationTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblStaffQualificationType
	 	 WHERE StaffQualificationTypeID = @StaffQualificationTypeID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblStaffQualificationType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblStaffQualificationType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblStaffQualificationType]
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffQualificationTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblStaffQualificationType
	 	 END 
GO 
