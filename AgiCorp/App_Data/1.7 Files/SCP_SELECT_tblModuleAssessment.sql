USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ModuleAssessmentID_tblModuleAssessment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentID_tblModuleAssessment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentID_tblModuleAssessment]
(
	 	 @ModuleAssessmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 Number
,	 	 	 	 Name
,	 	 	 	 TypeID
,	 	 	 	 FormatID
,	 	 	 	 LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 WHERE ModuleAssessmentID = @ModuleAssessmentID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblModuleAssessment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentID
,	 	 	 	 Number
,	 	 	 	 Name
,	 	 	 	 TypeID
,	 	 	 	 FormatID
,	 	 	 	 LOCovered
,	 	 	 	 TotalMarks
,	 	 	 	 MinPassPercentage
,	 	 	 	 ModuleID
	 	 FROM tblModuleAssessment
	 	 END 
GO 
