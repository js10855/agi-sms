USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_SET_tblModuleLO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_SET_tblModuleLO]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_SET_tblModuleLO]
(
	 	 	 	 @ModuleID int
	 	 	 	 ,@LOID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblModuleLO
	 	 	 	 ( 
	 	 	 	 ModuleID
	 	 	 	 ,LOID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ModuleID
	 	 	 	 ,@LOID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
