USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_DELETE_BY_ModuleAssessmentID_tblModuleAssessment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_DELETE_BY_ModuleAssessmentID_tblModuleAssessment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_DELETE_BY_ModuleAssessmentID_tblModuleAssessment]
(
	 	 @ModuleAssessmentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 DELETE FROM tblModuleAssessment
	 	 WHERE ModuleAssessmentID = @ModuleAssessmentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
