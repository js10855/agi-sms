USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_UPDATE_tblModuleAssessment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_UPDATE_tblModuleAssessment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblModuleAssessment]
(
	 	  @ModuleAssessmentID int
	 	 , @Number varchar(50)
	 	 , @Name varchar(50)
	 	 , @TypeID int
	 	 , @FormatID int
	 	 , @LOCovered varchar(500)
	 	 , @TotalMarks int
	 	 , @MinPassPercentage int
	 	 , @ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblModuleAssessment
	 	 SET 
	 	 	 	 	 Number = @Number
	 	 	 	 	 ,Name = @Name
	 	 	 	 	 ,TypeID = @TypeID
	 	 	 	 	 ,FormatID = @FormatID
	 	 	 	 	 ,LOCovered = @LOCovered
	 	 	 	 	 ,TotalMarks = @TotalMarks
	 	 	 	 	 ,MinPassPercentage = @MinPassPercentage
	 	 	 	 	 ,ModuleID = @ModuleID
	 	 	 	 	 
	 	 	 	 	 where ModuleAssessmentID = @ModuleAssessmentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
