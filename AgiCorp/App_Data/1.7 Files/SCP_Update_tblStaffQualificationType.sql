USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_UPDATE_tblStaffQualificationType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_UPDATE_tblStaffQualificationType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblStaffQualificationType]
(
	 	  @StaffQualificationTypeID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblStaffQualificationType
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 
	 	 	 	 	 where StaffQualificationTypeID = @StaffQualificationTypeID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
