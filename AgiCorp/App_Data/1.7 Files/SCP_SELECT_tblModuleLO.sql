USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ModuleLOID_tblModuleLO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ModuleLOID_tblModuleLO]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ModuleLOID_tblModuleLO]
(
	 	 @ModuleLOID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleLOID
,	 	 	 	 ModuleID
,	 	 	 	 LOID
	 	 FROM tblModuleLO
	 	 WHERE ModuleLOID = @ModuleLOID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblModuleLO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblModuleLO]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblModuleLO]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleLOID
,	 	 	 	 ModuleID
,	 	 	 	 LOID
	 	 FROM tblModuleLO
	 	 END 
GO 
