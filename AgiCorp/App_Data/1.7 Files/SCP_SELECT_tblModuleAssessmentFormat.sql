USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ModuleAssessmentFormatID_tblModuleAssessmentFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentFormatID_tblModuleAssessmentFormat]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ModuleAssessmentFormatID_tblModuleAssessmentFormat]
(
	 	 @ModuleAssessmentFormatID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentFormatID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblModuleAssessmentFormat
	 	 WHERE ModuleAssessmentFormatID = @ModuleAssessmentFormatID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblModuleAssessmentFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessmentFormat]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblModuleAssessmentFormat]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleAssessmentFormatID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblModuleAssessmentFormat
	 	 END 
GO 
