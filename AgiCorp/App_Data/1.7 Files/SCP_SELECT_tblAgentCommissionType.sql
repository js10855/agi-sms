USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_AgentCommissionTypeID_tblAgentCommissionType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_AgentCommissionTypeID_tblAgentCommissionType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AgentCommissionTypeID_tblAgentCommissionType]
(
	 	 @AgentCommissionTypeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblAgentCommissionType
	 	 WHERE AgentCommissionTypeID = @AgentCommissionTypeID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblAgentCommissionType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblAgentCommissionType]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblAgentCommissionType]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionTypeID
,	 	 	 	 Code
,	 	 	 	 Description
	 	 FROM tblAgentCommissionType
	 	 END 
GO 
