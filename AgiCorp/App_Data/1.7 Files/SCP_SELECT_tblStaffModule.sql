USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_StaffModuleID_tblStaffModule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_StaffModuleID_tblStaffModule]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_StaffModuleID_tblStaffModule]
(
	 	 @StaffModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffModuleID
,	 	 	 	 StaffID
,	 	 	 	 ModuleID
	 	 FROM tblStaffModule
	 	 WHERE StaffModuleID = @StaffModuleID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblStaffModule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblStaffModule]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblStaffModule]
AS
BEGIN
	 	 SELECT 
	 	 	 	 StaffModuleID
,	 	 	 	 StaffID
,	 	 	 	 ModuleID
	 	 FROM tblStaffModule
	 	 END 
GO 
