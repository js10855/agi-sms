USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ProgrammeModuleID_tblProgrammeModule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ProgrammeModuleID_tblProgrammeModule]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ProgrammeModuleID_tblProgrammeModule]
(
	 	 @ProgrammeModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeModuleID
,	 	 	 	 ProgrammeID
,	 	 	 	 ModuleID
	 	 FROM tblProgrammeModule
	 	 WHERE ProgrammeModuleID = @ProgrammeModuleID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblProgrammeModule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblProgrammeModule]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblProgrammeModule]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeModuleID
,	 	 	 	 ProgrammeID
,	 	 	 	 ModuleID
	 	 FROM tblProgrammeModule
	 	 END 
GO 
