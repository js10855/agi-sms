﻿USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblProgrammeModule]    Script Date: 3/6/2018 6:04:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblProgrammeModule](
	[ProgrammeModuleID] [int] IDENTITY(1,1) NOT NULL,
	[ProgrammeID] [int] NOT NULL,
	[ModuleID] [int] NOT NULL,
 CONSTRAINT [PK_tblProgrammeModule] PRIMARY KEY CLUSTERED 
(
	[ProgrammeModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

Alter table tblGradeMarks add GradeID int;
GO


/****** Object:  Table [dbo].[tblAgentCommissionType]    Script Date: 3/6/2018 6:15:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblAgentCommissionType](
	[AgentCommissionTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblAgentCommissionType] PRIMARY KEY CLUSTERED 
(
	[AgentCommissionTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblGradeMarks]    Script Date: 3/6/2018 6:54:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblGradeMarks]
(
	 	  @GradeMarksID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @MarksRangeFrom int
	 	 , @MarksRangeTo int
		  ,@GradeID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblGradeMarks
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,MarksRangeFrom = @MarksRangeFrom
	 	 	 	 	 ,MarksRangeTo = @MarksRangeTo
					 ,GradeID=@GradeID
	 	 	 	 	 
	 	 	 	 	 where GradeMarksID = @GradeMarksID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblGradeMarks]    Script Date: 3/6/2018 6:53:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblGradeMarks]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@MarksRangeFrom int
	 	 	 	 ,@MarksRangeTo int
				 ,@GradeID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblGradeMarks
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,MarksRangeFrom
	 	 	 	 ,MarksRangeTo
				 ,GradeID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@MarksRangeFrom
	 	 	 	 ,@MarksRangeTo
				 ,@GradeID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_GradeMarksID_tblGradeMarks]    Script Date: 3/6/2018 6:53:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_GradeMarksID_tblGradeMarks]
(
	 	 @GradeMarksID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 GradeMarksID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 MarksRangeFrom
,	 	 	 	 MarksRangeTo
,			     GradeID
	 	 FROM tblGradeMarks
	 	 WHERE GradeMarksID = @GradeMarksID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblGradeMarks]    Script Date: 3/6/2018 6:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblGradeMarks]
AS
BEGIN
	 	 SELECT 
	 	 	 	 GradeMarksID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 MarksRangeFrom
,	 	 	 	 MarksRangeTo
,			     GradeID
	 	 FROM tblGradeMarks
	 	 END 
GO

Insert into tblAgentCommissionType(Code,Description) values (1,'Fixed');
Insert into tblAgentCommissionType(Code,Description) values (1,'Percentage');
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ProgrammeID_tblProgrammeModule]    Script Date: 3/7/2018 5:05:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ProgrammeID_tblProgrammeModule]
(
	 	 @ProgrammeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblProgrammeModule.ProgrammeModuleID
,	 	 	 	 tblProgrammeModule.ProgrammeID
,	 	 	 	 tblProgrammeModule.ModuleID
,				 tblModule.Code as [Module Code]
,				 tblModule.Description as [Module Description]
,				 tblProgramme.Code [Programme Code]
,				 tblProgramme.Description [Programme Description]
	 	 FROM 
		 tblProgrammeModule left join tblProgramme on 
			tblProgrammeModule.ProgrammeID=tblProgramme.ProgrammeID
		 left join tblModule on
			tblProgrammeModule.ModuleID= tblModule.ModuleID
	 	 WHERE tblProgrammeModule.ProgrammeID = @ProgrammeID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblProgrammeModule]    Script Date: 3/7/2018 5:05:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblProgrammeModule]
(
	 	 	 	 @ProgrammeID int
	 	 	 	 ,@ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(ProgrammeModuleID) from tblProgrammeModule where
		ProgrammeID=@ProgrammeID and ModuleID=@ModuleID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblProgrammeModule
	 	 	 	 ( 
	 	 	 	 ProgrammeID
	 	 	 	 ,ModuleID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ProgrammeID
	 	 	 	 ,@ModuleID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_GradeID_tblGradeMarks]    Script Date: 3/7/2018 6:04:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_GradeID_tblGradeMarks]
(
	 	 @GradeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 GradeMarksID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 MarksRangeFrom
,	 	 	 	 MarksRangeTo
,			     GradeID
	 	 FROM tblGradeMarks
	 	 WHERE GradeID = @GradeID
	 	 END 
GO

Alter table tblClient add PhotoFileName varchar(50);
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblClient]    Script Date: 3/9/2018 5:08:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClient]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
	 	 FROM tblClient
	 	 WHERE ClientID = @ClientID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblClient]    Script Date: 3/9/2018 5:04:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblClient]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
,	 	 	 	 PhotoFileName
	 	 FROM tblClient
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblClient]    Script Date: 3/9/2018 5:10:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClient]
(
	 	  @ClientID int
	 	 , @StudentReference varchar(10)
	 	 , @FirstName varchar(50)
	 	 , @MiddleName varchar(50)
	 	 , @LastName varchar(50)
	 	 , @DateOfBirth datetime
	 	 , @Photo varbinary
	 	 , @PassportNumber varchar(20)
	 	 , @ResidenceID int
	 	 , @ClientTypeID int
	 	 , @NSNNumber varchar(50)
	 	 , @WorkNumber varchar(50)
	 	 , @PublicTrustNumber varchar(50)
	 	 , @ExternalRef1 varchar(50)
	 	 , @ExternalRef2 varchar(50)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @EthnicityID int
	 	 , @IwiAffiliation1 varchar(50)
	 	 , @IwiAffiliation2 varchar(50)
	 	 , @IwiAffiliation3 varchar(50)
	 	 , @IwiAffiliation4 varchar(50)
	 	 , @IwiAffiliation5 varchar(50)
	 	 , @AgentID int
		 ,@PhotoFileName varchar(50)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClient
	 	 SET 
	 	 	 	 	 StudentReference = @StudentReference
	 	 	 	 	 ,FirstName = @FirstName
	 	 	 	 	 ,MiddleName = @MiddleName
	 	 	 	 	 ,LastName = @LastName
	 	 	 	 	 ,DateOfBirth = @DateOfBirth
	 	 	 	 	 ,Photo = @Photo
	 	 	 	 	 ,PassportNumber = @PassportNumber
	 	 	 	 	 ,ResidenceID = @ResidenceID
	 	 	 	 	 ,ClientTypeID = @ClientTypeID
	 	 	 	 	 ,NSNNumber = @NSNNumber
	 	 	 	 	 ,WorkNumber = @WorkNumber
	 	 	 	 	 ,PublicTrustNumber = @PublicTrustNumber
	 	 	 	 	 ,ExternalRef1 = @ExternalRef1
	 	 	 	 	 ,ExternalRef2 = @ExternalRef2
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,EthnicityID = @EthnicityID
	 	 	 	 	 ,IwiAffiliation1 = @IwiAffiliation1
	 	 	 	 	 ,IwiAffiliation2 = @IwiAffiliation2
	 	 	 	 	 ,IwiAffiliation3 = @IwiAffiliation3
	 	 	 	 	 ,IwiAffiliation4 = @IwiAffiliation4
	 	 	 	 	 ,IwiAffiliation5 = @IwiAffiliation5
	 	 	 	 	 ,AgentID = @AgentID
					 ,PhotoFileName =@PhotoFileName
	 	 	 	 	 
	 	 	 	 	 where ClientID = @ClientID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClient]    Script Date: 3/9/2018 5:08:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClient]
(
	 	 	 	 @StudentReference varchar(10)
	 	 	 	 ,@FirstName varchar(50)
	 	 	 	 ,@MiddleName varchar(50)
	 	 	 	 ,@LastName varchar(50)
	 	 	 	 ,@DateOfBirth datetime
	 	 	 	 ,@Photo varbinary
	 	 	 	 ,@PassportNumber varchar(20)
	 	 	 	 ,@ResidenceID int
	 	 	 	 ,@ClientTypeID int
	 	 	 	 ,@NSNNumber varchar(50)
	 	 	 	 ,@WorkNumber varchar(50)
	 	 	 	 ,@PublicTrustNumber varchar(50)
	 	 	 	 ,@ExternalRef1 varchar(50)
	 	 	 	 ,@ExternalRef2 varchar(50)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@EthnicityID int
	 	 	 	 ,@IwiAffiliation1 varchar(50)
	 	 	 	 ,@IwiAffiliation2 varchar(50)
	 	 	 	 ,@IwiAffiliation3 varchar(50)
	 	 	 	 ,@IwiAffiliation4 varchar(50)
	 	 	 	 ,@IwiAffiliation5 varchar(50)
	 	 	 	 ,@AgentID int
				 ,@PhotoFileName varchar(50)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClient
	 	 	 	 ( 
	 	 	 	 StudentReference
	 	 	 	 ,FirstName
	 	 	 	 ,MiddleName
	 	 	 	 ,LastName
	 	 	 	 ,DateOfBirth
	 	 	 	 ,Photo
	 	 	 	 ,PassportNumber
	 	 	 	 ,ResidenceID
	 	 	 	 ,ClientTypeID
	 	 	 	 ,NSNNumber
	 	 	 	 ,WorkNumber
	 	 	 	 ,PublicTrustNumber
	 	 	 	 ,ExternalRef1
	 	 	 	 ,ExternalRef2
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,EthnicityID
	 	 	 	 ,IwiAffiliation1
	 	 	 	 ,IwiAffiliation2
	 	 	 	 ,IwiAffiliation3
	 	 	 	 ,IwiAffiliation4
	 	 	 	 ,IwiAffiliation5
	 	 	 	 ,AgentID
				 ,PhotoFileName
	 	 	 	 ) 
 SELECT 
	 	 	 	 @StudentReference
	 	 	 	 ,@FirstName
	 	 	 	 ,@MiddleName
	 	 	 	 ,@LastName
	 	 	 	 ,@DateOfBirth
	 	 	 	 ,@Photo
	 	 	 	 ,@PassportNumber
	 	 	 	 ,@ResidenceID
	 	 	 	 ,@ClientTypeID
	 	 	 	 ,@NSNNumber
	 	 	 	 ,@WorkNumber
	 	 	 	 ,@PublicTrustNumber
	 	 	 	 ,@ExternalRef1
	 	 	 	 ,@ExternalRef2
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@EthnicityID
	 	 	 	 ,@IwiAffiliation1
	 	 	 	 ,@IwiAffiliation2
	 	 	 	 ,@IwiAffiliation3
	 	 	 	 ,@IwiAffiliation4
	 	 	 	 ,@IwiAffiliation5
	 	 	 	 ,@AgentID
				 ,@PhotoFileName

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO


/****** Object:  Table [dbo].[tblStaffModule]    Script Date: 3/9/2018 10:21:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblStaffModule](
	[StaffModuleID] [int] IDENTITY(1,1) NOT NULL,
	[StaffID] [int] NOT NULL,
	[ModuleID] [int] NOT NULL,
 CONSTRAINT [PK_tblStaffModule] PRIMARY KEY CLUSTERED 
(
	[StaffModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblStaffModule]  WITH CHECK ADD  CONSTRAINT [FK_tblStaffModule_tblStaff] FOREIGN KEY([StaffID])
REFERENCES [dbo].[tblStaff] ([StaffID])
GO

ALTER TABLE [dbo].[tblStaffModule] CHECK CONSTRAINT [FK_tblStaffModule_tblStaff]
GO

ALTER TABLE [dbo].[tblStaffModule]  WITH CHECK ADD  CONSTRAINT [FK_tblStaffModule_tblModule] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[tblModule] ([ModuleID])
GO

ALTER TABLE [dbo].[tblStaffModule] CHECK CONSTRAINT [FK_tblStaffModule_tblModule]
GO

Alter table tblAgentDocument add DocsFileName varchar(50);
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblAgentDocument]    Script Date: 3/10/2018 6:43:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblAgentDocument]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
,				 AgentID
,				DocsFileName
	 	 FROM tblAgentDocument
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentID_tblAgentDocument]    Script Date: 3/10/2018 6:50:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_AgentID_tblAgentDocument]
(
	 	 @AgentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
,				 AgentID
,				DocsFileName
	 	 FROM tblAgentDocument
	 	 WHERE AgentID = @AgentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentDocumentID_tblAgentDocument]    Script Date: 3/10/2018 6:50:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_AgentDocumentID_tblAgentDocument]
(
	 	 @AgentDocumentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
,				 AgentID
,				DocsFileName
	 	 FROM tblAgentDocument
	 	 WHERE AgentDocumentID = @AgentDocumentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblAgentDocument]    Script Date: 3/10/2018 6:50:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblAgentDocument]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@File varbinary
				 ,@AgentID int
				 ,@DocsFileName varchar(50)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblAgentDocument
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,[File]
				 ,AgentID
				 ,DocsFileName
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@File
				 ,@AgentID
				 ,@DocsFileName

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblAgentDocument]    Script Date: 3/10/2018 6:54:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblAgentDocument]
(
	 	  @AgentDocumentID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @File varbinary
		  ,@AgentID int
		  ,@DocsFileName varchar(50)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblAgentDocument
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,[File] = @File
	 	 	 	 	 ,AgentID=@AgentID
					 ,DocsFileName=@DocsFileName
	 	 	 	 	 where AgentDocumentID = @AgentDocumentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

-----Stored Procedure
if exists(select * from sysobjects where name  ='SCP_GET_BY_StaffID_tblStaffModule')
Drop Procedure SCP_GET_BY_StaffID_tblStaffModule;
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_StaffID_tblStaffModule]
(
	 	 @StaffID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblStaffModule.StaffModuleID
,	 	 	 	 tblStaffModule.StaffID
,	 	 	 	 tblStaffModule.ModuleID
,				 tblModule.Code as [Module Code]
,				 tblModule.Description as [Module Description]
,				 tblStaff.Code [Staff Code]
,				 tblStaff.FirstName + ' '+ IsNull(tblStaff.LastName,'') as [Name]
	 	 FROM 
		 tblStaffModule left join tblStaff on 
			tblStaffModule.StaffID=tblStaff.StaffID
		 left join tblModule on
			tblStaffModule.ModuleID= tblModule.ModuleID
	 	 WHERE tblStaffModule.StaffID = @StaffID
	 	 END 

GO

if exists ( select 1 from sysobjects where name like 'SCP_SET_tblStaffModule')
Drop Procedure SCP_SET_tblStaffModule
Go
CREATE PROCEDURE [dbo].[SCP_SET_tblStaffModule]
(
	 	 	 	 @StaffID int
	 	 	 	 ,@ModuleID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(StaffModuleID) from tblStaffModule where
		StaffID=@StaffID and ModuleID=@ModuleID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblStaffModule
	 	 	 	 ( 
	 	 	 	 StaffID
	 	 	 	 ,ModuleID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @StaffID
	 	 	 	 ,@ModuleID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 


GO

-------------Relationship Related Changes------
if exists(select * from sysobjects where name  ='FK_tblStaffModule_tblModule')
Alter table tblStaffModule drop CONSTRAINT  FK_tblStaffModule_tblModule
GO


ALTER TABLE [dbo].[tblStaffModule]  WITH CHECK ADD  CONSTRAINT [FK_tblStaffModule_tblModule] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[tblModule] ([ModuleID])
GO

ALTER TABLE [dbo].[tblStaffModule] CHECK CONSTRAINT [FK_tblStaffModule_tblModule]
GO



if exists(select * from sysobjects where name  ='FK_tblStaffModule_tblStaff')
Alter table tblStaffModule drop CONSTRAINT  FK_tblStaffModule_tblStaff
GO

ALTER TABLE [dbo].[tblStaffModule]  WITH CHECK ADD  CONSTRAINT [FK_tblStaffModule_tblStaff] FOREIGN KEY([StaffID])
REFERENCES [dbo].[tblStaff] ([StaffID])
GO

ALTER TABLE [dbo].[tblStaffModule] CHECK CONSTRAINT [FK_tblStaffModule_tblStaff]
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ModuleID_tblStaffModule]    Script Date: 3/15/2018 2:21:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ModuleID_tblStaffModule]
(
	 	 @ModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblStaffModule.StaffModuleID
,	 	 	 	 tblStaffModule.StaffID
,	 	 	 	 tblStaffModule.ModuleID
,				 tblModule.Code as [Module Code]
,				 tblModule.Description as [Module Description]
,				 tblStaff.Code [Staff Code]
,				 tblStaff.FirstName + ' '+ IsNull(tblStaff.LastName,'') as [Name]
	 	 FROM 
		 tblStaffModule left join tblStaff on 
			tblStaffModule.StaffID=tblStaff.StaffID
		 left join tblModule on
			tblStaffModule.ModuleID= tblModule.ModuleID
	 	 WHERE tblStaffModule.ModuleID = @ModuleID
	 	 END 
GO

Alter table tblAssessments add ModuleID int;
GO


/****** Object:  Table [dbo].[tblModuleLO]    Script Date: 3/15/2018 3:40:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblModuleLO](
	[ModuleLOID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [int] NOT NULL,
	[LOID] [int] NOT NULL,
 CONSTRAINT [PK_tblModuleLO] PRIMARY KEY CLUSTERED 
(
	[ModuleLOID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblModuleLO]  WITH CHECK ADD  CONSTRAINT [FK_tblModuleLO_tblModule] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[tblModule] ([ModuleID])
GO

ALTER TABLE [dbo].[tblModuleLO] CHECK CONSTRAINT [FK_tblModuleLO_tblModule]
GO

ALTER TABLE [dbo].[tblModuleLO]  WITH CHECK ADD  CONSTRAINT [FK_tblModuleLO_tblLO] FOREIGN KEY([LOID])
REFERENCES [dbo].[tblLO] ([LOID])
GO

ALTER TABLE [dbo].[tblModuleLO] CHECK CONSTRAINT [FK_tblModuleLO_tblLO]
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblQualificationProgramme]    Script Date: 3/15/2018 3:46:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblQualificationProgramme](
	[QualificationProgrammeID] [int] IDENTITY(1,1) NOT NULL,
	[QualificationID] [int] NOT NULL,
	[ProgrammeID] [int] NOT NULL,
 CONSTRAINT [PK_tblQualificationProgramme] PRIMARY KEY CLUSTERED 
(
	[QualificationProgrammeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblQualificationProgramme]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationProgramme_tblQualification] FOREIGN KEY([QualificationID])
REFERENCES [dbo].[tblQualification] ([QualificationID])
GO

ALTER TABLE [dbo].[tblQualificationProgramme] CHECK CONSTRAINT [FK_tblQualificationProgramme_tblQualification]
GO

ALTER TABLE [dbo].[tblQualificationProgramme]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationProgramme_tblProgramme] FOREIGN KEY([ProgrammeID])
REFERENCES [dbo].[tblProgramme] ([ProgrammeID])
GO

ALTER TABLE [dbo].[tblQualificationProgramme] CHECK CONSTRAINT [FK_tblQualificationProgramme_tblProgramme]
GO


/****** Object:  Table [dbo].[tblQualificationGPO]    Script Date: 3/15/2018 3:50:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblQualificationGPO](
	[QualificationGPOID] [int] IDENTITY(1,1) NOT NULL,
	[QualificationID] [int] NOT NULL,
	[GPOID] [int] NOT NULL,
 CONSTRAINT [PK_tblQualificationGPO] PRIMARY KEY CLUSTERED 
(
	[QualificationGPOID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblQualificationGPO]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationGPO_tblQualification] FOREIGN KEY([QualificationID])
REFERENCES [dbo].[tblQualification] ([QualificationID])
GO

ALTER TABLE [dbo].[tblQualificationGPO] CHECK CONSTRAINT [FK_tblQualificationGPO_tblQualification]
GO

ALTER TABLE [dbo].[tblQualificationGPO]  WITH CHECK ADD  CONSTRAINT [FK_tblQualificationGPO_tblGPO] FOREIGN KEY([GPOID])
REFERENCES [dbo].[tblGPO] ([GPOID])
GO

ALTER TABLE [dbo].[tblQualificationGPO] CHECK CONSTRAINT [FK_tblQualificationGPO_tblGPO]
GO


/****** Object:  Table [dbo].[tblStaffQualification]    Script Date: 3/15/2018 3:53:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblStaffQualification](
	[StaffQualificationID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,	
	[Description] [varchar](200) NULL,	
	[StaffQualificationLevelID] [int] NULL,
	[StaffQualificationTypeID] [int] NULL,
	[StaffID] [int] NULL	
 CONSTRAINT [PK_tblStaffQualification] PRIMARY KEY CLUSTERED 
(
	[StaffQualificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[tblStaffQualificationLevel]    Script Date: 3/15/2018 4:04:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[tblStaffQualificationLevel](
	[StaffQualificationLevelID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblStaffQualificationLevel] PRIMARY KEY CLUSTERED 
(
	[StaffQualificationLevelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[tblStaffQualificationType]    Script Date: 3/15/2018 4:07:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[tblStaffQualificationType](
	[StaffQualificationTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblStaffQualificationType] PRIMARY KEY CLUSTERED 
(
	[StaffQualificationTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblModuleLO]    Script Date: 3/15/2018 7:58:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblModuleLO]
(
	 	 	 	 @ModuleID int
	 	 	 	 ,@LOID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(ModuleLOID) from tblModuleLO where
		ModuleID=@ModuleID and LOID=@LOID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblModuleLO
	 	 	 	 ( 
	 	 	 	 ModuleID
	 	 	 	 ,LOID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ModuleID
	 	 	 	 ,@LOID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblQualificationGPO]    Script Date: 3/15/2018 7:58:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblQualificationGPO]
(
	 	 	 	 @QualificationID int
	 	 	 	 ,@GPOID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(QualificationGPOID) from tblQualificationGPO where
		QualificationID=@QualificationID and GPOID=@GPOID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblQualificationGPO
	 	 	 	 ( 
	 	 	 	 QualificationID
	 	 	 	 ,GPOID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @QualificationID
	 	 	 	 ,@GPOID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblQualificationProgramme]    Script Date: 3/15/2018 7:58:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblQualificationProgramme]
(
	 	 	 	 @QualificationID int
	 	 	 	 ,@ProgrammeID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(QualificationProgrammeID) from tblQualificationProgramme where
		QualificationID=@QualificationID and ProgrammeID=@ProgrammeID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblQualificationProgramme
	 	 	 	 ( 
	 	 	 	 QualificationID
	 	 	 	 ,ProgrammeID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @QualificationID
	 	 	 	 ,@ProgrammeID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ModuleLOID_tblModuleLO]    Script Date: 3/15/2018 11:22:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ModuleID_tblModuleLO]
(
	 	 @ModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleLOID
,	 	 	 	 ModuleID
,	 	 	 	 LOID
	 	 FROM tblModuleLO
	 	 WHERE ModuleID = @ModuleID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_QualificationID_tblQualificationGPO]    Script Date: 3/15/2018 11:24:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_QualificationID_tblQualificationGPO]
(
	 	 @QualificationID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationGPOID
,	 	 	 	 QualificationID
,	 	 	 	 GPOID
	 	 FROM tblQualificationGPO
	 	 WHERE QualificationID = @QualificationID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_QualificationID_tblQualificationProgramme]    Script Date: 3/15/2018 11:23:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_QualificationID_tblQualificationProgramme]
(
	 	 @QualificationID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationProgrammeID
,	 	 	 	 QualificationID
,	 	 	 	 ProgrammeID
	 	 FROM tblQualificationProgramme
	 	 WHERE QualificationID = @QualificationID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ModuleID_tblModuleLO]    Script Date: 3/15/2018 11:35:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ModuleID_tblModuleLO]
(
	 	 @ModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblModuleLO.ModuleLOID
,	 	 	 	 tblModuleLO.ModuleID
,	 	 	 	 tblModuleLO.LOID
,				 tblLO.Code as [LO Code]
,				 tblLO.Description as [LO Description]
,				 tblModule.Code [Module Code]
,				 tblModule.Description [Module Description]
	 	 FROM 
		 tblModuleLO left join tblModule on 
			tblModuleLO.ModuleID=tblModule.ModuleID
		 left join tblLO on
			tblModuleLO.LOID= tblLO.LOID
	 	 WHERE tblModuleLO.ModuleID = @ModuleID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_QualificationID_tblQualificationProgramme]    Script Date: 3/15/2018 11:40:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_QualificationID_tblQualificationProgramme]
(
	 	 @QualificationID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblQualificationProgramme.QualificationProgrammeID
,	 	 	 	 tblQualificationProgramme.QualificationID
,	 	 	 	 tblQualificationProgramme.ProgrammeID
,				 tblProgramme.Code as [Programme Code]
,				 tblProgramme.Description as [Programme Description]
,				 tblQualification.Code [Qualification Code]
,				 tblQualification.Description [Qualification Description]
	 	 FROM 
		 tblQualificationProgramme left join tblQualification on 
			tblQualificationProgramme.QualificationID=tblQualification.QualificationID
		 left join tblProgramme on
			tblQualificationProgramme.ProgrammeID= tblProgramme.ProgrammeID
	 	 WHERE tblQualificationProgramme.QualificationID = @QualificationID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_QualificationID_tblQualificationGPO]    Script Date: 3/15/2018 11:40:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_QualificationID_tblQualificationGPO]
(
	 	 @QualificationID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblQualificationGPO.QualificationGPOID
,	 	 	 	 tblQualificationGPO.QualificationID
,	 	 	 	 tblQualificationGPO.GPOID
,				 tblGPO.Code as [GPO Code]
,				 tblGPO.Description as [GPO Description]
,				 tblQualification.Code [Qualification Code]
,				 tblQualification.Description [Qualification Description]
	 	 FROM 
		 tblQualificationGPO left join tblQualification on 
			tblQualificationGPO.QualificationID=tblQualification.QualificationID
		 left join tblGPO on
			tblQualificationGPO.GPOID= tblGPO.GPOID
	 	 WHERE tblQualificationGPO.QualificationID = @QualificationID
	 	 END 
GO

If Exists(select 1 from sysobjects where name='SCP_GET_BY_StaffID_tblStaffQualification')
Drop PROCEDURE SCP_GET_BY_StaffID_tblStaffQualification
GO


CREATE PROCEDURE [dbo].[SCP_GET_BY_StaffID_tblStaffQualification]
(
	 	 @StaffID int
)
AS
BEGIN
	 	
		SELECT 
		StaffQualificationID,
		Code,
		Description,
		StaffQualificationLevelID,
		StaffQualificationTypeID,
		StaffID
	 	 FROM tblStaffQualification
	 	 WHERE StaffID = @StaffID
	 	 END 
		 
GO	


If Not Exists(select 1 from tblStaffQualificationLevel where code='01')
BEGIN
	Insert into tblStaffQualificationLevel (code,Description) values('01','01')
	Insert into tblStaffQualificationLevel (code,Description) values('02','02')
	Insert into tblStaffQualificationLevel (code,Description) values('03','03')
	Insert into tblStaffQualificationLevel (code,Description) values('04','04')
	Insert into tblStaffQualificationLevel (code,Description) values('05','05')
	Insert into tblStaffQualificationLevel (code,Description) values('06','06')
	Insert into tblStaffQualificationLevel (code,Description) values('07','07')
	Insert into tblStaffQualificationLevel (code,Description) values('08','08')
	Insert into tblStaffQualificationLevel (code,Description) values('09','09')
	Insert into tblStaffQualificationLevel (code,Description) values('10','10')
END




If Not Exists(select 1 from tblStaffQualificationType where code='01')
BEGIN
	Insert into tblStaffQualificationType (code,Description) values('01','Diploma')
	Insert into tblStaffQualificationType (code,Description) values('02','Bachelors')
	Insert into tblStaffQualificationType (code,Description) values('03','Graduate Diploma')
	Insert into tblStaffQualificationType (code,Description) values('04','Post Graduate Diploma')
	Insert into tblStaffQualificationType (code,Description) values('05','Masters')
	Insert into tblStaffQualificationType (code,Description) values('05','Phd.')
END

Alter table tblAssessments drop column LastName;
GO

Alter table tblAssessments add AssessmentNumber int, LOCovered varchar(500);
GO


/****** Object:  Table [dbo].[tblModuleAssessment]    Script Date: 3/16/2018 7:43:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

drop table tblAssessments;
GO

CREATE TABLE [dbo].[tblModuleAssessment](
	[ModuleAssessmentID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[TypeID] int NULL,
	[FormatID] int NULL,
	[LOCovered] [varchar](500) NULL,
	[TotalMarks] [int] NULL,
	[MinPassPercentage] [int] NULL,
	[ModuleID] [int] NULL	
	
 CONSTRAINT [PK_tblModuleAssessment] PRIMARY KEY CLUSTERED 
(
	[ModuleAssessmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[tblModuleAssessmentType]    Script Date: 3/16/2018 7:57:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[tblModuleAssessmentType](
	[ModuleAssessmentTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblModuleAssessmentType] PRIMARY KEY CLUSTERED 
(
	[ModuleAssessmentTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[tblModuleAssessmentFormat]    Script Date: 3/16/2018 7:57:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[tblModuleAssessmentFormat](
	[ModuleAssessmentFormatID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblModuleAssessmentFormat] PRIMARY KEY CLUSTERED 
(
	[ModuleAssessmentFormatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


