USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_InstituteID_tblInstituteSchool]    Script Date: 1/31/2018 4:13:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_InstituteID_tblInstituteSchool]
(
	 	 @InstituteID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblInstituteSchool.InstituteSchoolID
,	 	 	 	 tblInstituteSchool.InstituteID
,	 	 	 	 tblInstituteSchool.SchoolID
,				 tblSchool.Code as [School Code]
,				 tblSchool.Description as [School Description]
,				 tblInstitution.Code [Institute Code]
,				 tblInstitution.Description [Institute Description]
	 	 FROM 
		 tblInstituteSchool left join tblInstitution on 
			tblInstituteSchool.InstituteID=tblInstitution.InstitutionID
		 left join tblSchool on
			tblInstituteSchool.SchoolID= tblSchool.SchoolID
	 	 WHERE InstituteID = @InstituteID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblInstitution]    Script Date: 2/7/2018 2:29:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblInstitution]
(
	 	 @Code varchar(10)
		 ,@InstitutionID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblInstitution WHERE Code = @Code and InstitutionID <>@InstitutionID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblAgent]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblAgent]
(
	 	 @Code varchar(10)
		 ,@AgentID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblAgent WHERE Code = @Code and AgentID <>@AgentID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblBuilding]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblBuilding]
(
	 	 @Code varchar(10)
		 ,@BuildingID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblBuilding WHERE Code = @Code and BuildingID <>@BuildingID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblCampus]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblCampus]
(
	 	 @Code varchar(10)
		 ,@CampusID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblCampus WHERE Code = @Code and CampusID <>@CampusID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblClass]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblClass]
(
	 	 @Code varchar(10)
		 ,@ClassID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblClass WHERE Code = @Code and ClassID <>@ClassID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblDepartment]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblDepartment]
(
	 	 @Code varchar(10)
		 ,@DepartmentID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblDepartment WHERE Code = @Code and DepartmentID <>@DepartmentID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblEquipment]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblEquipment]
(
	 	 @Code varchar(10)
		 ,@EquipmentID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblEquipment WHERE Code = @Code and EquipmentID <>@EquipmentID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblEquipmentType]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblEquipmentType]
(
	 	 @Code varchar(10)
		 ,@EquipmentTypeID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblEquipmentType WHERE Code = @Code and EquipmentTypeID <>@EquipmentTypeID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblSchool]    Script Date: 2/9/2018 11:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblSchool]
(
	 	 @Code varchar(10)
		 ,@SchoolID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblSchool WHERE Code = @Code and SchoolID <>@SchoolID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 

GO
