﻿USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblSchoolCampus]    Script Date: 2/27/2018 7:52:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblSchoolCampus](
	[SchoolCampusID] [int] IDENTITY(1,1) NOT NULL,	
	[SchoolID] [int] NOT NULL,
	[CampusID] [int] NOT NULL
 CONSTRAINT [PK_tblSchoolCampus] PRIMARY KEY CLUSTERED 
(
	[SchoolCampusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tblCampusBuilding]    Script Date: 2/28/2018 4:37:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblCampusBuilding](
	[CampusBuildingID] [int] IDENTITY(1,1) NOT NULL,
	[CampusID] [int] NOT NULL,
	[BuildingID] [int] NOT NULL,
 CONSTRAINT [PK_tblCampusBuilding] PRIMARY KEY CLUSTERED 
(
	[CampusBuildingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[tblBuildingDepartment]    Script Date: 2/28/2018 4:43:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblBuildingDepartment](
	[BuildingDepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[BuildingID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
 CONSTRAINT [PK_tblBuildingDepartment] PRIMARY KEY CLUSTERED 
(
	[BuildingDepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[tblBuildingClass]    Script Date: 2/28/2018 4:44:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblBuildingClass](
	[BuildingClassID] [int] IDENTITY(1,1) NOT NULL,
	[BuildingID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
 CONSTRAINT [PK_tblBuildingClass] PRIMARY KEY CLUSTERED 
(
	[BuildingClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[tblClassEquipment]    Script Date: 2/28/2018 4:49:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblClassEquipment](
	[ClassEquipmentID] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [int] NOT NULL,
	[EquipmentID] [int] NOT NULL,
 CONSTRAINT [PK_tblClassEquipment] PRIMARY KEY CLUSTERED 
(
	[ClassEquipmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[tblClassDepartment]    Script Date: 2/28/2018 4:49:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblClassDepartment](
	[ClassDepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
 CONSTRAINT [PK_tblClassDepartment] PRIMARY KEY CLUSTERED 
(
	[ClassDepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tblDepartmentBuilding]    Script Date: 2/28/2018 4:52:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblDepartmentBuilding](
	[DepartmentBuildingID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[BuildingID] [int] NOT NULL,
 CONSTRAINT [PK_tblDepartmentBuilding] PRIMARY KEY CLUSTERED 
(
	[DepartmentBuildingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[tblAgentContact]    Script Date: 2/28/2018 5:05:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblAgentContact](
	[AgentContactID] [int] IDENTITY(1,1) NOT NULL,
	[AgentID] [int] NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[PhoneCountryCode] [varchar](3) NULL,
	[PhoneStateCode] [varchar](2) NULL,
	[Phone] [varchar](10) NULL,
	[MobileCountryCode] [varchar](3) NULL,
	[Mobile] [varchar](10) NULL,
	[IsPrimaryContact] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblAgentContact] PRIMARY KEY CLUSTERED 
(
	[AgentContactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblAgentContact] ADD  CONSTRAINT [DF_tblAgentContact_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

/****** Object:  Table [dbo].[tblAgentDocument]    Script Date: 2/28/2018 5:08:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblAgentDocument](
	[AgentDocumentID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[File] [varbinary](max) NULL,
 CONSTRAINT [PK_tblAgentDocument] PRIMARY KEY CLUSTERED 
(
	[AgentDocumentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO


/****** Object:  StoredProcedure [dbo].[SCP_SET_tblCampusBuilding]    Script Date: 2/28/2018 10:50:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblCampusBuilding]
(
	 	 	 	 @CampusID int
	 	 	 	 ,@BuildingID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(CampusBuildingID) from tblCampusBuilding where
		CampusID=@CampusID and BuildingID=@BuildingID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblCampusBuilding
	 	 	 	 ( 
	 	 	 	 CampusID
	 	 	 	 ,BuildingID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @CampusID
	 	 	 	 ,@BuildingID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_CampusID_tblCampusBuilding]    Script Date: 2/28/2018 10:46:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_CampusID_tblCampusBuilding]
(
	 	 @CampusID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblCampusBuilding.CampusBuildingID
,	 	 	 	 tblCampusBuilding.CampusID
,	 	 	 	 tblCampusBuilding.BuildingID
,				 tblBuilding.Code as [Building Code]
,				 tblBuilding.Description as [Building Description]
,				 tblCampus.Code [Campus Code]
,				 tblCampus.Description [Campus Description]
	 	 FROM 
		 tblCampusBuilding left join tblCampus on 
			tblCampusBuilding.CampusID=tblCampus.CampusID
		 left join tblBuilding on
			tblCampusBuilding.BuildingID= tblBuilding.BuildingID
	 	 WHERE tblCampusBuilding.CampusID = @CampusID
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_SchoolID_tblSchoolCampus]    Script Date: 3/1/2018 1:48:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_SchoolID_tblSchoolCampus]
(
	 	 @SchoolID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblSchoolCampus.SchoolCampusID
,	 	 	 	 tblSchoolCampus.SchoolID
,	 	 	 	 tblSchoolCampus.CampusID
,				 tblCampus.Code as [Campus Code]
,				 tblCampus.Description as [Campus Description]
,				 tblSchool.Code [School Code]
,				 tblSchool.Description [School Description]
	 	 FROM 
		 tblSchoolCampus left join tblSchool on 
			tblSchoolCampus.SchoolID=tblSchool.SchoolID
		 left join tblCampus on
			tblSchoolCampus.CampusID= tblCampus.CampusID
	 	 WHERE tblSchoolCampus.SchoolID = @SchoolID
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_SET_tblSchoolCampus]    Script Date: 3/1/2018 12:58:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblSchoolCampus]
(
	 	 	 	 @SchoolID int
	 	 	 	 ,@CampusID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(SchoolCampusID) from tblSchoolCampus where
		SchoolID=@SchoolID and CampusID=@CampusID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblSchoolCampus
	 	 	 	 ( 
	 	 	 	 SchoolID
	 	 	 	 ,CampusID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @SchoolID
	 	 	 	 ,@CampusID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

ALTER PROCEDURE [dbo].[SCP_SET_tblBuildingDepartment]
(
	 	 	 	 @BuildingID int
	 	 	 	 ,@DepartmentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(BuildingDepartmentID) from tblBuildingDepartment where
		BuildingID=@BuildingID and DepartmentID=@DepartmentID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblBuildingDepartment
	 	 	 	 ( 
	 	 	 	 BuildingID
	 	 	 	 ,DepartmentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @BuildingID
	 	 	 	 ,@DepartmentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

ALTER PROCEDURE [dbo].[SCP_SET_tblBuildingClass]
(
	 	 	 	 @BuildingID int
	 	 	 	 ,@ClassID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(BuildingClassID) from tblBuildingClass where
		BuildingID=@BuildingID and ClassID=@ClassID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblBuildingClass
	 	 	 	 ( 
	 	 	 	 BuildingID
	 	 	 	 ,ClassID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @BuildingID
	 	 	 	 ,@ClassID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_BuildingID_tblBuildingClass]    Script Date: 3/1/2018 3:33:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_BuildingID_tblBuildingClass]
(
	 	 @BuildingID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblBuildingClass.BuildingClassID
,	 	 	 	 tblBuildingClass.BuildingID
,	 	 	 	 tblBuildingClass.ClassID
,				 tblClass.Code as [Class Code]
,				 tblClass.Description as [Class Description]
,				 tblBuilding.Code [Building Code]
,				 tblBuilding.Description [Building Description]
	 	 FROM 
		 tblBuildingClass left join tblBuilding on 
			tblBuildingClass.BuildingID=tblBuilding.BuildingID
		 left join tblClass on
			tblBuildingClass.ClassID= tblClass.ClassID
	 	 WHERE tblBuildingClass.BuildingID = @BuildingID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_BuildingID_tblBuildingDepartment]    Script Date: 3/1/2018 3:33:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_BuildingID_tblBuildingDepartment]
(
	 	 @BuildingID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblBuildingDepartment.BuildingDepartmentID
,	 	 	 	 tblBuildingDepartment.BuildingID
,	 	 	 	 tblBuildingDepartment.DepartmentID
,				 tblDepartment.Code as [Department Code]
,				 tblDepartment.Description as [Department Description]
,				 tblBuilding.Code [Building Code]
,				 tblBuilding.Description [Building Description]
	 	 FROM 
		 tblBuildingDepartment left join tblBuilding on 
			tblBuildingDepartment.BuildingID=tblBuilding.BuildingID
		 left join tblDepartment on
			tblBuildingDepartment.DepartmentID= tblDepartment.DepartmentID
	 	 WHERE tblBuildingDepartment.BuildingID = @BuildingID
	 	 END 
GO

Alter table tblClass add BuildingID int;
GO

Alter table tblClass drop column BuildingDescription;
GO


/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblClass]    Script Date: 3/1/2018 4:22:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClass]
(
	 	  @ClassID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @BuildingID varchar(200)
	 	 , @StudentCapacity int
	 	 , @IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClass
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,BuildingID = @BuildingID
	 	 	 	 	 ,StudentCapacity = @StudentCapacity
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 
	 	 	 	 	 where ClassID = @ClassID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClass]    Script Date: 3/1/2018 4:22:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClass]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@BuildingID varchar(200)
	 	 	 	 ,@StudentCapacity int
	 	 	 	 ,@IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClass
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,BuildingID
	 	 	 	 ,StudentCapacity
	 	 	 	 ,IsActive
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@BuildingID
	 	 	 	 ,@StudentCapacity
	 	 	 	 ,@IsActive

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClassID_tblClass]    Script Date: 3/1/2018 4:22:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClassID_tblClass]
(
	 	 @ClassID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClassID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 BuildingID
,	 	 	 	 StudentCapacity
,	 	 	 	 IsActive
	 	 FROM tblClass
	 	 WHERE ClassID = @ClassID
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblClass]    Script Date: 3/1/2018 4:21:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblClass]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClassID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 BuildingID
,	 	 	 	 StudentCapacity
,	 	 	 	 IsActive
	 	 FROM tblClass
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClassEquipment]    Script Date: 3/1/2018 5:07:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClassEquipment]
(
	 	 	 	 @ClassID int
	 	 	 	 ,@EquipmentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(ClassEquipmentID) from tblClassEquipment where
		ClassID=@ClassID and EquipmentID=@EquipmentID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClassEquipment
	 	 	 	 ( 
	 	 	 	 ClassID
	 	 	 	 ,EquipmentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClassID
	 	 	 	 ,@EquipmentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClassDepartment]    Script Date: 3/1/2018 5:07:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClassDepartment]
(
	 	 	 	 @ClassID int
	 	 	 	 ,@DepartmentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(ClassDepartmentID) from tblClassDepartment where
		ClassID=@ClassID and DepartmentID=@DepartmentID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClassDepartment
	 	 	 	 ( 
	 	 	 	 ClassID
	 	 	 	 ,DepartmentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @ClassID
	 	 	 	 ,@DepartmentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClassID_tblClassEquipment]    Script Date: 3/1/2018 5:10:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClassID_tblClassEquipment]
(
	 	 @ClassID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblClassEquipment.ClassEquipmentID
,	 	 	 	 tblClassEquipment.ClassID
,	 	 	 	 tblClassEquipment.EquipmentID
,				 tblEquipment.Code as [Equipment Code]
,				 tblEquipment.Description as [Equipment Description]
,				 tblClass.Code [Class Code]
,				 tblClass.Description [Class Description]
	 	 FROM 
		 tblClassEquipment left join tblClass on 
			tblClassEquipment.ClassID=tblClass.ClassID
		 left join tblEquipment on
			tblClassEquipment.EquipmentID= tblEquipment.EquipmentID
	 	 WHERE tblClassEquipment.ClassID = @ClassID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClassID_tblClassDepartment]    Script Date: 3/1/2018 5:10:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClassID_tblClassDepartment]
(
	 	 @ClassID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblClassDepartment.ClassDepartmentID
,	 	 	 	 tblClassDepartment.ClassID
,	 	 	 	 tblClassDepartment.DepartmentID
,				 tblDepartment.Code as [Department Code]
,				 tblDepartment.Description as [Department Description]
,				 tblClass.Code [Class Code]
,				 tblClass.Description [Class Description]
	 	 FROM 
		 tblClassDepartment left join tblClass on 
			tblClassDepartment.ClassID=tblClass.ClassID
		 left join tblDepartment on
			tblClassDepartment.DepartmentID= tblDepartment.DepartmentID
	 	 WHERE tblClassDepartment.ClassID = @ClassID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_DepartmentID_tblDepartmentBuilding]    Script Date: 3/1/2018 6:23:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_DepartmentID_tblDepartmentBuilding]
(
	 	 @DepartmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblDepartmentBuilding.DepartmentBuildingID
,	 	 	 	 tblDepartmentBuilding.DepartmentID
,	 	 	 	 tblDepartmentBuilding.BuildingID
,				 tblBuilding.Code as [Building Code]
,				 tblBuilding.Description as [Building Description]
,				 tblDepartment.Code [Department Code]
,				 tblDepartment.Description [Department Description]
	 	 FROM 
		 tblDepartmentBuilding left join tblDepartment on 
			tblDepartmentBuilding.DepartmentID=tblDepartment.DepartmentID
		 left join tblBuilding on
			tblDepartmentBuilding.BuildingID= tblBuilding.BuildingID
	 	 WHERE tblDepartmentBuilding.DepartmentID = @DepartmentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblDepartmentBuilding]    Script Date: 3/1/2018 6:22:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblDepartmentBuilding]
(
	 	 	 	 @DepartmentID int
	 	 	 	 ,@BuildingID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(DepartmentBuildingID) from tblDepartmentBuilding where
		DepartmentID=@DepartmentID and BuildingID=@BuildingID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblDepartmentBuilding
	 	 	 	 ( 
	 	 	 	 DepartmentID
	 	 	 	 ,BuildingID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @DepartmentID
	 	 	 	 ,@BuildingID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblAgentCountry]    Script Date: 3/1/2018 10:06:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblAgentCountry]
(
	 	 	 	 @AgentID int
	 	 	 	 ,@CountryID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		 ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

		Select @resCount = count(AgentCountryID) from tblAgentCountry where
		AgentID=@AgentID and CountryID=@CountryID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END


	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblAgentCountry
	 	 	 	 ( 
	 	 	 	 AgentID
	 	 	 	 ,CountryID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @AgentID
	 	 	 	 ,@CountryID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentID_tblAgentCountry]    Script Date: 3/1/2018 10:07:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AgentID_tblAgentCountry]
(
	 	 @AgentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 tblAgentCountry.AgentCountryID
,	 	 	 	 tblAgentCountry.AgentID
,	 	 	 	 tblAgentCountry.CountryID
--,				 tblCountry.Code as [Country Code]
,				 tblCountry.Country as [Country Description]
,				 tblAgent.Code [Agent Code]
,				 tblAgent.EntityName [Agent Description]
	 	 FROM 
		 tblAgentCountry left join tblAgent on 
			tblAgentCountry.AgentID=tblAgent.AgentID
		 left join tblCountry on
			tblAgentCountry.CountryID= tblCountry.CountryID
	 	 WHERE tblAgentCountry.AgentID = @AgentID
	 	 END 
GO

Alter table tblAgentDocument add AgentID int;
GO


Alter table tblAgentCommission add AgentID int;
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentID_tblAgentDocument]    Script Date: 3/1/2018 11:00:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AgentID_tblAgentDocument]
(
	 	 @AgentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
,				 AgentID
	 	 FROM tblAgentDocument
	 	 WHERE AgentID = @AgentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentContactID_tblAgentContact]    Script Date: 3/1/2018 10:47:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AgentID_tblAgentContact]
(
	 	 @AgentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentContactID
,	 	 	 	 AgentID
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 MobileCountryCode
,	 	 	 	 Mobile
,	 	 	 	 IsPrimaryContact
,	 	 	 	 IsActive
	 	 FROM tblAgentContact
	 	 WHERE AgentID = @AgentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentID_tblAgentCommission]    Script Date: 3/1/2018 11:08:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AgentID_tblAgentCommission]
(
	 	 @AgentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionID
,	 	 	 	 Code
,	 	 	 	 CountryID
,	 	 	 	 Programme
,	 	 	 	 CommissionTypeID
,	 	 	 	 CommissionPer
,	 	 	 	 CommissionFixed
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,				 AgentID
	 	 FROM tblAgentCommission
	 	 WHERE AgentID = @AgentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentCommissionID_tblAgentCommission]    Script Date: 3/1/2018 11:41:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_AgentCommissionID_tblAgentCommission]
(
	 	 @AgentCommissionID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionID
,	 	 	 	 Code
,	 	 	 	 CountryID
,	 	 	 	 Programme
,	 	 	 	 CommissionTypeID
,	 	 	 	 CommissionPer
,	 	 	 	 CommissionFixed
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,			     AgentID
	 	 FROM tblAgentCommission
	 	 WHERE AgentCommissionID = @AgentCommissionID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblAgentCommission]    Script Date: 3/1/2018 11:41:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblAgentCommission]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionID
,	 	 	 	 Code
,	 	 	 	 CountryID
,	 	 	 	 Programme
,	 	 	 	 CommissionTypeID
,	 	 	 	 CommissionPer
,	 	 	 	 CommissionFixed
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,			     AgentID
	 	 FROM tblAgentCommission
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentDocumentID_tblAgentDocument]    Script Date: 3/1/2018 11:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_AgentDocumentID_tblAgentDocument]
(
	 	 @AgentDocumentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
,				 AgentID
	 	 FROM tblAgentDocument
	 	 WHERE AgentDocumentID = @AgentDocumentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblAgentDocument]    Script Date: 3/1/2018 11:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblAgentDocument]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
,				 AgentID
	 	 FROM tblAgentDocument
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblAgentDocument]    Script Date: 3/1/2018 11:50:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblAgentDocument]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@File varbinary
				 ,@AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblAgentDocument
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,[File]
				 ,AgentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@File
				 ,@AgentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblAgentCommission]    Script Date: 3/1/2018 11:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblAgentCommission]
(
	 	 	 	 @Code nchar
	 	 	 	 ,@CountryID int
	 	 	 	 ,@Programme varchar(50)
	 	 	 	 ,@CommissionTypeID int
	 	 	 	 ,@CommissionPer varchar(50)
	 	 	 	 ,@CommissionFixed varchar(50)
	 	 	 	 ,@StartDate date
	 	 	 	 ,@EndDate date
				 ,@AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblAgentCommission
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,CountryID
	 	 	 	 ,Programme
	 	 	 	 ,CommissionTypeID
	 	 	 	 ,CommissionPer
	 	 	 	 ,CommissionFixed
	 	 	 	 ,StartDate
	 	 	 	 ,EndDate
				 ,AgentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@CountryID
	 	 	 	 ,@Programme
	 	 	 	 ,@CommissionTypeID
	 	 	 	 ,@CommissionPer
	 	 	 	 ,@CommissionFixed
	 	 	 	 ,@StartDate
	 	 	 	 ,@EndDate
				 ,@AgentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblAgentDocument]    Script Date: 3/1/2018 11:52:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblAgentDocument]
(
	 	  @AgentDocumentID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @File varbinary
		  ,@AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblAgentDocument
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,[File] = @File
	 	 	 	 	 ,AgentID=@AgentID
	 	 	 	 	 where AgentDocumentID = @AgentDocumentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblAgentCommission]    Script Date: 3/1/2018 11:46:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblAgentCommission]
(
	 	  @AgentCommissionID int
	 	 , @Code nchar
	 	 , @CountryID int
	 	 , @Programme varchar(50)
	 	 , @CommissionTypeID int
	 	 , @CommissionPer varchar(50)
	 	 , @CommissionFixed varchar(50)
	 	 , @StartDate date
	 	 , @EndDate date
		 ,@AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblAgentCommission
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Programme = @Programme
	 	 	 	 	 ,CommissionTypeID = @CommissionTypeID
	 	 	 	 	 ,CommissionPer = @CommissionPer
	 	 	 	 	 ,CommissionFixed = @CommissionFixed
	 	 	 	 	 ,StartDate = @StartDate
	 	 	 	 	 ,EndDate = @EndDate
	 	 	 	 	 ,AgentID=@AgentID
	 	 	 	 	 where AgentCommissionID = @AgentCommissionID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

sp_RENAME 'tblAgentCommission.Programme', 'ProgrammeID' , 'COLUMN';
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentID_tblAgentCommission]    Script Date: 3/3/2018 9:12:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_AgentID_tblAgentCommission]
(
	 	 @AgentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionID
,	 	 	 	 Code
,	 	 	 	 CountryID
,	 	 	 	 ProgrammeID
,	 	 	 	 CommissionTypeID
,	 	 	 	 CommissionPer
,	 	 	 	 CommissionFixed
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,				 AgentID
	 	 FROM tblAgentCommission
	 	 WHERE AgentID = @AgentID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_AgentCommissionID_tblAgentCommission]    Script Date: 3/3/2018 9:12:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_AgentCommissionID_tblAgentCommission]
(
	 	 @AgentCommissionID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionID
,	 	 	 	 Code
,	 	 	 	 CountryID
,	 	 	 	 ProgrammeID
,	 	 	 	 CommissionTypeID
,	 	 	 	 CommissionPer
,	 	 	 	 CommissionFixed
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,			     AgentID
	 	 FROM tblAgentCommission
	 	 WHERE AgentCommissionID = @AgentCommissionID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblAgentCommission]    Script Date: 3/3/2018 9:12:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblAgentCommission]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentCommissionID
,	 	 	 	 Code
,	 	 	 	 CountryID
,	 	 	 	 ProgrammeID
,	 	 	 	 CommissionTypeID
,	 	 	 	 CommissionPer
,	 	 	 	 CommissionFixed
,	 	 	 	 StartDate
,	 	 	 	 EndDate
,			     AgentID
	 	 FROM tblAgentCommission
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblAgentCommission]    Script Date: 3/3/2018 9:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblAgentCommission]
(
	 	  @AgentCommissionID int
	 	 , @Code nchar
	 	 , @CountryID int
	 	 , @ProgrammeID varchar(50)
	 	 , @CommissionTypeID int
	 	 , @CommissionPer varchar(50)
	 	 , @CommissionFixed varchar(50)
	 	 , @StartDate date
	 	 , @EndDate date
		 ,@AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblAgentCommission
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,ProgrammeID = @ProgrammeID
	 	 	 	 	 ,CommissionTypeID = @CommissionTypeID
	 	 	 	 	 ,CommissionPer = @CommissionPer
	 	 	 	 	 ,CommissionFixed = @CommissionFixed
	 	 	 	 	 ,StartDate = @StartDate
	 	 	 	 	 ,EndDate = @EndDate
	 	 	 	 	 ,AgentID=@AgentID
	 	 	 	 	 where AgentCommissionID = @AgentCommissionID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblAgentCommission]    Script Date: 3/3/2018 9:14:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblAgentCommission]
(
	 	 	 	 @Code nchar
	 	 	 	 ,@CountryID int
	 	 	 	 ,@ProgrammeID varchar(50)
	 	 	 	 ,@CommissionTypeID int
	 	 	 	 ,@CommissionPer varchar(50)
	 	 	 	 ,@CommissionFixed varchar(50)
	 	 	 	 ,@StartDate date
	 	 	 	 ,@EndDate date
				 ,@AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblAgentCommission
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,CountryID
	 	 	 	 ,ProgrammeID
	 	 	 	 ,CommissionTypeID
	 	 	 	 ,CommissionPer
	 	 	 	 ,CommissionFixed
	 	 	 	 ,StartDate
	 	 	 	 ,EndDate
				 ,AgentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@CountryID
	 	 	 	 ,@ProgrammeID
	 	 	 	 ,@CommissionTypeID
	 	 	 	 ,@CommissionPer
	 	 	 	 ,@CommissionFixed
	 	 	 	 ,@StartDate
	 	 	 	 ,@EndDate
				 ,@AgentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO