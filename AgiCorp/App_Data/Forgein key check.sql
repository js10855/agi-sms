﻿---Drop all constraints

if exists(select * from sysobjects where name  ='FK_tblBuilding_tblCampus')
Alter table tblBuilding drop CONSTRAINT  FK_tblBuilding_tblCampus
GO
if exists(select * from sysobjects where name  ='FK_tblClass_tblBuilding')
Alter table tblClass drop CONSTRAINT  FK_tblClass_tblBuilding
GO
if exists(select * from sysobjects where name  ='FK_tblDepartment_tblSchool')
Alter table tblDepartment drop CONSTRAINT  FK_tblDepartment_tblSchool
GO
if exists(select * from sysobjects where name  ='FK_tblEquipment_tblEquipmentType')
Alter table tblEquipment drop CONSTRAINT  FK_tblEquipment_tblEquipmentType
GO
if exists(select * from sysobjects where name  ='FK_tblEquipment_tblClass')
Alter table tblEquipment drop CONSTRAINT  FK_tblEquipment_tblClass
GO

---Drop  Constraints

if exists(select * from sysobjects where name  ='FK_tblProgrammeModule_tblProgramme')
Alter table tblProgrammeModule drop CONSTRAINT  FK_tblProgrammeModule_tblProgramme
GO


if exists(select * from sysobjects where name  ='FK_tblProgrammeModule_tblModule')
Alter table tblProgrammeModule drop CONSTRAINT  FK_tblProgrammeModule_tblModule
GO


if exists(select * from sysobjects where name  ='FK_tblGradeMarks_tblGrade')
Alter table tblGradeMarks drop CONSTRAINT  FK_tblGradeMarks_tblGrade
GO




----ProgrammeModule – Module - Programme

ALTER TABLE [dbo].[tblProgrammeModule]  WITH CHECK ADD  CONSTRAINT [FK_tblProgrammeModule_tblProgramme] FOREIGN KEY([ProgrammeID])
REFERENCES [dbo].[tblProgramme] ([ProgrammeID])
GO

ALTER TABLE [dbo].[tblProgrammeModule] CHECK CONSTRAINT [FK_tblProgrammeModule_tblProgramme]
GO


ALTER TABLE [dbo].[tblProgrammeModule]  WITH CHECK ADD  CONSTRAINT [FK_tblProgrammeModule_tblModule] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[tblModule] ([ModuleID])
GO

ALTER TABLE [dbo].[tblProgrammeModule] CHECK CONSTRAINT [FK_tblProgrammeModule_tblModule]
GO



---GradeMarks – Grade 


ALTER TABLE [dbo].[tblGradeMarks]  WITH CHECK ADD  CONSTRAINT [FK_tblGradeMarks_tblGrade] FOREIGN KEY([GradeID])
REFERENCES [dbo].[tblGrade] ([GradeID])
GO

ALTER TABLE [dbo].[tblGradeMarks] CHECK CONSTRAINT [FK_tblGradeMarks_tblGrade]
GO




if exists(select * from sysobjects where name  ='FK_tblInstituteSchool_tblInstitution')
Alter table tblInstituteSchool drop CONSTRAINT  FK_tblInstituteSchool_tblInstitution
GO
if exists(select * from sysobjects where name  ='FK_tblInstituteSchool_tblSchool')
Alter table tblInstituteSchool drop CONSTRAINT  FK_tblInstituteSchool_tblSchool
GO
if exists(select * from sysobjects where name  ='FK_SchoolCampus_tblCampus')
Alter table tblSchoolCampus drop CONSTRAINT  FK_SchoolCampus_tblCampus
GO

if exists(select * from sysobjects where name  ='FK_SchoolCampus_tblSchool')
Alter table tblSchoolCampus drop CONSTRAINT  FK_SchoolCampus_tblSchool
GO

if exists(select * from sysobjects where name  ='FK_tblCampusBuilding_tblBuilding')
Alter table tblCampusBuilding drop CONSTRAINT  FK_tblCampusBuilding_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblCampusBuilding_tblCampus')
Alter table tblCampusBuilding drop CONSTRAINT  FK_tblCampusBuilding_tblCampus
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingDepartment_tblBuilding')
Alter table tblBuildingDepartment drop CONSTRAINT  FK_tblBuildingDepartment_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingDepartment_tblDepartment')
Alter table tblBuildingDepartment drop CONSTRAINT  FK_tblBuildingDepartment_tblDepartment
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingClass_tblBuilding')
Alter table tblBuildingClass drop CONSTRAINT  FK_tblBuildingClass_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblBuildingClass_tblClass')
Alter table tblBuildingClass drop CONSTRAINT  FK_tblBuildingClass_tblClass
GO

if exists(select * from sysobjects where name  ='FK_tblClassEquipment_tblEquipment')
Alter table tblClassEquipment drop CONSTRAINT  FK_tblClassEquipment_tblEquipment
GO

if exists(select * from sysobjects where name  ='FK_tblClassEquipment_tblClass')
Alter table tblClassEquipment drop CONSTRAINT  FK_tblClassEquipment_tblClass
GO

if exists(select * from sysobjects where name  ='FK_tblDepartmentBuilding_tblBuilding')
Alter table tblDepartmentBuilding drop CONSTRAINT  FK_tblDepartmentBuilding_tblBuilding
GO

if exists(select * from sysobjects where name  ='FK_tblDepartmentBuilding_tblDepartment')
Alter table tblDepartmentBuilding drop CONSTRAINT  FK_tblDepartmentBuilding_tblDepartment
GO

if exists(select * from sysobjects where name  ='FK_tblAgentCommission_tblAgent')
Alter table tblAgentCommission drop CONSTRAINT  FK_tblAgentCommission_tblAgent
GO

if exists(select * from sysobjects where name  ='FK_tblAgentContact_tblAgent')
Alter table tblAgentContact drop CONSTRAINT  FK_tblAgentContact_tblAgent
GO

if exists(select * from sysobjects where name  ='FK_tblAgentCountry_tblAgent')
Alter table tblAgentCountry drop CONSTRAINT  FK_tblAgentCountry_tblAgent
GO

if exists(select * from sysobjects where name  ='FK_tblAgentCountry_tblCountry')
Alter table tblAgentCountry drop CONSTRAINT  FK_tblAgentCountry_tblCountry
GO

if exists(select * from sysobjects where name  ='FK_tblAgentDocument_tblAgent')
Alter table tblAgentDocument drop CONSTRAINT  FK_tblAgentDocument_tblAgent
GO



--Building - Campus 

Truncate table tblBuilding
Go


ALTER TABLE [dbo].[tblBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblBuilding_tblCampus] FOREIGN KEY([CampusID])
REFERENCES [dbo].[tblCampus] ([CampusID])
GO

ALTER TABLE [dbo].[tblBuilding] CHECK CONSTRAINT [FK_tblBuilding_tblCampus]
GO

--- ClassRoom - Building


Truncate table tblClass
Go


ALTER TABLE [dbo].[tblClass]  WITH CHECK ADD  CONSTRAINT [FK_tblClass_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblClass] CHECK CONSTRAINT [FK_tblClass_tblBuilding]
GO

--- Department - School

Truncate table tblDepartment
Go


ALTER TABLE [dbo].[tblDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblDepartment_tblSchool] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[tblSchool] ([SchoolID])
GO

ALTER TABLE [dbo].[tblDepartment] CHECK CONSTRAINT [FK_tblDepartment_tblSchool]
GO


--- Equipment - EquipmentType

Truncate table tblEquipment
Go


ALTER TABLE [dbo].[tblEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblEquipment_tblEquipmentType] FOREIGN KEY([EquipmentTypeID])
REFERENCES [dbo].[tblEquipmentType] ([EquipmentTypeID])
GO

ALTER TABLE [dbo].[tblEquipment] CHECK CONSTRAINT [FK_tblEquipment_tblEquipmentType]
GO


--- Equipment - Classroom




ALTER TABLE [dbo].[tblEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblEquipment_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblEquipment] CHECK CONSTRAINT [FK_tblEquipment_tblClass]
GO





Truncate table tblInstituteSchool
Go


ALTER TABLE [dbo].[tblInstituteSchool]  WITH CHECK ADD  CONSTRAINT [FK_tblInstituteSchool_tblInstitution] FOREIGN KEY([InstituteID])
REFERENCES [dbo].[tblInstitution] ([InstitutionID])
GO

ALTER TABLE [dbo].[tblInstituteSchool] CHECK CONSTRAINT [FK_tblInstituteSchool_tblInstitution]
GO





ALTER TABLE [dbo].[tblInstituteSchool]  WITH CHECK ADD  CONSTRAINT [FK_tblInstituteSchool_tblSchool] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[tblSchool] ([SchoolID])
GO

ALTER TABLE [dbo].[tblInstituteSchool] CHECK CONSTRAINT [FK_tblInstituteSchool_tblSchool]
GO






Truncate table tblSchoolCampus
Go



ALTER TABLE [dbo].[tblSchoolCampus]  WITH CHECK ADD  CONSTRAINT [FK_SchoolCampus_tblCampus] FOREIGN KEY([CampusID])
REFERENCES [dbo].[tblCampus] ([CampusID])
GO

ALTER TABLE [dbo].[tblSchoolCampus] CHECK CONSTRAINT [FK_SchoolCampus_tblCampus]
GO



ALTER TABLE [dbo].[tblSchoolCampus]  WITH CHECK ADD  CONSTRAINT [FK_SchoolCampus_tblSchool] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[tblSchool] ([SchoolID])
GO

ALTER TABLE [dbo].[tblSchoolCampus] CHECK CONSTRAINT [FK_SchoolCampus_tblSchool]
GO







Truncate table tblCampusBuilding
Go




ALTER TABLE [dbo].[tblCampusBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblCampusBuilding_tblCampus] FOREIGN KEY([CampusID])
REFERENCES [dbo].[tblCampus] ([CampusID])
GO

ALTER TABLE [dbo].[tblCampusBuilding] CHECK CONSTRAINT [FK_tblCampusBuilding_tblCampus]
GO

ALTER TABLE [dbo].[tblCampusBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblCampusBuilding_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblCampusBuilding] CHECK CONSTRAINT [FK_tblCampusBuilding_tblBuilding]
GO




Truncate table tblBuildingDepartment
Go



ALTER TABLE [dbo].[tblBuildingDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingDepartment_tblDepartment] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[tblDepartment] ([DepartmentID])
GO

ALTER TABLE [dbo].[tblBuildingDepartment] CHECK CONSTRAINT [FK_tblBuildingDepartment_tblDepartment]
GO

ALTER TABLE [dbo].[tblBuildingDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingDepartment_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblBuildingDepartment] CHECK CONSTRAINT [FK_tblBuildingDepartment_tblBuilding]
GO






Truncate table tblBuildingClass
Go


ALTER TABLE [dbo].[tblBuildingClass]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingClass_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblBuildingClass] CHECK CONSTRAINT [FK_tblBuildingClass_tblClass]
GO

ALTER TABLE [dbo].[tblBuildingClass]  WITH CHECK ADD  CONSTRAINT [FK_tblBuildingClass_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblBuildingClass] CHECK CONSTRAINT [FK_tblBuildingClass_tblBuilding]
GO





Truncate table tblClassEquipment
Go




ALTER TABLE [dbo].[tblClassEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassEquipment_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblClassEquipment] CHECK CONSTRAINT [FK_tblClassEquipment_tblClass]
GO

ALTER TABLE [dbo].[tblClassEquipment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassEquipment_tblEquipment] FOREIGN KEY([EquipmentID])
REFERENCES [dbo].[tblEquipment] ([EquipmentID])
GO

ALTER TABLE [dbo].[tblClassEquipment] CHECK CONSTRAINT [FK_tblClassEquipment_tblEquipment]
GO







Truncate table tblDepartmentBuilding
Go



ALTER TABLE [dbo].[tblDepartmentBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblDepartmentBuilding_tblDepartment] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[tblDepartment] ([DepartmentID])
GO

ALTER TABLE [dbo].[tblDepartmentBuilding] CHECK CONSTRAINT [FK_tblDepartmentBuilding_tblDepartment]
GO

ALTER TABLE [dbo].[tblDepartmentBuilding]  WITH CHECK ADD  CONSTRAINT [FK_tblDepartmentBuilding_tblBuilding] FOREIGN KEY([BuildingID])
REFERENCES [dbo].[tblBuilding] ([BuildingID])
GO

ALTER TABLE [dbo].[tblDepartmentBuilding] CHECK CONSTRAINT [FK_tblDepartmentBuilding_tblBuilding]
GO




Truncate table tblAgentCommission
Go


ALTER TABLE [dbo].[tblAgentCommission]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentCommission_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentCommission] CHECK CONSTRAINT [FK_tblAgentCommission_tblAgent]
GO


Truncate table tblAgentContact
Go




ALTER TABLE [dbo].[tblAgentContact]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentContact_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentContact] CHECK CONSTRAINT [FK_tblAgentContact_tblAgent]
GO






Truncate table tblAgentCountry
Go



ALTER TABLE [dbo].[tblAgentCountry]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentCountry_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentCountry] CHECK CONSTRAINT [FK_tblAgentCountry_tblAgent]
GO





ALTER TABLE [dbo].[tblAgentCountry]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentCountry_tblCountry] FOREIGN KEY([CountryID])
REFERENCES [dbo].[tblCountry] ([CountryID])
GO

ALTER TABLE [dbo].[tblAgentCountry] CHECK CONSTRAINT [FK_tblAgentCountry_tblCountry]
GO



Truncate table tblAgentDocument
Go



ALTER TABLE [dbo].[tblAgentDocument]  WITH CHECK ADD  CONSTRAINT [FK_tblAgentDocument_tblAgent] FOREIGN KEY([AgentID])
REFERENCES [dbo].[tblAgent] ([AgentID])
GO

ALTER TABLE [dbo].[tblAgentDocument] CHECK CONSTRAINT [FK_tblAgentDocument_tblAgent]
GO


if Exists(select * from sysobjects where name = 'SCP_DELETE_BY_AgentID_tblAgent' and Xtype='P')
       Drop procedure SCP_DELETE_BY_AgentID_tblAgent
go

CREATE PROCEDURE [dbo].[SCP_DELETE_BY_AgentID_tblAgent]
(
              @AgentID int
              ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
              BEGIN TRANSACTION
                     
                      DELETE FROM tblAgentCommission
                     WHERE AgentID = @AgentID

                     
                     DELETE FROM tblAgentContact
                     WHERE AgentID = @AgentID
                      
                      DELETE FROM tblAgentCountry
                     WHERE AgentID = @AgentID

                     DELETE FROM tblAgentDocument
                     WHERE AgentID = @AgentID

                     DELETE FROM tblAgent
                     WHERE AgentID = @AgentID
                     IF @@ERROR <> 0 
                      BEGIN 
                                    SET @RETURN_VALUE = 001 
                                    ROLLBACK TRANSACTION 
                      END 
               SET @RETURN_VALUE = 002 
               COMMIT TRANSACTION 
               RETURN 
               END 



if exists(select * from sysobjects where name  ='FK_tblClassDepartment_tblDepartment')
Alter table tblClassDepartment drop CONSTRAINT  FK_tblClassDepartment_tblDepartment
GO

if exists(select * from sysobjects where name  ='FK_tblClassDepartment_tblClass')
Alter table tblClassDepartment drop CONSTRAINT  FK_tblClassDepartment_tblClass
GO
 
Truncate table tblClassDepartment
Go


ALTER TABLE [dbo].[tblClassDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassDepartment_tblDepartment] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[tblDepartment] ([DepartmentID])
GO

ALTER TABLE [dbo].[tblClassDepartment] CHECK CONSTRAINT [FK_tblClassDepartment_tblDepartment]
GO


ALTER TABLE [dbo].[tblClassDepartment]  WITH CHECK ADD  CONSTRAINT [FK_tblClassDepartment_tblClass] FOREIGN KEY([ClassID])
REFERENCES [dbo].[tblClass] ([ClassID])
GO

ALTER TABLE [dbo].[tblClassDepartment] CHECK CONSTRAINT [FK_tblClassDepartment_tblClass]
GO 
 

