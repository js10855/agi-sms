﻿Alter table tblClientAddress
Add ClientID int;
GO

Alter table tblClientAssessments
Add ClientID int;
GO

Alter table tblClientAttendance
Add ClientID int;
GO

Alter table tblClientDocument
Add ClientID int;
GO

Alter table tblClientEnglishProficiency
Add ClientID int;
GO

Alter table tblClientGrade
Add ClientID int;
GO

Alter table tblClientOfferOfPlace
Add ClientID int;
GO

Alter table tblClientPassport
Add ClientID int;
GO

Alter table tblClientPlacement
Add ClientID int;
GO

Alter table tblClientWorkExperience
Add ClientID int;
GO

/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblStaffType]    Script Date: 18-02-2018 00:15:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblStaffType]
(
	 	 @Code varchar(10),
		 @StaffTypeID varchar(10)
	 	 ,@RETURN_VALUE  bit OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 'false'	 	 
	 	Select @cnt = Count(Code) FROM tblStaffType WHERE Code = @Code and StaffTypeID<>@StaffTypeID
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 'false'; 
	 	 ELSE
	 		SET @RETURN_VALUE = 'true';	 	 
	 	 
END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblStaff]    Script Date: 18-02-2018 00:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblStaff]
(
	 	 @Code varchar(10),
		 @StaffID varchar(10)
	 	 ,@RETURN_VALUE  bit OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 'false'	 	 
	 	Select @cnt = Count(Code) FROM tblStaff WHERE Code = @Code and StaffID<>@StaffID
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 'false'; 
	 	 ELSE
	 		SET @RETURN_VALUE = 'true';	 	 
	 	 
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblIntake]    Script Date: 2/18/2018 3:47:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblIntake]
(
	 	 @Code varchar(10)
		 ,@IntakeID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblIntake WHERE Code = @Code and IntakeID <>@IntakeID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblFees]    Script Date: 2/18/2018 4:57:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblFees](
	[FeeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[ProgrammeID] [int] NULL,
	[CountryID] [int] NULL,
	[IsActive] [bit] NULL,
	[TutionFees] [float] NULL,
	[MaterialFees] [float] NULL,
	[Insurance] [float] NULL,
	[RegistrationFees] [float] NULL,
	[AdministrationFees] [float] NULL,
	[OtherFees] [float] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_tblFees] PRIMARY KEY CLUSTERED 
(
	[FeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblFees] ADD  CONSTRAINT [DF_tblFees_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO


IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SCP_CHECK_FOR_UniqueCode_tblFees')
                    AND type IN ( N'P', N'PC' ) ) 
DROP Procedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblFees]
GO

CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblFees]
(
	 	 @Code varchar(10)
		 ,@FeeID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblFees WHERE Code = @Code and FeeID <>@FeeID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblMonth]    Script Date: 2/21/2018 4:55:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblMonth](
	[MonthID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblMonth] PRIMARY KEY CLUSTERED 
(
	[MonthID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblYear]    Script Date: 2/21/2018 4:55:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblYear](
	[YearID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [PK_tblYear] PRIMARY KEY CLUSTERED 
(
	[YearID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblResidence]    Script Date: 2/21/2018 4:55:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblResidence](
	[ResidenceID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblResidence] PRIMARY KEY CLUSTERED 
(
	[ResidenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblResidence] ADD  CONSTRAINT [DF_tblResidence_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblClientType]    Script Date: 2/21/2018 4:55:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblClientType](
	[ClientTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblClientType] PRIMARY KEY CLUSTERED 
(
	[ClientTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblClientType] ADD  CONSTRAINT [DF_tblClientType_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblOfferStatus]    Script Date: 2/21/2018 4:55:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblOfferStatus](
	[OfferStatusID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblOfferStatus] PRIMARY KEY CLUSTERED 
(
	[OfferStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblOfferStatus] ADD  CONSTRAINT [DF_tblOfferStatus_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblEthnicity]    Script Date: 2/21/2018 4:55:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblEthnicity](
	[EthnicityID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblEthnicity] PRIMARY KEY CLUSTERED 
(
	[EthnicityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblEthnicity] ADD  CONSTRAINT [DF_tblEthnicity_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

USE [SMSDB]
GO
SET IDENTITY_INSERT [dbo].[tblClientType] ON 

GO
INSERT [dbo].[tblClientType] ([ClientTypeID], [Code], [Description], [IsActive]) VALUES (1, N'1', N'Prospect', 1)
GO
INSERT [dbo].[tblClientType] ([ClientTypeID], [Code], [Description], [IsActive]) VALUES (2, N'2', N'Student', 1)
GO
INSERT [dbo].[tblClientType] ([ClientTypeID], [Code], [Description], [IsActive]) VALUES (3, N'3', N'Alumani', 1)
GO
SET IDENTITY_INSERT [dbo].[tblClientType] OFF
GO
SET IDENTITY_INSERT [dbo].[tblEthnicity] ON 

GO
INSERT [dbo].[tblEthnicity] ([EthnicityID], [Code], [Description], [IsActive]) VALUES (1, N'1', N'Ethnicity 1', 1)
GO
INSERT [dbo].[tblEthnicity] ([EthnicityID], [Code], [Description], [IsActive]) VALUES (2, N'2', N'Ethnicity 2', 1)
GO
SET IDENTITY_INSERT [dbo].[tblEthnicity] OFF
GO
SET IDENTITY_INSERT [dbo].[tblMonth] ON 

GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (1, N'1', N'1-January')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (2, N'2', N'2-February')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (3, N'3', N'3-March')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (4, N'4', N'4-April')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (5, N'5', N'5-May')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (6, N'6', N'6-June')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (7, N'7', N'7-July')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (8, N'8', N'8-August')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (9, N'9', N'9-September')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (10, N'10', N'10-October')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (11, N'11', N'11-November')
GO
INSERT [dbo].[tblMonth] ([MonthID], [Code], [Description]) VALUES (12, N'12', N'12-December')
GO
SET IDENTITY_INSERT [dbo].[tblMonth] OFF
GO
SET IDENTITY_INSERT [dbo].[tblResidence] ON 

GO
INSERT [dbo].[tblResidence] ([ResidenceID], [Code], [Description], [IsActive]) VALUES (1, N'1', N'Domestic', 1)
GO
INSERT [dbo].[tblResidence] ([ResidenceID], [Code], [Description], [IsActive]) VALUES (2, N'2', N'International', 1)
GO
SET IDENTITY_INSERT [dbo].[tblResidence] OFF
GO
SET IDENTITY_INSERT [dbo].[tblYear] ON 

GO
INSERT [dbo].[tblYear] ([YearID], [Code], [Description]) VALUES (1, N'1', N'2018')
GO
INSERT [dbo].[tblYear] ([YearID], [Code], [Description]) VALUES (2, N'2', N'2019')
GO
INSERT [dbo].[tblYear] ([YearID], [Code], [Description]) VALUES (3, N'3', N'2020')
GO
INSERT [dbo].[tblYear] ([YearID], [Code], [Description]) VALUES (4, N'4', N'2021')
GO
INSERT [dbo].[tblYear] ([YearID], [Code], [Description]) VALUES (5, N'5', N'2022')
GO
SET IDENTITY_INSERT [dbo].[tblYear] OFF
GO

sp_RENAME 'tblClient.EthenicityID', 'EthnicityID' , 'COLUMN'
GO

sp_RENAME 'tblClient.TypeID', 'ClientTypeID' , 'COLUMN'
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblClient]    Script Date: 2/22/2018 4:16:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClient]
(
	 	  @ClientID int
	 	 , @StudentReference varchar(10)
	 	 , @FirstName varchar(50)
	 	 , @MiddleName varchar(50)
	 	 , @LastName varchar(50)
	 	 , @DateOfBirth datetime
	 	 , @Photo varbinary
	 	 , @PassportNumber varchar(20)
	 	 , @ResidenceID int
	 	 , @ClientTypeID int
	 	 , @NSNNumber varchar(50)
	 	 , @WorkNumber varchar(50)
	 	 , @PublicTrustNumber varchar(50)
	 	 , @ExternalRef1 varchar(50)
	 	 , @ExternalRef2 varchar(50)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @EthnicityID int
	 	 , @IwiAffiliation1 varchar(50)
	 	 , @IwiAffiliation2 varchar(50)
	 	 , @IwiAffiliation3 varchar(50)
	 	 , @IwiAffiliation4 varchar(50)
	 	 , @IwiAffiliation5 varchar(50)
	 	 , @AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClient
	 	 SET 
	 	 	 	 	 StudentReference = @StudentReference
	 	 	 	 	 ,FirstName = @FirstName
	 	 	 	 	 ,MiddleName = @MiddleName
	 	 	 	 	 ,LastName = @LastName
	 	 	 	 	 ,DateOfBirth = @DateOfBirth
	 	 	 	 	 ,Photo = @Photo
	 	 	 	 	 ,PassportNumber = @PassportNumber
	 	 	 	 	 ,ResidenceID = @ResidenceID
	 	 	 	 	 ,ClientTypeID = @ClientTypeID
	 	 	 	 	 ,NSNNumber = @NSNNumber
	 	 	 	 	 ,WorkNumber = @WorkNumber
	 	 	 	 	 ,PublicTrustNumber = @PublicTrustNumber
	 	 	 	 	 ,ExternalRef1 = @ExternalRef1
	 	 	 	 	 ,ExternalRef2 = @ExternalRef2
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,EthnicityID = @EthnicityID
	 	 	 	 	 ,IwiAffiliation1 = @IwiAffiliation1
	 	 	 	 	 ,IwiAffiliation2 = @IwiAffiliation2
	 	 	 	 	 ,IwiAffiliation3 = @IwiAffiliation3
	 	 	 	 	 ,IwiAffiliation4 = @IwiAffiliation4
	 	 	 	 	 ,IwiAffiliation5 = @IwiAffiliation5
	 	 	 	 	 ,AgentID = @AgentID
	 	 	 	 	 
	 	 	 	 	 where ClientID = @ClientID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblClient]    Script Date: 2/22/2018 4:17:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClient]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
	 	 FROM tblClient
	 	 WHERE ClientID = @ClientID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClient]    Script Date: 2/22/2018 3:36:57 PM ******/
USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClient]    Script Date: 2/22/2018 4:18:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClient]
(
	 	 	 	 @StudentReference varchar(10)
	 	 	 	 ,@FirstName varchar(50)
	 	 	 	 ,@MiddleName varchar(50)
	 	 	 	 ,@LastName varchar(50)
	 	 	 	 ,@DateOfBirth datetime
	 	 	 	 ,@Photo varbinary
	 	 	 	 ,@PassportNumber varchar(20)
	 	 	 	 ,@ResidenceID int
	 	 	 	 ,@ClientTypeID int
	 	 	 	 ,@NSNNumber varchar(50)
	 	 	 	 ,@WorkNumber varchar(50)
	 	 	 	 ,@PublicTrustNumber varchar(50)
	 	 	 	 ,@ExternalRef1 varchar(50)
	 	 	 	 ,@ExternalRef2 varchar(50)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@EthnicityID int
	 	 	 	 ,@IwiAffiliation1 varchar(50)
	 	 	 	 ,@IwiAffiliation2 varchar(50)
	 	 	 	 ,@IwiAffiliation3 varchar(50)
	 	 	 	 ,@IwiAffiliation4 varchar(50)
	 	 	 	 ,@IwiAffiliation5 varchar(50)
	 	 	 	 ,@AgentID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClient
	 	 	 	 ( 
	 	 	 	 StudentReference
	 	 	 	 ,FirstName
	 	 	 	 ,MiddleName
	 	 	 	 ,LastName
	 	 	 	 ,DateOfBirth
	 	 	 	 ,Photo
	 	 	 	 ,PassportNumber
	 	 	 	 ,ResidenceID
	 	 	 	 ,ClientTypeID
	 	 	 	 ,NSNNumber
	 	 	 	 ,WorkNumber
	 	 	 	 ,PublicTrustNumber
	 	 	 	 ,ExternalRef1
	 	 	 	 ,ExternalRef2
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,EthnicityID
	 	 	 	 ,IwiAffiliation1
	 	 	 	 ,IwiAffiliation2
	 	 	 	 ,IwiAffiliation3
	 	 	 	 ,IwiAffiliation4
	 	 	 	 ,IwiAffiliation5
	 	 	 	 ,AgentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @StudentReference
	 	 	 	 ,@FirstName
	 	 	 	 ,@MiddleName
	 	 	 	 ,@LastName
	 	 	 	 ,@DateOfBirth
	 	 	 	 ,@Photo
	 	 	 	 ,@PassportNumber
	 	 	 	 ,@ResidenceID
	 	 	 	 ,@ClientTypeID
	 	 	 	 ,@NSNNumber
	 	 	 	 ,@WorkNumber
	 	 	 	 ,@PublicTrustNumber
	 	 	 	 ,@ExternalRef1
	 	 	 	 ,@ExternalRef2
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@EthnicityID
	 	 	 	 ,@IwiAffiliation1
	 	 	 	 ,@IwiAffiliation2
	 	 	 	 ,@IwiAffiliation3
	 	 	 	 ,@IwiAffiliation4
	 	 	 	 ,@IwiAffiliation5
	 	 	 	 ,@AgentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblClient]    Script Date: 2/22/2018 3:30:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblClient]    Script Date: 2/22/2018 4:19:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblClient]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientID
,	 	 	 	 StudentReference
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 DateOfBirth
,	 	 	 	 Photo
,	 	 	 	 PassportNumber
,	 	 	 	 ResidenceID
,	 	 	 	 ClientTypeID
,	 	 	 	 NSNNumber
,	 	 	 	 WorkNumber
,	 	 	 	 PublicTrustNumber
,	 	 	 	 ExternalRef1
,	 	 	 	 ExternalRef2
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 EthnicityID
,	 	 	 	 IwiAffiliation1
,	 	 	 	 IwiAffiliation2
,	 	 	 	 IwiAffiliation3
,	 	 	 	 IwiAffiliation4
,	 	 	 	 IwiAffiliation5
,	 	 	 	 AgentID
	 	 FROM tblClient
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueStudentReference_tblClient]    Script Date: 2/22/2018 5:52:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueStudentReference_tblClient]
(
	 	 @StudentReference varchar(10)
		 ,@ClientID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(StudentReference) FROM tblClient WHERE StudentReference = @StudentReference and ClientID <>@ClientID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientPassportID_tblClientPassport]    Script Date: 2/24/2018 6:28:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientPassport]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientPassportID
,	 	 	 	 PassportNumber
,	 	 	 	 PassportIssueDate
,	 	 	 	 PassportExpiryDate
,	 	 	 	 PassportIssueCountryID
,	 	 	 	 VisaNumber
,	 	 	 	 VisaType
,	 	 	 	 VisaIssueDate
,	 	 	 	 VisaExpiryDate
,	 	 	 	 InsuranceProvider
,	 	 	 	 InsuranceNumber
,	 	 	 	 InsuranceExpiryDate
,	 	 	 	 ClientID
	 	 FROM tblClientPassport
	 	 WHERE ClientID = @ClientID
	 	 END 
GO