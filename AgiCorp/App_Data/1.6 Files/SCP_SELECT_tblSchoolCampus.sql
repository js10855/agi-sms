USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_SchoolCampusID_tblSchoolCampus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_SchoolCampusID_tblSchoolCampus]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_SchoolCampusID_tblSchoolCampus]
(
	 	 @SchoolCampusID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 SchoolCampusID
,	 	 	 	 SchoolID
,	 	 	 	 CampusID
	 	 FROM tblSchoolCampus
	 	 WHERE SchoolCampusID = @SchoolCampusID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblSchoolCampus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblSchoolCampus]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblSchoolCampus]
AS
BEGIN
	 	 SELECT 
	 	 	 	 SchoolCampusID
,	 	 	 	 SchoolID
,	 	 	 	 CampusID
	 	 FROM tblSchoolCampus
	 	 END 
GO 
