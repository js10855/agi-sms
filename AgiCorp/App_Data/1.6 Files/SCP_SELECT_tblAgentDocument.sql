USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_AgentDocumentID_tblAgentDocument]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_AgentDocumentID_tblAgentDocument]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AgentDocumentID_tblAgentDocument]
(
	 	 @AgentDocumentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
	 	 FROM tblAgentDocument
	 	 WHERE AgentDocumentID = @AgentDocumentID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblAgentDocument]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblAgentDocument]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblAgentDocument]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentDocumentID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 [File]
	 	 FROM tblAgentDocument
	 	 END 
GO 
