USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ClassDepartmentID_tblClassDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ClassDepartmentID_tblClassDepartment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClassDepartmentID_tblClassDepartment]
(
	 	 @ClassDepartmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClassDepartmentID
,	 	 	 	 ClassID
,	 	 	 	 DepartmentID
	 	 FROM tblClassDepartment
	 	 WHERE ClassDepartmentID = @ClassDepartmentID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblClassDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblClassDepartment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblClassDepartment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClassDepartmentID
,	 	 	 	 ClassID
,	 	 	 	 DepartmentID
	 	 FROM tblClassDepartment
	 	 END 
GO 
