USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_BuildingClassID_tblBuildingClass]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_BuildingClassID_tblBuildingClass]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_BuildingClassID_tblBuildingClass]
(
	 	 @BuildingClassID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 BuildingClassID
,	 	 	 	 BuildingID
,	 	 	 	 ClassID
	 	 FROM tblBuildingClass
	 	 WHERE BuildingClassID = @BuildingClassID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblBuildingClass]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblBuildingClass]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblBuildingClass]
AS
BEGIN
	 	 SELECT 
	 	 	 	 BuildingClassID
,	 	 	 	 BuildingID
,	 	 	 	 ClassID
	 	 FROM tblBuildingClass
	 	 END 
GO 
