USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_CampusBuildingID_tblCampusBuilding]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_CampusBuildingID_tblCampusBuilding]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_CampusBuildingID_tblCampusBuilding]
(
	 	 @CampusBuildingID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 CampusBuildingID
,	 	 	 	 CampusID
,	 	 	 	 BuildingID
	 	 FROM tblCampusBuilding
	 	 WHERE CampusBuildingID = @CampusBuildingID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblCampusBuilding]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblCampusBuilding]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblCampusBuilding]
AS
BEGIN
	 	 SELECT 
	 	 	 	 CampusBuildingID
,	 	 	 	 CampusID
,	 	 	 	 BuildingID
	 	 FROM tblCampusBuilding
	 	 END 
GO 
