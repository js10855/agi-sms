USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_SET_tblDepartmentBuilding]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_SET_tblDepartmentBuilding]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_SET_tblDepartmentBuilding]
(
	 	 	 	 @DepartmentID int
	 	 	 	 ,@BuildingID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblDepartmentBuilding
	 	 	 	 ( 
	 	 	 	 DepartmentID
	 	 	 	 ,BuildingID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @DepartmentID
	 	 	 	 ,@BuildingID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
