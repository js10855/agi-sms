USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_BuildingDepartmentID_tblBuildingDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_BuildingDepartmentID_tblBuildingDepartment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_BuildingDepartmentID_tblBuildingDepartment]
(
	 	 @BuildingDepartmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 BuildingDepartmentID
,	 	 	 	 BuildingID
,	 	 	 	 DepartmentID
	 	 FROM tblBuildingDepartment
	 	 WHERE BuildingDepartmentID = @BuildingDepartmentID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblBuildingDepartment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblBuildingDepartment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblBuildingDepartment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 BuildingDepartmentID
,	 	 	 	 BuildingID
,	 	 	 	 DepartmentID
	 	 FROM tblBuildingDepartment
	 	 END 
GO 
