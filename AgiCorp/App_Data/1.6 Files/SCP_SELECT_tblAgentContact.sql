USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_AgentContactID_tblAgentContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_AgentContactID_tblAgentContact]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_AgentContactID_tblAgentContact]
(
	 	 @AgentContactID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentContactID
,	 	 	 	 AgentID
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 MobileCountryCode
,	 	 	 	 Mobile
,	 	 	 	 IsPrimaryContact
,	 	 	 	 IsActive
	 	 FROM tblAgentContact
	 	 WHERE AgentContactID = @AgentContactID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblAgentContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblAgentContact]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblAgentContact]
AS
BEGIN
	 	 SELECT 
	 	 	 	 AgentContactID
,	 	 	 	 AgentID
,	 	 	 	 FirstName
,	 	 	 	 MiddleName
,	 	 	 	 LastName
,	 	 	 	 Email
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 MobileCountryCode
,	 	 	 	 Mobile
,	 	 	 	 IsPrimaryContact
,	 	 	 	 IsActive
	 	 FROM tblAgentContact
	 	 END 
GO 
