USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_ClassEquipmentID_tblClassEquipment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_ClassEquipmentID_tblClassEquipment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_ClassEquipmentID_tblClassEquipment]
(
	 	 @ClassEquipmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClassEquipmentID
,	 	 	 	 ClassID
,	 	 	 	 EquipmentID
	 	 FROM tblClassEquipment
	 	 WHERE ClassEquipmentID = @ClassEquipmentID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblClassEquipment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblClassEquipment]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblClassEquipment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClassEquipmentID
,	 	 	 	 ClassID
,	 	 	 	 EquipmentID
	 	 FROM tblClassEquipment
	 	 END 
GO 
