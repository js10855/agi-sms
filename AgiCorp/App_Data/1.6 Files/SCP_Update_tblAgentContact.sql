USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_UPDATE_tblAgentContact]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_UPDATE_tblAgentContact]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblAgentContact]
(
	 	  @AgentContactID int
	 	 , @AgentID int
	 	 , @FirstName varchar(50)
	 	 , @MiddleName varchar(50)
	 	 , @LastName varchar(50)
	 	 , @Email varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @MobileCountryCode varchar(3)
	 	 , @Mobile varchar(10)
	 	 , @IsPrimaryContact bit
	 	 , @IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblAgentContact
	 	 SET 
	 	 	 	 	 AgentID = @AgentID
	 	 	 	 	 ,FirstName = @FirstName
	 	 	 	 	 ,MiddleName = @MiddleName
	 	 	 	 	 ,LastName = @LastName
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,MobileCountryCode = @MobileCountryCode
	 	 	 	 	 ,Mobile = @Mobile
	 	 	 	 	 ,IsPrimaryContact = @IsPrimaryContact
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 
	 	 	 	 	 where AgentContactID = @AgentContactID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
