USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_DepartmentBuildingID_tblDepartmentBuilding]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_DepartmentBuildingID_tblDepartmentBuilding]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_DepartmentBuildingID_tblDepartmentBuilding]
(
	 	 @DepartmentBuildingID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 DepartmentBuildingID
,	 	 	 	 DepartmentID
,	 	 	 	 BuildingID
	 	 FROM tblDepartmentBuilding
	 	 WHERE DepartmentBuildingID = @DepartmentBuildingID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblDepartmentBuilding]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblDepartmentBuilding]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblDepartmentBuilding]
AS
BEGIN
	 	 SELECT 
	 	 	 	 DepartmentBuildingID
,	 	 	 	 DepartmentID
,	 	 	 	 BuildingID
	 	 FROM tblDepartmentBuilding
	 	 END 
GO 
