﻿Alter table tblEquipment add Quantity int;
GO
Alter table tblFees add TotalFees float;
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblEquipment]    Script Date: 4/18/2018 7:06:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblEquipment]
(
	 	  @EquipmentID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
		 ,@ClassID int
		  ,@Quantity int
		  ,@EquipmentTypeID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblEquipment
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,ClassID=@ClassID
					 ,Quantity=@Quantity
					 ,EquipmentTypeID=@EquipmentTypeID
	 	 	 	 	 where EquipmentID = @EquipmentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblEquipment]    Script Date: 4/18/2018 7:05:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblEquipment]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
				 ,@ClassID int
				 ,@Quantity int
				  ,@EquipmentTypeID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblEquipment
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
				 ,ClassID
				 ,Quantity
				 ,EquipmentTypeID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
				 ,@ClassID
				 ,@Quantity
				 ,@EquipmentTypeID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_EquipmentID_tblEquipment]    Script Date: 4/18/2018 7:05:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_EquipmentID_tblEquipment]
(
	 	 @EquipmentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 EquipmentID
,	 	 	 	 Code
,	 	 	 	 Description
,				 ClassID
,				 Quantity
,				 EquipmentTypeID
	 	 FROM tblEquipment
	 	 WHERE EquipmentID = @EquipmentID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblEquipment]    Script Date: 4/18/2018 7:05:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblEquipment]
AS
BEGIN
	 	 SELECT 
	 	 	 	 EquipmentID
,	 	 	 	 Code
,	 	 	 	 Description
,				 ClassID
,				 Quantity
,				 EquipmentTypeID
	 	 FROM tblEquipment
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblFees]    Script Date: 4/18/2018 7:23:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblFees]
(
	 	  @FeeID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @ProgrammeID int
	 	 , @CountryID int
	 	 , @IsActive bit
	 	 , @TutionFees float
	 	 , @MaterialFees float
	 	 , @Insurance float
	 	 , @RegistrationFees float
	 	 , @AdministrationFees float
	 	 , @OtherFees float
		 ,@TotalFees float
	 	 , @StartDate datetime
	 	 , @EndDate datetime
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblFees
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,ProgrammeID = @ProgrammeID
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 ,TutionFees = @TutionFees
	 	 	 	 	 ,MaterialFees = @MaterialFees
	 	 	 	 	 ,Insurance = @Insurance
	 	 	 	 	 ,RegistrationFees = @RegistrationFees
	 	 	 	 	 ,AdministrationFees = @AdministrationFees
	 	 	 	 	 ,OtherFees = @OtherFees
					 ,TotalFees = @TotalFees
	 	 	 	 	 ,StartDate = @StartDate
	 	 	 	 	 ,EndDate = @EndDate
	 	 	 	 	 
	 	 	 	 	 where FeeID = @FeeID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblFees]    Script Date: 4/18/2018 7:23:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblFees]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@ProgrammeID int
	 	 	 	 ,@CountryID int
	 	 	 	 ,@IsActive bit
	 	 	 	 ,@TutionFees float
	 	 	 	 ,@MaterialFees float
	 	 	 	 ,@Insurance float
	 	 	 	 ,@RegistrationFees float
	 	 	 	 ,@AdministrationFees float
	 	 	 	 ,@OtherFees float
				 ,@TotalFees float
	 	 	 	 ,@StartDate datetime
	 	 	 	 ,@EndDate datetime
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblFees
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,ProgrammeID
	 	 	 	 ,CountryID
	 	 	 	 ,IsActive
	 	 	 	 ,TutionFees
	 	 	 	 ,MaterialFees
	 	 	 	 ,Insurance
	 	 	 	 ,RegistrationFees
	 	 	 	 ,AdministrationFees
	 	 	 	 ,OtherFees
				 ,TotalFees
	 	 	 	 ,StartDate
	 	 	 	 ,EndDate
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@ProgrammeID
	 	 	 	 ,@CountryID
	 	 	 	 ,@IsActive
	 	 	 	 ,@TutionFees
	 	 	 	 ,@MaterialFees
	 	 	 	 ,@Insurance
	 	 	 	 ,@RegistrationFees
	 	 	 	 ,@AdministrationFees
	 	 	 	 ,@OtherFees
				 ,@TotalFees
	 	 	 	 ,@StartDate
	 	 	 	 ,@EndDate

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblFees]    Script Date: 4/18/2018 7:22:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblFees]
AS
BEGIN
	 	 SELECT 
	 	 	 	 FeeID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 ProgrammeID
,	 	 	 	 CountryID
,	 	 	 	 IsActive
,	 	 	 	 TutionFees
,	 	 	 	 MaterialFees
,	 	 	 	 Insurance
,	 	 	 	 RegistrationFees
,	 	 	 	 AdministrationFees
,	 	 	 	 OtherFees
,				 TotalFees
,	 	 	 	 StartDate
,	 	 	 	 EndDate
	 	 FROM tblFees
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_FeeID_tblFees]    Script Date: 4/18/2018 7:22:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_FeeID_tblFees]
(
	 	 @FeeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 FeeID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 ProgrammeID
,	 	 	 	 CountryID
,	 	 	 	 IsActive
,	 	 	 	 TutionFees
,	 	 	 	 MaterialFees
,	 	 	 	 Insurance
,	 	 	 	 RegistrationFees
,	 	 	 	 AdministrationFees
,	 	 	 	 OtherFees
,				 TotalFees
,	 	 	 	 StartDate
,	 	 	 	 EndDate
	 	 FROM tblFees
	 	 WHERE FeeID = @FeeID
	 	 END 
GO