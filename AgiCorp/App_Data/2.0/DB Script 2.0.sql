﻿sp_RENAME 'tblClientDocument.[FileSize]' , 'File', 'COLUMN';
GO

sp_RENAME 'tblClientDocument.[FileType]' , 'DocsFileName', 'COLUMN';
GO

sp_RENAME 'tblClientPlacement.[FacilityName]' , 'PlacementID', 'COLUMN';
GO

INSERT [dbo].[tblClientType] ( [Code], [Description], [IsActive]) VALUES ( N'4', N'Withdrawn', 1);
GO

/****** Object:  Table [dbo].[tblPlacement]    Script Date: 4/4/2018 4:25:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblPlacement](
	[PlacementID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[Address3] [varchar](100) NULL,
	[PostCode] [varchar](10) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[CountryID] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[Website] [varchar](100) NULL,
	[PhoneCountryCode] [varchar](3) NULL,
	[PhoneStateCode] [varchar](2) NULL,
	[Phone] [varchar](10) NULL,
	[IsActive] [bit] NULL,
	DepartmentID [varchar] (100) NULL
 CONSTRAINT [PK_tblPlacement] PRIMARY KEY CLUSTERED 
(
	[PlacementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblPlacement] ADD  CONSTRAINT [DF_tblPlacement_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblClientDocument]    Script Date: 4/4/2018 6:13:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientDocument]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientDocumentID
		,Code
		,DocumentDescription
		,DocsFileName
		,[File]
		,ClientID
	 	 FROM tblClientDocument
	 	 WHERE ClientID = @ClientID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClientDocument]    Script Date: 4/4/2018 6:20:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClientDocument]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@DocumentDescription varchar(500)
	 	 	 	 ,@DocsFileName varchar(50)
	 	 	 	 ,@File varbinary(max)
	 	 	 	 ,@ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClientDocument
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,DocumentDescription
	 	 	 	 ,DocsFileName 
	 	 	 	 ,[File]
	 	 	 	 ,ClientID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@DocumentDescription
	 	 	 	 ,@DocsFileName 
	 	 	 	 ,@File
	 	 	 	 ,@ClientID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblClientDocument]    Script Date: 4/4/2018 6:20:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClientDocument]
(
	 	  @ClientDocumentID int
	 	 , @Code varchar(10)
	 	 , @DocumentDescription varchar(500)
	 	 , @DocsFileName  varchar(50)
	 	 , @File varbinary(max)
	 	 , @ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClientDocument
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,DocumentDescription = @DocumentDescription
	 	 	 	 	 ,DocsFileName = @DocsFileName
	 	 	 	 	 ,[File] = @File
	 	 	 	 	 ,ClientID = @ClientID
	 	 	 	 	 
	 	 	 	 	 where ClientDocumentID = @ClientDocumentID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblClientDocument]    Script Date: 4/4/2018 6:08:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblClientDocument]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientDocumentID
,	 	 	 	 Code
,	 	 	 	 DocumentDescription
,	 	 	 	 DocsFileName
,	 	 	 	 [File]
,	 	 	 	 ClientID
	 	 FROM tblClientDocument
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientDocumentID_tblClientDocument]    Script Date: 4/4/2018 6:13:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientDocumentID_tblClientDocument]
(
	 	 @ClientDocumentID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientDocumentID
,	 	 	 	 Code
,	 	 	 	 DocumentDescription
,	 	 	 	 DocsFileName
,	 	 	 	 [File]
,	 	 	 	 ClientID
	 	 FROM tblClientDocument
	 	 WHERE ClientDocumentID = @ClientDocumentID
	 	 END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientPlacementID_tblClientPlacement]    Script Date: 4/4/2018 7:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientPlacementID_tblClientPlacement]
(
	 	 @ClientPlacementID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientPlacementID
,	 	 	 	 Code
,	 	 	 	 PlacementID
,	 	 	 	 CurrentlyPlaced
,	 	 	 	 FromDate
,	 	 	 	 ToDate
,	 	 	 	 ClientID
	 	 FROM tblClientPlacement
	 	 WHERE ClientPlacementID = @ClientPlacementID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ClientID_tblClientPlacement]    Script Date: 4/4/2018 7:01:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_BY_ClientID_tblClientPlacement]
(
	 	 @ClientID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientPlacementID
		,Code
		,PlacementID
		,CurrentlyPlaced
		,FromDate
		,ToDate
		,ClientID
	 	 FROM tblClientPlacement
	 	 WHERE ClientID = @ClientID
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblClientPlacement]    Script Date: 4/4/2018 6:58:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblClientPlacement]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ClientPlacementID
,	 	 	 	 Code
,	 	 	 	 PlacementID
,	 	 	 	 CurrentlyPlaced
,	 	 	 	 FromDate
,	 	 	 	 ToDate
,	 	 	 	 ClientID
	 	 FROM tblClientPlacement
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClientPlacement]    Script Date: 4/4/2018 7:03:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClientPlacement]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@PlacementID varchar(50)
	 	 	 	 ,@CurrentlyPlaced bit
	 	 	 	 ,@FromDate datetime
	 	 	 	 ,@ToDate datetime
	 	 	 	 ,@ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClientPlacement
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,PlacementID
	 	 	 	 ,CurrentlyPlaced
	 	 	 	 ,FromDate
	 	 	 	 ,ToDate
	 	 	 	 ,ClientID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@PlacementID
	 	 	 	 ,@CurrentlyPlaced
	 	 	 	 ,@FromDate
	 	 	 	 ,@ToDate
	 	 	 	 ,@ClientID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblClientPlacement]    Script Date: 4/4/2018 7:03:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblClientPlacement]
(
	 	  @ClientPlacementID int
	 	 , @Code varchar(10)
	 	 , @PlacementID varchar(50)
	 	 , @CurrentlyPlaced bit
	 	 , @FromDate datetime
	 	 , @ToDate datetime
	 	 , @ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblClientPlacement
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,PlacementID = @PlacementID
	 	 	 	 	 ,CurrentlyPlaced = @CurrentlyPlaced
	 	 	 	 	 ,FromDate = @FromDate
	 	 	 	 	 ,ToDate = @ToDate
	 	 	 	 	 ,ClientID = @ClientID
	 	 	 	 	 
	 	 	 	 	 where ClientPlacementID = @ClientPlacementID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblPlacement]    Script Date: 4/4/2018 10:58:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblPlacement]
(
	 	 @Code varchar(10)
		 ,@PlacementID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblPlacement WHERE Code = @Code and PlacementID <>@PlacementID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_SET_tblClientPlacement]    Script Date: 4/4/2018 11:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblClientPlacement]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@PlacementID varchar(50)
	 	 	 	 ,@CurrentlyPlaced bit
	 	 	 	 ,@FromDate datetime
	 	 	 	 ,@ToDate datetime
	 	 	 	 ,@ClientID int
	 	 ,@RETURN_VALUE  INT OUTPUT
		  ,@ERROR_MSG  varchar(200) OUTPUT
)
AS
BEGIN
Declare @resCount int =0;
SET @Error_Msg = '';
SET @RETURN_VALUE = 0

Select @resCount = count(ClientPlacementID) from tblClientPlacement where
		ClientID=@ClientID and PlacementID=@PlacementID

		If (@resCount >0)
		BEGIN
			set @ERROR_MSG = 'Duplicate record!';
			RETURN
		END

	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblClientPlacement
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,PlacementID
	 	 	 	 ,CurrentlyPlaced
	 	 	 	 ,FromDate
	 	 	 	 ,ToDate
	 	 	 	 ,ClientID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@PlacementID
	 	 	 	 ,@CurrentlyPlaced
	 	 	 	 ,@FromDate
	 	 	 	 ,@ToDate
	 	 	 	 ,@ClientID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_PlacementID_tblClientPlacement]    Script Date: 4/4/2018 11:20:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_BY_PlacementID_tblClientPlacement]
(
	 	 @PlacementID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientPlacementID
		,Code
		,PlacementID
		,CurrentlyPlaced
		,FromDate
		,ToDate
		,ClientID
	 	 FROM tblClientPlacement
	 	 WHERE PlacementID = @PlacementID
	 	 END 
GO
	
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_PlacementID_tblClientPlacement]    Script Date: 4/5/2018 3:00:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_BY_PlacementID_tblClientPlacement]
(
	 	 @PlacementID int
)
AS
BEGIN
	 	 SELECT 
 	 	ClientPlacementID
		,tblClientPlacement.Code
		,tblClientPlacement.PlacementID
		,CurrentlyPlaced
		,FromDate
		,ToDate
		,tblClientPlacement.ClientID
		,tblClient.FirstName + ' ' + IsNull(tblClient.LastName,'') as Name
		,tblPlacement.Description
	 	 FROM tblClientPlacement
		 left join tblClient on tblClientPlacement.ClientID= tblClient.ClientID
		 left join tblPlacement on tblClientPlacement.PlacementID= tblPlacement.PlacementID
	 	 WHERE tblClientPlacement.PlacementID = @PlacementID
	 	 END 
GO
	

	

	
