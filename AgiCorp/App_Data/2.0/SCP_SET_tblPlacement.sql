USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_SET_tblPlacement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_SET_tblPlacement]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_SET_tblPlacement]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Address1 varchar(100)
	 	 	 	 ,@Address2 varchar(100)
	 	 	 	 ,@Address3 varchar(100)
	 	 	 	 ,@PostCode varchar(10)
	 	 	 	 ,@City varchar(100)
	 	 	 	 ,@State varchar(100)
	 	 	 	 ,@CountryID varchar(100)
	 	 	 	 ,@Email varchar(100)
	 	 	 	 ,@Website varchar(100)
	 	 	 	 ,@PhoneCountryCode varchar(3)
	 	 	 	 ,@PhoneStateCode varchar(2)
	 	 	 	 ,@Phone varchar(10)
	 	 	 	 ,@IsActive bit
	 	 	 	 ,@DepartmentID varchar(100)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblPlacement
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,Address1
	 	 	 	 ,Address2
	 	 	 	 ,Address3
	 	 	 	 ,PostCode
	 	 	 	 ,City
	 	 	 	 ,State
	 	 	 	 ,CountryID
	 	 	 	 ,Email
	 	 	 	 ,Website
	 	 	 	 ,PhoneCountryCode
	 	 	 	 ,PhoneStateCode
	 	 	 	 ,Phone
	 	 	 	 ,IsActive
	 	 	 	 ,DepartmentID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@Address1
	 	 	 	 ,@Address2
	 	 	 	 ,@Address3
	 	 	 	 ,@PostCode
	 	 	 	 ,@City
	 	 	 	 ,@State
	 	 	 	 ,@CountryID
	 	 	 	 ,@Email
	 	 	 	 ,@Website
	 	 	 	 ,@PhoneCountryCode
	 	 	 	 ,@PhoneStateCode
	 	 	 	 ,@Phone
	 	 	 	 ,@IsActive
	 	 	 	 ,@DepartmentID

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
