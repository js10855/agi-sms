USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_BY_PlacementID_tblPlacement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_BY_PlacementID_tblPlacement]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_GET_BY_PlacementID_tblPlacement]
(
	 	 @PlacementID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 PlacementID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 Website
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 IsActive
,	 	 	 	 DepartmentID
	 	 FROM tblPlacement
	 	 WHERE PlacementID = @PlacementID
	 	 END 
GO 
USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_GET_ALL_tblPlacement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_GET_ALL_tblPlacement]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_GET_ALL_tblPlacement]
AS
BEGIN
	 	 SELECT 
	 	 	 	 PlacementID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Address1
,	 	 	 	 Address2
,	 	 	 	 Address3
,	 	 	 	 PostCode
,	 	 	 	 City
,	 	 	 	 State
,	 	 	 	 CountryID
,	 	 	 	 Email
,	 	 	 	 Website
,	 	 	 	 PhoneCountryCode
,	 	 	 	 PhoneStateCode
,	 	 	 	 Phone
,	 	 	 	 IsActive
,	 	 	 	 DepartmentID
	 	 FROM tblPlacement
	 	 END 
GO 
