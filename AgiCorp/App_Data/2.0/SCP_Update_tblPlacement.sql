USE [SMSDB]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SCP_UPDATE_tblPlacement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SCP_UPDATE_tblPlacement]
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_UPDATE_tblPlacement]
(
	 	  @PlacementID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @Address1 varchar(100)
	 	 , @Address2 varchar(100)
	 	 , @Address3 varchar(100)
	 	 , @PostCode varchar(10)
	 	 , @City varchar(100)
	 	 , @State varchar(100)
	 	 , @CountryID varchar(100)
	 	 , @Email varchar(100)
	 	 , @Website varchar(100)
	 	 , @PhoneCountryCode varchar(3)
	 	 , @PhoneStateCode varchar(2)
	 	 , @Phone varchar(10)
	 	 , @IsActive bit
	 	 , @DepartmentID varchar(100)
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblPlacement
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,Address1 = @Address1
	 	 	 	 	 ,Address2 = @Address2
	 	 	 	 	 ,Address3 = @Address3
	 	 	 	 	 ,PostCode = @PostCode
	 	 	 	 	 ,City = @City
	 	 	 	 	 ,State = @State
	 	 	 	 	 ,CountryID = @CountryID
	 	 	 	 	 ,Email = @Email
	 	 	 	 	 ,Website = @Website
	 	 	 	 	 ,PhoneCountryCode = @PhoneCountryCode
	 	 	 	 	 ,PhoneStateCode = @PhoneStateCode
	 	 	 	 	 ,Phone = @Phone
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 ,DepartmentID = @DepartmentID
	 	 	 	 	 
	 	 	 	 	 where PlacementID = @PlacementID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO 
