﻿Alter table tblQualification Add
HoursPerCredit int,
TotalHours int;
GO

Alter table tblProgramme Add
ClassF2F int,
PlacementF2F int;
GO

Alter table tblModule Add
ClassF2F int,
PlacementF2F int;
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblQualification]    Script Date: 2/14/2018 5:53:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblQualification]
(
	 	  @QualificationID int
	 	 , @Code varchar(10)
	 	 , @NZQACode int
	 	 , @Description varchar(200)
	 	 , @Credits int
		 ,@HoursPerCredit int	
         ,@TotalHours int
	 	 , @F2FHours int
	 	 , @SDHours int
	 	 , @IsActive bit
		  ,@QualificationTypeID int
          ,@QualificationLevelID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblQualification
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,NZQACode = @NZQACode
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,Credits = @Credits
					  ,HoursPerCredit=@HoursPerCredit
                      ,TotalHours=@TotalHours
	 	 	 	 	 ,F2FHours = @F2FHours
	 	 	 	 	 ,SDHours = @SDHours
	 	 	 	 	 ,IsActive = @IsActive
					 ,QualificationTypeID=@QualificationTypeID
                     ,QualificationLevelID=@QualificationLevelID
	 	 	 	 	 
	 	 	 	 	 where QualificationID = @QualificationID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblQualification]    Script Date: 2/14/2018 5:49:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblQualification]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@NZQACode int
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Credits int
				 ,@HoursPerCredit int	
                 ,@TotalHours int
	 	 	 	 ,@F2FHours int
	 	 	 	 ,@SDHours int
	 	 	 	 ,@IsActive bit
				 ,@QualificationTypeID int
                 ,@QualificationLevelID int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblQualification
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,NZQACode
	 	 	 	 ,Description
	 	 	 	 ,Credits
				 ,HoursPerCredit	
                 ,TotalHours
	 	 	 	 ,F2FHours
	 	 	 	 ,SDHours
	 	 	 	 ,IsActive
				 ,QualificationTypeID
                 ,QualificationLevelID
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@NZQACode
	 	 	 	 ,@Description
	 	 	 	 ,@Credits
				 ,@HoursPerCredit	
                 ,@TotalHours
	 	 	 	 ,@F2FHours
	 	 	 	 ,@SDHours
	 	 	 	 ,@IsActive
				 ,@QualificationTypeID
                 ,@QualificationLevelID 

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblQualification]    Script Date: 2/14/2018 5:48:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblQualification]
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,				 HoursPerCredit	
,				 TotalHours
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,	 	 	 	 IsActive
,                QualificationTypeID
,				 QualificationLevelID
	 	 FROM tblQualification
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_QualificationID_tblQualification]    Script Date: 2/14/2018 5:49:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_QualificationID_tblQualification]
(
	 	 @QualificationID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 QualificationID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,				 HoursPerCredit	
,				 TotalHours
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,	 	 	 	 IsActive
,                QualificationTypeID
,				 QualificationLevelID
	 	 FROM tblQualification
	 	 WHERE QualificationID = @QualificationID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ModuleID_tblModule]    Script Date: 2/14/2018 7:49:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ModuleID_tblModule]
(
	 	 @ModuleID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 HoursPerCredit
,	 	 	 	 TotalHoursPerModule
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F 
,	 	 	 	 MinPassPercentage
,	 	 	 	 IsActive
	 	 FROM tblModule
	 	 WHERE ModuleID = @ModuleID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblModule]    Script Date: 2/14/2018 7:49:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblModule]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ModuleID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 HoursPerCredit
,	 	 	 	 TotalHoursPerModule
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F 
,	 	 	 	 MinPassPercentage
,	 	 	 	 IsActive
	 	 FROM tblModule
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_ProgrammeID_tblProgramme]    Script Date: 2/14/2018 7:48:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_ProgrammeID_tblProgramme]
(
	 	 @ProgrammeID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F 
	 	 FROM tblProgramme
	 	 WHERE ProgrammeID = @ProgrammeID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblProgramme]    Script Date: 2/14/2018 7:47:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblProgramme]
AS
BEGIN
	 	 SELECT 
	 	 	 	 ProgrammeID
,	 	 	 	 Code
,	 	 	 	 NZQACode
,	 	 	 	 Description
,	 	 	 	 Credits
,	 	 	 	 F2FHours
,	 	 	 	 SDHours
,				 ClassF2F
,				 PlacementF2F 
	 	 FROM tblProgramme
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblModule]    Script Date: 2/14/2018 7:52:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblModule]
(
	 	  @ModuleID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @Credits int
	 	 , @HoursPerCredit int
	 	 , @TotalHoursPerModule int
	 	 , @F2FHours int
	 	 , @SDHours int
		 ,@ClassF2F int
         ,@PlacementF2F int
	 	 , @MinPassPercentage int
	 	 , @IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblModule
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,Credits = @Credits
	 	 	 	 	 ,HoursPerCredit = @HoursPerCredit
	 	 	 	 	 ,TotalHoursPerModule = @TotalHoursPerModule
	 	 	 	 	 ,F2FHours = @F2FHours
	 	 	 	 	 ,SDHours = @SDHours
					 ,ClassF2F=@ClassF2F
					  ,PlacementF2F=@PlacementF2F
	 	 	 	 	 ,MinPassPercentage = @MinPassPercentage
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 
	 	 	 	 	 where ModuleID = @ModuleID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblProgramme]    Script Date: 2/14/2018 7:48:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblProgramme]
(
	 	  @ProgrammeID int
	 	 , @Code varchar(10)
	 	 , @NZQACode int
	 	 , @Description varchar(200)
	 	 , @Credits int
	 	 , @F2FHours int
	 	 , @SDHours int
		 ,@ClassF2F int
         ,@PlacementF2F int
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblProgramme
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,NZQACode = @NZQACode
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,Credits = @Credits
	 	 	 	 	 ,F2FHours = @F2FHours
	 	 	 	 	 ,SDHours = @SDHours
	 	 	 	 	  ,ClassF2F=@ClassF2F
					  ,PlacementF2F=@PlacementF2F
	 	 	 	 	 where ProgrammeID = @ProgrammeID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblProgramme]    Script Date: 2/14/2018 7:48:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblProgramme]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@NZQACode int
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Credits int
	 	 	 	 ,@F2FHours int
	 	 	 	 ,@SDHours int
				 ,@ClassF2F int
				 ,@PlacementF2F int
	 	         ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblProgramme
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,NZQACode
	 	 	 	 ,Description
	 	 	 	 ,Credits
	 	 	 	 ,F2FHours
	 	 	 	 ,SDHours
				 ,ClassF2F
				 ,PlacementF2F
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@NZQACode
	 	 	 	 ,@Description
	 	 	 	 ,@Credits
	 	 	 	 ,@F2FHours
	 	 	 	 ,@SDHours
				 ,@ClassF2F
				 ,@PlacementF2F

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblModule]    Script Date: 2/14/2018 7:55:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblModule]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Credits int
	 	 	 	 ,@HoursPerCredit int
	 	 	 	 ,@TotalHoursPerModule int
	 	 	 	 ,@F2FHours int
	 	 	 	 ,@SDHours int
				  ,@ClassF2F int
				 ,@PlacementF2F int
	 	 	 	 ,@MinPassPercentage int
	 	 	 	 ,@IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblModule
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,Credits
	 	 	 	 ,HoursPerCredit
	 	 	 	 ,TotalHoursPerModule
	 	 	 	 ,F2FHours
	 	 	 	 ,SDHours
				  ,ClassF2F
				 ,PlacementF2F
	 	 	 	 ,MinPassPercentage
	 	 	 	 ,IsActive
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@Credits
	 	 	 	 ,@HoursPerCredit
	 	 	 	 ,@TotalHoursPerModule
	 	 	 	 ,@F2FHours
	 	 	 	 ,@SDHours
				  ,@ClassF2F
				 ,@PlacementF2F
	 	 	 	 ,@MinPassPercentage
	 	 	 	 ,@IsActive

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblQualification]    Script Date: 2/15/2018 1:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblQualification]
(
	 	 @Code varchar(10)
		 ,@QualificationID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblQualification WHERE Code = @Code and QualificationID <>@QualificationID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblProgramme]    Script Date: 2/15/2018 1:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblProgramme]
(
	 	 @Code varchar(10)
		 ,@ProgrammeID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblProgramme WHERE Code = @Code and ProgrammeID <>@ProgrammeID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblModule]    Script Date: 2/15/2018 1:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblModule]
(
	 	 @Code varchar(10)
		 ,@ModuleID varchar(10)
	 	 ,@RETURN_VALUE  int OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 0	 	 
	 	Select @cnt = Count(Code) FROM tblModule WHERE Code = @Code and ModuleID <>@ModuleID		
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 0; 
	 	 ELSE
	 		SET @RETURN_VALUE = 1;	 	 
	 	 
END 
GO


/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblGPO]    Script Date: 16-02-2018 10:45:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblGPO]
(
	 	 @Code varchar(10),
		 @GPOID varchar(10)
	 	 ,@RETURN_VALUE  bit OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 'false'	 	 
	 	Select @cnt = Count(Code) FROM tblGPO WHERE Code = @Code and GPOID<>@GPOID
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 'false'; 
	 	 ELSE
	 		SET @RETURN_VALUE = 'true';	 	 
	 	 
END 


GO





/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblGrade]    Script Date: 16-02-2018 10:46:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblGrade]
(
	 	 @Code varchar(10),
		 @GradeID varchar(10)
	 	 ,@RETURN_VALUE  bit OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 'false'	 	 
	 	Select @cnt = Count(Code) FROM tblGrade WHERE Code = @Code and GradeID<>@GradeID
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 'false'; 
	 	 ELSE
	 		SET @RETURN_VALUE = 'true';	 	 
	 	 
END 


GO



/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblLO]    Script Date: 16-02-2018 10:46:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblLO]
(
	 	 @Code varchar(10),
		 @LOID varchar(10)
	 	 ,@RETURN_VALUE  bit OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 'false'	 	 
	 	Select @cnt = Count(Code) FROM tblLO WHERE Code = @Code and LOID<>@LOID
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 'false'; 
	 	 ELSE
	 		SET @RETURN_VALUE = 'true';	 	 
	 	 
END 


GO





/****** Object:  StoredProcedure [dbo].[SCP_CHECK_FOR_UniqueCode_tblQualificationType]    Script Date: 16-02-2018 10:46:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SCP_CHECK_FOR_UniqueCode_tblQualificationType]
(
	 	 @Code varchar(10),
		 @QualificationTypeID varchar(10)
	 	 ,@RETURN_VALUE  bit OUTPUT
)
AS
BEGIN
	DECLARE @cnt int;
	SET @RETURN_VALUE = 'false'	 	 
	 	Select @cnt = Count(Code) FROM tblQualificationType WHERE Code = @Code and QualificationTypeID<>@QualificationTypeID
	 	 IF @cnt > 0 	 	 
	 	 	 	 SET @RETURN_VALUE = 'false'; 
	 	 ELSE
	 		SET @RETURN_VALUE = 'true';	 	 
	 	 
END 
GO

Alter table tblBuilding add CampusID int;
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_ALL_tblBuilding]    Script Date: 2/16/2018 6:10:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SCP_GET_ALL_tblBuilding]
AS
BEGIN
	 	 SELECT 
	 	 	 	 BuildingID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Capacity
,				 CampusID
,	 	 	 	 IsActive
	 	 FROM tblBuilding
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_GET_BY_BuildingID_tblBuilding]    Script Date: 2/16/2018 6:11:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_GET_BY_BuildingID_tblBuilding]
(
	 	 @BuildingID int
)
AS
BEGIN
	 	 SELECT 
	 	 	 	 BuildingID
,	 	 	 	 Code
,	 	 	 	 Description
,	 	 	 	 Capacity
,	 	 	 	 IsActive
,				 CampusID
	 	 FROM tblBuilding
	 	 WHERE BuildingID = @BuildingID
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_SET_tblBuilding]    Script Date: 2/16/2018 6:11:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_SET_tblBuilding]
(
	 	 	 	 @Code varchar(10)
	 	 	 	 ,@Description varchar(200)
	 	 	 	 ,@Capacity int
				 ,@CampusID int
	 	 	 	 ,@IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 INSERT INTO
	 	 	 	 tblBuilding
	 	 	 	 ( 
	 	 	 	 Code
	 	 	 	 ,Description
	 	 	 	 ,Capacity
				 ,CampusID
	 	 	 	 ,IsActive
	 	 	 	 ) 
 SELECT 
	 	 	 	 @Code
	 	 	 	 ,@Description
	 	 	 	 ,@Capacity
				 ,@CampusID
	 	 	 	 ,@IsActive

	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 0 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = (SELECT @@IDENTITY) 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  StoredProcedure [dbo].[SCP_UPDATE_tblBuilding]    Script Date: 2/16/2018 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SCP_UPDATE_tblBuilding]
(
	 	  @BuildingID int
	 	 , @Code varchar(10)
	 	 , @Description varchar(200)
	 	 , @Capacity int
		 ,@CampusID int
	 	 , @IsActive bit
	 	 ,@RETURN_VALUE  INT OUTPUT
)
AS
BEGIN
SET @RETURN_VALUE = 0
	 	 BEGIN TRANSACTION
	 	 UPDATE tblBuilding
	 	 SET 
	 	 	 	 	 Code = @Code
	 	 	 	 	 ,Description = @Description
	 	 	 	 	 ,Capacity = @Capacity
					 ,CampusID=@CampusID
	 	 	 	 	 ,IsActive = @IsActive
	 	 	 	 	 
	 	 	 	 	 where BuildingID = @BuildingID
	 	 IF @@ERROR <> 0 
	 	 BEGIN 
	 	 	 	 SET @RETURN_VALUE = 001 
	 	 	 	 ROLLBACK TRANSACTION 
	 	 END 
	 	 SET @RETURN_VALUE = 002 
	 	 COMMIT TRANSACTION 
	 	 RETURN 
	 	 END 
GO

USE [SMSDB]
GO
/****** Object:  Table [dbo].[tblClient]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClient](
	[ClientID] [int] IDENTITY(1,1) NOT NULL,
	[StudentReference] [varchar](10) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[Photo] [varbinary](max) NULL,
	[PassportNumber] [varchar](20) NULL,
	[ResidenceID] [int] NULL,
	[TypeID] [int] NULL,
	[NSNNumber] [varchar](50) NULL,
	[WorkNumber] [varchar](50) NULL,
	[PublicTrustNumber] [varchar](50) NULL,
	[ExternalRef1] [varchar](50) NULL,
	[ExternalRef2] [varchar](50) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[Address3] [varchar](100) NULL,
	[PostCode] [varchar](10) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[CountryID] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[PhoneCountryCode] [varchar](3) NULL,
	[PhoneStateCode] [varchar](2) NULL,
	[Phone] [varchar](10) NULL,
	[EthenicityID] [int] NULL,
	[IwiAffiliation1] [varchar](50) NULL,
	[IwiAffiliation2] [varchar](50) NULL,
	[IwiAffiliation3] [varchar](50) NULL,
	[IwiAffiliation4] [varchar](50) NULL,
	[IwiAffiliation5] [varchar](50) NULL,
	[AgentID] [int] NULL,
 CONSTRAINT [PK_tblClient] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblClientAddress]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClientAddress](
	[ClientAddressID] [int] IDENTITY(1,1) NOT NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[Address3] [varchar](100) NULL,
	[PostCode] [varchar](10) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[CountryID] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[PhoneCountryCode] [varchar](3) NULL,
	[PhoneStateCode] [varchar](2) NULL,
	[Phone] [varchar](10) NULL,
 CONSTRAINT [PK_tblClientAddress] PRIMARY KEY CLUSTERED 
(
	[ClientAddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblClientAssessments]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClientAssessments](
	[ClientAssessmentID] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [int] NULL,
	[ModuleID] [int] NULL,
	[AssessmentID] [int] NULL,
	[TotalMarks] [int] NULL,
	[MarksObtained] [float] NULL,
 CONSTRAINT [PK_tblClientAssessments] PRIMARY KEY CLUSTERED 
(
	[ClientAssessmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblClientAttendance]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClientAttendance](
	[ClientAttendanceID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [int] NULL,
	[AttendancePercentage] [float] NULL,
	[OverallAttendance] [float] NULL,
 CONSTRAINT [PK_tblClientAttendance] PRIMARY KEY CLUSTERED 
(
	[ClientAttendanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblClientDocument]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClientDocument](
	[ClientDocumentID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[DocumentDescription] [varchar](500) NULL,
	[FileType] [varchar](50) NULL,
	[FileSize] [varbinary](max) NULL,
 CONSTRAINT [PK_tblClientDocument] PRIMARY KEY CLUSTERED 
(
	[ClientDocumentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblClientEnglishProficiency]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClientEnglishProficiency](
	[ClientEnglishProficiencyID] [int] IDENTITY(1,1) NOT NULL,
	[EnglishProficiencyType] [varchar](50) NULL,
	[EnglishProficiencyExamNumber] [varchar](50) NULL,
	[EnglishProficiencyExamDate] [datetime] NULL,
	[EnglishProficiencyExpiryDate] [datetime] NULL,
	[EnglishProficiencyOverallBand] [int] NULL,
	[IELTSReading] [int] NULL,
	[IELTSWriting] [int] NULL,
	[IELTSSpeaking] [int] NULL,
	[IELTSListening] [int] NULL,
 CONSTRAINT [PK_tblClientEnglishProficiency] PRIMARY KEY CLUSTERED 
(
	[ClientEnglishProficiencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblClientGrade]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClientGrade](
	[ClientGradeID] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [int] NULL,
	[ModuleID] [int] NULL,
	[GradeID] [int] NULL,
 CONSTRAINT [PK_tblClientGrade] PRIMARY KEY CLUSTERED 
(
	[ClientGradeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblClientOfferOfPlace]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClientOfferOfPlace](
	[ClientOfferOfPlaceID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[DocumentDescription] [varchar](500) NULL,
	[DocumentTypeID] [int] NULL,
 CONSTRAINT [PK_tblClientOfferOfPlace] PRIMARY KEY CLUSTERED 
(
	[ClientOfferOfPlaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblClientPassport]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClientPassport](
	[ClientPassportID] [int] IDENTITY(1,1) NOT NULL,
	[PassportNumber] [varchar](20) NULL,
	[PassportIssueDate] [datetime] NULL,
	[PassportExpiryDate] [datetime] NULL,
	[PassportIssueCountryID] [int] NULL,
	[VisaNumber] [varchar](50) NULL,
	[VisaType] [varchar](50) NULL,
	[VisaIssueDate] [datetime] NULL,
	[VisaExpiryDate] [datetime] NULL,
	[InsuranceProvider] [varchar](50) NULL,
	[InsuranceNumber] [varchar](50) NULL,
	[InsuranceExpiryDate] [datetime] NULL,
 CONSTRAINT [PK_tblClientPassport] PRIMARY KEY CLUSTERED 
(
	[ClientPassportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblClientPlacement]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClientPlacement](
	[ClientPlacementID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[FacilityName] [varchar](50) NULL,
	[CurrentlyPlaced] [bit] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
 CONSTRAINT [PK_tblClientPlacement] PRIMARY KEY CLUSTERED 
(
	[ClientPlacementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblClientWorkExperience]    Script Date: 2/17/2018 12:18:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblClientWorkExperience](
	[ClientWorkExperienceID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[EmployerName] [varchar](50) NULL,
	[EmployerEmail] [varchar](50) NULL,
	[EmployerWebsite] [varchar](50) NULL,
	[PhoneCountryCode] [varchar](3) NULL,
	[PhoneStateCode] [varchar](2) NULL,
	[Phone] [varchar](10) NULL,
	[JobTitle] [varchar](50) NULL,
	[CurrentlyWorking] [bit] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[PostCode] [varchar](10) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[CountryID] [varchar](100) NULL,
 CONSTRAINT [PK_tblClientWorkExperience] PRIMARY KEY CLUSTERED 
(
	[ClientWorkExperienceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE [SMSDB]
GO

/****** Object:  Table [dbo].[tblIntake]    Script Date: 2/17/2018 12:20:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblIntake](
	[IntakeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](50) NULL,
	[ProgrammeID] [int] NULL,
	[FeeID] [int] NULL,
	[Capacity] [int] NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[BreakStartDate] [datetime] NULL,
	[BreakEndDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblIntake] PRIMARY KEY CLUSTERED 
(
	[IntakeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblIntake] ADD  CONSTRAINT [DF_tblIntake_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO



