﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public class SchoolController : MainController
    {
        public override string Name { get { return "School"; } }
        
        BALSchool balSchool = new BALSchool();
        BALCampus balCampus = new BALCampus();
        BALSchoolCampus balSchoolCampus = new BALSchoolCampus();
        public ActionResult Index()
        {
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return DemoView("Index", dtSchool);
        }
        public ActionResult GridViewPartial()
        {           
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            List<EntitySchool> listSchool = new List<EntitySchool>();

            foreach (DataRow dr in dtSchool.Rows)
            {
                EntitySchool entityschool = new EntitySchool();
                entityschool.Code = dr["Code"].ToString();
                entityschool.Description = dr["Description"].ToString();
                entityschool.Address1 = dr["Address1"].ToString();
                entityschool.Address2 = dr["Address2"].ToString();
                entityschool.Address3 = dr["Address3"].ToString();
                entityschool.City = dr["City"].ToString();
                entityschool.State = dr["State"].ToString();
                entityschool.PostCode = dr["PostCode"].ToString();
                entityschool.CountryID = dr["CountryID"].ToString();
                entityschool.Email = dr["Email"].ToString();
                entityschool.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                entityschool.PhoneStateCode = dr["PhoneStateCode"].ToString();
                entityschool.Phone = dr["Phone"].ToString();
                entityschool.SchoolID = Convert.ToInt32(dr["SchoolID"]);
                entityschool.IsActive = Convert.ToBoolean(dr["IsActive"]);
                entityschool.AccountName = dr["AccountName"].ToString();
                entityschool.AccountNumber = dr["AccountNumber"].ToString();
                entityschool.BankName = dr["BankName"].ToString();
                entityschool.SwiftCode = dr["SwiftCode"].ToString();
                entityschool.SPOCEmail = dr["SPOCEmail"].ToString();
                listSchool.Add(entityschool);
            }
            
            return PartialView("GridViewPartial", listSchool);
        }

        public string AddAssociatedCampus(int CampusID, int SchoolID)
        {
            EntitySchoolCampus entitySchoolCampus = new EntitySchoolCampus();
            entitySchoolCampus.SchoolID = SchoolID;
            entitySchoolCampus.CampusID = CampusID;
            string errorMsg = "";

            int result = balSchoolCampus.BALSchoolCampus_SET(entitySchoolCampus, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetSchoolCampusData(int SchoolID)
        {
            DataSet dsSchoolCampus = balSchoolCampus.BALSchoolCampus_Get_By_SchoolID(SchoolID);
            var dtSchoolCampus = dsSchoolCampus.Tables[0];
            return PartialView("CampusViewPartial", dtSchoolCampus);
        }

        public ActionResult SchoolPopup()
        {
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("SchoolPopup", dtSchool);
        }

        public ActionResult GetCampusData()
        {            
            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return PartialView("CampusViewPartial", dtCampus);
        }

        public ActionResult CheckUniqueCode(string Code,string SchoolID)
        {            
            var result = balSchool.Check_For_UniqueCode(Code,SchoolID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntitySchool School)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balSchool.BALSchool_SET(School);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("GridViewPartial", dtSchool);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntitySchool School)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balSchool.BALSchool_Update(School);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("GridViewPartial", dtSchool);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int SchoolID = -1)
        {
            if (SchoolID >= 0)
            {
                try
                {
                    balSchool.BALSchool_Delete_By_SchoolID(SchoolID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Institution/Campus Record.";
                    }
                }
            }
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("GridViewPartial", dtSchool);
        }

        public ActionResult DeleteSchoolCampusPartial(int SchoolID, int SchoolCampusID = -1)
        {
            if (SchoolCampusID >= 0)
            {
                try
                {
                    balSchoolCampus.BALSchoolCampus_Delete_By_SchoolCampusID(SchoolCampusID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetSchoolCampusData(SchoolID);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                SchoolHelper.ExportGridSettings, dtSchool
            );
        }

    }
}