﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;
using DevExpress.Web.Mvc;
using DevExpress.Web;

namespace AgiCorp.Controllers
{
    public partial class AgentController : MainController
    {
        public override string Name { get { return "Agent"; } }
        BALAgent balAgent = new BALAgent();
        BALClass balClass = new BALClass();
        BALDepartment balDepartment = new BALDepartment();
        BALAgentCountry balAgentCountry = new BALAgentCountry();
        BALAgentCommission balAgentCommission = new BALAgentCommission();
        BALAgentContact balAgentContact = new BALAgentContact();
        BALAgentDocument balAgentDocument = new BALAgentDocument();
        public ActionResult Index()
        {
            DataSet dsAgent = balAgent.BALAgent_Get_All();
            var dtAgent = dsAgent.Tables[0];
            return DemoView("Index", dtAgent);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsAgent = balAgent.BALAgent_Get_All();
            var dtAgent = dsAgent.Tables[0];
            List<EntityAgent> listAgent = new List<EntityAgent>();

            foreach (DataRow dr in dtAgent.Rows)
            {
                EntityAgent entityAgent = new EntityAgent();
                entityAgent.Code = dr["Code"].ToString();
                entityAgent.EntityName = dr["EntityName"].ToString();
                entityAgent.EntityTypeID = dr["EntityTypeID"].ToString();
                entityAgent.Address1 = dr["Address1"].ToString();
                entityAgent.Address2 = dr["Address2"].ToString();
                entityAgent.Address3 = dr["Address3"].ToString();
                entityAgent.PostCode = dr["PostCode"].ToString();
                entityAgent.City = dr["City"].ToString();
                entityAgent.State = dr["State"].ToString();                
                entityAgent.CountryID = dr["CountryID"].ToString();                
                entityAgent.Email = dr["Email"].ToString();

                entityAgent.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                entityAgent.PhoneStateCode = dr["PhoneStateCode"].ToString();
                entityAgent.Phone = dr["Phone"].ToString();
                entityAgent.MobileCountryCode = dr["MobileCountryCode"].ToString();
                entityAgent.Mobile = dr["Mobile"].ToString();
                entityAgent.ContractStartDate = Convert.ToDateTime(dr["ContractStartDate"]);
                entityAgent.ContractEndDate = Convert.ToDateTime(dr["ContractEndDate"].ToString());
                entityAgent.WebSite = dr["WebSite"].ToString();

                entityAgent.AgentID = Convert.ToInt32(dr["AgentID"]);
                entityAgent.IsActive = Convert.ToBoolean(dr["IsActive"]);

                listAgent.Add(entityAgent);
            }
            
            return PartialView("GridViewPartial", listAgent);

        }

        public string AddAssociatedCountry(int CountryID, int AgentID)
        {
            EntityAgentCountry entityAgentCountry = new EntityAgentCountry();
            entityAgentCountry.AgentID = AgentID;
            entityAgentCountry.CountryID = CountryID;
            string errorMsg = "";

            int result = balAgentCountry.BALAgentCountry_SET(entityAgentCountry, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetAgentCountryData(int AgentID)
        {
            DataSet dsAgentCountry = balAgentCountry.BALAgentCountry_Get_By_AgentID(AgentID);
            var dtAgentCountry = dsAgentCountry.Tables[0];
            return PartialView("AgentCountryViewPartial", dtAgentCountry);
        }

        public ActionResult AgentPopup()
        {
            DataSet dsAgent = balAgent.BALAgent_Get_All();
            var dtAgent = dsAgent.Tables[0];
            return PartialView("AgentPopup", dtAgent);
        }

        public ActionResult GetClassData()
        {
            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return PartialView("ClassViewPartial", dtClass);
        }

        public ActionResult GetDepartmentData()
        {
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return PartialView("DepartmentViewPartial", dtDepartment);
        }

        public ActionResult CheckUniqueCode(string Code,string AgentID)
        {          
            var result = balAgent.Check_For_UniqueCode(Code,AgentID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityAgent Agent)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balAgent.BALAgent_SET(Agent);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            DataSet dsAgent = balAgent.BALAgent_Get_All();
            var dtAgent = dsAgent.Tables[0];
            return PartialView("GridViewPartial", dtAgent);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityAgent Agent)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balAgent.BALAgent_Update(Agent);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsAgent = balAgent.BALAgent_Get_All();
            var dtAgent = dsAgent.Tables[0];
            return PartialView("GridViewPartial", dtAgent);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int AgentID = -1)
        {
            if (AgentID >= 0)
            {
                try
                {
                    balAgent.BALAgent_Delete_By_AgentID(AgentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsAgent = balAgent.BALAgent_Get_All();
            var dtAgent = dsAgent.Tables[0];
            return PartialView("GridViewPartial", dtAgent);
        }

        public ActionResult DeleteAgentCountryPartial(int AgentID, int AgentCountryID = -1)
        {
            if (AgentCountryID >= 0)
            {
                try
                {
                    balAgentCountry.BALAgentCountry_Delete_By_AgentCountryID(AgentCountryID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetAgentCountryData(AgentID);
        }
        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsAgent = balAgent.BALAgent_Get_All();
            var dtAgent = dsAgent.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                AgentHelper.ExportGridSettings, dtAgent
            );
        }

        [ValidateInput(false)]
        public ActionResult AgentContactAddNewPartial(EntityAgentContact AgentContact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AgentContact.AgentID = Convert.ToInt32(Session["AgenID"]);
                    //AgentContact.AgentID = TextBoxExtension.GetValue<Int32>("PassportNumber");
                    balAgentContact.BALAgentContact_SET(AgentContact);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetAgentContactData(AgentContact.AgentID);
           
        }

        [ValidateInput(false)]
        public ActionResult AgentContactUpdatePartial(EntityAgentContact AgentContact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AgentContact.AgentID = Convert.ToInt32(Session["AgenID"]);
                    balAgentContact.BALAgentContact_Update(AgentContact);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetAgentContactData(AgentContact.AgentID);
           
        }
        [ValidateInput(false)]
        public ActionResult AgentContactDeletePartial(int AgentContactID = -1)
        {
            if (AgentContactID >= 0)
            {
                try
                {
                    balAgentContact.BALAgentContact_Delete_By_AgentContactID(AgentContactID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetAgentContactData(Convert.ToInt32(Session["AgenID"]));
            //DataSet dsAgentContact = balAgentContact.BALAgentContact_Get_All();
            //var dtAgentContact = dsAgentContact.Tables[0];
            //return PartialView("AgentContactViewPartial", dtAgentContact);
        }


        [ValidateInput(false)]
        public ActionResult AgentCommissionAddNewPartial(EntityAgentCommission AgentCommission)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AgentCommission.AgentID = Convert.ToInt32(Session["AgenID"]);
                    //AgentCommission.AgentID = TextBoxExtension.GetValue<Int32>("PassportNumber");
                    balAgentCommission.BALAgentCommission_SET(AgentCommission);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetAgentCommissionData(AgentCommission.AgentID);            
        }

        [ValidateInput(false)]
        public ActionResult AgentCommissionUpdatePartial(EntityAgentCommission AgentCommission)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AgentCommission.AgentID = Convert.ToInt32(Session["AgenID"]);
                    balAgentCommission.BALAgentCommission_Update(AgentCommission);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetAgentCommissionData(AgentCommission.AgentID);            
        }
        [ValidateInput(false)]
        public ActionResult AgentCommissionDeletePartial(int AgentCommissionID = -1)
        {
            if (AgentCommissionID >= 0)
            {
                try
                {
                    balAgentCommission.BALAgentCommission_Delete_By_AgentCommissionID(AgentCommissionID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetAgentCommissionData(Convert.ToInt32(Session["AgenID"]));            
        }


        [ValidateInput(false)]
        public ActionResult AgentDocumentAddNewPartial(EntityAgentDocument AgentDocument)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AgentDocument.AgentID = Convert.ToInt32(Session["AgenID"]);
                    //AgentDocument.AgentID = TextBoxExtension.GetValue<Int32>("PassportNumber");
                    List<byte[]> listUploadedFile = new List<byte[]>();
                    listUploadedFile = (System.Collections.Generic.List<byte[]>)Session["DocsUploadedFile"];
                    AgentDocument.File = listUploadedFile[0];
                    AgentDocument.DocsFileName = Session["DocsFileName"].ToString();
                    balAgentDocument.BALAgentDocument_SET(AgentDocument);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetAgentDocumentData(AgentDocument.AgentID);            
        }

        [ValidateInput(false)]
        public ActionResult AgentDocumentUpdatePartial(EntityAgentDocument AgentDocument)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AgentDocument.AgentID = Convert.ToInt32(Session["AgenID"]);
                    List<byte[]> listUploadedFile = new List<byte[]>();
                    listUploadedFile = (System.Collections.Generic.List<byte[]>)Session["DocsUploadedFile"];
                    AgentDocument.File = listUploadedFile[0];
                    AgentDocument.DocsFileName = Session["DocsFileName"].ToString();
                    balAgentDocument.BALAgentDocument_Update(AgentDocument);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetAgentDocumentData(AgentDocument.AgentID);
            //DataSet dsAgentDocument = balAgentDocument.BALAgentDocument_Get_All();
            //var dtAgentDocument = dsAgentDocument.Tables[0];
            //return PartialView("AgentDocumentViewPartial", dtAgentDocument);
        }
        [ValidateInput(false)]
        public ActionResult AgentDocumentDeletePartial(int AgentDocumentID = -1)
        {
            if (AgentDocumentID >= 0)
            {
                try
                {
                    balAgentDocument.BALAgentDocument_Delete_By_AgentDocumentID(AgentDocumentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetAgentDocumentData(Convert.ToInt32(Session["AgenID"]));            
        }
        public ActionResult GetAgentContactData(int AgentID)
        {
            DataSet dsAgentContact = balAgentContact.BALAgentContact_Get_By_AgentID(AgentID);
            var dtAgentContact = dsAgentContact.Tables[0];
            return PartialView("AgentContactViewPartial", dtAgentContact);
        }

        public ActionResult GetAgentCommissionData(int AgentID)
        {
            DataSet dsAgentCommission = balAgentCommission.BALAgentCommission_Get_By_AgentID(AgentID);
            var dtAgentCommission = dsAgentCommission.Tables[0];
            return PartialView("AgentCommissionViewPartial", dtAgentCommission);
        }

        public ActionResult GetAgentDocumentData(int AgentID)
        {
            DataSet dsAgentDocument = balAgentDocument.BALAgentDocument_Get_By_AgentID(AgentID);
            var dtAgentDocument = dsAgentDocument.Tables[0];
            return PartialView("AgentDocumentViewPartial", dtAgentDocument);
        }

        public ActionResult DocsUpload()
        {
            UploadControlExtension.GetUploadedFiles("Docs", DocsUploadControlHelper.ValidationSettings, DocsUploadControlHelper.uploadControl_FileUploadComplete);
            return null;
        }
    }

    public class DocsUploadControlHelper
    {
        public static readonly UploadControlValidationSettings ValidationSettings = new UploadControlValidationSettings
        {
            AllowedFileExtensions = new string[] { ".pdf", ".doc", ".docx", ".txt", ".xls", ".xlsx", ".ppt", ".pptx",".png", ".jpg", ".jpeg", ".jpe" },
            MaxFileSize = 4194304
        };

        public static void uploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                var AgenId = HttpContext.Current.Session["AgenID"];
                string dirPath = HttpContext.Current.Request.MapPath("~/assets/Content/AgentDoc/" + Convert.ToString(AgenId));
                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }
                string resultFilePath = "~/assets/Content/AgentDoc/" + Convert.ToString(AgenId) + "/" + string.Format("{0}", e.UploadedFile.FileName);
                e.UploadedFile.SaveAs(HttpContext.Current.Request.MapPath(resultFilePath));
                HttpContext.Current.Session["DocsUploadedFile"] = new List<byte[]>();
                ((List<byte[]>)HttpContext.Current.Session["DocsUploadedFile"]).Add(e.UploadedFile.FileBytes);
                HttpContext.Current.Session["DocsFileName"] = e.UploadedFile.FileName;                
            }
        }
    }
}