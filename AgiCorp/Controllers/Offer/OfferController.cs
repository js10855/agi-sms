﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Pdf;
using System.Drawing;
//using DevExpress.Docs;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System.IO;
//using DevExpress.XtraPrinting;
using DevExpress.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public class OfferController : MainController
    {
        string path = "~/assets/Content/OfferDoc/";       
        string docFileName = "OfferCn.docx";
        string strTargetFileName = "OfferCn.docx";
        string AuthorName = "Chandan Ohri";

        BALOffer balOffer = new BALOffer();

        public ActionResult DoCancel(int OfferID)
        {
            balOffer.BALOffer_DoCancel(OfferID);
            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            return PartialView("GridViewPartial", dtOffer);
        }

        public ActionResult DoWithdraw(int OfferID)
        {
            balOffer.BALOffer_DoWithdraw(OfferID);
            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            return PartialView("GridViewPartial", dtOffer);
        }
        public override string Name { get { return "Offer"; } }
        
        public DataTable GenerateOffer(int ClientID,string OfferType,int ProgrammeID1,int ProgrammeID2,int IntakeID1, int IntakeID2, int OfferYearID,int SchoolID,EntityOffer Offer)
        {
            BALClient balClient = new BALClient();
            DataSet dsClient = balClient.BALClient_Get_By_ClientID(ClientID);
            DataTable dtClient = dsClient.Tables[0];
            string strStudentType = dtClient.Rows[0]["StudentType"].ToString();
            if (strStudentType == "Domestic")
            {
                if (OfferType == "Conditional")
                {
                    docFileName = "OfferCn_domestic_1year.docx";
                    strTargetFileName = "OfferCn_domestic_1year" + System.DateTime.Now.ToString("ddmmyyyyhhmmss") + ".docx";
                }
                else
                {
                    docFileName = "OfferUn_domestic_1year.docx";
                    strTargetFileName = "OfferUn_domestic_1year" + System.DateTime.Now.ToString("ddmmyyyyhhmmss") + ".docx";
                }
            }
            else if (strStudentType == "International")
            {
                if (OfferType == "Conditional")
                {
                    if (string.IsNullOrEmpty(ProgrammeID2.ToString()) || ProgrammeID2<=0)
                    {
                        docFileName = "OfferCn_1year.docx";
                        strTargetFileName = "OfferCn_1year" + System.DateTime.Now.ToString("ddmmyyyyhhmmss") + ".docx";
                    }
                    else
                    {
                        docFileName = "OfferCn_2year.docx";
                        strTargetFileName = "OfferCn_2year" + System.DateTime.Now.ToString("ddmmyyyyhhmmss") + ".docx";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(ProgrammeID2.ToString()) || ProgrammeID2<=0)
                    {
                        docFileName = "OfferUn_1year.docx";
                        strTargetFileName = "OfferUn_1year" + System.DateTime.Now.ToString("ddmmyyyyhhmmss") + ".docx";
                    }
                    else
                    {
                        docFileName = "OfferUn_2year.docx";
                        strTargetFileName = "OfferUn_2year" + System.DateTime.Now.ToString("ddmmyyyyhhmmss") + ".docx";
                    }
                   
                }
            }

            
            //strTargetFileName = "Test" + System.DateTime.Now.ToString("ddmmyyyyhhmmss") + ".docx";
            path = System.Web.HttpContext.Current.Request.MapPath(path); 
            System.IO.File.Copy(path + docFileName, path + strTargetFileName);
            
            Stream result = null;
            RichEditDocumentServer docServer = new RichEditDocumentServer();
            docServer.LoadDocument(path + strTargetFileName, DocumentFormat.OpenXml);
            result = new MemoryStream();            

           
            BALClientGender balClientGender = new BALClientGender();
            BALClientNationality balClientNationality = new BALClientNationality();
            BALCountry balCountry = new BALCountry();
            BALAgent balAgent = new BALAgent();           
            BALSchool balSchool = new BALSchool();
            BALProgramme balProgramme = new BALProgramme();
            BALProgrammeLevel balProgrammeLevel = new BALProgrammeLevel();
            BALFees balFees = new BALFees();
            BALProgrammeRating balProgrammeRating = new BALProgrammeRating();
            BALClientVisaType balClientVisaType = new BALClientVisaType();
            BALOfferYear balOfferYear = new BALOfferYear();
            BALIntake balIntake = new BALIntake();
            BALEthnicity balEthnicity = new BALEthnicity();

            string strFind;
            string strReplace;

            if (strStudentType == "Domestic")
            { 
                strFind = "«IRD Number»";
                strReplace = dtClient.Rows[0]["IRDNumber"].ToString();
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "«Client Ethnicity1»";
                int ethID = Convert.ToInt32(dtClient.Rows[0]["EthnicityID"]);
                DataSet dsEthnicity = balEthnicity.BALEthnicity_Get_By_EthnicityID(ethID);
                DataTable dtEthnicity = dsEthnicity.Tables[0];
                strReplace= dtEthnicity.Rows[0]["Description"].ToString(); 
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "«Client Residential Status»";
                strReplace = Convert.ToBoolean(dtClient.Rows[0]["NZPermanantResidence"]) == true?"Yes":"No";
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);           

                strFind = "«Client NSN»";
                strReplace = dtClient.Rows[0]["NSNNumber"].ToString();
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            }

            strFind = "«Client RefExternal»";
            strReplace = dtClient.Rows[0]["StudentReference"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client First Name»";
            strReplace = dtClient.Rows[0]["FirstName"].ToString();            
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);          

            strFind = "«Client Last Name»";
            strReplace = dtClient.Rows[0]["LastName"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client Gender»";
            int genderID = Convert.ToInt32(dtClient.Rows[0]["ClientGenderID"]);           
            DataSet dsClientGender =  balClientGender.BALClientGender_Get_By_ClientGenderID(genderID);
            DataTable dtClientGender = dsClientGender.Tables[0];
            strReplace = dtClientGender.Rows[0]["Description"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client Nationality»";
            int NationalityID = Convert.ToInt32(dtClient.Rows[0]["ClientNationalityID"]);            
            DataSet dsClientNationality = balClientNationality.BALClientNationality_Get_By_ClientNationalityID(NationalityID);
            DataTable dtClientNationality = dsClientNationality.Tables[0];
            strReplace = dtClientNationality.Rows[0]["Description"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client Post Add 1»";
            strReplace = dtClient.Rows[0]["Address1"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client Post Add 2»";
            strReplace = dtClient.Rows[0]["Address2"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client Post Add 3»";
            strReplace = dtClient.Rows[0]["Address3"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client Post Country»";
            int CountryID = Convert.ToInt32(dtClient.Rows[0]["CountryID"]);           
            DataSet dsCountry = balCountry.BALCountry_Get_By_CountryID(CountryID);
            DataTable dtCountry = dsCountry.Tables[0];
            strReplace = dtCountry.Rows[0]["Country"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
            strFind = "«Client DOB»";
            DateTime dt;
            DateTime.TryParse(dtClient.Rows[0]["DateOfBirth"].ToString(),out dt);
            strReplace = dt.ToString("dd/MM/yyyy");            
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Client Passport No»";
            strReplace = dtClient.Rows[0]["PassportNumber"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Agent Name»";
            int AgentID = Convert.ToInt32(dtClient.Rows[0]["AgentID"]);           
            DataSet dsAgent = balAgent.BALAgent_Get_By_AgentID(AgentID);
            DataTable dtAgent = dsAgent.Tables[0];
            strReplace = dtAgent.Rows[0]["EntityName"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Agent Contact Email»";
            strReplace = dtAgent.Rows[0]["Email"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            //strFind = "<<Year1>>";
            //DataSet dsOfferYear = balOfferYear.BALOfferYear_Get_By_OfferYearID(OfferYearID);
            //DataTable dtOfferYear = dsOfferYear.Tables[0];
            //strReplace = "Year 1"; //dtOfferYear.Rows[0]["Description"].ToString();
            ////strReplace = Year;
            //FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<Programme1>>";           
            DataSet dsProgramme1 = balProgramme.BALProgramme_Get_By_ProgrammeID(ProgrammeID1);
            DataTable dtProgramme1 = dsProgramme1.Tables[0];
            strReplace = dtProgramme1.Rows[0]["Description"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
            strFind = "<<Level1>>";
            int ProgrammeLevelID1 = Convert.ToInt32(dtProgramme1.Rows[0]["ProgrammeLevelID"]);
            DataSet dsProgrammeLevel1 = balProgrammeLevel.BALProgrammeLevel_Get_By_ProgrammeLevelID(ProgrammeLevelID1);
            DataTable dtProgrammeLevel1 = dsProgrammeLevel1.Tables[0];
            strReplace = dtProgrammeLevel1.Rows[0]["Description"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<Programme1WithLevel1>>";
            strReplace = dtProgramme1.Rows[0]["Description"].ToString() + " (" + dtProgrammeLevel1.Rows[0]["Description"].ToString() + ")";
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<Programme1Length>>";
            strReplace = dtProgramme1.Rows[0]["ProgrammeLength"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            if (!string.IsNullOrEmpty(ProgrammeID2.ToString()) && ProgrammeID2>0)
            { 
                strFind = "<<Programme2>>";
                DataSet dsProgramme2 = balProgramme.BALProgramme_Get_By_ProgrammeID(ProgrammeID2);
                DataTable dtProgramme2 = dsProgramme2.Tables[0];
                strReplace = dtProgramme2.Rows[0]["Description"].ToString();
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<Level2>>";
                int ProgrammeLevelID2 = Convert.ToInt32(dtProgramme2.Rows[0]["ProgrammeLevelID"]);
                DataSet dsProgrammeLevel2 = balProgrammeLevel.BALProgrammeLevel_Get_By_ProgrammeLevelID(ProgrammeLevelID2);
                DataTable dtProgrammeLevel2 = dsProgrammeLevel2.Tables[0];
                strReplace = dtProgrammeLevel2.Rows[0]["Description"].ToString();
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
                strFind = "<<Programme2WithLevel2>>";
                strReplace = dtProgramme2.Rows[0]["Description"].ToString() + " (" + dtProgrammeLevel2.Rows[0]["Description"].ToString() + ")";
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);            

                strFind = "<<Programme2Length>>";
                strReplace = dtProgramme2.Rows[0]["ProgrammeLength"].ToString();
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            }

            strFind = "<<StartDate1>>";
            DataSet dsIntake1 = balIntake.BALIntake_Get_By_IntakeID(IntakeID1);
            DataTable dtIntake1 = dsIntake1.Tables[0];            
            DateTime.TryParse(dtIntake1.Rows[0]["StartDate"].ToString(), out dt);
            strReplace = dt.ToString("dd/MM/yyyy");           
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
            strFind = "<<EndDate1>>";
            DateTime.TryParse(dtIntake1.Rows[0]["EndDate"].ToString(), out dt);
            strReplace = dt.ToString("dd/MM/yyyy");           
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<SemesterBreak1>>";
            bool isBreak = Convert.ToBoolean(string.IsNullOrEmpty(dtIntake1.Rows[0]["IsSemesterBreakApplicable"].ToString())?false:dtIntake1.Rows[0]["IsSemesterBreakApplicable"]);
            if (!isBreak) { strReplace = "Not Applicable"; }
            else
            {
                string strSemFromDate = dtIntake1.Rows[0]["SemesterBreakFromDate"].ToString();
                string strSemToDate = dtIntake1.Rows[0]["SemesterBreakToDate"].ToString();
                DateTime fromDate;
                DateTime toDate;
                DateTime.TryParse(strSemFromDate, out fromDate);
                DateTime.TryParse(strSemToDate, out toDate);
                strReplace = fromDate.ToString("dd/MM/yyyy") + " to " + toDate.ToString("dd/MM/yyyy");
            }
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<SummerBreak1>>";
            string strSummerFromDate = dtIntake1.Rows[0]["SummerBreakFromDate"].ToString();
            string strSummerToDate = dtIntake1.Rows[0]["SummerBreakToDate"].ToString();
            if (string.IsNullOrEmpty(strSummerFromDate) || string.IsNullOrEmpty(strSummerToDate))
            {
                strReplace = "Not Applicable";
            }
            else
            {
                DateTime fromDate;
                DateTime toDate;
                DateTime.TryParse(strSummerFromDate, out fromDate);
                DateTime.TryParse(strSummerToDate, out toDate);
                strReplace = fromDate.ToString("dd/MM/yyyy") + " to " + toDate.ToString("dd/MM/yyyy"); 
            }            
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            if (!string.IsNullOrEmpty(IntakeID2.ToString()) && IntakeID2>0)
            {
                strFind = "<<StartDate2>>";
                DataSet dsIntake2 = balIntake.BALIntake_Get_By_IntakeID(IntakeID2);
                DataTable dtIntake2 = dsIntake2.Tables[0];
                DateTime.TryParse(dtIntake2.Rows[0]["StartDate"].ToString(), out dt);
                strReplace = dt.ToString("dd/MM/yyyy");
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<EndDate2>>";
                DateTime.TryParse(dtIntake2.Rows[0]["EndDate"].ToString(), out dt);
                strReplace = dt.ToString("dd/MM/yyyy");
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<SemesterBreak2>>";
                isBreak = Convert.ToBoolean(string.IsNullOrEmpty(dtIntake2.Rows[0]["IsSemesterBreakApplicable"].ToString()) ? false : dtIntake2.Rows[0]["IsSemesterBreakApplicable"]);
                if (!isBreak) { strReplace = "Not Applicable"; }
                else
                {
                    string strSemFromDate = dtIntake2.Rows[0]["SemesterBreakFromDate"].ToString();
                    string strSemToDate = dtIntake2.Rows[0]["SemesterBreakToDate"].ToString();
                    DateTime fromDate;
                    DateTime toDate;
                    DateTime.TryParse(strSemFromDate, out fromDate);
                    DateTime.TryParse(strSemToDate, out toDate);
                    strReplace = fromDate.ToString("dd/MM/yyyy") + " to " + toDate.ToString("dd/MM/yyyy");
                }
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<SummerBreak2>>";
                strSummerFromDate = dtIntake2.Rows[0]["SummerBreakFromDate"].ToString();
                strSummerToDate = dtIntake2.Rows[0]["SummerBreakToDate"].ToString();
                if (string.IsNullOrEmpty(strSummerFromDate) || string.IsNullOrEmpty(strSummerToDate))
                {
                    strReplace = "Not Applicable";
                }
                else
                {
                    DateTime fromDate;
                    DateTime toDate;
                    DateTime.TryParse(strSummerFromDate, out fromDate);
                    DateTime.TryParse(strSummerToDate, out toDate);
                    strReplace = fromDate.ToString("dd/MM/yyyy") + " to " + toDate.ToString("dd/MM/yyyy");
                }
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            }

            double totalFees=0;
            double programme1Fees = 0;
            double programme2Fees = 0;
            strFind = "<<TuitionFees1>>";
            DataSet dsFees1= balFees.BALFees_Get_By_ProgrammeID_CountryID(ProgrammeID1,CountryID);
            DataTable dtFees1 = dsFees1.Tables[0];
            strReplace= dtFees1.Rows[0]["TutionFees"].ToString();
            programme1Fees += Convert.ToDouble(strReplace);
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<MaterialFees1>>";
            strReplace = dtFees1.Rows[0]["MaterialFees"].ToString();
            programme1Fees += Convert.ToDouble(strReplace);
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<RegistrationFees1>>";
            strReplace = dtFees1.Rows[0]["RegistrationFees"].ToString();
            programme1Fees += Convert.ToDouble(strReplace);
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<InsuranceFees1>>";
            strReplace = dtFees1.Rows[0]["Insurance"].ToString();
            programme1Fees += Convert.ToDouble(strReplace);
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<TotalFees1>>";
            //strReplace = dtFees1.Rows[0]["TotalFees"].ToString();
            strReplace = programme1Fees.ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            if (!string.IsNullOrEmpty(ProgrammeID2.ToString()) && ProgrammeID2>0)
            {
                strFind = "<<TuitionFees2>>";
                DataSet dsFees2 = balFees.BALFees_Get_By_ProgrammeID_CountryID(ProgrammeID2, CountryID);
                DataTable dtFees2 = dsFees2.Tables[0];
                strReplace = dtFees2.Rows[0]["TutionFees"].ToString();
                programme2Fees += Convert.ToDouble(strReplace);
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<MaterialFees2>>";
                strReplace = dtFees2.Rows[0]["MaterialFees"].ToString();
                programme2Fees += Convert.ToDouble(strReplace);
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<RegistrationFees2>>";
                strReplace = dtFees2.Rows[0]["RegistrationFees"].ToString();
                programme2Fees += Convert.ToDouble(strReplace);
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<InsuranceFees2>>";
                strReplace = dtFees2.Rows[0]["Insurance"].ToString();
                programme2Fees += Convert.ToDouble(strReplace);
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<TotalFees2>>";
                //strReplace = dtFees2.Rows[0]["TotalFees"].ToString();
                strReplace = programme2Fees.ToString();
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);

                strFind = "<<TotalFeesFor2Years>>";                
                strReplace = Convert.ToString(programme2Fees + programme1Fees);
                FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            }

            strFind = "<<AirportPickUpFees>>";
            strReplace = dtFees1.Rows[0]["AirportPickUpFees"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
            strFind = "<<HomeStayPlacementFees>>";
            strReplace = dtFees1.Rows[0]["HomeStayPlacementFees"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
            strFind = "<<HomeStayFeesPerWeek>>";
            strReplace = dtFees1.Rows[0]["HomeStayFeesPerWeek"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
            strFind = "<<Programme1Rating>>";
            int ProgrammeRatingID = Convert.ToInt32(Convert.IsDBNull(dtProgramme1.Rows[0]["ProgrammeRatingID"])?0: dtProgramme1.Rows[0]["ProgrammeRatingID"]);
            if (ProgrammeRatingID>0)
            { 
            DataSet dsProgrammeRating1 = balProgrammeRating.BALProgrammeRating_Get_By_ProgrammeRatingID(ProgrammeRatingID);
            DataTable dtProgrammeRating1 = dsProgrammeRating1.Tables[0];
            strReplace = dtProgrammeRating1.Rows[0]["Description"].ToString();
            }
            else { strReplace = ""; }
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<VisaType>>";
            int ClientVisaTypeID = Convert.ToInt32(Convert.IsDBNull(dtClient.Rows[0]["ClientVisaTypeID"])?0: dtClient.Rows[0]["ClientVisaTypeID"]);
            if (ClientVisaTypeID>0)
            { 
            DataSet dsClientVisaType = balClientVisaType.BALClientVisaType_Get_By_ClientVisaTypeID(ClientVisaTypeID);
            DataTable dtClientVisaType = dsClientVisaType.Tables[0];
            strReplace = dtClientVisaType.Rows[0]["Description"].ToString();
            }
            else { strReplace = ""; }
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<AccountName>>";           
            DataSet dsSchool = balSchool.BALSchool_Get_By_SchoolID(SchoolID);
            DataTable dtSchool = dsSchool.Tables[0];
            strReplace = dtSchool.Rows[0]["AccountName"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<AccountNumber>>";
            strReplace = dtSchool.Rows[0]["AccountNumber"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<BankName>>";
            strReplace = dtSchool.Rows[0]["BankName"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "<<SwiftCode>>";
            strReplace = dtSchool.Rows[0]["SwiftCode"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);

            strFind = "«Contact Email»";
            strReplace = dtSchool.Rows[0]["SPOCEmail"].ToString();
            FindAndReplaceTextInDoc(docServer, strFind, strReplace);
            
            docServer.SaveDocument(strTargetFileName, DocumentFormat.OpenXml);
            ExportDocToPdf(docServer, strTargetFileName);
            //AddNewOffer(ClientID, OfferType, ProgrammeID, IntakeID, OfferYearID, InstitutionID);  
            if (OfferType == "Conditional")
            {
                PdfDocumentProcessor processor = new PdfDocumentProcessor();
                AddWatermark(processor, "Conditional", path + strTargetFileName + ".pdf", path + strTargetFileName + ".pdf");
            }
            byte[] file;
            using (var stream = new FileStream(path + strTargetFileName + ".pdf", FileMode.Open, FileAccess.Read))
            {
                using (var reader = new BinaryReader(stream))
                {
                    file = reader.ReadBytes((int)stream.Length);
                }
            }
            
            Offer.OfferCreatedDate = DateTime.Now;
            Offer.OfferStatusID = "1";
            Offer.File = file;
            Offer.DocsFileName = strTargetFileName + ".pdf";

            if (ModelState.IsValid)
            {
                try
                {
                    balOffer.BALOffer_SET(Offer);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            return dtOffer;
            //System.Diagnostics.Process.Start(path + strTargetFileName + ".pdf");
            //return PartialView("GridViewPartial", dtOffer);
           // return file;          
        }

        //public ActionResult AddNewOffer(int ClientID, string OfferType, int ProgrammeID, int IntakeID, int OfferYearID, int InstitutionID)
        //{           
        //    EntityOffer Offer = new EntityOffer();
        //    Offer.ClientID = ClientID;
        //    Offer.IntakeID = IntakeID;
        //    Offer.OfferCreatedDate = System.DateTime.Now;
        //    Offer.OfferDate = System.DateTime.Now; //Ask Question for this
        //    Offer.OfferStatusID = 1;
        //    Offer.OfferType = OfferType;
        //    Offer.OfferYearID = OfferYearID;
        //    Offer.ProgrammeID = ProgrammeID;
        //    Offer.OfferCancelledDate = null;
        //    Offer.OfferLastEditedDate = null;
        //    Offer.OfferWithdrawnDate = null;


        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            balOffer.BALOffer_SET(Offer);
        //        }
        //        catch (Exception e)
        //        {
        //            ViewData["EditError"] = e.Message;
        //        }
        //    }
        //    else
        //        ViewData["EditError"] = "Please, correct all errors.";
        //    DataSet dsOffer = balOffer.BALOffer_Get_All();
        //    var dtOffer = dsOffer.Tables[0];
        //    return PartialView("GridViewPartial", dtOffer);
        //}

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityOffer Offer)
        {           
            DataTable dtOffer1 = GenerateOffer(Convert.ToInt32(Offer.ClientID), Offer.OfferType, Convert.ToInt32(Offer.ProgrammeID1), Convert.ToInt32(Offer.ProgrammeID2), Convert.ToInt32(Offer.IntakeID1), Convert.ToInt32(Offer.IntakeID2), Convert.ToInt32(Offer.OfferYearID), Convert.ToInt32(Offer.SchoolID), Offer);
            System.Diagnostics.Process.Start(path + strTargetFileName + ".pdf");

            return PartialView("GridViewPartial", dtOffer1);         
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityOffer Offer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balOffer.BALOffer_Update(Offer);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            return PartialView("GridViewPartial", dtOffer);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int OfferID = -1)
        {
            if (OfferID >= 0)
            {
                try
                {
                    balOffer.BALOffer_Delete_By_OfferID(OfferID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Institution/Campus Record.";
                    }
                }
            }
            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            return PartialView("GridViewPartial", dtOffer);
        }

        public ActionResult Index()
        {
            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            return DemoView("Index", dtOffer);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            List<EntityOffer> listOffer = new List<EntityOffer>();

            foreach (DataRow dr in dtOffer.Rows)
            {
                EntityOffer Offer = new EntityOffer();
                //Offer.ClientID = Convert.ToInt32(dr["ClientID"]);
                //Offer.IntakeID = Convert.ToInt32(dr["IntakeID"]);
                //Offer.OfferCreatedDate = Convert.ToDateTime(dr["OfferCreatedDate"]);
                //Offer.OfferDate = Convert.ToDateTime(dr["OfferDate"]);
                //Offer.OfferStatusID = Convert.ToInt32(dr["OfferStatusID"]);
                //Offer.OfferType = Convert.ToString(dr["OfferType"]);
                //Offer.OfferYearID = Convert.ToInt32(dr["OfferYearID"]);
                //Offer.ProgrammeID = Convert.ToInt32(dr["ProgrammeID"]);
                Offer.OfferID = Convert.ToInt32(dr["OfferID"]);
                Offer.ClientID = dr["ClientID"].ToString();
                Offer.IntakeID1 = dr["IntakeID1"].ToString();
                Offer.IntakeID2 = dr["IntakeID2"].ToString();
                Offer.OfferCreatedDate = Convert.ToDateTime(dr["OfferCreatedDate"]);
                Offer.OfferDate = Convert.ToDateTime(dr["OfferDate"]);
                Offer.OfferStatusID = dr["OfferStatusID"].ToString();
                Offer.OfferType = Convert.ToString(dr["OfferType"]);
                Offer.OfferYearID = dr["OfferYearID"].ToString();
                Offer.ProgrammeID1 = dr["ProgrammeID1"].ToString();
                Offer.ProgrammeID2 = dr["ProgrammeID2"].ToString();
                Offer.OfferCancelledDate = Convert.ToDateTime(Convert.IsDBNull(dr["OfferCancelledDate"])?System.DateTime.MinValue:dr["OfferCancelledDate"]);
                Offer.OfferLastEditedDate = Convert.ToDateTime(Convert.IsDBNull(dr["OfferLastEditedDate"])?System.DateTime.MinValue:dr["OfferLastEditedDate"]);
                Offer.OfferWithdrawnDate = Convert.ToDateTime(Convert.IsDBNull(dr["OfferWithdrawnDate"])?System.DateTime.MinValue: dr["OfferWithdrawnDate"]);                
                Offer.SchoolID = dr["SchoolID"].ToString();
                listOffer.Add(Offer);
            }
            return PartialView("GridViewPartial", listOffer);

        }

        private void ExportDocToPdf(RichEditDocumentServer docServer, string strTargetFileName)
        {
            DevExpress.XtraPrinting.PdfExportOptions options = new DevExpress.XtraPrinting.PdfExportOptions();
            options.DocumentOptions.Author = AuthorName;
            options.Compressed = false;
            options.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Highest;
            using (FileStream pdfFileStream = new FileStream(path + strTargetFileName + ".pdf", FileMode.Create))
            {
                docServer.ExportToPdf(pdfFileStream, options);
            }
        }
        private void FindAndReplaceTextInDoc(RichEditDocumentServer docServer,string strFind, string strReplace)
        {
            DocumentRange[] ranges = docServer.Document.FindAll(strFind, SearchOptions.None, docServer.Document.Range);
            ranges = docServer.Document.FindAll(strFind, SearchOptions.None, docServer.Document.Range);
            for (int i = 0; i < ranges.Length; i++)
            {
                if (strReplace == "null")
                    strReplace = String.Empty;
                docServer.Document.Replace(ranges[i], strReplace);
                CharacterProperties cp = docServer.Document.BeginUpdateCharacters(ranges[i]);                
                docServer.Document.EndUpdateCharacters(cp);
            };
        }

        //private void usingPDF(int ClientID, string OfferType, int ProgrammeID, int IntakeID)
        //{
        //    using (PdfDocumentProcessor processor = new PdfDocumentProcessor())
        //    {
        //        // Create an empty document using PDF creation options.                
        //        processor.CreateEmptyDocument(path + pdfFileName);

        //        // Create and draw PDF graphics.
        //        using (PdfGraphics graph = processor.CreateGraphics())
        //        {
        //            DrawGraphics(graph);
        //            processor.RenderNewPage(PdfPaperSize.Letter, graph);
        //        }
        //        AddWatermark(processor, "Conditional", path + pdfFileName, path + pdfFileName);
        //        RichEditDocumentServer server = new RichEditDocumentServer();
        //        DrawTable(ClientID, OfferType, ProgrammeID, IntakeID);
        //        string fpath = path + docFileName + ".pdf";
        //        processor.CreateEmptyDocument(path + "abc.pdf");
        //        processor.AppendDocument(path + pdfFileName);
        //        processor.AppendDocument(fpath);
        //    }
        //}

        //public void DrawGraphics(PdfGraphics graph)
        //{
        //    // Draw an image on the page. 
        //    using (Bitmap image = new Bitmap(path + "AGI1.png"))
        //    {
        //        float width = image.Width;
        //        float height = image.Height;
        //        graph.DrawImage(image, new RectangleF(20, 40, width/5,  height / 4));
        //    }

        //    // Draw text lines on the page. 
        //    SolidBrush black = (SolidBrush)Brushes.Black;
        //    //using (Font font1 = new Font("Times New Roman", 32, FontStyle.Bold))
        //    //{
        //    //    graph.DrawString("PDF Document Processor", font1, black, 180, 150);
        //    //}
        //    //using (Font font2 = new Font("Arial", 20))
        //    //{
        //    //    graph.DrawString("Display, Print and Export PDF Documents", font2, black, 168, 230);
        //    //}
        //    using (Font font3 = new Font("Arial", 10))
        //    {
        //        graph.DrawString("Dear Student, \n" +
        //            "AGI Education Limited(AGI) is pleased to inform you that your application" + "\n" +
        //            "has been successful with" + "\n" +
        //            "the condition of submitting all the required documents." + "\n" +
        //            "This offer of place is for one year/ s full time study.", font3, black, 16, 260);



        //        //graph.DrawString(" ", font3, black, 17, 180);
        //        //graph.DrawString("This offer of place is for one year/s full time study. ", font3, black, 18, 210);

        //        //graph.DrawString("The PDF Document Processor is a non-visual component " +
        //        //                  "that provides the application programming interface of the PDF Viewer.", font3, black, 16, 300);
        //    }
        //}


        private void AddWatermark(PdfDocumentProcessor documentProcessor, string text, string fileName, string resultFileName)
        {
            string fontName = "Arial Black";
            int fontSize = 12;
            PdfStringFormat stringFormat = PdfStringFormat.GenericTypographic;
            stringFormat.Alignment = PdfStringAlignment.Center;
            stringFormat.LineAlignment = PdfStringAlignment.Center;
            documentProcessor.LoadDocument(fileName);
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(63, Color.Black)))
            {
                using (Font font = new Font(fontName, fontSize))
                {
                    foreach (var page in documentProcessor.Document.Pages)
                    {
                        var watermarkSize = page.CropBox.Width * 0.75;
                        using (PdfGraphics graphics = documentProcessor.CreateGraphics())
                        {
                            SizeF stringSize = graphics.MeasureString(text, font);
                            Single scale = Convert.ToSingle(watermarkSize / stringSize.Width);
                            graphics.TranslateTransform(Convert.ToSingle(page.CropBox.Width * 0.5), Convert.ToSingle(page.CropBox.Height * 0.5));
                            graphics.RotateTransform(-45);
                            graphics.TranslateTransform(Convert.ToSingle(-stringSize.Width * scale * 0.5), Convert.ToSingle(-stringSize.Height * scale * 0.5));
                            using (Font actualFont = new Font(fontName, fontSize * scale))
                            {
                                RectangleF rect = new RectangleF(0, 0, stringSize.Width * scale, stringSize.Height * scale);
                                graphics.DrawString(text, actualFont, brush, rect, stringFormat);
                            }

                            graphics.AddToPageForeground(page, 72, 72);
                        }
                    }
                }
            }

            documentProcessor.SaveDocument(resultFileName);
            //}
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsOffer = balOffer.BALOffer_Get_All();
            var dtOffer = dsOffer.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                OfferHelper.ExportGridSettings, dtOffer
            );
        }

        //public void DrawTable(int ClientID, string OfferType, int ProgrammeID, int IntakeID)
        //{
        //    RichEditDocumentServer server = new RichEditDocumentServer();
        //    DevExpress.XtraRichEdit.API.Native.Document document = server.Document;
        //    // Insert new table.
        //    Table tbl = document.Tables.Create(document.Range.Start, 1, 3, AutoFitBehaviorType.AutoFitToWindow);
        //    // Create a table header.
        //    document.InsertText(tbl[0, 0].Range.Start, "Year");
        //    document.InsertText(tbl[0, 1].Range.Start, "Programme");
        //    document.InsertText(tbl[0, 2].Range.Start, "Level");

        //    DevExpress.XtraRichEdit.API.Native.TableRow row = tbl.Rows.Append();
        //    TableCell cell = row.FirstCell;

        //    BALProgramme balProgramme = new BALProgramme();
        //    DataSet dsProgramme=  balProgramme.BALProgramme_Get_By_ProgrammeID(ProgrammeID);
        //    DataTable dtProgramme = dsProgramme.Tables[0];
        //    string pr = dtProgramme.Rows[0]["Description"].ToString();

        //    document.InsertSingleLineText(cell.Range.Start, "Year 1");
        //    document.InsertSingleLineText(cell.Next.Range.Start,pr );
        //    document.InsertSingleLineText(cell.Next.Next.Range.Start,"Level 7" );
        //    tbl.EndUpdate();

        //    //// Insert table data.
        //    //DirectoryInfo dirinfo = new DirectoryInfo(path);
        //    //try
        //    //{
        //    //    tbl.BeginUpdate();
        //    //    foreach (FileInfo fi in dirinfo.GetFiles())
        //    //    {
        //    //        DevExpress.XtraRichEdit.API.Native.TableRow row = tbl.Rows.Append();
        //    //        TableCell cell = row.FirstCell;
        //    //        string fileName = fi.Name;
        //    //        string fileLength = String.Format("{0:N0}", fi.Length);
        //    //        string fileLastTime = String.Format("{0:g}", fi.LastWriteTime);
        //    //        document.InsertSingleLineText(cell.Range.Start, fileName);
        //    //        document.InsertSingleLineText(cell.Next.Range.Start, fileLength);
        //    //        document.InsertSingleLineText(cell.Next.Next.Range.Start, fileLastTime);
        //    //    }
        //    //    // Center the table header.
        //    //    foreach (Paragraph p in document.Paragraphs.Get(tbl.FirstRow.Range))
        //    //    {
        //    //        p.Alignment = ParagraphAlignment.Center;
        //    //    }
        //    //}
        //    //finally
        //    //{
        //    //    tbl.EndUpdate();
        //    //}
        //    //tbl.Cell(2, 1).Split(1, 3);
        //    server.SaveDocument(path + docFileName, DocumentFormat.OpenXml);
        //    server.LoadDocument(path + docFileName, DocumentFormat.OpenXml);
        //    //Specify export options:
        //    DevExpress.XtraPrinting.PdfExportOptions options = new DevExpress.XtraPrinting.PdfExportOptions();
        //    options.DocumentOptions.Author = "Chandan Ohri";
        //    options.Compressed = false;
        //    options.ImageQuality = DevExpress.XtraPrinting.PdfJpegImageQuality.Highest;


        //    //Export the document to the stream: 
        //    using (FileStream pdfFileStream = new FileStream(path + docFileName + ".pdf", FileMode.Create))
        //    {
        //        server.ExportToPdf(pdfFileStream, options);
        //    }
        //    //System.Diagnostics.Process.Start(path + pdfFileName);
        //    //System.Diagnostics.Process.Start("explorer.exe", "/select," + path + docFileName);
        //}
    }
}