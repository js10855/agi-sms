﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class CampusController : MainController
    {
        public override string Name { get { return "Campus"; } }
        BALCampus balCampus = new BALCampus();
        BALBuilding balBuilding = new BALBuilding();
        BALCampusBuilding balCampusBuilding = new BALCampusBuilding();
        public ActionResult Index()
        {
            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return DemoView("Index", dtCampus);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            List<EntityCampus> listInst = new List<EntityCampus>();

            foreach (DataRow dr in dtCampus.Rows)
            {
                EntityCampus entitycampus = new EntityCampus();
                entitycampus.Code = dr["Code"].ToString();
                entitycampus.Description = dr["Description"].ToString();
                entitycampus.Address1 = dr["Address1"].ToString();
                entitycampus.Address2 = dr["Address2"].ToString();
                entitycampus.Address3 = dr["Address3"].ToString();
                entitycampus.City = dr["City"].ToString();
                entitycampus.State = dr["State"].ToString();
                entitycampus.PostCode = dr["PostCode"].ToString();
                entitycampus.CountryID = dr["CountryID"].ToString();                
                entitycampus.Email = dr["Email"].ToString();
                entitycampus.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                entitycampus.PhoneStateCode = dr["PhoneStateCode"].ToString();
                entitycampus.Phone = dr["Phone"].ToString();
                entitycampus.CampusID = Convert.ToInt32(dr["CampusID"]);
                entitycampus.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listInst.Add(entitycampus);
            }
            return PartialView("GridViewPartial", listInst);
        }

        public string AddAssociatedBuilding(int BuildingID, int CampusID)
        {
            EntityCampusBuilding entityCampusBuilding = new EntityCampusBuilding();
            entityCampusBuilding.CampusID = CampusID;
            entityCampusBuilding.BuildingID = BuildingID;
            string errorMsg = "";

            int result = balCampusBuilding.BALCampusBuilding_SET(entityCampusBuilding, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetCampusBuildingData(int CampusID)
        {
            DataSet dsCampusBuilding = balCampusBuilding.BALCampusBuilding_Get_By_CampusID(CampusID);
            var dtCampusBuilding = dsCampusBuilding.Tables[0];
            return PartialView("BuildingViewPartial", dtCampusBuilding);
        }

        public ActionResult CampusPopup()
        {
            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return PartialView("CampusPopup", dtCampus);
        }

        public ActionResult GetBuildingData()
        {
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("BuildingViewPartial", dtBuilding);
        }

        public ActionResult CheckUniqueCode(string Code,string CampusID)
        {            
            var result =  balCampus.Check_For_UniqueCode(Code,CampusID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityCampus Campus)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balCampus.BALCampus_SET(Campus);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return PartialView("GridViewPartial", dtCampus);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityCampus Campus)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balCampus.BALCampus_Update(Campus);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return PartialView("GridViewPartial", dtCampus);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int CampusID = -1)
        {
            if (CampusID >= 0)
            {
                try
                {
                    balCampus.BALCampus_Delete_By_CampusID(CampusID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with School/Building Record.";
                    }
                }
            }
            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return PartialView("GridViewPartial", dtCampus);
        }

        public ActionResult DeleteCampusBuildingPartial(int CampusID, int CampusBuildingID = -1)
        {
            if (CampusBuildingID >= 0)
            {
                try
                {
                    balCampusBuilding.BALCampusBuilding_Delete_By_CampusBuildingID(CampusBuildingID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }            
            return GetCampusBuildingData(CampusID);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                CampusHelper.ExportGridSettings, dtCampus
            );
        }
    }
}