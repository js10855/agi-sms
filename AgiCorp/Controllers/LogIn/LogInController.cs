﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;

namespace AgiCorp.Controllers
{
    public class LogInController : Controller
    {
        BALUser balUser = new BALUser();
        // GET: LogIn
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogIn(EntityUser user)
        {
            if (balUser.CheckLogin(user.UserName, user.Password))
            {
                return RedirectToAction("Index", "Institution");
            }
            ModelState.AddModelError("Val_Login", "Wrong user name or password");  
            return View("Index",user);
        }
    }
}