﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public class IntakeController : MainController
    {
        public override string Name { get { return "Intake"; } }

        BALIntake balIntake = new BALIntake();
        BALCampus balCampus = new BALCampus();
        BALOffer balOffer = new BALOffer();
        public ActionResult Index()
        {
            DataSet dsIntake = balIntake.BALIntake_Get_All();
            var dtIntake = dsIntake.Tables[0];
            return DemoView("Index", dtIntake);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsIntake = balIntake.BALIntake_Get_All();
            var dtIntake = dsIntake.Tables[0];
            List<EntityIntake> listIntake = new List<EntityIntake>();

            foreach (DataRow dr in dtIntake.Rows)
            {
                EntityIntake entityIntake = new EntityIntake();
                entityIntake.Code = dr["Code"].ToString();
                entityIntake.Description = dr["Description"].ToString();
                //entityIntake.BreakStartDate = Convert.ToDateTime(dr["BreakStartDate"]);
                //entityIntake.BreakEndDate = Convert.ToDateTime(dr["BreakEndDate"]);                
                entityIntake.StartDate = Convert.ToDateTime(dr["StartDate"]);
                entityIntake.EndDate = Convert.ToDateTime(dr["EndDate"]);

                //entityIntake.SemesterBreakFromDate = Convert.ToDateTime(dr["SemesterBreakFromDate"]);
                //entityIntake.SemesterBreakToDate = Convert.ToDateTime(dr["SemesterBreakToDate"]);
                //entityIntake.SummerBreakFromDate = Convert.ToDateTime(dr["SummerBreakFromDate"]);
                //entityIntake.SummerBreakToDate = Convert.ToDateTime(dr["SummerBreakToDate"]);
                //entityIntake.IsSemesterBreakApplicable = Convert.ToBoolean(dr["IsSemesterBreakApplicable"]);
                //entityIntake.IsSummerBreakApplicable = Convert.ToBoolean(dr["IsSummerBreakApplicable"]);

                if (!string.IsNullOrEmpty(Convert.ToString(dr["SemesterBreakFromDate"])))
                    entityIntake.SemesterBreakFromDate = Convert.ToDateTime(Convert.ToString(dr["SemesterBreakFromDate"]));
                if (!string.IsNullOrEmpty(Convert.ToString(dr["SemesterBreakToDate"])))
                    entityIntake.SemesterBreakToDate = Convert.ToDateTime(Convert.ToString(dr["SemesterBreakToDate"]));
                if (!string.IsNullOrEmpty(Convert.ToString(dr["IsSemesterBreakApplicable"])))
                    entityIntake.IsSemesterBreakApplicable = Convert.ToBoolean(dr["IsSemesterBreakApplicable"]);
                if (!string.IsNullOrEmpty(Convert.ToString(dr["SummerBreakFromDate"])))
                    entityIntake.SummerBreakFromDate = Convert.ToDateTime(Convert.ToString(dr["SummerBreakFromDate"]));
                if (!string.IsNullOrEmpty(Convert.ToString(dr["SummerBreakToDate"])))
                    entityIntake.SummerBreakToDate = Convert.ToDateTime(Convert.ToString(dr["SummerBreakToDate"]));
                if (!string.IsNullOrEmpty(Convert.ToString(dr["IsSummerBreakApplicable"])))
                    entityIntake.IsSummerBreakApplicable = Convert.ToBoolean(dr["IsSummerBreakApplicable"]);

                entityIntake.Capacity = Convert.ToInt32(dr["Capacity"]);
                //entityIntake.FeeID = Convert.ToInt32(dr["FeeID"]); 
                entityIntake.Month = Convert.ToInt32(string.IsNullOrEmpty(dr["Month"].ToString())?0: dr["Month"]);
                entityIntake.ProgrammeID = Convert.ToInt32(dr["ProgrammeID"]);
                entityIntake.Year = Convert.ToInt32(string.IsNullOrEmpty(dr["Year"].ToString()) ? 0 : dr["Year"]);
                entityIntake.IntakeID = Convert.ToInt32(dr["IntakeID"]);
                entityIntake.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listIntake.Add(entityIntake);
            }

            return PartialView("GridViewPartial", listIntake);


        }

        public ActionResult IntakePopup()
        {
            DataSet dsIntake = balIntake.BALIntake_Get_All();
            var dtIntake = dsIntake.Tables[0];
            return PartialView("IntakePopup", dtIntake);
        }

        public ActionResult GetIntakeOfferData(int IkID)
        {
            DataSet dsOffer = balOffer.BALOffer_Get_By_IntakeID(IkID);
            var dtOffer = dsOffer.Tables[0];
            return PartialView("OfferViewPartial", dtOffer);
        }
        public ActionResult GetCampusData()
        {
            DataSet dsCampus = balCampus.BALCampus_Get_All();
            var dtCampus = dsCampus.Tables[0];
            return PartialView("CampusViewPartial", dtCampus);
        }

        public ActionResult CheckUniqueCode(string Code, string IntakeID)
        {
            var result =balIntake.Check_For_UniqueCode(Code, IntakeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityIntake Intake)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balIntake.BALIntake_SET(Intake);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsIntake = balIntake.BALIntake_Get_All();
            var dtIntake = dsIntake.Tables[0];
            return PartialView("GridViewPartial", dtIntake);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityIntake Intake)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balIntake.BALIntake_Update(Intake);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsIntake = balIntake.BALIntake_Get_All();
            var dtIntake = dsIntake.Tables[0];
            return PartialView("GridViewPartial", dtIntake);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int IntakeID = -1)
        {
            if (IntakeID >= 0)
            {
                try
                {
                    balIntake.BALIntake_Delete_By_IntakeID(IntakeID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsIntake = balIntake.BALIntake_Get_All();
            var dtIntake = dsIntake.Tables[0];
            return PartialView("GridViewPartial", dtIntake);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsIntake = balIntake.BALIntake_Get_All();
            var dtIntake = dsIntake.Tables[0];
           return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                IntakeHelper.ExportGridSettings, dtIntake
            );
        }

    }
}