﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public class PlacementController : MainController
    {
        public override string Name { get { return "Placement"; } }

        BALPlacement balPlacement = new BALPlacement();
        BALClient balClient = new BALClient();
        BALClientPlacement balClientPlacement = new BALClientPlacement();
        public ActionResult Index()
        {
            DataSet dsPlacement = balPlacement.BALPlacement_Get_All();
            var dtPlacement = dsPlacement.Tables[0];
            return DemoView("Index", dtPlacement);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsPlacement = balPlacement.BALPlacement_Get_All();
            var dtPlacement = dsPlacement.Tables[0];
            List<EntityPlacement> listPlacement = new List<EntityPlacement>();

            foreach (DataRow dr in dtPlacement.Rows)
            {
                EntityPlacement entityPlacement = new EntityPlacement();
                entityPlacement.Code = dr["Code"].ToString();
                entityPlacement.Description = dr["Description"].ToString();
                entityPlacement.Address1 = dr["Address1"].ToString();
                entityPlacement.Address2 = dr["Address2"].ToString();
                entityPlacement.Address3 = dr["Address3"].ToString();
                entityPlacement.City = dr["City"].ToString();
                entityPlacement.State = dr["State"].ToString();
                entityPlacement.PostCode = dr["PostCode"].ToString();
                entityPlacement.CountryID = dr["CountryID"].ToString();
                entityPlacement.Email = dr["Email"].ToString();
                entityPlacement.Website = dr["Website"].ToString();
                entityPlacement.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                entityPlacement.PhoneStateCode = dr["PhoneStateCode"].ToString();
                entityPlacement.Phone = dr["Phone"].ToString();
                entityPlacement.PlacementID = Convert.ToInt32(dr["PlacementID"]);
                entityPlacement.DepartmentID = dr["DepartmentID"].ToString();
                entityPlacement.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listPlacement.Add(entityPlacement);
            }

            return PartialView("GridViewPartial", listPlacement);
        }

        public string AddAssociatedClient(int ClientID, int PlacementID)
        {
            EntityClientPlacement entityClientPlacement = new EntityClientPlacement();
            entityClientPlacement.PlacementID = PlacementID.ToString();
            entityClientPlacement.ClientID = ClientID;
            string errorMsg = "";

            int result = balClientPlacement.BALClientPlacement_SET(entityClientPlacement, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetClientPlacementData(int PlacementID)
        {
            DataSet dsClientPlacement = balClientPlacement.BALClientPlacement_Get_By_PlacementID(PlacementID);
            var dtClientPlacement = dsClientPlacement.Tables[0];
            return PartialView("ClientViewPartial", dtClientPlacement);
        }

        public ActionResult PlacementPopup()
        {
            DataSet dsPlacement = balPlacement.BALPlacement_Get_All();
            var dtPlacement = dsPlacement.Tables[0];
            return PartialView("PlacementPopup", dtPlacement);
        }

        public ActionResult GetClientData()
        {
            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            return PartialView("ClientViewPartial", dtClient);
        }

        public ActionResult CheckUniqueCode(string Code, string PlacementID)
        {
            var result = balPlacement.Check_For_UniqueCode(Code, PlacementID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityPlacement Placement)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balPlacement.BALPlacement_SET(Placement);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsPlacement = balPlacement.BALPlacement_Get_All();
            var dtPlacement = dsPlacement.Tables[0];
            return PartialView("GridViewPartial", dtPlacement);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityPlacement Placement)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balPlacement.BALPlacement_Update(Placement);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsPlacement = balPlacement.BALPlacement_Get_All();
            var dtPlacement = dsPlacement.Tables[0];
            return PartialView("GridViewPartial", dtPlacement);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int PlacementID = -1)
        {
            if (PlacementID >= 0)
            {
                try
                {
                    balPlacement.BALPlacement_Delete_By_PlacementID(PlacementID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Institution/Client Record.";
                    }
                }
            }
            DataSet dsPlacement = balPlacement.BALPlacement_Get_All();
            var dtPlacement = dsPlacement.Tables[0];
            return PartialView("GridViewPartial", dtPlacement);
        }

        public ActionResult DeleteClientPlacementPartial(int PlacementID, int ClientPlacementID = -1)
        {
            if (ClientPlacementID >= 0)
            {
                try
                {
                    balClientPlacement.BALClientPlacement_Delete_By_ClientPlacementID(ClientPlacementID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientPlacementData(PlacementID);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsPlacement = balPlacement.BALPlacement_Get_All();
            var dtPlacement = dsPlacement.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                PlacementHelper.ExportGridSettings, dtPlacement
            );
        }

    }
}