﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class QualificationTypeController : MainController
    {
        public override string Name { get { return "QualificationType"; } }
        BALQualificationType balQualificationType = new BALQualificationType();
        BALSchool balSchool = new BALSchool();
        public ActionResult Index()
        {
            DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            var dtQualificationType = dsQualificationType.Tables[0];
            return DemoView("Index", dtQualificationType);
        }
        public ActionResult GridViewPartial()
        {
            //DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            //var dtQualificationType = dsQualificationType.Tables[0];
            //return PartialView("GridViewPartial", dtQualificationType);

            DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            var dtQualificationType = dsQualificationType.Tables[0];
            List<EntityQualificationType> listQualificationType = new List<EntityQualificationType>();

            foreach (DataRow dr in dtQualificationType.Rows)
            {
                EntityQualificationType entityQualificationType = new EntityQualificationType();
                entityQualificationType.Code = dr["Code"].ToString();
                entityQualificationType.Description = dr["Description"].ToString();
                entityQualificationType.QualificationTypeID = Convert.ToInt32(dr["QualificationTypeID"]);
                //entityQualificationType.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listQualificationType.Add(entityQualificationType);
            }
            return PartialView("GridViewPartial", listQualificationType);
        }

        public ActionResult QualificationTypePopup()
        {
            DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            var dtQualificationType = dsQualificationType.Tables[0];
            return PartialView("QualificationTypePopup", dtQualificationType);
        }

        public ActionResult GetSchoolData()
        {
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("SchoolViewPartial", dtSchool);
        }

        public ActionResult CheckUniqueCode(string Code, string QualificationTypeID)
        {
            //Correct this
            var result =  balQualificationType.Check_For_UniqueCode(Code, QualificationTypeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityQualificationType QualificationType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balQualificationType.BALQualificationType_SET(QualificationType);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            var dtQualificationType = dsQualificationType.Tables[0];
            return PartialView("GridViewPartial", dtQualificationType);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityQualificationType QualificationType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balQualificationType.BALQualificationType_Update(QualificationType);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            var dtQualificationType = dsQualificationType.Tables[0];
            return PartialView("GridViewPartial", dtQualificationType);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int QualificationTypeID = -1)
        {
            if (QualificationTypeID >= 0)
            {
                try
                {
                    balQualificationType.BALQualificationType_Delete_By_QualificationTypeID(QualificationTypeID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Qualification Record.";
                    }
                }
            }
            DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            var dtQualificationType = dsQualificationType.Tables[0];
            return PartialView("GridViewPartial", dtQualificationType);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsQualificationType = balQualificationType.BALQualificationType_Get_All();
            var dtQualificationType = dsQualificationType.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                QualificationTypeHelper.ExportGridSettings, dtQualificationType
            );
        }
    }
}