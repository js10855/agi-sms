﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;
using System.Reflection;

namespace AgiCorp.Controllers
{
    public class ModuleController : MainController
    {
        public override string Name { get { return "Module"; } }

        BALModule balModule = new BALModule();
        BALLO balLO = new BALLO();
        BALStaff balStaff = new BALStaff();
        //BALAssessments balAssessments = new BALAssessments();
        BALModuleAssessment balModuleAssessment = new BALModuleAssessment();
        BALStaffModule balStaffModule = new BALStaffModule();
        BALModuleLO balModuleLO = new BALModuleLO();
        public ActionResult Index()
        {
            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return DemoView("Index", dtModule);
        }
        public ActionResult GridViewPartial()
        {
            //DataSet dsModule = balModule.BALModule_Get_All();
            //var dtModule = dsModule.Tables[0];
            //return PartialView("GridViewPartial", dtModule);

            DataSet dsProgramme = balModule.BALModule_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            List<EntityModule> listPrgm = new List<EntityModule>();

            foreach (DataRow dr in dtProgramme.Rows)
            {
                EntityModule entityModule = new EntityModule();
                entityModule.Code = dr["Code"].ToString();
                entityModule.Description = dr["Description"].ToString();
                //entityModule.NZQACode = Convert.ToInt32(dr["NZQACode"]);
                entityModule.Credits = Convert.ToInt32(dr["Credits"]);
                entityModule.HoursPerCredit = Convert.ToInt32(dr["HoursPerCredit"]);
                entityModule.TotalHoursPerModule = Convert.ToInt32(dr["TotalHoursPerModule"]);
                entityModule.F2FHours = Convert.ToInt32(dr["F2FHours"]);
                entityModule.SDHours = Convert.ToInt32(dr["SDHours"]);
                entityModule.MinPassPercentage = Convert.ToInt32(dr["MinPassPercentage"]);
                entityModule.ModuleID = Convert.ToInt32(dr["ModuleID"]);
                entityModule.IsActive = Convert.ToBoolean(dr["IsActive"]);
                entityModule.ClassF2F = Convert.ToInt32(string.IsNullOrEmpty(dr["ClassF2F"].ToString()) ? 0 : dr["ClassF2F"]);
                entityModule.PlacementF2F = Convert.ToInt32(string.IsNullOrEmpty(dr["PlacementF2F"].ToString()) ? 0 : dr["PlacementF2F"]);
                listPrgm.Add(entityModule);
            }
            return PartialView("GridViewPartial", listPrgm);
        }

        public ActionResult ModulePopup()
        {
            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return PartialView("ModulePopup", dtModule);
        }

        public ActionResult GetLOData()
        {
            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return PartialView("LOViewPartial", dtLO);
        }

        public string AddAssociatedLO(int LOID, int ModuleID)
        {
            EntityModuleLO entityModuleLO = new EntityModuleLO();
            entityModuleLO.ModuleID = ModuleID;
            entityModuleLO.LOID = LOID;
            string errorMsg = "";

            int result = balModuleLO.BALModuleLO_SET(entityModuleLO, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetModuleLOData(int ModuleID)
        {
            DataSet dsModuleLO = balModuleLO.BALModuleLO_Get_By_ModuleID(ModuleID);
            var dtModuleLO = dsModuleLO.Tables[0];
            return PartialView("LOViewPartial", dtModuleLO);
        }

        public ActionResult DeleteModuleLOPartial(int ModuleID, int ModuleLOID = -1)
        {
            if (ModuleLOID >= 0)
            {
                try
                {
                    balModuleLO.BALModuleLO_Delete_By_ModuleLOID(ModuleLOID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }            
            return GetModuleLOData(ModuleID);
        }

        public ActionResult GetStaffData()
        {
            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return PartialView("StaffViewPartial", dtStaff);
        }

        public string AddAssociatedStaff(int ModuleID, int StaffID)
        {
            EntityStaffModule entityStaffModule = new EntityStaffModule();
            entityStaffModule.StaffID = StaffID;
            entityStaffModule.ModuleID = ModuleID;
            string errorMsg = "";

            int result = balStaffModule.BALStaffModule_SET(entityStaffModule, out errorMsg);
            return errorMsg;
        }
        public ActionResult GetStaffModuleData(int ModuleID)
        {
            DataSet dsStaffModule = balStaffModule.BALStaffModule_Get_By_ModuleID(ModuleID);
            var dtStaffModule = dsStaffModule.Tables[0];
            return PartialView("StaffViewPartial", dtStaffModule);
        }

        public ActionResult DeleteStaffModulePartial(int ModuleID, int StaffModuleID = -1)
        {
            if (StaffModuleID >= 0)
            {
                try
                {
                    balStaffModule.BALStaffModule_Delete_By_StaffModuleID(StaffModuleID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetStaffModuleData(ModuleID);
        }

        public ActionResult GetModuleAssessmentData(int ModuleID)
        {
            DataSet dsAssessments = balModuleAssessment.BALModuleAssessment_Get_By_ModuleID(ModuleID);
            var dtAssessments = dsAssessments.Tables[0];
            return PartialView("AssessmentsViewPartial", dtAssessments);
        }
          

       
       

        int[] GetArrayToBind(string LOCovered)
        {
            if (String.IsNullOrEmpty(LOCovered)) return new int[0];
            string[] ids = LOCovered.Split(',');
            int[] intArray = Array.ConvertAll(ids,
                                      delegate (string s) { return int.Parse(s); });
            return intArray;
        }
        public ActionResult LOMultiSelectPartial(int? CurrentID)
        {
            EntityModuleAssessment model = new EntityModuleAssessment();
            model.ModuleAssessmentID = -1;
            model.LOCovered = String.Empty;
            if (CurrentID > -1)
            {
                DataSet dsAssessments = balModuleAssessment.BALModuleAssessment_Get_All();
                DataTable table = dsAssessments.Tables[0];
                var row = table.AsEnumerable().First(a => a.Field<int>("ModuleAssessmentID") == CurrentID);
                foreach (var prop in model.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = model.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(model, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            ViewData["IDs"] = this.GetArrayToBind(model.LOCovered);
            return PartialView("LOMultiSelectPartial", model);
        }

        [ValidateInput(false)]
        public ActionResult ModuleAssessmentAddNewPartial(EntityModuleAssessment ModuleAssessment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ModuleAssessment.ModuleID = Convert.ToInt32(Session["ModuleID"]);
                    //ModuleAssessment.StaffID = TextBoxExtension.GetValue<Int32>("PassportNumber");
                    balModuleAssessment.BALModuleAssessment_SET(ModuleAssessment);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetModuleAssessmentData(ModuleAssessment.ModuleID);

        }



              [ValidateInput(false)]
        public ActionResult ModuleAssessmentUpdatePartial(EntityModuleAssessment ModuleAssessment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ModuleAssessment.ModuleID = Convert.ToInt32(Session["ModuleID"]);
                    balModuleAssessment.BALModuleAssessment_Update(ModuleAssessment);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetModuleAssessmentData(ModuleAssessment.ModuleID);

        }
        [ValidateInput(false)]
        public ActionResult ModuleAssessmentDeletePartial(int ModuleAssessmentID = -1)
        {
            if (ModuleAssessmentID >= 0)
            {
                try
                {
                    balModuleAssessment.BALModuleAssessment_Delete_By_ModuleAssessmentID(ModuleAssessmentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetModuleAssessmentData(Convert.ToInt32(Session["ModuleID"]));
            //DataSet dsModuleAssessment = balModuleAssessment.BALModuleAssessment_Get_All();
            //var dtModuleAssessment = dsModuleAssessment.Tables[0];
            //return PartialView("ModuleAssessmentViewPartial", dtModuleAssessment);
        }


        public ActionResult CheckUniqueCode(string Code,string ModuleID)
        {
            var result = balModule.Check_For_UniqueCode(Code, ModuleID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityModule Module)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balModule.BALModule_SET(Module);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return PartialView("GridViewPartial", dtModule);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityModule Module)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balModule.BALModule_Update(Module);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return PartialView("GridViewPartial", dtModule);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int ModuleID = -1)
        {
            if (ModuleID >= 0)
            {
                try
                {
                    balModule.BALModule_Delete_By_ModuleID(ModuleID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Programme Record.";

                    }
                }
            }
            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return PartialView("GridViewPartial", dtModule);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                ModuleHelper.ExportGridSettings, dtModule
            );
        }

    }
}