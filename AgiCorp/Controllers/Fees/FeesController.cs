﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class FeesController : MainController
    {
        public override string Name { get { return "Fees"; } }
        BALFees balFees = new BALFees();
        BALBuilding balBuilding = new BALBuilding();
        public ActionResult Index()
        {
            DataSet dsFees = balFees.BALFees_Get_All();
            var dtFees = dsFees.Tables[0];
            return DemoView("Index", dtFees);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsFees = balFees.BALFees_Get_All();
            var dtFees = dsFees.Tables[0];
            List<EntityFees> listInst = new List<EntityFees>();

            foreach (DataRow dr in dtFees.Rows)
            {
                EntityFees entitycampus = new EntityFees();
                entitycampus.Code = dr["Code"].ToString();
                entitycampus.Description = dr["Description"].ToString();
                entitycampus.ProgrammeID = Convert.ToInt32(dr["ProgrammeID"].ToString());
                entitycampus.CountryID = Convert.ToInt32(dr["CountryID"].ToString());
                entitycampus.TutionFees = Convert.ToInt32(dr["TutionFees"].ToString());
                entitycampus.MaterialFees = Convert.ToInt32(dr["MaterialFees"].ToString());
                entitycampus.Insurance = Convert.ToInt32(dr["Insurance"].ToString());
                entitycampus.RegistrationFees = Convert.ToInt32(dr["RegistrationFees"].ToString());
                entitycampus.AdministrationFees = Convert.ToInt32(dr["AdministrationFees"].ToString());
                entitycampus.OtherFees = Convert.ToInt32(dr["OtherFees"].ToString());
                entitycampus.TotalFees = Convert.ToInt32(string.IsNullOrEmpty(dr["TotalFees"].ToString())? Convert.ToString(0) : dr["TotalFees"].ToString());
                entitycampus.AirportPickupFees = string.IsNullOrEmpty(dr["AirportPickupFees"].ToString()) ? Convert.ToString(string.Empty) : dr["AirportPickupFees"].ToString();
                entitycampus.HomeStayPlacementFees = string.IsNullOrEmpty(dr["HomeStayPlacementFees"].ToString()) ? Convert.ToString(0) : dr["HomeStayPlacementFees"].ToString();
                entitycampus.HomeStayFeesPerWeek = string.IsNullOrEmpty(dr["HomeStayFeesPerWeek"].ToString()) ? Convert.ToString(0) : dr["HomeStayFeesPerWeek"].ToString();
                entitycampus.StartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                entitycampus.EndDate = Convert.ToDateTime(dr["EndDate"].ToString());
                entitycampus.FeeID = Convert.ToInt32(dr["FeeID"]);
                entitycampus.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listInst.Add(entitycampus);
            }
            return PartialView("GridViewPartial", listInst);
        }

        public ActionResult FeesPopup()
        {
            DataSet dsFees = balFees.BALFees_Get_All();
            var dtFees = dsFees.Tables[0];
            return PartialView("FeesPopup", dtFees);
        }

        public ActionResult GetBuildingData()
        {
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("BuildingViewPartial", dtBuilding);
        }

        public ActionResult CheckUniqueCode(string Code,string FeeID)
        {            
            var result =  balFees.Check_For_UniqueCode(Code,FeeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityFees Fees)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balFees.BALFees_SET(Fees);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsFees = balFees.BALFees_Get_All();
            var dtFees = dsFees.Tables[0];
            return PartialView("GridViewPartial", dtFees);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityFees Fees)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balFees.BALFees_Update(Fees);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsFees = balFees.BALFees_Get_All();
            var dtFees = dsFees.Tables[0];
            return PartialView("GridViewPartial", dtFees);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int FeeID = -1)
        {
            if (FeeID >= 0)
            {
                try
                {
                    balFees.BALFees_Delete_By_FeesID(FeeID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsFees = balFees.BALFees_Get_All();
            var dtFees = dsFees.Tables[0];
            return PartialView("GridViewPartial", dtFees);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsFees = balFees.BALFees_Get_All();
            var dtFees = dsFees.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                FeesHelper.ExportGridSettings, dtFees
            );
        }
    }
}