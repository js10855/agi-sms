﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class ClassController : MainController
    {
        public override string Name { get { return "Class"; } }
        BALClass balClass = new BALClass();
        BALDepartment balDepartment = new BALDepartment();
        BALEquipment balEquipment = new BALEquipment();
        BALClassDepartment balClassDepartment = new BALClassDepartment();
        BALClassEquipment balClassEquipment = new BALClassEquipment();
        public ActionResult Index()
        {
            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return DemoView("Index", dtClass);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            List<EntityClass> listInst = new List<EntityClass>();

            foreach (DataRow dr in dtClass.Rows)
            {
                EntityClass entityClass = new EntityClass();
                entityClass.Code = dr["Code"].ToString();
                entityClass.Description = dr["Description"].ToString();
                entityClass.BuildingID = dr["BuildingID"].ToString();
                entityClass.StudentCapacity = Convert.ToInt32(dr["StudentCapacity"]);
                entityClass.ClassID = Convert.ToInt32(dr["ClassID"]);
                entityClass.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listInst.Add(entityClass);
            }
            return PartialView("GridViewPartial", listInst);
        }

        public string AddAssociatedDepartment(int DepartmentID, int ClassID)
        {
            EntityClassDepartment entityClassDepartment = new EntityClassDepartment();
            entityClassDepartment.ClassID = ClassID;
            entityClassDepartment.DepartmentID = DepartmentID;
            string errorMsg = "";

            int result = balClassDepartment.BALClassDepartment_SET(entityClassDepartment, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetClassDepartmentData(int ClassID)
        {
            DataSet dsClassDepartment = balClassDepartment.BALClassDepartment_Get_By_ClassID(ClassID);
            var dtClassDepartment = dsClassDepartment.Tables[0];
            return PartialView("DepartmentViewPartial", dtClassDepartment);
        }

        public string AddAssociatedEquipment(int EquipmentID, int ClassID)
        {
            EntityClassEquipment entityClassEquipment = new EntityClassEquipment();
            entityClassEquipment.ClassID = ClassID;
            entityClassEquipment.EquipmentID = EquipmentID;
            string errorMsg = "";

            int result = balClassEquipment.BALClassEquipment_SET(entityClassEquipment, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetClassEquipmentData(int ClassID)
        {
            DataSet dsClassEquipment = balClassEquipment.BALClassEquipment_Get_By_ClassID(ClassID);
            var dtClassEquipment = dsClassEquipment.Tables[0];
            return PartialView("EquipmentViewPartial", dtClassEquipment);
        }

        public ActionResult ClassPopup()
        {
            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return PartialView("ClassPopup", dtClass);
        }

        public ActionResult GetDepartmentData()
        {
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return PartialView("DepartmentViewPartial", dtDepartment);
        }

        public ActionResult GetEquipmentData()
        {
            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            return PartialView("EquipmentViewPartial", dtEquipment);
        }

        public ActionResult CheckUniqueCode(string Code,string ClassID)
        {            
            var result =  balClass.Check_For_UniqueCode(Code,ClassID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityClass Class)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balClass.BALClass_SET(Class);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return PartialView("GridViewPartial", dtClass);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityClass Class)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balClass.BALClass_Update(Class);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return PartialView("GridViewPartial", dtClass);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int ClassID = -1)
        {
            if (ClassID >= 0)
            {
                try
                {
                    balClass.BALClass_Delete_By_ClassID(ClassID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Building/Department/Equipment Record.";
                    }
                }
            }
            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return PartialView("GridViewPartial", dtClass);
        }

        public ActionResult DeleteClassDepartmentPartial(int ClassID, int ClassDepartmentID = -1)
        {
            if (ClassDepartmentID >= 0)
            {
                try
                {
                    balClassDepartment.BALClassDepartment_Delete_By_ClassDepartmentID(ClassDepartmentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClassDepartmentData(ClassID);
        }

        public ActionResult DeleteClassEquipmentPartial(int ClassID, int ClassEquipmentID = -1)
        {
            if (ClassEquipmentID >= 0)
            {
                try
                {
                    balClassEquipment.BALClassEquipment_Delete_By_ClassEquipmentID(ClassEquipmentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClassEquipmentData(ClassID);
        }
        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                ClassHelper.ExportGridSettings, dtClass
            );
        }
    }
}