﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers.Staff
{
    public partial class StaffController : MainController
    {
        public override string Name { get { return "Staff"; } }
        BALStaff balStaff = new BALStaff();
        BALQualification balQualification = new BALQualification();
        BALModule balModule = new BALModule();
        BALStaffModule balStaffModule = new BALStaffModule();
        BALStaffQualification balStaffQualification = new BALStaffQualification();
        public ActionResult Index()
        {
            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return DemoView("Index", dtStaff);
        }

        public string AddAssociatedModule(int StaffID, int ModuleID)
        {
            EntityStaffModule entityStaffModule = new EntityStaffModule();
            entityStaffModule.StaffID = StaffID;
            entityStaffModule.ModuleID = ModuleID;
            string errorMsg = "";

            int result = balStaffModule.BALStaffModule_SET(entityStaffModule, out errorMsg);
            return errorMsg;
        }
        public ActionResult GetStaffModuleData(int StaffID)
        {
            DataSet dsStaffModule = balStaffModule.BALStaffModule_Get_By_StaffID(StaffID);
            var dtStaffModule = dsStaffModule.Tables[0];
            return PartialView("ModuleViewPartial", dtStaffModule);
        }


        public ActionResult DeleteStaffModulePartial(int StaffID, int StaffModuleID = -1)
        {
            if (StaffModuleID >= 0)
            {
                try
                {
                    balStaffModule.BALStaffModule_Delete_By_StaffModuleID(StaffModuleID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetStaffModuleData(StaffID);
        }


        public ActionResult GridViewPartial()
        {          
            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            List<EntityStaff> listStaff = new List<EntityStaff>();

            foreach (DataRow dr in dtStaff.Rows)
            {
                EntityStaff entityStaff = new EntityStaff();
                entityStaff.Code = dr["Code"].ToString();
                entityStaff.Salutation = dr["Salutation"].ToString();
                entityStaff.FirstName = dr["FirstName"].ToString();
                entityStaff.MiddleName = dr["MiddleName"].ToString();
                entityStaff.LastName = dr["LastName"].ToString();
                entityStaff.DateOfBirth = Convert.ToDateTime(dr["DateOfBirth"]);
                entityStaff.WorkEmail = dr["WorkEmail"].ToString();
                entityStaff.HomeEmail = dr["HomeEmail"].ToString();
                entityStaff.Phone = dr["Phone"].ToString();
                entityStaff.Mobile = dr["Mobile"].ToString();
                entityStaff.Address1 = dr["Address1"].ToString();
                entityStaff.Address2 = dr["Address2"].ToString();
                entityStaff.Address3 = dr["Address3"].ToString();
                entityStaff.City = dr["City"].ToString();
                entityStaff.State = dr["State"].ToString();
                //entityStaff.PostCode = dr["PostCode"].ToString();
                entityStaff.CountryID = dr["CountryID"].ToString();
                entityStaff.StaffTypeID = Convert.ToInt32(dr["StaffTypeID"]);
                entityStaff.StatusID =  Convert.ToInt32(dr["StatusID"]);              
                entityStaff.CostPerHour = Convert.ToInt32(dr["CostPerHour"]);
                entityStaff.StaffID = Convert.ToInt32(dr["StaffID"]);
                entityStaff.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listStaff.Add(entityStaff);
            }

            return PartialView("GridViewPartial", listStaff);
        }

        public ActionResult StaffPopup()
        {
            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return PartialView("StaffPopup", dtStaff);
        }

        public ActionResult GetQualificationData()
        {
            //Correct this
            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            return PartialView("QualificationViewPartial", dtQualification);
        }

        public ActionResult GetModuleData()
        {
            //Correct this
            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return PartialView("ModuleViewPartial", dtModule);
        }

        public ActionResult CheckUniqueCode(string Code,string StaffID)
        {            
           var result = balStaff.Check_For_UniqueCode(Code,StaffID);
           return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult StaffQualificationAddNewPartial(EntityStaffQualification StaffQualification)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    StaffQualification.StaffID = Convert.ToInt32(Session["StaffID"]);
                    //StaffQualification.StaffID = TextBoxExtension.GetValue<Int32>("PassportNumber");
                    balStaffQualification.BALStaffQualification_SET(StaffQualification);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetStaffQualificationData(StaffQualification.StaffID);

        }

        [ValidateInput(false)]
        public ActionResult StaffQualificationUpdatePartial(EntityStaffQualification StaffQualification)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    StaffQualification.StaffID = Convert.ToInt32(Session["StaffID"]);
                    balStaffQualification.BALStaffQualification_Update(StaffQualification);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetStaffQualificationData(StaffQualification.StaffID);

        }
        [ValidateInput(false)]
        public ActionResult StaffQualificationDeletePartial(int StaffQualificationID = -1)
        {
            if (StaffQualificationID >= 0)
            {
                try
                {
                    balStaffQualification.BALStaffQualification_Delete_By_StaffQualificationID(StaffQualificationID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetStaffQualificationData(Convert.ToInt32(Session["StaffID"]));
            //DataSet dsStaffQualification = balStaffQualification.BALStaffQualification_Get_All();
            //var dtStaffQualification = dsStaffQualification.Tables[0];
            //return PartialView("StaffQualificationViewPartial", dtStaffQualification);
        }

        public ActionResult GetStaffQualificationData(int StaffID)
        {
            DataSet dsStaffQualification = balStaffQualification.BALStaffQualification_Get_By_StaffID(StaffID);
            var dtStaffQualification = dsStaffQualification.Tables[0];
            return PartialView("QualificationViewPartial", dtStaffQualification);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityStaff Staff)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balStaff.BALStaff_SET(Staff);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return PartialView("GridViewPartial", dtStaff);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityStaff Staff)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balStaff.BALStaff_Update(Staff);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return PartialView("GridViewPartial", dtStaff);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int StaffID = -1)
        {
            if (StaffID >= 0)
            {
                try
                {
                    balStaff.BALStaff_Delete_By_StaffID(StaffID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return PartialView("GridViewPartial", dtStaff);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                StaffHelper.ExportGridSettings, dtStaff
            );
        }
    }
}