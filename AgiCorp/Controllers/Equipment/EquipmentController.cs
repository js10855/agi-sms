﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class EquipmentController : MainController
    {
        public override string Name { get { return "Equipment"; } }
        BALEquipment balEquipment = new BALEquipment();
        BALSchool balSchool = new BALSchool();
        public ActionResult Index()
        {
            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            return DemoView("Index", dtEquipment);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            List<EntityEquipment> listInst = new List<EntityEquipment>();

            foreach (DataRow dr in dtEquipment.Rows)
            {
                EntityEquipment entityEquipment = new EntityEquipment();
                entityEquipment.Code = dr["Code"].ToString();               
                entityEquipment.Description = dr["Description"].ToString();
                entityEquipment.ClassID = Convert.ToInt32(dr["ClassID"]);
                entityEquipment.EquipmentTypeID = Convert.ToInt32(dr["EquipmentTypeID"]);
                entityEquipment.EquipmentID = Convert.ToInt32(dr["EquipmentID"]);
                entityEquipment.Quantity = Convert.ToInt32(dr["Quantity"]);
                listInst.Add(entityEquipment);
            }
            return PartialView("GridViewPartial", listInst);
        }

        public ActionResult EquipmentPopup()
        {
            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            return PartialView("EquipmentPopup", dtEquipment);
        }

        public ActionResult GetSchoolData()
        {
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("SchoolViewPartial", dtSchool);
        }

        public ActionResult CheckUniqueCode(string Code,string EquipmentID)
        {           
            var result = balEquipment.Check_For_UniqueCode(Code,EquipmentID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityEquipment Equipment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balEquipment.BALEquipment_SET(Equipment);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            return PartialView("GridViewPartial", dtEquipment);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityEquipment Equipment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balEquipment.BALEquipment_Update(Equipment);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            return PartialView("GridViewPartial", dtEquipment);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int EquipmentID = -1)
        {
            if (EquipmentID >= 0)
            {
                try
                {
                    balEquipment.BALEquipment_Delete_By_EquipmentID(EquipmentID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Classroom Record.";
                    }
                }
            }
            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            return PartialView("GridViewPartial", dtEquipment);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsEquipment = balEquipment.BALEquipment_Get_All();
            var dtEquipment = dsEquipment.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                EquipmentHelper.ExportGridSettings, dtEquipment
            );
        }
    }
}