﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;
using DevExpress.Web.Mvc;
using System.Reflection;


namespace AgiCorp.Controllers
{
    public class GPOController : MainController
    {
        public override string Name { get { return "GPO"; } }

        BALGPO balGPO = new BALGPO();
        BALModule balModule = new BALModule();
        BALGPOLO balGPOLO = new BALGPOLO();
        public ActionResult Index()
        {
            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            return DemoView("Index", dtGPO);
        }

        public ActionResult GetGPOLOData(int GPOID)
        {
            DataSet dsGPOLO = balGPOLO.BALGPOLO_Get_By_GPOID(GPOID);
            var dtGPOLO = dsGPOLO.Tables[0];
            return PartialView("LOViewPartial", dtGPOLO);
        }

        public string AddAssociatedLO(int LOID, int GPOID)
        {
            EntityGPOLO entityGPOLO = new EntityGPOLO();
            entityGPOLO.GPOID = GPOID;
            entityGPOLO.LOID = LOID;
            string errorMsg = "";

            int result = balGPOLO.BALGPOLO_SET(entityGPOLO, out errorMsg);
            return errorMsg;
        }

        public ActionResult DeleteGPOLOPartial(int GPOID, int GPOLOID = -1)
        {
            if (GPOLOID >= 0)
            {
                try
                {
                    balGPOLO.BALGPOLO_Delete_By_GPOLOID(GPOLOID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetGPOLOData(GPOID);
        }

        public ActionResult GridViewPartial()
        {
            //DataSet dsGPO = balGPO.BALGPO_Get_All();
            //var dtGPO = dsGPO.Tables[0];
            //return PartialView("GridViewPartial", dtGPO);

            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            List<EntityGPO> listGPO = new List<EntityGPO>();

            foreach (DataRow dr in dtGPO.Rows)
            {
                EntityGPO entityGPO = new EntityGPO();
                entityGPO.Code = dr["Code"].ToString();
                entityGPO.Description = dr["Description"].ToString();
                entityGPO.Credits = Convert.ToInt32(dr["Credits"]);
                entityGPO.F2FHours = Convert.ToInt32(dr["F2FHours"]);
                entityGPO.SDHours = Convert.ToInt32(dr["SDHours"]);
                entityGPO.GPOID = Convert.ToInt32(dr["GPOID"]);
                //entityGPO.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listGPO.Add(entityGPO);
            }
            return PartialView("GridViewPartial", listGPO);
        }

        public ActionResult GPOPopup()
        {
            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            return PartialView("GPOPopup", dtGPO);
        }

        public ActionResult GetModuleData()
        {
            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return PartialView("ModuleViewPartial", dtModule);
        }

        public ActionResult CheckUniqueCode(string Code , string GPOID)
        {
            //correct this, 
            var result = balGPO.Check_For_UniqueCode(Code,GPOID);
            return Json(result, JsonRequestBehavior.AllowGet);
            //var result = true;
            //return result;
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityGPO GPO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balGPO.BALGPO_SET(GPO);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            return PartialView("GridViewPartial", dtGPO);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityGPO GPO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balGPO.BALGPO_Update(GPO);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            return PartialView("GridViewPartial", dtGPO);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int GPOID = -1)
        {
            if (GPOID >= 0)
            {
                try
                {
                    balGPO.BALGPO_Delete_By_GPOID(GPOID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Qualification  Record.";
                    }
                }
            }
            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            return PartialView("GridViewPartial", dtGPO);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                GPOHelper.ExportGridSettings, dtGPO
            );
        }

    }
}