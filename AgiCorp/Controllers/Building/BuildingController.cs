﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class BuildingController : MainController
    {
        public override string Name { get { return "Building"; } }
        BALBuilding balBuilding = new BALBuilding();
        BALClass balClass = new BALClass();
        BALDepartment balDepartment = new BALDepartment();
        BALBuildingDepartment balBuildingDepartment = new BALBuildingDepartment();
        BALBuildingClass balBuildingClass = new BALBuildingClass();
        public ActionResult Index()
        {
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return DemoView("Index", dtBuilding);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            List<EntityBuilding> listInst = new List<EntityBuilding>();

            foreach (DataRow dr in dtBuilding.Rows)
            {
                EntityBuilding entityBuilding = new EntityBuilding();
                entityBuilding.Code = dr["Code"].ToString();               
                entityBuilding.Description = dr["Description"].ToString();
                entityBuilding.Capacity = Convert.ToInt32( dr["Capacity"]);
                entityBuilding.CampusID = Convert.ToInt32(string.IsNullOrEmpty(dr["CampusID"].ToString())? 0: dr["CampusID"]);
                entityBuilding.BuildingID = Convert.ToInt32(dr["BuildingID"]);
                entityBuilding.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listInst.Add(entityBuilding);
            }           
            return PartialView("GridViewPartial", listInst);

        }

        public string AddAssociatedDepartment(int DepartmentID, int BuildingID)
        {
            EntityBuildingDepartment entityBuildingDepartment = new EntityBuildingDepartment();
            entityBuildingDepartment.BuildingID = BuildingID;
            entityBuildingDepartment.DepartmentID = DepartmentID;
            string errorMsg = "";

            int result = balBuildingDepartment.BALBuildingDepartment_SET(entityBuildingDepartment, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetBuildingDepartmentData(int BuildingID)
        {
            DataSet dsBuildingDepartment = balBuildingDepartment.BALBuildingDepartment_Get_By_BuildingID(BuildingID);
            var dtBuildingDepartment = dsBuildingDepartment.Tables[0];
            return PartialView("DepartmentViewPartial", dtBuildingDepartment);
        }

        public string AddAssociatedClass(int ClassID, int BuildingID)
        {
            EntityBuildingClass entityBuildingClass = new EntityBuildingClass();
            entityBuildingClass.BuildingID = BuildingID;
            entityBuildingClass.ClassID = ClassID;
            string errorMsg = "";

            int result = balBuildingClass.BALBuildingClass_SET(entityBuildingClass, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetBuildingClassData(int BuildingID)
        {
            DataSet dsBuildingClass = balBuildingClass.BALBuildingClass_Get_By_BuildingID(BuildingID);
            var dtBuildingClass = dsBuildingClass.Tables[0];
            return PartialView("ClassViewPartial", dtBuildingClass);
        }
        public ActionResult BuildingPopup()
        {
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("BuildingPopup", dtBuilding);
        }

        public ActionResult GetClassData()
        {
            DataSet dsClass = balClass.BALClass_Get_All();
            var dtClass = dsClass.Tables[0];
            return PartialView("ClassViewPartial", dtClass);
        }

        public ActionResult GetDepartmentData()
        {
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return PartialView("DepartmentViewPartial", dtDepartment);
        }

        public ActionResult CheckUniqueCode(string Code,string BuildingID)
        {            
            var result = balBuilding.Check_For_UniqueCode(Code,BuildingID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityBuilding Building)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balBuilding.BALBuilding_SET(Building);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("GridViewPartial", dtBuilding);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityBuilding Building)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balBuilding.BALBuilding_Update(Building);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("GridViewPartial", dtBuilding);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int BuildingID = -1)
        {
            if (BuildingID >= 0)
            {
                try
                {
                    balBuilding.BALBuilding_Delete_By_BuildingID(BuildingID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Department/Class Record.";

                    }
                }
            }
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("GridViewPartial", dtBuilding);
        }

        public ActionResult DeleteBuildingDepartmentPartial(int BuildingID, int BuildingDepartmentID = -1)
        {
            if (BuildingDepartmentID >= 0)
            {
                try
                {
                    balBuildingDepartment.BALBuildingDepartment_Delete_By_BuildingDepartmentID(BuildingDepartmentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetBuildingDepartmentData(BuildingID);
        }

        public ActionResult DeleteBuildingClassPartial(int BuildingID, int BuildingClassID = -1)
        {
            if (BuildingClassID >= 0)
            {
                try
                {
                    balBuildingClass.BALBuildingClass_Delete_By_BuildingClassID(BuildingClassID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetBuildingClassData(BuildingID);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                BuildingHelper.ExportGridSettings, dtBuilding
            );
        }
    }
}