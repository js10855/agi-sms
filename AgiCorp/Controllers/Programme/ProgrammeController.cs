﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public class ProgrammeController : MainController
    {
        public override string Name { get { return "Programme"; } }

        BALProgramme balProgramme = new BALProgramme();
        BALModule balModule = new BALModule();
        BALProgrammeModule balProgrammeModule = new BALProgrammeModule();
        public ActionResult Index()
        {
            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            return DemoView("Index", dtProgramme);
        }
        public ActionResult GridViewPartial()
        {
            //DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            //var dtProgramme = dsProgramme.Tables[0];
            //return PartialView("GridViewPartial", dtProgramme);

            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            List<EntityProgramme> listPrgm = new List<EntityProgramme>();

            foreach (DataRow dr in dtProgramme.Rows)
            {
                EntityProgramme entityProgramme = new EntityProgramme();
                entityProgramme.Code = dr["Code"].ToString();
                entityProgramme.Description = dr["Description"].ToString();
                entityProgramme.NZQACode = Convert.ToInt32(dr["NZQACode"]);
                entityProgramme.Credits = Convert.ToInt32(dr["Credits"]);
                entityProgramme.F2FHours = Convert.ToInt32(dr["F2FHours"]);
                entityProgramme.SDHours = Convert.ToInt32(dr["SDHours"]);
                entityProgramme.ProgrammeID = Convert.ToInt32(dr["ProgrammeID"]);
                entityProgramme.ClassF2F = Convert.ToInt32(string.IsNullOrEmpty(dr["ClassF2F"].ToString()) ? 0 : dr["ClassF2F"]);
                entityProgramme.PlacementF2F = Convert.ToInt32(string.IsNullOrEmpty(dr["PlacementF2F"].ToString()) ? 0 : dr["PlacementF2F"]);

                entityProgramme.ProgrammeLevelID = Convert.ToString(string.IsNullOrEmpty(Convert.ToString(dr["ProgrammeLevelID"])) ? 0 : Convert.ToInt32(dr["ProgrammeLevelID"]));
                entityProgramme.ProgrammeLength = string.IsNullOrEmpty(Convert.ToString(dr["ProgrammeLength"])) ? Convert.ToString(0) : Convert.ToString(dr["ProgrammeLength"]);
                //if(!string.IsNullOrEmpty(Convert.ToString(dr["StartDate"])))
                //    entityProgramme.StartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                ////entityProgramme.StartDate= Convert.ToDateTime(Convert.ToString(dr["StartDate"]));
                //if (!string.IsNullOrEmpty(Convert.ToString(dr["EndDate"])))
                //    entityProgramme.EndDate = Convert.ToDateTime(Convert.ToString(dr["EndDate"]));
                //if (!string.IsNullOrEmpty(Convert.ToString(dr["SemesterBreakFromDate"])))
                //    entityProgramme.SemesterBreakFromDate = Convert.ToDateTime(Convert.ToString(dr["SemesterBreakFromDate"]));
                //if (!string.IsNullOrEmpty(Convert.ToString(dr["SemesterBreakToDate"])))
                //    entityProgramme.SemesterBreakToDate = Convert.ToDateTime(Convert.ToString(dr["SemesterBreakToDate"]));
                //if (!string.IsNullOrEmpty(Convert.ToString(dr["IsSemesterBreakApplicable"])))
                //    entityProgramme.IsSemesterBreakApplicable = Convert.ToBoolean(dr["IsSemesterBreakApplicable"]);
                //if (!string.IsNullOrEmpty(Convert.ToString(dr["SummerBreakFromDate"])))
                //    entityProgramme.SummerBreakFromDate = Convert.ToDateTime(Convert.ToString(dr["SummerBreakFromDate"]));
                //if (!string.IsNullOrEmpty(Convert.ToString(dr["SummerBreakToDate"])))
                //    entityProgramme.SummerBreakToDate = Convert.ToDateTime(Convert.ToString(dr["SummerBreakToDate"]));
                if (!string.IsNullOrEmpty(Convert.ToString(dr["ProgrammeRatingID"])))
                    entityProgramme.ProgrammeRatingID = Convert.ToString(dr["ProgrammeRatingID"]);

                //entityProgramme.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listPrgm.Add(entityProgramme);
            }
            return PartialView("GridViewPartial", listPrgm);
        }

        public ActionResult ProgrammePopup()
        {
            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            return PartialView("ProgrammePopup", dtProgramme);
        }

        public string AddAssociatedModule(int ModuleID, int ProgrammeID)
        {
            EntityProgrammeModule entityProgrammeModule = new EntityProgrammeModule();
            entityProgrammeModule.ProgrammeID = ProgrammeID;
            entityProgrammeModule.ModuleID = ModuleID;
            string errorMsg = "";

            int result = balProgrammeModule.BALProgrammeModule_SET(entityProgrammeModule, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetProgrammeModuleData(int ProgrammeID)
        {
            DataSet dsProgrammeModule = balProgrammeModule.BALProgrammeModule_Get_By_ProgrammeID(ProgrammeID);
            var dtProgrammeModule = dsProgrammeModule.Tables[0];
            return PartialView("ModuleViewPartial", dtProgrammeModule);
        }

        public ActionResult DeleteProgrammeModulePartial(int ProgrammeID, int ProgrammeModuleID = -1)
        {
            if (ProgrammeModuleID >= 0)
            {
                try
                {
                    balProgrammeModule.BALProgrammeModule_Delete_By_ProgrammeModuleID(ProgrammeModuleID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }            
            return GetProgrammeModuleData(ProgrammeID);
        }

        public ActionResult GetModuleData()
        {
            DataSet dsModule = balModule.BALModule_Get_All();
            var dtModule = dsModule.Tables[0];
            return PartialView("ModuleViewPartial", dtModule);
        }

        public ActionResult CheckUniqueCode(string Code,string ProgrammeID)
        {
            var result = balProgramme.Check_For_UniqueCode(Code, ProgrammeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityProgramme Programme)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balProgramme.BALProgramme_SET(Programme);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            return PartialView("GridViewPartial", dtProgramme);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityProgramme Programme)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balProgramme.BALProgramme_Update(Programme);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            return PartialView("GridViewPartial", dtProgramme);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int ProgrammeID = -1)
        {
            if (ProgrammeID >= 0)
            {
                try
                {
                    balProgramme.BALProgramme_Delete_By_ProgrammeID(ProgrammeID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Campus/Department/Class/Qualification/Module Record.";
                    }
                }
            }
            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            return PartialView("GridViewPartial", dtProgramme);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                ProgrammeHelper.ExportGridSettings, dtProgramme
            );
        }

    }
}