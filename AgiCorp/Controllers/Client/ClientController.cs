﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;
using DevExpress.Web.Mvc;
using DevExpress.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.IO;

namespace AgiCorp.Controllers
{
    public class ClientController : MainController
    {
        public override string Name { get { return "Client"; } }

        BALClient balClient = new BALClient();
        BALClientPassport balClientPassport = new BALClientPassport();
        BALClientEnglishProficiency balClientEnglishProficiency = new BALClientEnglishProficiency();
        BALClientAddress balClientAddress = new BALClientAddress();
        BALClientAssessments balClientAssessments = new BALClientAssessments();
        BALClientAttendance balClientAttendance = new BALClientAttendance();
        BALClientDocument balClientDocument = new BALClientDocument();
        BALClientDocumentType balClientDocumentType = new BALClientDocumentType();
        BALClientGrade balClientGrade = new BALClientGrade();
        BALClientOfferOfPlace balClientOfferOfPlace = new BALClientOfferOfPlace();
        BALClientPlacement balClientPlacement = new BALClientPlacement();
        BALClientType balClientType = new BALClientType();
        BALClientWorkExperience balClientWorkExperience = new BALClientWorkExperience();
        
        public ActionResult Index()
        {
            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            return DemoView("Index", dtClient);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            List<EntityClient> listClient = new List<EntityClient>();

            foreach (DataRow dr in dtClient.Rows)
            {
                EntityClient entityClient = new EntityClient();
                entityClient.StudentReference = dr["StudentReference"].ToString();
                entityClient.FirstName = dr["FirstName"].ToString();
                entityClient.MiddleName = dr["MiddleName"].ToString();
                entityClient.LastName = dr["LastName"].ToString();
                entityClient.DateOfBirth = Convert.ToDateTime(dr["DateOfBirth"]);
                entityClient.Photo = (byte[])(dr["Photo"].Equals(DBNull.Value)? System.Text.Encoding.UTF8.GetBytes(String.Empty): dr["Photo"]);
                //entityClient.Photo = (UploadedFile) dr["Photo"];
                entityClient.PassportNumber = dr["PassportNumber"].ToString();
                entityClient.ResidenceID = Convert.ToInt32(dr["ResidenceID"]);
                entityClient.ClientTypeID = Convert.ToInt32(dr["ClientTypeID"]);
                entityClient.NSNNumber = dr["NSNNumber"].ToString();
                entityClient.WorkNumber = dr["WorkNumber"].ToString();
                entityClient.PublicTrustNumber = dr["PublicTrustNumber"].ToString();
                entityClient.ExternalRef1 = dr["ExternalRef1"].ToString();
                entityClient.ExternalRef2 = dr["ExternalRef2"].ToString();
                entityClient.Address1 = dr["Address1"].ToString();
                entityClient.Address2 = dr["Address2"].ToString();
                entityClient.Address3 = dr["Address3"].ToString();
                entityClient.City = dr["City"].ToString();
                entityClient.State = dr["State"].ToString();
                entityClient.PostCode = dr["PostCode"].ToString();
                entityClient.CountryID = dr["CountryID"].ToString();
                entityClient.Email = dr["Email"].ToString();
                entityClient.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                entityClient.PhoneStateCode = dr["PhoneStateCode"].ToString();
                entityClient.Phone = dr["Phone"].ToString();
                entityClient.EthnicityID = Convert.ToInt32(dr["EthnicityID"]);
                entityClient.IwiAffiliation1 = dr["IwiAffiliation1"].ToString();
                entityClient.IwiAffiliation2 = dr["IwiAffiliation2"].ToString();
                entityClient.IwiAffiliation3 = dr["IwiAffiliation3"].ToString();
                entityClient.IwiAffiliation4 = dr["IwiAffiliation4"].ToString();
                entityClient.IwiAffiliation5 = dr["IwiAffiliation5"].ToString();
                entityClient.AgentID = Convert.ToInt32(dr["AgentID"]);
                entityClient.PhotoFileName = dr["PhotoFileName"].ToString();
                entityClient.ClientGenderID = dr["ClientGenderID"].ToString();
                entityClient.ClientNationalityID = dr["ClientNationalityID"].ToString();
                entityClient.ClientVisaTypeID = dr["ClientvisatypeID"].ToString();
                entityClient.StudentType = dr["StudentType"].ToString();
                entityClient.ClientID = Convert.ToInt32(dr["ClientID"]);
                entityClient.IRDNumber = dr["IRDNumber"].ToString();
                entityClient.NZPermanantResidence = Convert.ToBoolean(string.IsNullOrEmpty(dr["NZPermanantResidence"].ToString())?false:dr["NZPermanantResidence"]);               
                listClient.Add(entityClient);
            }

            return PartialView("GridViewPartial", listClient);


        }

        public ActionResult BinaryImageColumnPhotoUpdate()
        {
            return BinaryImageEditExtension.GetCallbackResult();
        }

        public ActionResult ImageUpload()
        {
            return BinaryImageEditExtension.GetCallbackResult();
        }

        public ActionResult ClientPopup()
        {
            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            return PartialView("ClientPopup", dtClient);
        }

        public ActionResult GetClientPassportData(int ClntID)
        {
            DataSet dsClientPassport = balClientPassport.BALClientPassport_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientPassportViewPartial", dtClientPassport);
        }

        public ActionResult CheckUniqueCode(string StudentReference, string ClientID)
        {
            var result = balClient.Check_For_UniqueCode(StudentReference, ClientID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityClient Client)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //List<byte[]> listUploadedFile = new List<byte[]>();
                    //listUploadedFile = (System.Collections.Generic.List<byte[]>)Session["UploadedFile"];
                    //Client.Photo = listUploadedFile[0];
                    //Client.PhotoFileName = Session["PhotoFileName"].ToString();
                    Client.Photo = BinaryImageEditExtension.GetValue<byte[]>("imgBack_");
                    balClient.BALClient_SET(Client);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            return PartialView("GridViewPartial", dtClient);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityClient Client)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //List<byte[]> listUploadedFile = new List<byte[]>();
                    //listUploadedFile = (System.Collections.Generic.List<byte[]>)Session["UploadedFile"];
                    //Client.Photo = listUploadedFile[0]; 
                    //Client.PhotoFileName = Session["PhotoFileName"].ToString();
                    Client.Photo = BinaryImageEditExtension.GetValue<byte[]>("imgBack_");
                    balClient.BALClient_Update(Client);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            return PartialView("GridViewPartial", dtClient);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int ClientID = -1)
        {
            if (ClientID >= 0)
            {
                try
                {
                    balClient.BALClient_Delete_By_ClientID(ClientID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            return PartialView("GridViewPartial", dtClient);
        }

        [ValidateInput(false)]
        public ActionResult ClientPassportAddNewPartial(EntityClientPassport ClientPassport)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientPassport.ClientID = Convert.ToInt32(Session["ClntID"]);
                    //ClientPassport.ClientID = TextBoxExtension.GetValue<Int32>("PassportNumber");
                    balClientPassport.BALClientPassport_SET(ClientPassport);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientPassportData(ClientPassport.ClientID);
            //DataSet dsClientPassport = balClientPassport.BALClientPassport_Get_All();
            //var dtClientPassport = dsClientPassport.Tables[0];
            //return PartialView("ClientPassportViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientPassportUpdatePartial(EntityClientPassport ClientPassport)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientPassport.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientPassport.BALClientPassport_Update(ClientPassport);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientPassportData(ClientPassport.ClientID);
            //DataSet dsClientPassport = balClientPassport.BALClientPassport_Get_All();
            //var dtClientPassport = dsClientPassport.Tables[0];
            //return PartialView("ClientPassportViewPartial", dtClientPassport);
        }
        [ValidateInput(false)]
        public ActionResult ClientPassportDeletePartial(int ClientPassportID = -1)
        {
            if (ClientPassportID >= 0)
            {
                try
                {
                    balClientPassport.BALClientPassport_Delete_By_ClientPassportID(ClientPassportID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientPassportData(Convert.ToInt32(Session["ClntID"]));
            //DataSet dsClientPassport = balClientPassport.BALClientPassport_Get_All();
            //var dtClientPassport = dsClientPassport.Tables[0];
            //return PartialView("ClientPassportViewPartial", dtClientPassport);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsClient = balClient.BALClient_Get_All();
            var dtClient = dsClient.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                ClientHelper.ExportGridSettings, dtClient
            );
        }

        //public ActionResult BinaryImageColumnPhotoUpdate()
        //{
        //    return BinaryImageEditExtension.GetCallbackResult();
        //}

        //public ActionResult ImageUpload()
        //{
        //    UploadControlExtension.GetUploadedFiles("Photo", UploadControlHelper.ValidationSettings, UploadControlHelper.uploadControl_FileUploadComplete);
        //    return null;
        //}


        public ActionResult GetClientEnglishProficiencyData(int ClntID)
        {
            DataSet dsClientPassport = balClientEnglishProficiency.BALClientEnglishProficiency_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientEnglishProficiencyViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientEnglishProficiencyAddNewPartial(EntityClientEnglishProficiency ClientEnglishProficiency)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientEnglishProficiency.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientEnglishProficiency.BALClientEnglishProficiency_SET(ClientEnglishProficiency);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientEnglishProficiencyData(ClientEnglishProficiency.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientEnglishProficiencyUpdatePartial(EntityClientEnglishProficiency ClientEnglishProficiency)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientEnglishProficiency.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientEnglishProficiency.BALClientEnglishProficiency_Update(ClientEnglishProficiency);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientEnglishProficiencyData(ClientEnglishProficiency.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientEnglishProficiencyDeletePartial(int ClientEnglishProficiencyID = -1)
        {
            if (ClientEnglishProficiencyID >= 0)
            {
                try
                {
                    balClientEnglishProficiency.BALClientEnglishProficiency_Delete_By_ClientEnglishProficiencyID(ClientEnglishProficiencyID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientEnglishProficiencyData(Convert.ToInt32(Session["ClntID"]));
        }

        public ActionResult GetClientAddressData(int ClntID)
        {
            DataSet dsClientAddress = balClientAddress.BALClientAddress_Get_By_ClientID(ClntID);
            var dtClientAddress = dsClientAddress.Tables[0];
            return PartialView("ClientAddressViewPartial", dtClientAddress);
        }

        [ValidateInput(false)]
        public ActionResult ClientAddressAddNewPartial(EntityClientAddress ClientAddress)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientAddress.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientAddress.BALClientAddress_SET(ClientAddress);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientAddressData(ClientAddress.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientAddressUpdatePartial(EntityClientAddress ClientAddress)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientAddress.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientAddress.BALClientAddress_Update(ClientAddress);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientAddressData(ClientAddress.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientAddressDeletePartial(int ClientAddressID = -1)
        {
            if (ClientAddressID >= 0)
            {
                try
                {
                    balClientAddress.BALClientAddress_Delete_By_ClientAddressID(ClientAddressID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientAddressData(Convert.ToInt32(Session["ClntID"]));
        }



        public ActionResult GetClientAssessmentsData(int ClntID)
        {
            DataSet dsClientPassport = balClientAssessments.BALClientAssessments_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientAssessmentsViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientAssessmentsAddNewPartial(EntityClientAssessments ClientAssessments)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientAssessments.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientAssessments.BALClientAssessments_SET(ClientAssessments);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientAssessmentsData(ClientAssessments.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientAssessmentsUpdatePartial(EntityClientAssessments ClientAssessments)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientAssessments.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientAssessments.BALClientAssessments_Update(ClientAssessments);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientAssessmentsData(ClientAssessments.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientAssessmentsDeletePartial(int ClientAssessmentID = -1)
        {
            if (ClientAssessmentID >= 0)
            {
                try
                {
                    balClientAssessments.BALClientAssessments_Delete_By_ClientAssessmentID(ClientAssessmentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientAssessmentsData(Convert.ToInt32(Session["ClntID"]));
        }




        public ActionResult GetClientAttendanceData(int ClntID)
        {
            DataSet dsClientPassport = balClientAttendance.BALClientAttendance_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientAttendanceViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientAttendanceAddNewPartial(EntityClientAttendance ClientAttendance)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientAttendance.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientAttendance.BALClientAttendance_SET(ClientAttendance);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientAttendanceData(ClientAttendance.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientAttendanceUpdatePartial(EntityClientAttendance ClientAttendance)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientAttendance.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientAttendance.BALClientAttendance_Update(ClientAttendance);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientAttendanceData(ClientAttendance.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientAttendanceDeletePartial(int ClientAttendanceID = -1)
        {
            if (ClientAttendanceID >= 0)
            {
                try
                {
                    balClientAttendance.BALClientAttendance_Delete_By_ClientAttendanceID(ClientAttendanceID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientAttendanceData(Convert.ToInt32(Session["ClntID"]));
        }


        public ActionResult GetClientDocumentData(int ClntID)
        {
            DataSet dsClientPassport = balClientDocument.BALClientDocument_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientDocumentViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientDocumentAddNewPartial(EntityClientDocument ClientDocument)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientDocument.ClientID = Convert.ToInt32(Session["ClntID"]);                   
                    List<byte[]> listUploadedFile = new List<byte[]>();
                    listUploadedFile = (System.Collections.Generic.List<byte[]>)Session["DocsUploadedFile"];
                    ClientDocument.File = listUploadedFile[0];
                    ClientDocument.DocsFileName = Session["DocsFileName"].ToString();
                    balClientDocument.BALClientDocument_SET(ClientDocument);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientDocumentData(ClientDocument.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientDocumentUpdatePartial(EntityClientDocument ClientDocument)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientDocument.ClientID = Convert.ToInt32(Session["ClntID"]);
                    List<byte[]> listUploadedFile = new List<byte[]>();
                    listUploadedFile = (System.Collections.Generic.List<byte[]>)Session["DocsUploadedFile"];
                    ClientDocument.File = listUploadedFile[0];
                    ClientDocument.DocsFileName = Session["DocsFileName"].ToString();
                    balClientDocument.BALClientDocument_Update(ClientDocument);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientDocumentData(ClientDocument.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientDocumentDeletePartial(int ClientDocumentID = -1)
        {
            if (ClientDocumentID >= 0)
            {
                try
                {
                    balClientDocument.BALClientDocument_Delete_By_ClientDocumentID(ClientDocumentID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientDocumentData(Convert.ToInt32(Session["ClntID"]));
        }



        public ActionResult GetClientGradeData(int ClntID)
        {
            DataSet dsClientPassport = balClientGrade.BALClientGrade_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientGradeViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientGradeAddNewPartial(EntityClientGrade ClientGrade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientGrade.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientGrade.BALClientGrade_SET(ClientGrade);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientGradeData(ClientGrade.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientGradeUpdatePartial(EntityClientGrade ClientGrade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientGrade.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientGrade.BALClientGrade_Update(ClientGrade);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientGradeData(ClientGrade.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientGradeDeletePartial(int ClientGradeID = -1)
        {
            if (ClientGradeID >= 0)
            {
                try
                {
                    balClientGrade.BALClientGrade_Delete_By_ClientGradeID(ClientGradeID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientGradeData(Convert.ToInt32(Session["ClntID"]));
        }



        public ActionResult GetClientOfferOfPlaceData(int ClntID)
        {
            DataSet dsClientPassport = balClientOfferOfPlace.BALClientOfferOfPlace_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientOfferOfPlaceViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientOfferOfPlaceAddNewPartial(EntityClientOfferOfPlace ClientOfferOfPlace)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientOfferOfPlace.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientOfferOfPlace.BALClientOfferOfPlace_SET(ClientOfferOfPlace);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientOfferOfPlaceData(ClientOfferOfPlace.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientOfferOfPlaceUpdatePartial(EntityClientOfferOfPlace ClientOfferOfPlace)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientOfferOfPlace.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientOfferOfPlace.BALClientOfferOfPlace_Update(ClientOfferOfPlace);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientOfferOfPlaceData(ClientOfferOfPlace.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientOfferOfPlaceDeletePartial(int ClientOfferOfPlaceID = -1)
        {
            if (ClientOfferOfPlaceID >= 0)
            {
                try
                {
                    balClientOfferOfPlace.BALClientOfferOfPlace_Delete_By_ClientOfferOfPlaceID(ClientOfferOfPlaceID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientOfferOfPlaceData(Convert.ToInt32(Session["ClntID"]));
        }


        public ActionResult GetClientPlacementData(int ClntID)
        {
            DataSet dsClientPassport = balClientPlacement.BALClientPlacement_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientPlacementViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientPlacementAddNewPartial(EntityClientPlacement ClientPlacement)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientPlacement.ClientID = Convert.ToInt32(Session["ClntID"]);
                    string errorMsg = "";
                    balClientPlacement.BALClientPlacement_SET(ClientPlacement, out errorMsg);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientPlacementData(ClientPlacement.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientPlacementUpdatePartial(EntityClientPlacement ClientPlacement)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientPlacement.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientPlacement.BALClientPlacement_Update(ClientPlacement);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientPlacementData(ClientPlacement.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientPlacementDeletePartial(int ClientPlacementID = -1)
        {
            if (ClientPlacementID >= 0)
            {
                try
                {
                    balClientPlacement.BALClientPlacement_Delete_By_ClientPlacementID(ClientPlacementID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientPlacementData(Convert.ToInt32(Session["ClntID"]));
        }


        public ActionResult GetClientWorkExperienceData(int ClntID)
        {
            DataSet dsClientPassport = balClientWorkExperience.BALClientWorkExperience_Get_By_ClientID(ClntID);
            var dtClientPassport = dsClientPassport.Tables[0];
            return PartialView("ClientWorkExperienceViewPartial", dtClientPassport);
        }

        [ValidateInput(false)]
        public ActionResult ClientWorkExperienceAddNewPartial(EntityClientWorkExperience ClientWorkExperience)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientWorkExperience.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientWorkExperience.BALClientWorkExperience_SET(ClientWorkExperience);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetClientWorkExperienceData(ClientWorkExperience.ClientID);

        }

        [ValidateInput(false)]
        public ActionResult ClientWorkExperienceUpdatePartial(EntityClientWorkExperience ClientWorkExperience)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ClientWorkExperience.ClientID = Convert.ToInt32(Session["ClntID"]);
                    balClientWorkExperience.BALClientWorkExperience_Update(ClientWorkExperience);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetClientWorkExperienceData(ClientWorkExperience.ClientID);
        }
        [ValidateInput(false)]
        public ActionResult ClientWorkExperienceDeletePartial(int ClientWorkExperienceID = -1)
        {
            if (ClientWorkExperienceID >= 0)
            {
                try
                {
                    balClientWorkExperience.BALClientWorkExperience_Delete_By_ClientWorkExperienceID(ClientWorkExperienceID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetClientWorkExperienceData(Convert.ToInt32(Session["ClntID"]));
        }


        public ActionResult DocsUpload()
        {
            UploadControlExtension.GetUploadedFiles("Docs", UploadControlHelper.ValidationSettings,UploadControlHelper.uploadControl_FileUploadComplete);
            return null;
        }





    }

    public class UploadControlHelper
    {
        public static readonly UploadControlValidationSettings ValidationSettings = new UploadControlValidationSettings
        {
            AllowedFileExtensions = new string[] { ".pdf", ".doc", ".docx", ".txt", ".xls", ".xlsx", ".ppt", ".pptx", ".png", ".jpg", ".jpeg", ".jpe" },
            MaxFileSize = 4194304
        };

        public static void uploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                var ClntId = HttpContext.Current.Session["ClntID"];
                string dirPath = HttpContext.Current.Request.MapPath("~/assets/Content/ClientDoc/" + Convert.ToString(ClntId));
                if (!System.IO.Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }
                string resultFilePath = "~/assets/Content/ClientDoc/" + Convert.ToString(ClntId) + "/" + string.Format("{0}", e.UploadedFile.FileName);
                e.UploadedFile.SaveAs(HttpContext.Current.Request.MapPath(resultFilePath));
                HttpContext.Current.Session["DocsUploadedFile"] = new List<byte[]>();
                ((List<byte[]>)HttpContext.Current.Session["DocsUploadedFile"]).Add(e.UploadedFile.FileBytes);
                HttpContext.Current.Session["DocsFileName"] = e.UploadedFile.FileName;
            }
        }
       
    }
}