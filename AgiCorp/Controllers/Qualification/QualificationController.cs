﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public class QualificationController : MainController
    {
        public override string Name { get { return "Qualification"; } }

        BALQualification balQualification = new BALQualification();
        BALProgramme balProgramme = new BALProgramme();
        BALGPO balGPO = new BALGPO();
        BALQualificationGPO balQualificationGPO = new BALQualificationGPO();
        BALQualificationProgramme balQualificationProgramme = new BALQualificationProgramme();
        public ActionResult Index()
        {
            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            return DemoView("Index", dtQualification);
        }
       
        public ActionResult GridViewPartial()
        {
            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            List<EntityQualification> listInst = new List<EntityQualification>();

            foreach (DataRow dr in dtQualification.Rows)
            {
                EntityQualification entityQualification = new EntityQualification();
                entityQualification.Code = dr["Code"].ToString();
                entityQualification.Description = dr["Description"].ToString();
                entityQualification.NZQACode = Convert.ToInt32(dr["NZQACode"]);
                entityQualification.Credits =   Convert.ToInt32(dr["Credits"]);
                entityQualification.TotalHours = Convert.ToInt32(string.IsNullOrEmpty(dr["TotalHours"].ToString()) ? 0 : dr["TotalHours"]);
                entityQualification.HoursPerCredit = Convert.ToInt32(string.IsNullOrEmpty(dr["HoursPerCredit"].ToString()) ? 0 : dr["HoursPerCredit"]);
                entityQualification.F2FHours = Convert.ToInt32(dr["F2FHours"]);
                entityQualification.SDHours = Convert.ToInt32(dr["SDHours"]);
                entityQualification.QualificationLevelID =  Convert.ToInt32(string.IsNullOrEmpty(dr["QualificationLevelID"].ToString())? 0: dr["QualificationLevelID"]);
                entityQualification.QualificationTypeID = Convert.ToInt32(string.IsNullOrEmpty(dr["QualificationTypeID"].ToString()) ? 0 : dr["QualificationTypeID"]);
                entityQualification.QualificationID = Convert.ToInt32(dr["QualificationID"]);
                entityQualification.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listInst.Add(entityQualification);
            }
            return PartialView("GridViewPartial", listInst);
        }

        public ActionResult QualificationPopup()
        {
            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            return PartialView("QualificationPopup", dtQualification);
        }

        public string AddAssociatedGPO(int GPOID, int QualificationID)
        {
            EntityQualificationGPO entityQualificationGPO = new EntityQualificationGPO();
            entityQualificationGPO.QualificationID = QualificationID;
            entityQualificationGPO.GPOID = GPOID;
            string errorMsg = "";

            int result = balQualificationGPO.BALQualificationGPO_SET(entityQualificationGPO, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetQualificationGPOData(int QualificationID)
        {
            DataSet dsQualificationGPO = balQualificationGPO.BALQualificationGPO_Get_By_QualificationID(QualificationID);
            var dtQualificationGPO = dsQualificationGPO.Tables[0];
            return PartialView("GPOViewPartial", dtQualificationGPO);
        }

        public ActionResult DeleteQualificationGPOPartial(int QualificationID, int QualificationGPOID = -1)
        {
            if (QualificationGPOID >= 0)
            {
                try
                {
                    balQualificationGPO.BALQualificationGPO_Delete_By_QualificationGPOID(QualificationGPOID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetQualificationGPOData(QualificationID);
        }

        public string AddAssociatedProgramme(int ProgrammeID, int QualificationID)
        {
            EntityQualificationProgramme entityQualificationProgramme = new EntityQualificationProgramme();
            entityQualificationProgramme.QualificationID = QualificationID;
            entityQualificationProgramme.ProgrammeID = ProgrammeID;
            string errorMsg = "";

            int result = balQualificationProgramme.BALQualificationProgramme_SET(entityQualificationProgramme, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetQualificationProgrammeData(int QualificationID)
        {
            DataSet dsQualificationProgramme = balQualificationProgramme.BALQualificationProgramme_Get_By_QualificationID(QualificationID);
            var dtQualificationProgramme = dsQualificationProgramme.Tables[0];
            return PartialView("ProgrammeViewPartial", dtQualificationProgramme);
        }

        public ActionResult DeleteQualificationProgrammePartial(int QualificationID, int QualificationProgrammeID = -1)
        {
            if (QualificationProgrammeID >= 0)
            {
                try
                {
                    balQualificationProgramme.BALQualificationProgramme_Delete_By_QualificationProgrammeID(QualificationProgrammeID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetQualificationProgrammeData(QualificationID);
        }

        public ActionResult GetProgrammeData()
        {
            DataSet dsProgramme = balProgramme.BALProgramme_Get_All();
            var dtProgramme = dsProgramme.Tables[0];
            return PartialView("ProgrammeViewPartial", dtProgramme);
        }

        public ActionResult GetGPOData()
        {
            DataSet dsGPO = balGPO.BALGPO_Get_All();
            var dtGPO = dsGPO.Tables[0];
            return PartialView("GPOViewPartial", dtGPO);
        }

        public ActionResult CheckUniqueCode(string Code,string QualificationID)
        {
            var result = balQualification.Check_For_UniqueCode(Code, QualificationID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityQualification Qualification)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balQualification.BALQualification_SET(Qualification);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            return PartialView("GridViewPartial", dtQualification);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityQualification Qualification)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balQualification.BALQualification_Update(Qualification);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            return PartialView("GridViewPartial", dtQualification);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int QualificationID = -1)
        {
            if (QualificationID >= 0)
            {
                try
                {
                    balQualification.BALQualification_Delete_By_QualificationID(QualificationID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Programme/GPO Record.";
                    }
                }
            }
            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            return PartialView("GridViewPartial", dtQualification);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsQualification = balQualification.BALQualification_Get_All();
            var dtQualification = dsQualification.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                QualificationHelper.ExportGridSettings, dtQualification
            );
        }

    }
}