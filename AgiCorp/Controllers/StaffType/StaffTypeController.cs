﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class StaffTypeController : MainController
    {
        public override string Name { get { return "StaffType"; } }
        BALStaffType balStaffType = new BALStaffType();
        BALBuilding balBuilding = new BALBuilding();
        public ActionResult Index()
        {
            DataSet dsStaffType = balStaffType.BALStaffType_Get_All();
            var dtStaffType = dsStaffType.Tables[0];
            return DemoView("Index", dtStaffType);
        }
        public ActionResult GridViewPartial()
        {         
            DataSet dsStaffType = balStaffType.BALStaffType_Get_All();
            var dtStaffType = dsStaffType.Tables[0];
            List<EntityStaffType> listStaffType = new List<EntityStaffType>();

            foreach (DataRow dr in dtStaffType.Rows)
            {
                EntityStaffType entityStaffType = new EntityStaffType();
                entityStaffType.Code = dr["Code"].ToString();
                entityStaffType.Description = dr["Description"].ToString();
                entityStaffType.StaffTypeID = Convert.ToInt32(dr["StaffTypeID"]);
                listStaffType.Add(entityStaffType);
            }

            return PartialView("GridViewPartial", listStaffType);
        }

        public ActionResult StaffTypePopup()
        {
            DataSet dsStaffType = balStaffType.BALStaffType_Get_All();
            var dtStaffType = dsStaffType.Tables[0];
            return PartialView("StaffTypePopup", dtStaffType);
        }

        public ActionResult GetBuildingData()
        {
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("BuildingViewPartial", dtBuilding);
        }

        public ActionResult CheckUniqueCode(string Code, string StaffTypeID)
        {
            var result = balStaffType.Check_For_UniqueCode(Code, StaffTypeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityStaffType StaffType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balStaffType.BALStaffType_SET(StaffType);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsStaffType = balStaffType.BALStaffType_Get_All();
            var dtStaffType = dsStaffType.Tables[0];
            return PartialView("GridViewPartial", dtStaffType);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityStaffType StaffType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balStaffType.BALStaffType_Update(StaffType);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsStaffType = balStaffType.BALStaffType_Get_All();
            var dtStaffType = dsStaffType.Tables[0];
            return PartialView("GridViewPartial", dtStaffType);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int StaffTypeID = -1)
        {
            if (StaffTypeID >= 0)
            {
                try
                {
                    balStaffType.BALStaffType_Delete_By_StaffTypeID(StaffTypeID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsStaffType = balStaffType.BALStaffType_Get_All();
            var dtStaffType = dsStaffType.Tables[0];
            return PartialView("GridViewPartial", dtStaffType);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsStaffType = balStaffType.BALStaffType_Get_All();
            var dtStaffType = dsStaffType.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                StaffTypeHelper.ExportGridSettings, dtStaffType
            );
        }
    }
}