﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class DepartmentController : MainController
    {
        public override string Name { get { return "Department"; } }
        BALDepartment balDepartment = new BALDepartment();
        BALBuilding balBuilding = new BALBuilding();
        BALDepartmentBuilding balDepartmentBuilding = new BALDepartmentBuilding();
        public ActionResult Index()
        {
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return DemoView("Index", dtDepartment);
        }
        public ActionResult GridViewPartial()
        {            
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            List<EntityDepartment> listInst = new List<EntityDepartment>();

            foreach (DataRow dr in dtDepartment.Rows)
            {
                EntityDepartment entityDepartment = new EntityDepartment();
                entityDepartment.Code = dr["Code"].ToString();
                entityDepartment.Description = dr["Description"].ToString();
                entityDepartment.DepartmentID = Convert.ToInt32(dr["DepartmentID"]);
                entityDepartment.IsActive = Convert.ToBoolean(dr["IsActive"]);
                entityDepartment.SchoolID = Convert.ToInt32(dr["SchoolID"]);
                listInst.Add(entityDepartment);
            }
            return PartialView("GridViewPartial", listInst);
        }

        public string AddAssociatedBuilding(int BuildingID, int DepartmentID)
        {
            EntityDepartmentBuilding entityDepartmentBuilding = new EntityDepartmentBuilding();
            entityDepartmentBuilding.DepartmentID = DepartmentID;
            entityDepartmentBuilding.BuildingID = BuildingID;
            string errorMsg = "";

            int result = balDepartmentBuilding.BALDepartmentBuilding_SET(entityDepartmentBuilding, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetDepartmentBuildingData(int DepartmentID)
        {
            DataSet dsDepartmentBuilding = balDepartmentBuilding.BALDepartmentBuilding_Get_By_DepartmentID(DepartmentID);
            var dtDepartmentBuilding = dsDepartmentBuilding.Tables[0];
            return PartialView("BuildingViewPartial", dtDepartmentBuilding);
        }
        public ActionResult DepartmentPopup()
        {
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return PartialView("DepartmentPopup", dtDepartment);
        }

        public ActionResult GetBuildingData()
        {
            DataSet dsBuilding = balBuilding.BALBuilding_Get_All();
            var dtBuilding = dsBuilding.Tables[0];
            return PartialView("BuildingViewPartial", dtBuilding);
        }

        public ActionResult CheckUniqueCode(string Code,string DepartmentID)
        {            
            var result = balDepartment.Check_For_UniqueCode(Code,DepartmentID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityDepartment Department)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balDepartment.BALDepartment_SET(Department);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return PartialView("GridViewPartial", dtDepartment);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityDepartment Department)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balDepartment.BALDepartment_Update(Department);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return PartialView("GridViewPartial", dtDepartment);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int DepartmentID = -1)
        {
            if (DepartmentID >= 0)
            {
                try
                {
                    balDepartment.BALDepartment_Delete_By_DepartmentID(DepartmentID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Classroom/Building  Record.";
                    }
                }
            }
            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return PartialView("GridViewPartial", dtDepartment);
        }

        public ActionResult DeleteDepartmentBuildingPartial(int DepartmentID, int DepartmentBuildingID = -1)
        {
            if (DepartmentBuildingID >= 0)
            {
                try
                {
                    balDepartmentBuilding.BALDepartmentBuilding_Delete_By_DepartmentBuildingID(DepartmentBuildingID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetDepartmentBuildingData(DepartmentID);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsDepartment = balDepartment.BALDepartment_Get_All();
            var dtDepartment = dsDepartment.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                DepartmentHelper.ExportGridSettings, dtDepartment
            );
        }
    }
}