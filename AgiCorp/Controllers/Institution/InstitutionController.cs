﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;
using System.Reflection;

namespace AgiCorp.Controllers
{
    public partial class InstitutionController : MainController
    {
        public override string Name { get { return "Institution"; } }
        BALInstitution balInstituion = new BALInstitution();
        BALSchool balSchool = new BALSchool();
        BALInstituteSchool balInstituteSchool = new BALInstituteSchool(); 
        public ActionResult Index()
        {
            DataSet dsInstitution = balInstituion.BALInstitution_Get_All();
            var dtInstitution = dsInstitution.Tables[0];            
            return DemoView("Index", dtInstitution);
        }
        public ActionResult GridViewPartial()
        {
            DataSet dsInstitution = balInstituion.BALInstitution_Get_All();
            var dtInstitution = dsInstitution.Tables[0];
            List<EntityInstitution> listInst = new List<EntityInstitution>();

            foreach (DataRow dr in dtInstitution.Rows)
            {
                EntityInstitution entityInst = new EntityInstitution();
                entityInst.Code = dr["Code"].ToString();
                entityInst.Description = dr["Description"].ToString();
                entityInst.Address1 = dr["Address1"].ToString();
                entityInst.Address2 = dr["Address2"].ToString();
                entityInst.Address3 = dr["Address3"].ToString();
                entityInst.City = dr["City"].ToString();
                entityInst.State = dr["State"].ToString();
                entityInst.PostCode = dr["PostCode"].ToString();
                entityInst.CountryID = dr["CountryID"].ToString();
                entityInst.Email = dr["Email"].ToString();
                entityInst.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                entityInst.PhoneStateCode = dr["PhoneStateCode"].ToString();
                entityInst.Phone = dr["Phone"].ToString();
                entityInst.InstitutionID= Convert.ToInt32(dr["InstitutionID"]);
                entityInst.IsActive = Convert.ToBoolean(dr["IsActive"]);
                entityInst.AccountName = dr["AccountName"].ToString();
                entityInst.AccountNumber = dr["AccountNumber"].ToString();
                entityInst.BankName = dr["BankName"].ToString();
                entityInst.SwiftCode = dr["SwiftCode"].ToString();
                entityInst.SPOCEmail = dr["SPOCEmail"].ToString();
                listInst.Add(entityInst);
            }
           
            return PartialView("GridViewPartial", listInst);
        }

        //private static List<T> ConvertDataTable<T>(DataTable dt)
        //{
        //    List<T> data = new List<T>();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        T item = GetItem<T>(row);
        //        data.Add(item);
        //    }
        //    return data;
        //}
        //private static T GetItem<T>(DataRow dr)
        //{
        //    Type temp = typeof(T);
        //    T obj = Activator.CreateInstance<T>();

        //    foreach (DataColumn column in dr.Table.Columns)
        //    {
        //        foreach (PropertyInfo pro in temp.GetProperties())
        //        {
        //            if (pro.Name == column.ColumnName)
        //                pro.SetValue(obj, dr[column.ColumnName]);
        //            else
        //                continue;
        //        }
        //    }
        //    return obj;
        //}

        //public ActionResult InstitutionPopup()
        //{
        //    DataSet dsInstitution = balInstituion.BALInstitution_Get_All();
        //    var dtInstitution = dsInstitution.Tables[0];
        //    return PartialView("InstitutionPopup", dtInstitution);
        //}

        public string AddAssociatedSchool(int SchoolID, int InstitutionID)
        {
            EntityInstituteSchool entityInstituteSchool = new EntityInstituteSchool();
            entityInstituteSchool.InstituteID = InstitutionID;
            entityInstituteSchool.SchoolID = SchoolID;
            string errorMsg = "";

            int result = balInstituteSchool.BALInstituteSchool_SET(entityInstituteSchool, out errorMsg);
            return errorMsg;
        }

        public ActionResult GetInstituteSchoolData(int InstituteID)
        {
            DataSet dsInstituteSchool = balInstituteSchool.BALInstituteSchool_Get_By_InstituteID(InstituteID);
            var dtInstituteSchool = dsInstituteSchool.Tables[0];
            return PartialView("SchoolViewPartial", dtInstituteSchool);            
        }

        public ActionResult GetSchoolData()
        {
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("SchoolViewPartial", dtSchool);
        }

        public ActionResult CheckUniqueCode(string Code,string InstitutionID)
        {
            var result = balInstituion.Check_For_UniqueCode(Code,InstitutionID);           
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityInstitution institution)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balInstituion.BALInstitution_SET(institution);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsInstitution = balInstituion.BALInstitution_Get_All();
            var dtInstitution = dsInstitution.Tables[0];
            return PartialView("GridViewPartial", dtInstitution);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityInstitution institution)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balInstituion.BALInstitution_Update(institution);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            { 
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsInstitution = balInstituion.BALInstitution_Get_All();
            var dtInstitution = dsInstitution.Tables[0];
            return PartialView("GridViewPartial", dtInstitution);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int institutionID = -1)
        {
            if (institutionID >= 0)
            {
                try
                {
                    balInstituion.BALInstitution_Delete_By_InstitutionID(institutionID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with School Record.";
                    }
                }
            }
            DataSet dsInstitution = balInstituion.BALInstitution_Get_All();
            var dtInstitution = dsInstitution.Tables[0];
            return PartialView("GridViewPartial", dtInstitution);
        }

        public ActionResult DeleteInstituteSchoolPartial(int InstituteID,int instituteSchoolID = -1)
        {
            if (instituteSchoolID >= 0)
            {
                try
                {
                    balInstituteSchool.BALInstituteSchool_Delete_By_InstituteSchoolID(instituteSchoolID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            //DataSet dsInstituteSchool = balInstituteSchool.BALInstituteSchool_Get_All();
            //var dtInstituteSchool = dsInstituteSchool.Tables[0];
            //return PartialView("SchoolViewPartial", dtInstituteSchool);
            return GetInstituteSchoolData(InstituteID);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsInstitution = balInstituion.BALInstitution_Get_All();
            var dtInstitution = dsInstitution.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                InstitutionHelper.ExportGridSettings, dtInstitution
            );
        }
    }
}