﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class EquipmentTypeController : MainController
    {
        public override string Name { get { return "EquipmentType"; } }
        BALEquipmentType balEquipmentType = new BALEquipmentType();
        BALSchool balSchool = new BALSchool();
        public ActionResult Index()
        {
            DataSet dsEquipmentType = balEquipmentType.BALEquipmentType_Get_All();
            var dtEquipmentType = dsEquipmentType.Tables[0];
            return DemoView("Index", dtEquipmentType);
        }
        public ActionResult GridViewPartial()
        {            

            DataSet dsEquipmentType = balEquipmentType.BALEquipmentType_Get_All();
            var dtEquipmentType = dsEquipmentType.Tables[0];
            List<EntityEquipmentType> listInst = new List<EntityEquipmentType>();

            foreach (DataRow dr in dtEquipmentType.Rows)
            {
                EntityEquipmentType entityEquipmentType = new EntityEquipmentType();
                entityEquipmentType.Code = dr["Code"].ToString();                
                entityEquipmentType.Description = dr["Description"].ToString();
                entityEquipmentType.EquipmentTypeID = Convert.ToInt32(dr["EquipmentTypeID"]);
                listInst.Add(entityEquipmentType);
           }
            return PartialView("GridViewPartial", listInst);
        }

        public ActionResult EquipmentTypePopup()
        {
            DataSet dsEquipmentType = balEquipmentType.BALEquipmentType_Get_All();
            var dtEquipmentType = dsEquipmentType.Tables[0];
            return PartialView("EquipmentTypePopup", dtEquipmentType);
        }

        public ActionResult GetSchoolData()
        {
            DataSet dsSchool = balSchool.BALSchool_Get_All();
            var dtSchool = dsSchool.Tables[0];
            return PartialView("SchoolViewPartial", dtSchool);
        }

        public ActionResult CheckUniqueCode(string Code,string EquipmentTypeID)
        {          
            var result = balEquipmentType.Check_For_UniqueCode(Code,EquipmentTypeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityEquipmentType EquipmentType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balEquipmentType.BALEquipmentType_SET(EquipmentType);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsEquipmentType = balEquipmentType.BALEquipmentType_Get_All();
            var dtEquipmentType = dsEquipmentType.Tables[0];
            return PartialView("GridViewPartial", dtEquipmentType);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityEquipmentType EquipmentType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balEquipmentType.BALEquipmentType_Update(EquipmentType);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsEquipmentType = balEquipmentType.BALEquipmentType_Get_All();
            var dtEquipmentType = dsEquipmentType.Tables[0];
            return PartialView("GridViewPartial", dtEquipmentType);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int EquipmentTypeID = -1)
        {
            if (EquipmentTypeID >= 0)
            {
                try
                {
                    balEquipmentType.BALEquipmentType_Delete_By_EquipmentTypeID(EquipmentTypeID);
                    ViewData["DeleteError"] = "";
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    if (e.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    {
                        ViewData["DeleteError"] = "You Cannot Delete This record as it is referenced with Equipment Record.";
                    }
                }
            }
            DataSet dsEquipmentType = balEquipmentType.BALEquipmentType_Get_All();
            var dtEquipmentType = dsEquipmentType.Tables[0];
            return PartialView("GridViewPartial", dtEquipmentType);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsEquipmentType = balEquipmentType.BALEquipmentType_Get_All();
            var dtEquipmentType = dsEquipmentType.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                EquipmentTypeHelper.ExportGridSettings, dtEquipmentType
            );
        }
    }
}