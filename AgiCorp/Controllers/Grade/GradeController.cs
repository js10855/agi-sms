﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public partial class GradeController : MainController
    {
        public override string Name { get { return "Grade"; } }
        BALGrade balGrade = new BALGrade();
        BALGradeMarks balGradeMarks = new BALGradeMarks();
        public ActionResult Index()
        {
            DataSet dsGrade = balGrade.BALGrade_Get_All();
            var dtGrade = dsGrade.Tables[0];
            return DemoView("Index", dtGrade);
        }
        public ActionResult GridViewPartial()
        {
            //DataSet dsGrade = balGrade.BALGrade_Get_All();
            //var dtGrade = dsGrade.Tables[0];
            //return PartialView("GridViewPartial", dtGrade);

            DataSet dsGrade = balGrade.BALGrade_Get_All();
            var dtGrade = dsGrade.Tables[0];
            List<EntityGrade> listGrade = new List<EntityGrade>();

            foreach (DataRow dr in dtGrade.Rows)
            {
                EntityGrade entityGrade = new EntityGrade();
                entityGrade.Code = dr["Code"].ToString();
                entityGrade.Description = dr["Description"].ToString();
                entityGrade.GradeID = Convert.ToInt32(dr["GradeID"]);
                //entityGrade.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listGrade.Add(entityGrade);
            }
            return PartialView("GridViewPartial", listGrade);

        }

        public ActionResult GradePopup()
        {
            DataSet dsGrade = balGrade.BALGrade_Get_All();
            var dtGrade = dsGrade.Tables[0];
            return PartialView("GradePopup", dtGrade);
        }

        //public ActionResult GetGradeMarksData()
        //{
        //    DataSet dsGradeMarks = balGradeMarks.BALGradeMarks_Get_All();
        //    var dtGradeMarks = dsGradeMarks.Tables[0];
        //    return PartialView("GradeMarksViewPartial", dtGradeMarks);
        //}

        public ActionResult GetGradeMarksData(int GradeID)
        {
            DataSet dsGradeMarks = balGradeMarks.BALGradeMarks_Get_By_GradeID(GradeID);
            var dtGradeMarks = dsGradeMarks.Tables[0];
            return PartialView("GradeMarksViewPartial", dtGradeMarks);
        }

        public ActionResult CheckUniqueCode(string Code, string GradeID)
        {
            //Correct this
            var result =  balGrade.Check_For_UniqueCode(Code, GradeID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityGrade Grade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balGrade.BALGrade_SET(Grade);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsGrade = balGrade.BALGrade_Get_All();
            var dtGrade = dsGrade.Tables[0];
            return PartialView("GridViewPartial", dtGrade);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityGrade Grade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balGrade.BALGrade_Update(Grade);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsGrade = balGrade.BALGrade_Get_All();
            var dtGrade = dsGrade.Tables[0];
            return PartialView("GridViewPartial", dtGrade);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int GradeID = -1)
        {
            if (GradeID >= 0)
            {
                try
                {
                    balGrade.BALGrade_Delete_By_GradeID(GradeID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsGrade = balGrade.BALGrade_Get_All();
            var dtGrade = dsGrade.Tables[0];
            return PartialView("GridViewPartial", dtGrade);
        }

        [ValidateInput(false)]
        public ActionResult GradeMarksAddNewPartial(EntityGradeMarks GradeMarks)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GradeMarks.GradeID = Convert.ToInt32(Session["GrID"]);                    
                    balGradeMarks.BALGradeMarks_SET(GradeMarks);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return GetGradeMarksData(GradeMarks.GradeID);           
        }

        [ValidateInput(false)]
        public ActionResult GradeMarksUpdatePartial(EntityGradeMarks GradeMarks)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GradeMarks.GradeID = Convert.ToInt32(Session["GrID"]);
                    balGradeMarks.BALGradeMarks_Update(GradeMarks);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }
            return GetGradeMarksData(GradeMarks.GradeID);           
        }
        [ValidateInput(false)]
        public ActionResult GradeMarksDeletePartial(int GradeMarksID = -1)
        {
            if (GradeMarksID >= 0)
            {
                try
                {
                    balGradeMarks.BALGradeMarks_Delete_By_GradeMarksID(GradeMarksID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return GetGradeMarksData(Convert.ToInt32(Session["GrID"]));            
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsGrade = balGrade.BALGrade_Get_All();
            var dtGrade = dsGrade.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                GradeHelper.ExportGridSettings, dtGrade
            );
        }
    }
}