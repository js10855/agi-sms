﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgiCorp.BLL;
using AgiCorp.Entity;
using System.Data;
using AgiCorp.Code.Helpers;

namespace AgiCorp.Controllers
{
    public class LOController : MainController
    {
        public override string Name { get { return "LO"; } }

        BALLO balLO = new BALLO();        
        BALStaff balStaff = new BALStaff();
        BALAssessments balAssessments = new BALAssessments();
        public ActionResult Index()
        {
            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return DemoView("Index", dtLO);
        }
        public ActionResult GridViewPartial()
        {
            //DataSet dsLO = balLO.BALLO_Get_All();
            //var dtLO = dsLO.Tables[0];
            //return PartialView("GridViewPartial", dtLO);
            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            List<EntityLO> listLO = new List<EntityLO>();

            foreach (DataRow dr in dtLO.Rows)
            {
                EntityLO entityLO = new EntityLO();
                entityLO.Code = dr["Code"].ToString();
                entityLO.Description = dr["Description"].ToString();
                entityLO.Credits = Convert.ToInt32(dr["Credits"]);
                entityLO.F2FHours = Convert.ToInt32(dr["F2FHours"]);
                entityLO.SDHours = Convert.ToInt32(dr["SDHours"]);
                entityLO.LOID = Convert.ToInt32(dr["LOID"]);
                //entityLO.IsActive = Convert.ToBoolean(dr["IsActive"]);
                listLO.Add(entityLO);
            }
            return PartialView("GridViewPartial", listLO);

        }

        public ActionResult LOPopup()
        {
            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return PartialView("LOPopup", dtLO);
        }

        public ActionResult GetLOData()
        {
            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return PartialView("LOViewPartial", dtLO);
        }

        public ActionResult GetStaffData()
        {
            DataSet dsStaff = balStaff.BALStaff_Get_All();
            var dtStaff = dsStaff.Tables[0];
            return PartialView("StaffViewPartial", dtStaff);
        }

        public ActionResult GetAssessmentsData()
        {
            DataSet dsAssessments = balAssessments.BALAssessments_Get_All();
            var dtAssessments = dsAssessments.Tables[0];
            return PartialView("AssessmentsViewPartial", dtAssessments);
        }

        public ActionResult CheckUniqueCode(string Code, string LOID)
        {
            //correct this
            var result = balLO.Check_For_UniqueCode(Code, LOID);
            //var result = true;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult AddNewPartial(EntityLO LO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balLO.BALLO_SET(LO);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return PartialView("GridViewPartial", dtLO);
        }

        [ValidateInput(false)]
        public ActionResult UpdatePartial(EntityLO LO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    balLO.BALLO_Update(LO);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                ViewData["EditError"] = "Please, correct all errors.";
            }

            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return PartialView("GridViewPartial", dtLO);
        }
        [ValidateInput(false)]
        public ActionResult DeletePartial(int LOID = -1)
        {
            if (LOID >= 0)
            {
                try
                {
                    balLO.BALLO_Delete_By_LOID(LOID);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return PartialView("GridViewPartial", dtLO);
        }

        public ActionResult ExportTo(GridViewExportFormat? exportFormat)
        {
            if (exportFormat == null || !GridViewExportHelper.ExportFormatsInfo.ContainsKey(exportFormat.Value))
                return RedirectToAction("Index");

            DataSet dsLO = balLO.BALLO_Get_All();
            var dtLO = dsLO.Tables[0];
            return GridViewExportHelper.ExportFormatsInfo[exportFormat.Value](
                LOHelper.ExportGridSettings, dtLO
            );
        }

    }
}