using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALAgentCountry
	 {
		 public BALAgentCountry() 
		 {
		 }

		 public DataSet BALAgentCountry_Get_All()
		 {
		 	 return DALAgentCountry.Get_All();
		 }

		 public DataSet BALAgentCountry_Get_By_AgentCountryID(int AgentCountryID)
		 {
		 	 return DALAgentCountry.Get_By_AgentCountryID(AgentCountryID);
		 }

        public DataSet BALAgentCountry_Get_By_AgentID(int AgentID)
        {
            return DALAgentCountry.Get_By_AgentID(AgentID);
        }
        public int BALAgentCountry_SET(EntityAgentCountry entity, out string errorMsg) 
		 {
			 return DALAgentCountry.SET(entity, out errorMsg);
		 }

		 public int BALAgentCountry_Update(EntityAgentCountry entity) 
		 {
			 return DALAgentCountry.Update(entity);
		 }

		 public int BALAgentCountry_Delete_By_AgentCountryID(int AgentCountryID) 
		 {
			 return DALAgentCountry.Delete_By_AgentCountryID(AgentCountryID);
		 }

	}
}
