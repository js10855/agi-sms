using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientAttendance
	 {
		 public BALClientAttendance() 
		 {
		 }

		 public DataSet BALClientAttendance_Get_All()
		 {
		 	 return DALClientAttendance.Get_All();
		 }

		 public DataSet BALClientAttendance_Get_By_ClientAttendanceID(int ClientAttendanceID)
		 {
		 	 return DALClientAttendance.Get_By_ClientAttendanceID(ClientAttendanceID);
		 }

		 public int BALClientAttendance_SET(EntityClientAttendance entity) 
		 {
			 return DALClientAttendance.SET(entity);
		 }

		 public int BALClientAttendance_Update(EntityClientAttendance entity) 
		 {
			 return DALClientAttendance.Update(entity);
		 }

		 public int BALClientAttendance_Delete_By_ClientAttendanceID(int ClientAttendanceID) 
		 {
			 return DALClientAttendance.Delete_By_ClientAttendanceID(ClientAttendanceID);
		 }
        public DataSet BALClientAttendance_Get_By_ClientID(int ClientID)
        {
            return DALClientAttendance.Get_By_ClientID(ClientID);
        }

    }
}
