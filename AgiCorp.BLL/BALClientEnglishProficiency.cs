using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientEnglishProficiency
	 {
		 public BALClientEnglishProficiency() 
		 {
		 }

		 public DataSet BALClientEnglishProficiency_Get_All()
		 {
		 	 return DALClientEnglishProficiency.Get_All();
		 }

		 public DataSet BALClientEnglishProficiency_Get_By_ClientEnglishProficiencyID(int ClientEnglishProficiencyID)
		 {
		 	 return DALClientEnglishProficiency.Get_By_ClientEnglishProficiencyID(ClientEnglishProficiencyID);
		 }

		 public int BALClientEnglishProficiency_SET(EntityClientEnglishProficiency entity) 
		 {
			 return DALClientEnglishProficiency.SET(entity);
		 }

		 public int BALClientEnglishProficiency_Update(EntityClientEnglishProficiency entity) 
		 {
			 return DALClientEnglishProficiency.Update(entity);
		 }

		 public int BALClientEnglishProficiency_Delete_By_ClientEnglishProficiencyID(int ClientEnglishProficiencyID) 
		 {
			 return DALClientEnglishProficiency.Delete_By_ClientEnglishProficiencyID(ClientEnglishProficiencyID);
		 }
        
        public DataSet BALClientEnglishProficiency_Get_By_ClientID(int ClientID)
        {
            return DALClientEnglishProficiency.Get_By_ClientID(ClientID);
        }
    }
}
