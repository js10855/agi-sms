using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALAgentCommissionType
	 {
		 public BALAgentCommissionType() 
		 {
		 }

		 public DataSet BALAgentCommissionType_Get_All()
		 {
		 	 return DALAgentCommissionType.Get_All();
		 }

		 public DataSet BALAgentCommissionType_Get_By_AgentCommissionTypeID(int AgentCommissionTypeID)
		 {
		 	 return DALAgentCommissionType.Get_By_AgentCommissionTypeID(AgentCommissionTypeID);
		 }

		 public int BALAgentCommissionType_SET(EntityAgentCommissionType entity) 
		 {
			 return DALAgentCommissionType.SET(entity);
		 }

		 public int BALAgentCommissionType_Update(EntityAgentCommissionType entity) 
		 {
			 return DALAgentCommissionType.Update(entity);
		 }

		 public int BALAgentCommissionType_Delete_By_AgentCommissionTypeID(int AgentCommissionTypeID) 
		 {
			 return DALAgentCommissionType.Delete_By_AgentCommissionTypeID(AgentCommissionTypeID);
		 }

	}
}
