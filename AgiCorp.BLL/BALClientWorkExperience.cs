using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientWorkExperience
	 {
		 public BALClientWorkExperience() 
		 {
		 }

		 public DataSet BALClientWorkExperience_Get_All()
		 {
		 	 return DALClientWorkExperience.Get_All();
		 }

		 public DataSet BALClientWorkExperience_Get_By_ClientWorkExperienceID(int ClientWorkExperienceID)
		 {
		 	 return DALClientWorkExperience.Get_By_ClientWorkExperienceID(ClientWorkExperienceID);
		 }

		 public int BALClientWorkExperience_SET(EntityClientWorkExperience entity) 
		 {
			 return DALClientWorkExperience.SET(entity);
		 }

		 public int BALClientWorkExperience_Update(EntityClientWorkExperience entity) 
		 {
			 return DALClientWorkExperience.Update(entity);
		 }

		 public int BALClientWorkExperience_Delete_By_ClientWorkExperienceID(int ClientWorkExperienceID) 
		 {
			 return DALClientWorkExperience.Delete_By_ClientWorkExperienceID(ClientWorkExperienceID);
		 }
        public DataSet BALClientWorkExperience_Get_By_ClientID(int ClientID)
        {
            return DALClientWorkExperience.Get_By_ClientID(ClientID);
        }
    }
}
