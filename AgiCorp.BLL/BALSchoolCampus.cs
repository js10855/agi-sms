using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;


namespace AgiCorp.BLL
{
	 public class BALSchoolCampus
	 {
		 public BALSchoolCampus() 
		 {
		 }

		 public DataSet BALSchoolCampus_Get_All()
		 {
		 	 return DALSchoolCampus.Get_All();
		 }

		 public DataSet BALSchoolCampus_Get_By_SchoolCampusID(int SchoolCampusID)
		 {
		 	 return DALSchoolCampus.Get_By_SchoolCampusID(SchoolCampusID);
		 }

        public DataSet BALSchoolCampus_Get_By_SchoolID(int SchoolID)
        {
            return DALSchoolCampus.Get_By_SchoolID(SchoolID);
        }
        public int BALSchoolCampus_SET(EntitySchoolCampus entity, out string errorMsg) 
		 {
			 return DALSchoolCampus.SET(entity, out errorMsg);
		 }

		 public int BALSchoolCampus_Update(EntitySchoolCampus entity) 
		 {
			 return DALSchoolCampus.Update(entity);
		 }

		 public int BALSchoolCampus_Delete_By_SchoolCampusID(int SchoolCampusID) 
		 {
			 return DALSchoolCampus.Delete_By_SchoolCampusID(SchoolCampusID);
		 }

	}
}
