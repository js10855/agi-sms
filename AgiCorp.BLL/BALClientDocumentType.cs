using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;


namespace AgiCorp
{
	 public class BALClientDocumentType
	 {
		 public BALClientDocumentType() 
		 {
		 }

		 public DataSet BALClientDocumentType_Get_All()
		 {
		 	 return DALClientDocumentType.Get_All();
		 }

		 public DataSet BALClientDocumentType_Get_By_ClientDocumentTypeID(int ClientDocumentTypeID)
		 {
		 	 return DALClientDocumentType.Get_By_ClientDocumentTypeID(ClientDocumentTypeID);
		 }

		 public int BALClientDocumentType_SET(EntityClientDocumentType entity) 
		 {
			 return DALClientDocumentType.SET(entity);
		 }

		 public int BALClientDocumentType_Update(EntityClientDocumentType entity) 
		 {
			 return DALClientDocumentType.Update(entity);
		 }

		 public int BALClientDocumentType_Delete_By_ClientDocumentTypeID(int ClientDocumentTypeID) 
		 {
			 return DALClientDocumentType.Delete_By_ClientDocumentTypeID(ClientDocumentTypeID);
		 }

	}
}
