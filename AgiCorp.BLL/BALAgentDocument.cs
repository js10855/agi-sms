using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALAgentDocument
	 {
		 public BALAgentDocument() 
		 {
		 }

		 public DataSet BALAgentDocument_Get_All()
		 {
		 	 return DALAgentDocument.Get_All();
		 }

        public DataSet BALAgentDocument_Get_By_AgentID(int AgentID)
        {
            return DALAgentDocument.Get_By_AgentID(AgentID);
        }
        public DataSet BALAgentDocument_Get_By_AgentDocumentID(int AgentDocumentID)
		 {
		 	 return DALAgentDocument.Get_By_AgentDocumentID(AgentDocumentID);
		 }

		 public int BALAgentDocument_SET(EntityAgentDocument entity) 
		 {
			 return DALAgentDocument.SET(entity);
		 }

		 public int BALAgentDocument_Update(EntityAgentDocument entity) 
		 {
			 return DALAgentDocument.Update(entity);
		 }

		 public int BALAgentDocument_Delete_By_AgentDocumentID(int AgentDocumentID) 
		 {
			 return DALAgentDocument.Delete_By_AgentDocumentID(AgentDocumentID);
		 }

	}
}
