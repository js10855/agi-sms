using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALStaffStatus
	 {
		 public BALStaffStatus() 
		 {
		 }

		 public DataSet BALStaffStatus_Get_All()
		 {
		 	 return DALStaffStatus.Get_All();
		 }

		 public DataSet BALStaffStatus_Get_By_StatusID(int StatusID)
		 {
		 	 return DALStaffStatus.Get_By_StatusID(StatusID);
		 }

		 public int BALStaffStatus_SET(EntityStatus entity) 
		 {
			 return DALStaffStatus.SET(entity);
		 }

		 public int BALStaffStatus_Update(EntityStatus entity) 
		 {
			 return DALStaffStatus.Update(entity);
		 }

		 public int BALStaffStatus_Delete_By_StatusID(int StatusID) 
		 {
			 return DALStaffStatus.Delete_By_StatusID(StatusID);
		 }

	}
}
