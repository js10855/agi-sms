using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALYear
	 {
		 public BALYear() 
		 {
		 }

		 public DataSet BALYear_Get_All()
		 {
		 	 return DALYear.Get_All();
		 }

		 public DataSet BALYear_Get_By_YearID(int YearID)
		 {
		 	 return DALYear.Get_By_YearID(YearID);
		 }

		 public int BALYear_SET(EntityYear entity) 
		 {
			 return DALYear.SET(entity);
		 }

		 public int BALYear_Update(EntityYear entity) 
		 {
			 return DALYear.Update(entity);
		 }

		 public int BALYear_Delete_By_YearID(int YearID) 
		 {
			 return DALYear.Delete_By_YearID(YearID);
		 }

	}
}
