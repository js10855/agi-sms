using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientGrade
	 {
		 public BALClientGrade() 
		 {
		 }

		 public DataSet BALClientGrade_Get_All()
		 {
		 	 return DALClientGrade.Get_All();
		 }

		 public DataSet BALClientGrade_Get_By_ClientGradeID(int ClientGradeID)
		 {
		 	 return DALClientGrade.Get_By_ClientGradeID(ClientGradeID);
		 }

		 public int BALClientGrade_SET(EntityClientGrade entity) 
		 {
			 return DALClientGrade.SET(entity);
		 }

		 public int BALClientGrade_Update(EntityClientGrade entity) 
		 {
			 return DALClientGrade.Update(entity);
		 }

		 public int BALClientGrade_Delete_By_ClientGradeID(int ClientGradeID) 
		 {
			 return DALClientGrade.Delete_By_ClientGradeID(ClientGradeID);
		 }
        public DataSet BALClientGrade_Get_By_ClientID(int ClientID)
        {
            return DALClientGrade.Get_By_ClientID(ClientID);
        }
    }
}
