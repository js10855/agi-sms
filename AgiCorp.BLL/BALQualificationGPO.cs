using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALQualificationGPO
	 {
		 public BALQualificationGPO() 
		 {
		 }

		 public DataSet BALQualificationGPO_Get_All()
		 {
		 	 return DALQualificationGPO.Get_All();
		 }

		 public DataSet BALQualificationGPO_Get_By_QualificationGPOID(int QualificationGPOID)
		 {
		 	 return DALQualificationGPO.Get_By_QualificationGPOID(QualificationGPOID);
		 }

        public DataSet BALQualificationGPO_Get_By_QualificationID(int QualificationID)
        {
            return DALQualificationGPO.Get_By_QualificationID(QualificationID);
        }

        public int BALQualificationGPO_SET(EntityQualificationGPO entity, out string errorMsg) 
		 {
			 return DALQualificationGPO.SET(entity, out errorMsg);
		 }

		 public int BALQualificationGPO_Update(EntityQualificationGPO entity) 
		 {
			 return DALQualificationGPO.Update(entity);
		 }

		 public int BALQualificationGPO_Delete_By_QualificationGPOID(int QualificationGPOID) 
		 {
			 return DALQualificationGPO.Delete_By_QualificationGPOID(QualificationGPOID);
		 }

	}
}
