using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALStaff
	 {
		 public BALStaff() 
		 {
		 }

		 public DataSet BALStaff_Get_All()
		 {
		 	 return DALStaff.Get_All();
		 }

		 public DataSet BALStaff_Get_By_StaffID(int StaffID)
		 {
		 	 return DALStaff.Get_By_StaffID(StaffID);
		 }

		 public int BALStaff_SET(EntityStaff entity) 
		 {
			 return DALStaff.SET(entity);
		 }

		 public int BALStaff_Update(EntityStaff entity) 
		 {
			 return DALStaff.Update(entity);
		 }

		 public int BALStaff_Delete_By_StaffID(int StaffID) 
		 {
			 return DALStaff.Delete_By_StaffID(StaffID);
		 }

        public Boolean Check_For_UniqueCode(string Code, string StaffID)
        {
            return DALStaff.Check_For_UniqueCode(Code, StaffID);
        }


    }
}
