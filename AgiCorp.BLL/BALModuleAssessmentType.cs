using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALModuleAssessmentType
	 {
		 public BALModuleAssessmentType() 
		 {
		 }

		 public DataSet BALModuleAssessmentType_Get_All()
		 {
		 	 return DALModuleAssessmentType.Get_All();
		 }

		 public DataSet BALModuleAssessmentType_Get_By_ModuleAssessmentTypeID(int ModuleAssessmentTypeID)
		 {
		 	 return DALModuleAssessmentType.Get_By_ModuleAssessmentTypeID(ModuleAssessmentTypeID);
		 }

		 public int BALModuleAssessmentType_SET(EntityModuleAssessmentType entity) 
		 {
			 return DALModuleAssessmentType.SET(entity);
		 }

		 public int BALModuleAssessmentType_Update(EntityModuleAssessmentType entity) 
		 {
			 return DALModuleAssessmentType.Update(entity);
		 }

		 public int BALModuleAssessmentType_Delete_By_ModuleAssessmentTypeID(int ModuleAssessmentTypeID) 
		 {
			 return DALModuleAssessmentType.Delete_By_ModuleAssessmentTypeID(ModuleAssessmentTypeID);
		 }

	}
}
