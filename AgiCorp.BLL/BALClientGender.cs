using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientGender
	 {
		 public BALClientGender() 
		 {
		 }

		 public DataSet BALClientGender_Get_All()
		 {
		 	 return DALClientGender.Get_All();
		 }

		 public DataSet BALClientGender_Get_By_ClientGenderID(int ClientGenderID)
		 {
		 	 return DALClientGender.Get_By_ClientGenderID(ClientGenderID);
		 }

		 public int BALClientGender_SET(EntityClientGender entity) 
		 {
			 return DALClientGender.SET(entity);
		 }

		 public int BALClientGender_Update(EntityClientGender entity) 
		 {
			 return DALClientGender.Update(entity);
		 }

		 public int BALClientGender_Delete_By_ClientGenderID(int ClientGenderID) 
		 {
			 return DALClientGender.Delete_By_ClientGenderID(ClientGenderID);
		 }

	}
}
