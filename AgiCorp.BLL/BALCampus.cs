using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALCampus
	 {
		 public BALCampus() 
		 {
		 }

		 public DataSet BALCampus_Get_All()
		 {
		 	 return DALCampus.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string CampusID)
        {
            return DALCampus.Check_For_UniqueCode(Code, CampusID);
        }
        public DataSet BALCampus_Get_By_CampusID(int CampusID)
		 {
		 	 return DALCampus.Get_By_CampusID(CampusID);
		 }

		 public int BALCampus_SET(EntityCampus entity) 
		 {
			 return DALCampus.SET(entity);
		 }

		 public int BALCampus_Update(EntityCampus entity) 
		 {
			 return DALCampus.Update(entity);
		 }

		 public int BALCampus_Delete_By_CampusID(int CampusID) 
		 {
			 return DALCampus.Delete_By_CampusID(CampusID);
		 }

	}
}
