using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALDepartmentBuilding
	 {
		 public BALDepartmentBuilding() 
		 {
		 }

		 public DataSet BALDepartmentBuilding_Get_All()
		 {
		 	 return DALDepartmentBuilding.Get_All();
		 }

        public DataSet BALDepartmentBuilding_Get_By_DepartmentID(int DepartmentID)
        {
            return DALDepartmentBuilding.Get_By_DepartmentID(DepartmentID);
        }
        public DataSet BALDepartmentBuilding_Get_By_DepartmentBuildingID(int DepartmentBuildingID)
		 {
		 	 return DALDepartmentBuilding.Get_By_DepartmentBuildingID(DepartmentBuildingID);
		 }

		 public int BALDepartmentBuilding_SET(EntityDepartmentBuilding entity, out string errorMsg) 
		 {
			 return DALDepartmentBuilding.SET(entity, out errorMsg);
		 }

		 public int BALDepartmentBuilding_Update(EntityDepartmentBuilding entity) 
		 {
			 return DALDepartmentBuilding.Update(entity);
		 }

		 public int BALDepartmentBuilding_Delete_By_DepartmentBuildingID(int DepartmentBuildingID) 
		 {
			 return DALDepartmentBuilding.Delete_By_DepartmentBuildingID(DepartmentBuildingID);
		 }

	}
}
