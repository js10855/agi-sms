using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALBuildingClass
	 {
		 public BALBuildingClass() 
		 {
		 }

		 public DataSet BALBuildingClass_Get_All()
		 {
		 	 return DALBuildingClass.Get_All();
		 }

		 public DataSet BALBuildingClass_Get_By_BuildingClassID(int BuildingClassID)
		 {
		 	 return DALBuildingClass.Get_By_BuildingClassID(BuildingClassID);
		 }

        public DataSet BALBuildingClass_Get_By_BuildingID(int BuildingID)
        {
            return DALBuildingClass.Get_By_BuildingID(BuildingID);
        }

        public int BALBuildingClass_SET(EntityBuildingClass entity, out string errorMsg) 
		 {
			 return DALBuildingClass.SET(entity, out errorMsg);
		 }


		 public int BALBuildingClass_Update(EntityBuildingClass entity) 
		 {
			 return DALBuildingClass.Update(entity);
		 }

		 public int BALBuildingClass_Delete_By_BuildingClassID(int BuildingClassID) 
		 {
			 return DALBuildingClass.Delete_By_BuildingClassID(BuildingClassID);
		 }

	}
}
