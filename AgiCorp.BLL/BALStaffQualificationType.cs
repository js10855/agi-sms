using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALStaffQualificationType
	 {
		 public BALStaffQualificationType() 
		 {
		 }

		 public DataSet BALStaffQualificationType_Get_All()
		 {
		 	 return DALStaffQualificationType.Get_All();
		 }

		 public DataSet BALStaffQualificationType_Get_By_StaffQualificationTypeID(int StaffQualificationTypeID)
		 {
		 	 return DALStaffQualificationType.Get_By_StaffQualificationTypeID(StaffQualificationTypeID);
		 }

		 public int BALStaffQualificationType_SET(EntityStaffQualificationType entity) 
		 {
			 return DALStaffQualificationType.SET(entity);
		 }

		 public int BALStaffQualificationType_Update(EntityStaffQualificationType entity) 
		 {
			 return DALStaffQualificationType.Update(entity);
		 }

		 public int BALStaffQualificationType_Delete_By_StaffQualificationTypeID(int StaffQualificationTypeID) 
		 {
			 return DALStaffQualificationType.Delete_By_StaffQualificationTypeID(StaffQualificationTypeID);
		 }

	}
}
