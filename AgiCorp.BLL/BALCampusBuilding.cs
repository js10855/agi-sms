using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALCampusBuilding
	 {
		 public BALCampusBuilding() 
		 {
		 }

		 public DataSet BALCampusBuilding_Get_All()
		 {
		 	 return DALCampusBuilding.Get_All();
		 }

		 public DataSet BALCampusBuilding_Get_By_CampusBuildingID(int CampusBuildingID)
		 {
		 	 return DALCampusBuilding.Get_By_CampusBuildingID(CampusBuildingID);
		 }

        public DataSet BALCampusBuilding_Get_By_CampusID(int CampusID)
        {
            return DALCampusBuilding.Get_By_CampusID(CampusID);
        }

        public int BALCampusBuilding_SET(EntityCampusBuilding entity, out string errorMsg) 
		 {
			 return DALCampusBuilding.SET(entity, out errorMsg);
		 }

		 public int BALCampusBuilding_Update(EntityCampusBuilding entity) 
		 {
			 return DALCampusBuilding.Update(entity);
		 }

		 public int BALCampusBuilding_Delete_By_CampusBuildingID(int CampusBuildingID) 
		 {
			 return DALCampusBuilding.Delete_By_CampusBuildingID(CampusBuildingID);
		 }

	}
}
