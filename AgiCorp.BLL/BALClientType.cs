using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientType
	 {
		 public BALClientType() 
		 {
		 }

		 public DataSet BALClientType_Get_All()
		 {
		 	 return DALClientType.Get_All();
		 }

		 public DataSet BALClientType_Get_By_ClientTypeID(int ClientTypeID)
		 {
		 	 return DALClientType.Get_By_ClientTypeID(ClientTypeID);
		 }

		 public int BALClientType_SET(EntityClientType entity) 
		 {
			 return DALClientType.SET(entity);
		 }

		 public int BALClientType_Update(EntityClientType entity) 
		 {
			 return DALClientType.Update(entity);
		 }

		 public int BALClientType_Delete_By_ClientTypeID(int ClientTypeID) 
		 {
			 return DALClientType.Delete_By_ClientTypeID(ClientTypeID);
		 }

	}
}
