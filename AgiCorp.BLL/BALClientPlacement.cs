using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientPlacement
	 {
		 public BALClientPlacement() 
		 {
		 }

		 public DataSet BALClientPlacement_Get_All()
		 {
		 	 return DALClientPlacement.Get_All();
		 }

		 public DataSet BALClientPlacement_Get_By_ClientPlacementID(int ClientPlacementID)
		 {
		 	 return DALClientPlacement.Get_By_ClientPlacementID(ClientPlacementID);
		 }

		 public int BALClientPlacement_SET(EntityClientPlacement entity, out string errorMsg) 
		 {
			 return DALClientPlacement.SET(entity, out errorMsg);
		 }

		 public int BALClientPlacement_Update(EntityClientPlacement entity) 
		 {
			 return DALClientPlacement.Update(entity);
		 }

		 public int BALClientPlacement_Delete_By_ClientPlacementID(int ClientPlacementID) 
		 {
			 return DALClientPlacement.Delete_By_ClientPlacementID(ClientPlacementID);
		 }
        public DataSet BALClientPlacement_Get_By_ClientID(int ClientID)
        {
            return DALClientPlacement.Get_By_ClientID(ClientID);
        }

        public DataSet BALClientPlacement_Get_By_PlacementID(int PlacementID)
        {
            return DALClientPlacement.Get_By_PlacementID(PlacementID);
        }

    }
}
