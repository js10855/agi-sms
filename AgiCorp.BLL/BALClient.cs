using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClient
	 {
		 public BALClient() 
		 {
		 }

		 public DataSet BALClient_Get_All()
		 {
		 	 return DALClient.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string StudentReference, string ClientID)
        {
            return DALClient.Check_For_UniqueCode(StudentReference, ClientID);
        }

        public DataSet BALClient_Get_By_ClientID(int ClientID)
		 {
		 	 return DALClient.Get_By_ClientID(ClientID);
		 }

		 public int BALClient_SET(EntityClient entity) 
		 {
			 return DALClient.SET(entity);
		 }

		 public int BALClient_Update(EntityClient entity) 
		 {
			 return DALClient.Update(entity);
		 }

		 public int BALClient_Delete_By_ClientID(int ClientID) 
		 {
			 return DALClient.Delete_By_ClientID(ClientID);
		 }
        public DataSet BALClient_GetStudentType()
        {
            return DALClient.BALClient_GetStudentType();
        }
        

    }
}
