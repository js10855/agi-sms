using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALResidence
	 {
		 public BALResidence() 
		 {
		 }

		 public DataSet BALResidence_Get_All()
		 {
		 	 return DALResidence.Get_All();
		 }

		 public DataSet BALResidence_Get_By_ResidenceID(int ResidenceID)
		 {
		 	 return DALResidence.Get_By_ResidenceID(ResidenceID);
		 }

		 public int BALResidence_SET(EntityResidence entity) 
		 {
			 return DALResidence.SET(entity);
		 }

		 public int BALResidence_Update(EntityResidence entity) 
		 {
			 return DALResidence.Update(entity);
		 }

		 public int BALResidence_Delete_By_ResidenceID(int ResidenceID) 
		 {
			 return DALResidence.Delete_By_ResidenceID(ResidenceID);
		 }

	}
}
