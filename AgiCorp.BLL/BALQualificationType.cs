using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALQualificationType
	 {
		 public BALQualificationType() 
		 {
		 }

		 public DataSet BALQualificationType_Get_All()
		 {
		 	 return DALQualificationType.Get_All();
		 }

		 public DataSet BALQualificationType_Get_By_QualificationTypeID(int QualificationTypeID)
		 {
		 	 return DALQualificationType.Get_By_QualificationTypeID(QualificationTypeID);
		 }

		 public int BALQualificationType_SET(EntityQualificationType entity) 
		 {
			 return DALQualificationType.SET(entity);
		 }

		 public int BALQualificationType_Update(EntityQualificationType entity) 
		 {
			 return DALQualificationType.Update(entity);
		 }

		 public int BALQualificationType_Delete_By_QualificationTypeID(int QualificationTypeID) 
		 {
			 return DALQualificationType.Delete_By_QualificationTypeID(QualificationTypeID);
		 }
        public Boolean Check_For_UniqueCode(string Code, string QualificationTypeID)
        {
            return DALQualificationType.Check_For_UniqueCode(Code, QualificationTypeID);
        }
    }
}
