﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALCountry
	 {
		 public BALCountry() 
		 {
		 }

		 public DataSet BALCountry_Get_All()
		 {
		 	 return DALCountry.Get_All();
		 }

		 public DataSet BALCountry_Get_By_CountryID(int CountryID)
		 {
		 	 return DALCountry.Get_By_CountryID(CountryID);
		 }

		 public int BALCountry_SET(EntityCountry entity) 
		 {
			 return DALCountry.SET(entity);
		 }

		 public int BALCountry_Update(EntityCountry entity) 
		 {
			 return DALCountry.Update(entity);
		 }

		 public int BALCountry_Delete_By_CountryID(int CountryID) 
		 {
			 return DALCountry.Delete_By_CountryID(CountryID);
		 }

	}
}
