using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientAddress
	 {
		 public BALClientAddress() 
		 {
		 }

		 public DataSet BALClientAddress_Get_All()
		 {
		 	 return DALClientAddress.Get_All();
		 }

		 public DataSet BALClientAddress_Get_By_ClientAddressID(int ClientAddressID)
		 {
		 	 return DALClientAddress.Get_By_ClientAddressID(ClientAddressID);
		 }

		 public int BALClientAddress_SET(EntityClientAddress entity) 
		 {
			 return DALClientAddress.SET(entity);
		 }

		 public int BALClientAddress_Update(EntityClientAddress entity) 
		 {
			 return DALClientAddress.Update(entity);
		 }

		 public int BALClientAddress_Delete_By_ClientAddressID(int ClientAddressID) 
		 {
			 return DALClientAddress.Delete_By_ClientAddressID(ClientAddressID);
		 }

        public DataSet BALClientAddress_Get_By_ClientID(int ClientID)
        {
            return DALClientAddress.Get_By_ClientID(ClientID);
        }

    }
}
