using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClass
	 {
		 public BALClass() 
		 {
		 }

		 public DataSet BALClass_Get_All()
		 {
		 	 return DALClass.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string ClassID)
        {
            return DALClass.Check_For_UniqueCode(Code, ClassID);
        }

        public DataSet BALClass_Get_By_ClassID(int ClassID)
		 {
		 	 return DALClass.Get_By_ClassID(ClassID);
		 }

		 public int BALClass_SET(EntityClass entity) 
		 {
			 return DALClass.SET(entity);
		 }

		 public int BALClass_Update(EntityClass entity) 
		 {
			 return DALClass.Update(entity);
		 }

		 public int BALClass_Delete_By_ClassID(int ClassID) 
		 {
			 return DALClass.Delete_By_ClassID(ClassID);
		 }

	}
}
