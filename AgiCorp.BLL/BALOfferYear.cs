using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALOfferYear
	 {
		 public BALOfferYear() 
		 {
		 }

		 public DataSet BALOfferYear_Get_All()
		 {
		 	 return DALOfferYear.Get_All();
		 }

		 public DataSet BALOfferYear_Get_By_OfferYearID(int OfferYearID)
		 {
		 	 return DALOfferYear.Get_By_OfferYearID(OfferYearID);
		 }

		 public int BALOfferYear_SET(EntityOfferYear entity) 
		 {
			 return DALOfferYear.SET(entity);
		 }

		 public int BALOfferYear_Update(EntityOfferYear entity) 
		 {
			 return DALOfferYear.Update(entity);
		 }

		 public int BALOfferYear_Delete_By_OfferYearID(int OfferYearID) 
		 {
			 return DALOfferYear.Delete_By_OfferYearID(OfferYearID);
		 }

	}
}
