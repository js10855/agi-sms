using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientOfferOfPlace
	 {
		 public BALClientOfferOfPlace() 
		 {
		 }

		 public DataSet BALClientOfferOfPlace_Get_All()
		 {
		 	 return DALClientOfferOfPlace.Get_All();
		 }

		 public DataSet BALClientOfferOfPlace_Get_By_ClientOfferOfPlaceID(int ClientOfferOfPlaceID)
		 {
		 	 return DALClientOfferOfPlace.Get_By_ClientOfferOfPlaceID(ClientOfferOfPlaceID);
		 }

		 public int BALClientOfferOfPlace_SET(EntityClientOfferOfPlace entity) 
		 {
			 return DALClientOfferOfPlace.SET(entity);
		 }

		 public int BALClientOfferOfPlace_Update(EntityClientOfferOfPlace entity) 
		 {
			 return DALClientOfferOfPlace.Update(entity);
		 }

		 public int BALClientOfferOfPlace_Delete_By_ClientOfferOfPlaceID(int ClientOfferOfPlaceID) 
		 {
			 return DALClientOfferOfPlace.Delete_By_ClientOfferOfPlaceID(ClientOfferOfPlaceID);
		 }
        public DataSet BALClientOfferOfPlace_Get_By_ClientID(int ClientID)
        {
            return DALClientOfferOfPlace.Get_By_ClientID(ClientID);
        }
    }
}
