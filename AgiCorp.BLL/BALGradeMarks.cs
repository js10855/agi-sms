using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALGradeMarks
	 {
		 public BALGradeMarks() 
		 {
		 }

		 public DataSet BALGradeMarks_Get_All()
		 {
		 	 return DALGradeMarks.Get_All();
		 }

		 public DataSet BALGradeMarks_Get_By_GradeMarksID(int GradeMarksID)
		 {
		 	 return DALGradeMarks.Get_By_GradeMarksID(GradeMarksID);
		 }

        public DataSet BALGradeMarks_Get_By_GradeID(int GradeID)
        {
            return DALGradeMarks.Get_By_GradeID(GradeID);
        }

        public int BALGradeMarks_SET(EntityGradeMarks entity) 
		 {
			 return DALGradeMarks.SET(entity);
		 }

		 public int BALGradeMarks_Update(EntityGradeMarks entity) 
		 {
			 return DALGradeMarks.Update(entity);
		 }

		 public int BALGradeMarks_Delete_By_GradeMarksID(int GradeMarksID) 
		 {
			 return DALGradeMarks.Delete_By_GradeMarksID(GradeMarksID);
		 }

	}
}
