using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALEthnicity
	 {
		 public BALEthnicity() 
		 {
		 }

		 public DataSet BALEthnicity_Get_All()
		 {
		 	 return DALEthnicity.Get_All();
		 }

		 public DataSet BALEthnicity_Get_By_EthnicityID(int EthnicityID)
		 {
		 	 return DALEthnicity.Get_By_EthnicityID(EthnicityID);
		 }

		 public int BALEthnicity_SET(EntityEthnicity entity) 
		 {
			 return DALEthnicity.SET(entity);
		 }

		 public int BALEthnicity_Update(EntityEthnicity entity) 
		 {
			 return DALEthnicity.Update(entity);
		 }

		 public int BALEthnicity_Delete_By_EthnicityID(int EthnicityID) 
		 {
			 return DALEthnicity.Delete_By_EthnicityID(EthnicityID);
		 }

	}
}
