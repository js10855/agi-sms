using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALGPO
	 {
		 public BALGPO() 
		 {
		 }

		 public DataSet BALGPO_Get_All()
		 {
		 	 return DALGPO.Get_All();
		 }

		 public DataSet BALGPO_Get_By_GPOID(int GPOID)
		 {
		 	 return DALGPO.Get_By_GPOID(GPOID);
		 }

		 public int BALGPO_SET(EntityGPO entity) 
		 {
			 return DALGPO.SET(entity);
		 }

		 public int BALGPO_Update(EntityGPO entity) 
		 {
			 return DALGPO.Update(entity);
		 }

		 public int BALGPO_Delete_By_GPOID(int GPOID) 
		 {
			 return DALGPO.Delete_By_GPOID(GPOID);
		 }
        public Boolean Check_For_UniqueCode(string Code, string GPOID)
        {
            return DALGPO.Check_For_UniqueCode(Code, GPOID);
        }

    }
}
