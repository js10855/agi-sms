using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALProgrammeModule
	 {
		 public BALProgrammeModule() 
		 {
		 }

		 public DataSet BALProgrammeModule_Get_All()
		 {
		 	 return DALProgrammeModule.Get_All();
		 }

		 public DataSet BALProgrammeModule_Get_By_ProgrammeModuleID(int ProgrammeModuleID)
		 {
		 	 return DALProgrammeModule.Get_By_ProgrammeModuleID(ProgrammeModuleID);
		 }

        public DataSet BALProgrammeModule_Get_By_ProgrammeID(int ProgrammeID)
        {
            return DALProgrammeModule.Get_By_ProgrammeID(ProgrammeID);
        }

        public int BALProgrammeModule_SET(EntityProgrammeModule entity, out string errorMsg) 
		 {
			 return DALProgrammeModule.SET(entity, out errorMsg);
		 }

		 public int BALProgrammeModule_Update(EntityProgrammeModule entity) 
		 {
			 return DALProgrammeModule.Update(entity);
		 }

		 public int BALProgrammeModule_Delete_By_ProgrammeModuleID(int ProgrammeModuleID) 
		 {
			 return DALProgrammeModule.Delete_By_ProgrammeModuleID(ProgrammeModuleID);
		 }

	}
}
