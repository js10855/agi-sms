using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALAgent
	 {
		 public BALAgent() 
		 {
		 }

		 public DataSet BALAgent_Get_All()
		 {
		 	 return DALAgent.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string AgentID)
        {
            return DALAgent.Check_For_UniqueCode(Code, AgentID);
        }
        public DataSet BALAgent_Get_By_AgentID(int AgentID)
		 {
		 	 return DALAgent.Get_By_AgentID(AgentID);
		 }

		 public int BALAgent_SET(EntityAgent entity) 
		 {
			 return DALAgent.SET(entity);
		 }

		 public int BALAgent_Update(EntityAgent entity) 
		 {
			 return DALAgent.Update(entity);
		 }

		 public int BALAgent_Delete_By_AgentID(int AgentID) 
		 {
			 return DALAgent.Delete_By_AgentID(AgentID);
		 }

	}
}
