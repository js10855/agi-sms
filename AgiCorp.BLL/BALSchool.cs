using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALSchool
	 {
		 public BALSchool() 
		 {
		 }

		 public DataSet BALSchool_Get_All()
		 {
		 	 return DALSchool.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string SchoolID)
        {
            return DALSchool.Check_For_UniqueCode(Code, SchoolID);
        }

        public DataSet BALSchool_Get_By_SchoolID(int SchoolID)
		 {
		 	 return DALSchool.Get_By_SchoolID(SchoolID);
		 }

		 public int BALSchool_SET(EntitySchool entity) 
		 {
			 return DALSchool.SET(entity);
		 }

		 public int BALSchool_Update(EntitySchool entity) 
		 {
			 return DALSchool.Update(entity);
		 }

		 public int BALSchool_Delete_By_SchoolID(int SchoolID) 
		 {
			 return DALSchool.Delete_By_SchoolID(SchoolID);
		 }

	}
}
