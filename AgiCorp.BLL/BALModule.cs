using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALModule
	 {
		 public BALModule() 
		 {
		 }

		 public DataSet BALModule_Get_All()
		 {
		 	 return DALModule.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string ModuleID)
        {
            return DALModule.Check_For_UniqueCode(Code, ModuleID);
        }

        public DataSet BALModule_Get_By_ModuleID(int ModuleID)
		 {
		 	 return DALModule.Get_By_ModuleID(ModuleID);
		 }

		 public int BALModule_SET(EntityModule entity) 
		 {
			 return DALModule.SET(entity);
		 }

		 public int BALModule_Update(EntityModule entity) 
		 {
			 return DALModule.Update(entity);
		 }

		 public int BALModule_Delete_By_ModuleID(int ModuleID) 
		 {
			 return DALModule.Delete_By_ModuleID(ModuleID);
		 }

	}
}
