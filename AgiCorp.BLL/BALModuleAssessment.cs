using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALModuleAssessment
	 {
		 public BALModuleAssessment() 
		 {
		 }

		 public DataSet BALModuleAssessment_Get_All()
		 {
		 	 return DALModuleAssessment.Get_All();
		 }

		 public DataSet BALModuleAssessment_Get_By_ModuleID(int ModuleID)
		 {
		 	 return DALModuleAssessment.Get_By_ModuleID(ModuleID);
		 }


		 public int BALModuleAssessment_SET(EntityModuleAssessment entity) 
		 {
			 return DALModuleAssessment.SET(entity);
		 }

		 public int BALModuleAssessment_Update(EntityModuleAssessment entity) 
		 {
			 return DALModuleAssessment.Update(entity);
		 }

		 public int BALModuleAssessment_Delete_By_ModuleAssessmentID(int ModuleAssessmentID) 
		 {
			 return DALModuleAssessment.Delete_By_ModuleAssessmentID(ModuleAssessmentID);
		 }

	}
}
