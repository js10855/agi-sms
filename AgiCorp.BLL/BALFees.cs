using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALFees
	 {
		 public BALFees() 
		 {
		 }

		 public DataSet BALFees_Get_All()
		 {
		 	 return DALFees.Get_All();
		 }

        public DataSet BALFees_Get_Fee_Desc_Country()
        {
            return DALFees.Get_Fee_Desc_Country();
        }

        public DataSet BALFees_Get_By_FeeID(int FeeID)
		 {
		 	 return DALFees.Get_By_FeeID(FeeID);
		 }

        public DataSet BALFees_Get_By_ProgrammeID(int ProgrammeID)
        {
            return DALFees.Get_By_ProgrammeID(ProgrammeID);
        }

        public DataSet BALFees_Get_By_ProgrammeID_CountryID(int ProgrammeID,int CountryID)
        {
            return DALFees.Get_By_ProgrammeID_CountryID(ProgrammeID,CountryID);
        }

        public int BALFees_SET(EntityFees entity) 
		 {
			 return DALFees.SET(entity);
		 }

		 public int BALFees_Update(EntityFees entity) 
		 {
			 return DALFees.Update(entity);
		 }

	
        public Boolean Check_For_UniqueCode(string Code, string FeeID)
        {
            return DALFees.Check_For_UniqueCode(Code, FeeID);
        }

        
        public int BALFees_Delete_By_FeesID(int CampusID)
        {
            return DALFees.Delete_By_FeesID(CampusID);
        }
    }
}
