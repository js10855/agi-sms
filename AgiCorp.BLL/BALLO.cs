using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALLO
	 {
		 public BALLO() 
		 {
		 }

		 public DataSet BALLO_Get_All()
		 {
		 	 return DALLO.Get_All();
		 }

		 public DataSet BALLO_Get_By_LOID(int LOID)
		 {
		 	 return DALLO.Get_By_LOID(LOID);
		 }

		 public int BALLO_SET(EntityLO entity) 
		 {
			 return DALLO.SET(entity);
		 }

		 public int BALLO_Update(EntityLO entity) 
		 {
			 return DALLO.Update(entity);
		 }

		 public int BALLO_Delete_By_LOID(int LOID) 
		 {
			 return DALLO.Delete_By_LOID(LOID);
		 }

        public Boolean Check_For_UniqueCode(string Code, string LOID)
        {
            return DALLO.Check_For_UniqueCode(Code, LOID);
        }

    }
}
