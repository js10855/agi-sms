using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientPassport
	 {
		 public BALClientPassport() 
		 {
		 }

		 public DataSet BALClientPassport_Get_All()
		 {
		 	 return DALClientPassport.Get_All();
		 }

        public DataSet BALClientPassport_Get_By_ClientID(int ClientID)
        {
            return DALClientPassport.Get_By_ClientID(ClientID);
        }

        public DataSet BALClientPassport_Get_By_ClientPassportID(int ClientPassportID)
		 {
		 	 return DALClientPassport.Get_By_ClientPassportID(ClientPassportID);
		 }

		 public int BALClientPassport_SET(EntityClientPassport entity) 
		 {
			 return DALClientPassport.SET(entity);
		 }

		 public int BALClientPassport_Update(EntityClientPassport entity) 
		 {
			 return DALClientPassport.Update(entity);
		 }

		 public int BALClientPassport_Delete_By_ClientPassportID(int ClientPassportID) 
		 {
			 return DALClientPassport.Delete_By_ClientPassportID(ClientPassportID);
		 }

	}
}
