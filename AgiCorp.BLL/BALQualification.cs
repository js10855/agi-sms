using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALQualification
	 {
		 public BALQualification() 
		 {
		 }

		 public DataSet BALQualification_Get_All()
		 {
		 	 return DALQualification.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string QualificationID)
        {
            return DALQualification.Check_For_UniqueCode(Code, QualificationID);
        }
        public DataSet BALQualification_Get_By_QualificationID(int QualificationID)
		 {
		 	 return DALQualification.Get_By_QualificationID(QualificationID);
		 }

		 public int BALQualification_SET(EntityQualification entity) 
		 {
			 return DALQualification.SET(entity);
		 }

		 public int BALQualification_Update(EntityQualification entity) 
		 {
			 return DALQualification.Update(entity);
		 }

		 public int BALQualification_Delete_By_QualificationID(int QualificationID) 
		 {
			 return DALQualification.Delete_By_QualificationID(QualificationID);
		 }

	}
}
