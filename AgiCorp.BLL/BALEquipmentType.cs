using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALEquipmentType
	 {
		 public BALEquipmentType() 
		 {
		 }

		 public DataSet BALEquipmentType_Get_All()
		 {
		 	 return DALEquipmentType.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string EquipmentTypeID)
        {
            return DALEquipmentType.Check_For_UniqueCode(Code, EquipmentTypeID);
        }

        public DataSet BALEquipmentType_Get_By_EquipmentTypeID(int EquipmentTypeID)
		 {
		 	 return DALEquipmentType.Get_By_EquipmentTypeID(EquipmentTypeID);
		 }

		 public int BALEquipmentType_SET(EntityEquipmentType entity) 
		 {
			 return DALEquipmentType.SET(entity);
		 }

		 public int BALEquipmentType_Update(EntityEquipmentType entity) 
		 {
			 return DALEquipmentType.Update(entity);
		 }

		 public int BALEquipmentType_Delete_By_EquipmentTypeID(int EquipmentTypeID) 
		 {
			 return DALEquipmentType.Delete_By_EquipmentTypeID(EquipmentTypeID);
		 }

	}
}
