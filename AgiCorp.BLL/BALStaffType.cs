using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALStaffType
	 {
		 public BALStaffType() 
		 {
		 }

		 public DataSet BALStaffType_Get_All()
		 {
		 	 return DALStaffType.Get_All();
		 }

		 public DataSet BALStaffType_Get_By_StaffTypeID(int StaffTypeID)
		 {
		 	 return DALStaffType.Get_By_StaffTypeID(StaffTypeID);
		 }

		 public int BALStaffType_SET(EntityStaffType entity) 
		 {
			 return DALStaffType.SET(entity);
		 }

		 public int BALStaffType_Update(EntityStaffType entity) 
		 {
			 return DALStaffType.Update(entity);
		 }

		 public int BALStaffType_Delete_By_StaffTypeID(int StaffTypeID) 
		 {
			 return DALStaffType.Delete_By_StaffTypeID(StaffTypeID);
		 }

        public Boolean Check_For_UniqueCode(string Code, string StaffTypeID)
        {
            return DALStaffType.Check_For_UniqueCode(Code, StaffTypeID);
        }


    }
}
