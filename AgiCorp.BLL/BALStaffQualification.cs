using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALStaffQualification
	 {
		 public BALStaffQualification() 
		 {
		 }

		 public DataSet BALStaffQualification_Get_All()
		 {
		 	 return DALStaffQualification.Get_All();
		 }

		 public DataSet BALStaffQualification_Get_By_StaffQualificationID(int StaffQualificationID)
		 {
		 	 return DALStaffQualification.Get_By_StaffQualificationID(StaffQualificationID);
		 }

        public DataSet BALStaffQualification_Get_By_StaffID(int StaffID)
        {
            return DALStaffQualification.Get_By_StaffID(StaffID);
        }

        public int BALStaffQualification_SET(EntityStaffQualification entity) 
		 {
			 return DALStaffQualification.SET(entity);
		 }

		 public int BALStaffQualification_Update(EntityStaffQualification entity) 
		 {
			 return DALStaffQualification.Update(entity);
		 }

		 public int BALStaffQualification_Delete_By_StaffQualificationID(int StaffQualificationID) 
		 {
			 return DALStaffQualification.Delete_By_StaffQualificationID(StaffQualificationID);
		 }

	}
}
