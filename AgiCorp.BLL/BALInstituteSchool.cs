using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALInstituteSchool
	 {
		 public BALInstituteSchool() 
		 {
		 }

		 public DataSet BALInstituteSchool_Get_All()
		 {
		 	 return DALInstituteSchool.Get_All();
		 }

		 public DataSet BALInstituteSchool_Get_By_InstituteSchoolID(int InstituteSchoolID)
		 {
		 	 return DALInstituteSchool.Get_By_InstituteSchoolID(InstituteSchoolID);
		 }

        public DataSet BALInstituteSchool_Get_By_InstituteID(int InstituteID)
        {
            return DALInstituteSchool.Get_By_InstituteID(InstituteID);
        }

        public int BALInstituteSchool_SET(EntityInstituteSchool entity,out string errorMsg) 
		 {
			 return DALInstituteSchool.SET(entity,out errorMsg);
		 }

		 public int BALInstituteSchool_Update(EntityInstituteSchool entity) 
		 {
			 return DALInstituteSchool.Update(entity);
		 }

		 public int BALInstituteSchool_Delete_By_InstituteSchoolID(int InstituteSchoolID) 
		 {
			 return DALInstituteSchool.Delete_By_InstituteSchoolID(InstituteSchoolID);
		 }

	}
}
