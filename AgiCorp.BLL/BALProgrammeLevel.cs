using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALProgrammeLevel
	 {
		 public BALProgrammeLevel() 
		 {
		 }

		 public DataSet BALProgrammeLevel_Get_All()
		 {
		 	 return DALProgrammeLevel.Get_All();
		 }

		 public DataSet BALProgrammeLevel_Get_By_ProgrammeLevelID(int ProgrammeLevelID)
		 {
		 	 return DALProgrammeLevel.Get_By_ProgrammeLevelID(ProgrammeLevelID);
		 }

		 public int BALProgrammeLevel_SET(EntityProgrammeLevel entity) 
		 {
			 return DALProgrammeLevel.SET(entity);
		 }

		 public int BALProgrammeLevel_Update(EntityProgrammeLevel entity) 
		 {
			 return DALProgrammeLevel.Update(entity);
		 }

		 public int BALProgrammeLevel_Delete_By_ProgrammeLevelID(int ProgrammeLevelID) 
		 {
			 return DALProgrammeLevel.Delete_By_ProgrammeLevelID(ProgrammeLevelID);
		 }

	}
}
