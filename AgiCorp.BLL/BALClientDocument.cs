using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientDocument
	 {
		 public BALClientDocument() 
		 {
		 }

		 public DataSet BALClientDocument_Get_All()
		 {
		 	 return DALClientDocument.Get_All();
		 }

		 public DataSet BALClientDocument_Get_By_ClientDocumentID(int ClientDocumentID)
		 {
		 	 return DALClientDocument.Get_By_ClientDocumentID(ClientDocumentID);
		 }

		 public int BALClientDocument_SET(EntityClientDocument entity) 
		 {
			 return DALClientDocument.SET(entity);
		 }

		 public int BALClientDocument_Update(EntityClientDocument entity) 
		 {
			 return DALClientDocument.Update(entity);
		 }

		 public int BALClientDocument_Delete_By_ClientDocumentID(int ClientDocumentID) 
		 {
			 return DALClientDocument.Delete_By_ClientDocumentID(ClientDocumentID);
		 }
        public DataSet BALClientDocument_Get_By_ClientID(int ClientID)
        {
            return DALClientDocument.Get_By_ClientID(ClientID);
        }
    }
}
