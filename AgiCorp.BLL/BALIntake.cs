using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALIntake
	 {
		 public BALIntake() 
		 {
		 }

		 public DataSet BALIntake_Get_All()
		 {
		 	 return DALIntake.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string IntakeID)
        {
            return DALIntake.Check_For_UniqueCode(Code, IntakeID);
        }
        public DataSet BALIntake_Get_By_IntakeID(int IntakeID)
		 {
		 	 return DALIntake.Get_By_IntakeID(IntakeID);
		 }

		 public int BALIntake_SET(EntityIntake entity) 
		 {
			 return DALIntake.SET(entity);
		 }

		 public int BALIntake_Update(EntityIntake entity) 
		 {
			 return DALIntake.Update(entity);
		 }

		 public int BALIntake_Delete_By_IntakeID(int IntakeID) 
		 {
			 return DALIntake.Delete_By_IntakeID(IntakeID);
		 }

	}
}
