using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALQualificationLevel
	 {
		 public BALQualificationLevel() 
		 {
		 }

		 public DataSet BALQualificationLevel_Get_All()
		 {
		 	 return DALQualificationLevel.Get_All();
		 }

		 public DataSet BALQualificationLevel_Get_By_QualificationLevelID(int QualificationLevelID)
		 {
		 	 return DALQualificationLevel.Get_By_QualificationLevelID(QualificationLevelID);
		 }

		 public int BALQualificationLevel_SET(EntityQualificationLevel entity) 
		 {
			 return DALQualificationLevel.SET(entity);
		 }

		 public int BALQualificationLevel_Update(EntityQualificationLevel entity) 
		 {
			 return DALQualificationLevel.Update(entity);
		 }

		 public int BALQualificationLevel_Delete_By_QualificationLevelID(int QualificationLevelID) 
		 {
			 return DALQualificationLevel.Delete_By_QualificationLevelID(QualificationLevelID);
		 }

	}
}
