using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALStaffQualificationLevel
	 {
		 public BALStaffQualificationLevel() 
		 {
		 }

		 public DataSet BALStaffQualificationLevel_Get_All()
		 {
		 	 return DALStaffQualificationLevel.Get_All();
		 }

		 public DataSet BALStaffQualificationLevel_Get_By_StaffQualificationLevelID(int StaffQualificationLevelID)
		 {
		 	 return DALStaffQualificationLevel.Get_By_StaffQualificationLevelID(StaffQualificationLevelID);
		 }

		 public int BALStaffQualificationLevel_SET(EntityStaffQualificationLevel entity) 
		 {
			 return DALStaffQualificationLevel.SET(entity);
		 }

		 public int BALStaffQualificationLevel_Update(EntityStaffQualificationLevel entity) 
		 {
			 return DALStaffQualificationLevel.Update(entity);
		 }

		 public int BALStaffQualificationLevel_Delete_By_StaffQualificationLevelID(int StaffQualificationLevelID) 
		 {
			 return DALStaffQualificationLevel.Delete_By_StaffQualificationLevelID(StaffQualificationLevelID);
		 }

	}
}
