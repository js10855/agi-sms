using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALModuleAssessmentFormat
	 {
		 public BALModuleAssessmentFormat() 
		 {
		 }

		 public DataSet BALModuleAssessmentFormat_Get_All()
		 {
		 	 return DALModuleAssessmentFormat.Get_All();
		 }

		 public DataSet BALModuleAssessmentFormat_Get_By_ModuleAssessmentFormatID(int ModuleAssessmentFormatID)
		 {
		 	 return DALModuleAssessmentFormat.Get_By_ModuleAssessmentFormatID(ModuleAssessmentFormatID);
		 }

		 public int BALModuleAssessmentFormat_SET(EntityModuleAssessmentFormat entity) 
		 {
			 return DALModuleAssessmentFormat.SET(entity);
		 }

		 public int BALModuleAssessmentFormat_Update(EntityModuleAssessmentFormat entity) 
		 {
			 return DALModuleAssessmentFormat.Update(entity);
		 }

		 public int BALModuleAssessmentFormat_Delete_By_ModuleAssessmentFormatID(int ModuleAssessmentFormatID) 
		 {
			 return DALModuleAssessmentFormat.Delete_By_ModuleAssessmentFormatID(ModuleAssessmentFormatID);
		 }

	}
}
