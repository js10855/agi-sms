using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALBuildingDepartment
	 {
		 public BALBuildingDepartment() 
		 {
		 }

		 public DataSet BALBuildingDepartment_Get_All()
		 {
		 	 return DALBuildingDepartment.Get_All();
		 }

		 public DataSet BALBuildingDepartment_Get_By_BuildingDepartmentID(int BuildingDepartmentID)
		 {
		 	 return DALBuildingDepartment.Get_By_BuildingDepartmentID(BuildingDepartmentID);
		 }

		 public int BALBuildingDepartment_SET(EntityBuildingDepartment entity, out string errorMsg) 
		 {
			 return DALBuildingDepartment.SET(entity, out errorMsg);
		 }

        public DataSet BALBuildingDepartment_Get_By_BuildingID(int BuildingID)
        {
            return DALBuildingDepartment.Get_By_BuildingID(BuildingID);
        }

        

        public int BALBuildingDepartment_Update(EntityBuildingDepartment entity) 
		 {
			 return DALBuildingDepartment.Update(entity);
		 }

		 public int BALBuildingDepartment_Delete_By_BuildingDepartmentID(int BuildingDepartmentID) 
		 {
			 return DALBuildingDepartment.Delete_By_BuildingDepartmentID(BuildingDepartmentID);
		 }

	}
}
