﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
   public class BALInstitution
    {
        public BALInstitution()
        {
        }

        public DataSet BALInstitution_Get_All()
        {
            return DALInstitution.Get_All();
        }

        public Boolean Check_For_UniqueCode(string Code,string InstitutionID)
        {
            return DALInstitution.Check_For_UniqueCode(Code,InstitutionID);
        }

        public DataSet BALInstitution_Get_By_InstitutionID(int InstitutionID)
        {
            return DALInstitution.Get_By_InstitutionID(InstitutionID);
        }

        public int BALInstitution_SET(EntityInstitution entity)
        {
            return DALInstitution.SET(entity);
        }

        public int BALInstitution_Update(EntityInstitution entity)
        {
            return DALInstitution.Update(entity);
        }

        public int BALInstitution_Delete_By_InstitutionID(int InstitutionID)
        {
            return DALInstitution.Delete_By_InstitutionID(InstitutionID);
        }

    }
}
