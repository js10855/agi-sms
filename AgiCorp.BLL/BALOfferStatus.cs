using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;


namespace AgiCorp.BLL
{
	 public class BALOfferStatus
	 {
		 public BALOfferStatus() 
		 {
		 }

		 public DataSet BALOfferStatus_Get_All()
		 {
		 	 return DALOfferStatus.Get_All();
		 }

		 public DataSet BALOfferStatus_Get_By_OfferStatusID(int OfferStatusID)
		 {
		 	 return DALOfferStatus.Get_By_OfferStatusID(OfferStatusID);
		 }

		 public int BALOfferStatus_SET(EntityOfferStatus entity) 
		 {
			 return DALOfferStatus.SET(entity);
		 }

		 public int BALOfferStatus_Update(EntityOfferStatus entity) 
		 {
			 return DALOfferStatus.Update(entity);
		 }

		 public int BALOfferStatus_Delete_By_OfferStatusID(int OfferStatusID) 
		 {
			 return DALOfferStatus.Delete_By_OfferStatusID(OfferStatusID);
		 }

	}
}
