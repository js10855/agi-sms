using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALClassEquipment
	 {
		 public BALClassEquipment() 
		 {
		 }

		 public DataSet BALClassEquipment_Get_All()
		 {
		 	 return DALClassEquipment.Get_All();
		 }

        public DataSet BALClassEquipment_Get_By_ClassID(int ClassID)
        {
            return DALClassEquipment.Get_By_ClassID(ClassID);
        }
        public DataSet BALClassEquipment_Get_By_ClassEquipmentID(int ClassEquipmentID)
		 {
		 	 return DALClassEquipment.Get_By_ClassEquipmentID(ClassEquipmentID);
		 }

		 public int BALClassEquipment_SET(EntityClassEquipment entity, out string errorMsg) 
		 {
			 return DALClassEquipment.SET(entity, out errorMsg);
		 }

		 public int BALClassEquipment_Update(EntityClassEquipment entity) 
		 {
			 return DALClassEquipment.Update(entity);
		 }

		 public int BALClassEquipment_Delete_By_ClassEquipmentID(int ClassEquipmentID) 
		 {
			 return DALClassEquipment.Delete_By_ClassEquipmentID(ClassEquipmentID);
		 }

	}
}
