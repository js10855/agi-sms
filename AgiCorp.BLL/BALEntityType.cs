using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALEntityType
	 {
		 public BALEntityType() 
		 {
		 }

		 public DataSet BALEntityType_Get_All()
		 {
		 	 return DALEntityType.Get_All();
		 }

		 public DataSet BALEntityType_Get_By_EntityTypeID(int EntityTypeID)
		 {
		 	 return DALEntityType.Get_By_EntityTypeID(EntityTypeID);
		 }

		 public int BALEntityType_SET(EntityEntityType entity) 
		 {
			 return DALEntityType.SET(entity);
		 }

		 public int BALEntityType_Update(EntityEntityType entity) 
		 {
			 return DALEntityType.Update(entity);
		 }

		 public int BALEntityType_Delete_By_EntityTypeID(int EntityTypeID) 
		 {
			 return DALEntityType.Delete_By_EntityTypeID(EntityTypeID);
		 }

	}
}
