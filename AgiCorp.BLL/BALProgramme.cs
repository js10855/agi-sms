using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALProgramme
	 {
		 public BALProgramme() 
		 {
		 }

		 public DataSet BALProgramme_Get_All()
		 {
		 	 return DALProgramme.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string ProgrammeID)
        {
            return DALProgramme.Check_For_UniqueCode(Code, ProgrammeID);
        }

        public DataSet BALProgramme_Get_By_ProgrammeID(int ProgrammeID)
		 {
		 	 return DALProgramme.Get_By_ProgrammeID(ProgrammeID);
		 }

		 public int BALProgramme_SET(EntityProgramme entity) 
		 {
			 return DALProgramme.SET(entity);
		 }

		 public int BALProgramme_Update(EntityProgramme entity) 
		 {
			 return DALProgramme.Update(entity);
		 }

		 public int BALProgramme_Delete_By_ProgrammeID(int ProgrammeID) 
		 {
			 return DALProgramme.Delete_By_ProgrammeID(ProgrammeID);
		 }

	}
}
