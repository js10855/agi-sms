using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALPlacement
	 {
		 public BALPlacement() 
		 {
		 }

		 public DataSet BALPlacement_Get_All()
		 {
		 	 return DALPlacement.Get_All();
		 }

		 public DataSet BALPlacement_Get_By_PlacementID(int PlacementID)
		 {
		 	 return DALPlacement.Get_By_PlacementID(PlacementID);
		 }

        public Boolean Check_For_UniqueCode(string Code, string PlacementID)
        {
            return DALPlacement.Check_For_UniqueCode(Code, PlacementID);
        }

        public int BALPlacement_SET(EntityPlacement entity) 
		 {
			 return DALPlacement.SET(entity);
		 }

		 public int BALPlacement_Update(EntityPlacement entity) 
		 {
			 return DALPlacement.Update(entity);
		 }

		 public int BALPlacement_Delete_By_PlacementID(int PlacementID) 
		 {
			 return DALPlacement.Delete_By_PlacementID(PlacementID);
		 }

	}
}
