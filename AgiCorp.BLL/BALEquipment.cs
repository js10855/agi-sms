using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALEquipment
	 {
		 public BALEquipment() 
		 {
		 }

		 public DataSet BALEquipment_Get_All()
		 {
		 	 return DALEquipment.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string EquipmentID)
        {
            return DALEquipment.Check_For_UniqueCode(Code, EquipmentID);
        }
        public DataSet BALEquipment_Get_By_EquipmentID(int EquipmentID)
		 {
		 	 return DALEquipment.Get_By_EquipmentID(EquipmentID);
		 }

		 public int BALEquipment_SET(EntityEquipment entity) 
		 {
			 return DALEquipment.SET(entity);
		 }

		 public int BALEquipment_Update(EntityEquipment entity) 
		 {
			 return DALEquipment.Update(entity);
		 }

		 public int BALEquipment_Delete_By_EquipmentID(int EquipmentID) 
		 {
			 return DALEquipment.Delete_By_EquipmentID(EquipmentID);
		 }

	}
}
