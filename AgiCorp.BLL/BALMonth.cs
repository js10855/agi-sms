using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALMonth
	 {
		 public BALMonth() 
		 {
		 }

		 public DataSet BALMonth_Get_All()
		 {
		 	 return DALMonth.Get_All();
		 }

		 public DataSet BALMonth_Get_By_MonthID(int MonthID)
		 {
		 	 return DALMonth.Get_By_MonthID(MonthID);
		 }

		 public int BALMonth_SET(EntityMonth entity) 
		 {
			 return DALMonth.SET(entity);
		 }

		 public int BALMonth_Update(EntityMonth entity) 
		 {
			 return DALMonth.Update(entity);
		 }

		 public int BALMonth_Delete_By_MonthID(int MonthID) 
		 {
			 return DALMonth.Delete_By_MonthID(MonthID);
		 }

	}
}
