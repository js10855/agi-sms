using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALModuleLO
	 {
		 public BALModuleLO() 
		 {
		 }

		 public DataSet BALModuleLO_Get_All()
		 {
		 	 return DALModuleLO.Get_All();
		 }

		 public DataSet BALModuleLO_Get_By_ModuleLOID(int ModuleLOID)
		 {
		 	 return DALModuleLO.Get_By_ModuleLOID(ModuleLOID);
		 }

        public DataSet BALModuleLO_Get_By_ModuleID(int ModuleID)
        {
            return DALModuleLO.Get_By_ModuleID(ModuleID);
        }

        public int BALModuleLO_SET(EntityModuleLO entity, out string errorMsg) 
		 {
			 return DALModuleLO.SET(entity, out errorMsg);
		 }

		 public int BALModuleLO_Update(EntityModuleLO entity) 
		 {
			 return DALModuleLO.Update(entity);
		 }

		 public int BALModuleLO_Delete_By_ModuleLOID(int ModuleLOID) 
		 {
			 return DALModuleLO.Delete_By_ModuleLOID(ModuleLOID);
		 }

	}
}
