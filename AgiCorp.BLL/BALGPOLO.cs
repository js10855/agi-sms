using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALGPOLO
	 {
		 public BALGPOLO() 
		 {
		 }

		 public DataSet BALGPOLO_Get_All()
		 {
		 	 return DALGPOLO.Get_All();
		 }

        public DataSet BALGPOLO_Get_By_GPOID(int GPOID)
        {
            return DALGPOLO.Get_By_GPOID(GPOID);
        }

        public DataSet BALGPOLO_Get_By_GPOLOID(int GPOLOID)
		 {
		 	 return DALGPOLO.Get_By_GPOLOID(GPOLOID);
		 }

		 public int BALGPOLO_SET(EntityGPOLO entity, out string errorMsg) 
		 {
			 return DALGPOLO.SET(entity, out errorMsg);
		 }

		 public int BALGPOLO_Update(EntityGPOLO entity) 
		 {
			 return DALGPOLO.Update(entity);
		 }

		 public int BALGPOLO_Delete_By_GPOLOID(int GPOLOID) 
		 {
			 return DALGPOLO.Delete_By_GPOLOID(GPOLOID);
		 }

	}
}
