using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALClassDepartment
	 {
		 public BALClassDepartment() 
		 {
		 }

		 public DataSet BALClassDepartment_Get_All()
		 {
		 	 return DALClassDepartment.Get_All();
		 }

        public DataSet BALClassDepartment_Get_By_ClassID(int ClassID)
        {
            return DALClassDepartment.Get_By_ClassID(ClassID);
        }
        public DataSet BALClassDepartment_Get_By_ClassDepartmentID(int ClassDepartmentID)
		 {
		 	 return DALClassDepartment.Get_By_ClassDepartmentID(ClassDepartmentID);
		 }

		 public int BALClassDepartment_SET(EntityClassDepartment entity, out string errorMsg) 
		 {
			 return DALClassDepartment.SET(entity, out errorMsg);
		 }

		 public int BALClassDepartment_Update(EntityClassDepartment entity) 
		 {
			 return DALClassDepartment.Update(entity);
		 }

		 public int BALClassDepartment_Delete_By_ClassDepartmentID(int ClassDepartmentID) 
		 {
			 return DALClassDepartment.Delete_By_ClassDepartmentID(ClassDepartmentID);
		 }

	}
}
