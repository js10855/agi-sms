using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALAgentContact
	 {
		 public BALAgentContact() 
		 {
		 }

		 public DataSet BALAgentContact_Get_All()
		 {
		 	 return DALAgentContact.Get_All();
		 }

		 public DataSet BALAgentContact_Get_By_AgentContactID(int AgentContactID)
		 {
		 	 return DALAgentContact.Get_By_AgentContactID(AgentContactID);
		 }

        public DataSet BALAgentContact_Get_By_AgentID(int AgentID)
        {
            return DALAgentContact.Get_By_AgentID(AgentID);
        }

        public int BALAgentContact_SET(EntityAgentContact entity) 
		 {
			 return DALAgentContact.SET(entity);
		 }

		 public int BALAgentContact_Update(EntityAgentContact entity) 
		 {
			 return DALAgentContact.Update(entity);
		 }

		 public int BALAgentContact_Delete_By_AgentContactID(int AgentContactID) 
		 {
			 return DALAgentContact.Delete_By_AgentContactID(AgentContactID);
		 }

	}
}
