using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALAssessments
	 {
		 public BALAssessments() 
		 {
		 }

		 public DataSet BALAssessments_Get_All()
		 {
		 	 return DALAssessments.Get_All();
		 }

		 public DataSet BALAssessments_Get_By_AssessmentsID(int AssessmentsID)
		 {
		 	 return DALAssessments.Get_By_AssessmentsID(AssessmentsID);
		 }

		 public int BALAssessments_SET(EntityAssessments entity) 
		 {
			 return DALAssessments.SET(entity);
		 }

		 public int BALAssessments_Update(EntityAssessments entity) 
		 {
			 return DALAssessments.Update(entity);
		 }

		 public int BALAssessments_Delete_By_AssessmentsID(int AssessmentsID) 
		 {
			 return DALAssessments.Delete_By_AssessmentsID(AssessmentsID);
		 }



     
    }
}
