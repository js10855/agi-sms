using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALQualificationProgramme
	 {
		 public BALQualificationProgramme() 
		 {
		 }

		 public DataSet BALQualificationProgramme_Get_All()
		 {
		 	 return DALQualificationProgramme.Get_All();
		 }

		 public DataSet BALQualificationProgramme_Get_By_QualificationProgrammeID(int QualificationProgrammeID)
		 {
		 	 return DALQualificationProgramme.Get_By_QualificationProgrammeID(QualificationProgrammeID);
		 }

        public DataSet BALQualificationProgramme_Get_By_QualificationID(int QualificationID)
        {
            return DALQualificationProgramme.Get_By_QualificationID(QualificationID);
        }

        public int BALQualificationProgramme_SET(EntityQualificationProgramme entity, out string errorMsg) 
		 {
			 return DALQualificationProgramme.SET(entity, out errorMsg);
		 }

		 public int BALQualificationProgramme_Update(EntityQualificationProgramme entity) 
		 {
			 return DALQualificationProgramme.Update(entity);
		 }

		 public int BALQualificationProgramme_Delete_By_QualificationProgrammeID(int QualificationProgrammeID) 
		 {
			 return DALQualificationProgramme.Delete_By_QualificationProgrammeID(QualificationProgrammeID);
		 }

	}
}
