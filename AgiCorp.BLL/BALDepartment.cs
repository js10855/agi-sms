using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALDepartment
	 {
		 public BALDepartment() 
		 {
		 }

		 public DataSet BALDepartment_Get_All()
		 {
		 	 return DALDepartment.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string DepartmentID)
        {
            return DALDepartment.Check_For_UniqueCode(Code, DepartmentID);
        }
        public DataSet BALDepartment_Get_By_DepartmentID(int DepartmentID)
		 {
		 	 return DALDepartment.Get_By_DepartmentID(DepartmentID);
		 }

		 public int BALDepartment_SET(EntityDepartment entity) 
		 {
			 return DALDepartment.SET(entity);
		 }

		 public int BALDepartment_Update(EntityDepartment entity) 
		 {
			 return DALDepartment.Update(entity);
		 }

		 public int BALDepartment_Delete_By_DepartmentID(int DepartmentID) 
		 {
			 return DALDepartment.Delete_By_DepartmentID(DepartmentID);
		 }

	}
}
