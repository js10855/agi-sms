using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALClientNationality
	 {
		 public BALClientNationality() 
		 {
		 }

		 public DataSet BALClientNationality_Get_All()
		 {
		 	 return DALClientNationality.Get_All();
		 }

		 public DataSet BALClientNationality_Get_By_ClientNationalityID(int ClientNationalityID)
		 {
		 	 return DALClientNationality.Get_By_ClientNationalityID(ClientNationalityID);
		 }

		 public int BALClientNationality_SET(EntityClientNationality entity) 
		 {
			 return DALClientNationality.SET(entity);
		 }

		 public int BALClientNationality_Update(EntityClientNationality entity) 
		 {
			 return DALClientNationality.Update(entity);
		 }

		 public int BALClientNationality_Delete_By_ClientNationalityID(int ClientNationalityID) 
		 {
			 return DALClientNationality.Delete_By_ClientNationalityID(ClientNationalityID);
		 }

	}
}
