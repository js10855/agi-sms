using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;


namespace AgiCorp.BLL
{
	 public class BALAgentCommission
	 {
		 public BALAgentCommission() 
		 {
		 }

		 public DataSet BALAgentCommission_Get_All()
		 {
		 	 return DALAgentCommission.Get_All();
		 }

        public DataSet BALAgentCommission_Get_By_AgentID(int AgentID)
        {
            return DALAgentCommission.Get_By_AgentID(AgentID);
        }
        public DataSet BALAgentCommission_Get_By_AgentCommissionID(int AgentCommissionID)
		 {
		 	 return DALAgentCommission.Get_By_AgentCommissionID(AgentCommissionID);
		 }

		 public int BALAgentCommission_SET(EntityAgentCommission entity) 
		 {
			 return DALAgentCommission.SET(entity);
		 }

		 public int BALAgentCommission_Update(EntityAgentCommission entity) 
		 {
			 return DALAgentCommission.Update(entity);
		 }

		 public int BALAgentCommission_Delete_By_AgentCommissionID(int AgentCommissionID) 
		 {
			 return DALAgentCommission.Delete_By_AgentCommissionID(AgentCommissionID);
		 }

	}
}
