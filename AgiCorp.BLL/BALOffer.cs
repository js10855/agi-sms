using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALOffer
	 {
		 public BALOffer() 
		 {
		 }

        public DataSet BALOffer_Get_By_IntakeID(int IntakeID)
        {
            return DALOffer.Get_By_IntakeID(IntakeID);
        }

        public DataSet BALOffer_Get_All()
		 {
		 	 return DALOffer.Get_All();
		 }

		 public DataSet BALOffer_Get_By_OfferID(int OfferID)
		 {
		 	 return DALOffer.Get_By_OfferID(OfferID);
		 }

		 public int BALOffer_SET(EntityOffer entity) 
		 {
			 return DALOffer.SET(entity);
		 }

		 public int BALOffer_Update(EntityOffer entity) 
		 {
			 return DALOffer.Update(entity);
		 }

        public int BALOffer_DoCancel(int OfferID)
        {
            return DALOffer.DoCancel(OfferID);
        }

        public int BALOffer_DoWithdraw(int OfferID)
        {
            return DALOffer.DoWithdraw(OfferID);
        }

        public int BALOffer_Delete_By_OfferID(int OfferID) 
		 {
			 return DALOffer.Delete_By_OfferID(OfferID);
		 }

	}
}
