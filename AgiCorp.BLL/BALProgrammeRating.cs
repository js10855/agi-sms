using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALProgrammeRating
	 {
		 public BALProgrammeRating() 
		 {
		 }

		 public DataSet BALProgrammeRating_Get_All()
		 {
		 	 return DALProgrammeRating.Get_All();
		 }

		 public DataSet BALProgrammeRating_Get_By_ProgrammeRatingID(int ProgrammeRatingID)
		 {
		 	 return DALProgrammeRating.Get_By_ProgrammeRatingID(ProgrammeRatingID);
		 }

		 public int BALProgrammeRating_SET(EntityProgrammeRating entity) 
		 {
			 return DALProgrammeRating.SET(entity);
		 }

		 public int BALProgrammeRating_Update(EntityProgrammeRating entity) 
		 {
			 return DALProgrammeRating.Update(entity);
		 }

		 public int BALProgrammeRating_Delete_By_ProgrammeRatingID(int ProgrammeRatingID) 
		 {
			 return DALProgrammeRating.Delete_By_ProgrammeRatingID(ProgrammeRatingID);
		 }

	}
}
