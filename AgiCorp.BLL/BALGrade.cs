using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALGrade
	 {
		 public BALGrade() 
		 {
		 }

		 public DataSet BALGrade_Get_All()
		 {
		 	 return DALGrade.Get_All();
		 }

		 public DataSet BALGrade_Get_By_GradeID(int GradeID)
		 {
		 	 return DALGrade.Get_By_GradeID(GradeID);
		 }

		 public int BALGrade_SET(EntityGrade entity) 
		 {
			 return DALGrade.SET(entity);
		 }

		 public int BALGrade_Update(EntityGrade entity) 
		 {
			 return DALGrade.Update(entity);
		 }

		 public int BALGrade_Delete_By_GradeID(int GradeID) 
		 {
			 return DALGrade.Delete_By_GradeID(GradeID);
		 }
        public Boolean Check_For_UniqueCode(string Code, string GradeID)
        {
            return DALGrade.Check_For_UniqueCode(Code, GradeID);
        }


    }
}
