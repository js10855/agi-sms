using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALClientVisaType
	 {
		 public BALClientVisaType() 
		 {
		 }

		 public DataSet BALClientVisaType_Get_All()
		 {
		 	 return DALClientVisaType.Get_All();
		 }

		 public DataSet BALClientVisaType_Get_By_ClientVisaTypeID(int ClientVisaTypeID)
		 {
		 	 return DALClientVisaType.Get_By_ClientVisaTypeID(ClientVisaTypeID);
		 }

		 public int BALClientVisaType_SET(EntityClientVisaType entity) 
		 {
			 return DALClientVisaType.SET(entity);
		 }

		 public int BALClientVisaType_Update(EntityClientVisaType entity) 
		 {
			 return DALClientVisaType.Update(entity);
		 }

		 public int BALClientVisaType_Delete_By_ClientVisaTypeID(int ClientVisaTypeID) 
		 {
			 return DALClientVisaType.Delete_By_ClientVisaTypeID(ClientVisaTypeID);
		 }

	}
}
