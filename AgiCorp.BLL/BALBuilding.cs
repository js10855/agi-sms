using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALBuilding
	 {
		 public BALBuilding() 
		 {
		 }

		 public DataSet BALBuilding_Get_All()
		 {
		 	 return DALBuilding.Get_All();
		 }

        public Boolean Check_For_UniqueCode(string Code, string BuildingID)
        {
            return DALBuilding.Check_For_UniqueCode(Code, BuildingID);
        }
        public DataSet BALBuilding_Get_By_BuildingID(int BuildingID)
		 {
		 	 return DALBuilding.Get_By_BuildingID(BuildingID);
		 }

		 public int BALBuilding_SET(EntityBuilding entity) 
		 {
			 return DALBuilding.SET(entity);
		 }

		 public int BALBuilding_Update(EntityBuilding entity) 
		 {
			 return DALBuilding.Update(entity);
		 }

		 public int BALBuilding_Delete_By_BuildingID(int BuildingID) 
		 {
			 return DALBuilding.Delete_By_BuildingID(BuildingID);
		 }

	}
}
