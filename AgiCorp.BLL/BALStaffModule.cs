using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.DAL;
using AgiCorp.Entity;

namespace AgiCorp.BLL
{
	 public class BALStaffModule
	 {
		 public BALStaffModule() 
		 {
		 }

		 public DataSet BALStaffModule_Get_All()
		 {
		 	 return DALStaffModule.Get_All();
		 }

		 public DataSet BALStaffModule_Get_By_StaffModuleID(int StaffModuleID)
		 {
		 	 return DALStaffModule.Get_By_StaffModuleID(StaffModuleID);
		 }

		 public int BALStaffModule_SET(EntityStaffModule entity, out string errorMsg) 
		 {
			 return DALStaffModule.SET(entity,out errorMsg);
		 }

        public DataSet BALStaffModule_Get_By_StaffID(int StaffID)
        {
            return DALStaffModule.Get_By_StaffID(StaffID);
        }

        public DataSet BALStaffModule_Get_By_ModuleID(int ModuleID)
        {
            return DALStaffModule.Get_By_ModuleID(ModuleID);
        }


        public int BALStaffModule_Update(EntityStaffModule entity) 
		 {
			 return DALStaffModule.Update(entity);
		 }

		 public int BALStaffModule_Delete_By_StaffModuleID(int StaffModuleID) 
		 {
			 return DALStaffModule.Delete_By_StaffModuleID(StaffModuleID);
		 }

	}
}
