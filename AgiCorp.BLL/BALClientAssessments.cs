using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AgiCorp.Entity;
using AgiCorp.DAL;

namespace AgiCorp.BLL
{
	 public class BALClientAssessments
	 {
		 public BALClientAssessments() 
		 {
		 }

		 public DataSet BALClientAssessments_Get_All()
		 {
		 	 return DALClientAssessments.Get_All();
		 }

		 public DataSet BALClientAssessments_Get_By_ClientAssessmentID(int ClientAssessmentID)
		 {
		 	 return DALClientAssessments.Get_By_ClientAssessmentID(ClientAssessmentID);
		 }

		 public int BALClientAssessments_SET(EntityClientAssessments entity) 
		 {
			 return DALClientAssessments.SET(entity);
		 }

		 public int BALClientAssessments_Update(EntityClientAssessments entity) 
		 {
			 return DALClientAssessments.Update(entity);
		 }

		 public int BALClientAssessments_Delete_By_ClientAssessmentID(int ClientAssessmentID) 
		 {
			 return DALClientAssessments.Delete_By_ClientAssessmentID(ClientAssessmentID);
		 }
        public DataSet BALClientAssessments_Get_By_ClientID(int ClientID)
        {
            return DALClientAssessments.Get_By_ClientID(ClientID);
        }
    }
}
