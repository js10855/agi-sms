using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityProgramme
	 {
		 public EntityProgramme() 
		 {
		 }

		 private int? _ProgrammeID;
		 public int? ProgrammeID
		 { 
			 get { return  _ProgrammeID; } 
			 set {  _ProgrammeID = value; } 
		 } 

		 private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Programme", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "ProgrammeID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private int _NZQACode;
		 public int NZQACode
		 { 
			 get { return  _NZQACode; } 
			 set {  _NZQACode = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _Credits;
		 public int Credits
		 { 
			 get { return  _Credits; } 
			 set {  _Credits = value; } 
		 } 

		 private int _F2FHours;
		 public int F2FHours
		 { 
			 get { return  _F2FHours; } 
			 set {  _F2FHours = value; } 
		 } 

		 private int _SDHours;
		 public int SDHours
		 { 
			 get { return  _SDHours; } 
			 set {  _SDHours = value; } 
		 }

        private int _ClassF2F;
        public int ClassF2F
        {
            get { return _ClassF2F; }
            set { _ClassF2F = value; }
        }

        private int _PlacementF2F;
        public int PlacementF2F
        {
            get { return _PlacementF2F; }
            set { _PlacementF2F = value; }
        }

        private string _ProgrammeLevelID;
        [Required(ErrorMessage = "Programme Level is required")]
        public string ProgrammeLevelID
        {
            get { return _ProgrammeLevelID; }
            set { _ProgrammeLevelID = value; }
        }

        private string _ProgrammeLength;
       [Required(ErrorMessage = "Programme Length is required")]
        public string ProgrammeLength
        {
            get { return _ProgrammeLength; }
            set { _ProgrammeLength = value; }
        }
        private DateTime ?_StartDate;
        public DateTime ?StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        private DateTime ?_EndDate;
        public DateTime ? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        private bool _IsSemesterBreakApplicable;
        public bool IsSemesterBreakApplicable
        {
            get { return _IsSemesterBreakApplicable; }
            set { _IsSemesterBreakApplicable = value; }
        }


        private DateTime? _SemesterBreakFromDate;
        public DateTime? SemesterBreakFromDate
        {
            get { return _SemesterBreakFromDate; }
            set { _SemesterBreakFromDate = value; }
        }
        private DateTime? _SemesterBreakToDate;
        public DateTime? SemesterBreakToDate
        {
            get { return _SemesterBreakToDate; }
            set { _SemesterBreakToDate = value; }
        }
        private DateTime _SummerBreakFromDate;
        public DateTime SummerBreakFromDate
        {
            get { return _SummerBreakFromDate; }
            set { _SummerBreakFromDate = value; }
        }

        private DateTime _SummerBreakToDate;
        public DateTime SummerBreakToDate
        {
            get { return _SummerBreakToDate; }
            set { _SummerBreakToDate = value; }
        }

        private string _ProgrammeRatingID;

        [Required(ErrorMessage = "Programme Rating is required")]
        public string ProgrammeRatingID
        {
            get { return _ProgrammeRatingID; }
            set { _ProgrammeRatingID = value; }
        }



    }
}
