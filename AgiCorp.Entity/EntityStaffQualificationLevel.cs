using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityStaffQualificationLevel
	 {
		 public EntityStaffQualificationLevel() 
		 {
		 }

		 private int _StaffQualificationLevelID;
		 public int StaffQualificationLevelID
		 { 
			 get { return  _StaffQualificationLevelID; } 
			 set {  _StaffQualificationLevelID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
