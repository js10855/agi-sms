using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityModuleAssessmentType
	 {
		 public EntityModuleAssessmentType() 
		 {
		 }

		 private int _ModuleAssessmentTypeID;
		 public int ModuleAssessmentTypeID
		 { 
			 get { return  _ModuleAssessmentTypeID; } 
			 set {  _ModuleAssessmentTypeID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
