using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityCampusBuilding
	 {
		 public EntityCampusBuilding() 
		 {
		 }

		 private int _CampusBuildingID;
		 public int CampusBuildingID
		 { 
			 get { return  _CampusBuildingID; } 
			 set {  _CampusBuildingID = value; } 
		 } 

		 private int _CampusID;
		 public int CampusID
		 { 
			 get { return  _CampusID; } 
			 set {  _CampusID = value; } 
		 } 

		 private int _BuildingID;
		 public int BuildingID
		 { 
			 get { return  _BuildingID; } 
			 set {  _BuildingID = value; } 
		 } 

	}
}
