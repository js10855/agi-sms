using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityAgentCommissionType
	 {
		 public EntityAgentCommissionType() 
		 {
		 }

		 private int _AgentCommissionTypeID;
		 public int AgentCommissionTypeID
		 { 
			 get { return  _AgentCommissionTypeID; } 
			 set {  _AgentCommissionTypeID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
