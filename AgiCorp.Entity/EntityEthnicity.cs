using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityEthnicity
	 {
		 public EntityEthnicity() 
		 {
		 }

		 private int _EthnicityID;
		 public int EthnicityID
		 { 
			 get { return  _EthnicityID; } 
			 set {  _EthnicityID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private bool _IsActive;
		 public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
