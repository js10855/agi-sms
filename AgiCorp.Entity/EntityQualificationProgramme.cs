using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityQualificationProgramme
	 {
		 public EntityQualificationProgramme() 
		 {
		 }

		 private int _QualificationProgrammeID;
		 public int QualificationProgrammeID
		 { 
			 get { return  _QualificationProgrammeID; } 
			 set {  _QualificationProgrammeID = value; } 
		 } 

		 private int _QualificationID;
		 public int QualificationID
		 { 
			 get { return  _QualificationID; } 
			 set {  _QualificationID = value; } 
		 } 

		 private int _ProgrammeID;
		 public int ProgrammeID
		 { 
			 get { return  _ProgrammeID; } 
			 set {  _ProgrammeID = value; } 
		 } 

	}
}
