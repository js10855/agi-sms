using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;



namespace AgiCorp.Entity
{
	 public class EntityFees
	 {
		 public EntityFees() 
		 {
		 }

		 private int _FeeID;
		 public int FeeID
		 { 
			 get { return  _FeeID; } 
			 set {  _FeeID = value; } 
		 } 

		 private string _Code;

        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Fees", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "FeeID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _ProgrammeID;
		 public int ProgrammeID
		 { 
			 get { return  _ProgrammeID; } 
			 set {  _ProgrammeID = value; } 
		 } 

		 private int _CountryID;
		 public int CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

		 private float _TutionFees;
		 public float TutionFees
		 { 
			 get { return  _TutionFees; } 
			 set {  _TutionFees = value; } 
		 } 

		 private float _MaterialFees;
		 public float MaterialFees
		 { 
			 get { return  _MaterialFees; } 
			 set {  _MaterialFees = value; } 
		 } 

		 private float _Insurance;
		 public float Insurance
		 { 
			 get { return  _Insurance; } 
			 set {  _Insurance = value; } 
		 } 

		 private float _RegistrationFees;
		 public float RegistrationFees
		 { 
			 get { return  _RegistrationFees; } 
			 set {  _RegistrationFees = value; } 
		 } 

		 private float _AdministrationFees;
		 public float AdministrationFees
		 { 
			 get { return  _AdministrationFees; } 
			 set {  _AdministrationFees = value; } 
		 } 

		 private float _OtherFees;
		 public float OtherFees
		 { 
			 get { return  _OtherFees; } 
			 set {  _OtherFees = value; } 
		 }

        private float _TotalFees;
        public float TotalFees
        {
            get { return _TotalFees; }
            set { _TotalFees = value; }
        }

        private string _AirportPickupFees;
        [Required(ErrorMessage = "Airport Pickup Fees is required")]
        public string AirportPickupFees
        {
            get { return _AirportPickupFees; }
            set { _AirportPickupFees = value; }
        }

        private string _HomeStayPlacementFees;
        [Required(ErrorMessage = "Home Stay Placement Fees is required")]
        public string HomeStayPlacementFees
        {
            get { return _HomeStayPlacementFees; }
            set { _HomeStayPlacementFees = value; }
        }

        private string _HomeStayFeesPerWeek;
        [Required(ErrorMessage = "Home Stay Fees Per Week is required")]
        public string HomeStayFeesPerWeek
        {
            get { return _HomeStayFeesPerWeek; }
            set { _HomeStayFeesPerWeek = value; }
        }

        private DateTime _StartDate;
		 public DateTime StartDate
		 { 
			 get { return  _StartDate; } 
			 set {  _StartDate = value; } 
		 } 

		 private DateTime _EndDate;
		 public DateTime EndDate
		 { 
			 get { return  _EndDate; } 
			 set {  _EndDate = value; } 
		 } 

	}
}
