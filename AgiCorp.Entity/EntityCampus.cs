using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityCampus
	 {
		 public EntityCampus() 
		 {
		 }

		 private int? _CampusID;
		 public int? CampusID
		 { 
			 get { return  _CampusID; } 
			 set {  _CampusID = value; } 
		 } 

		private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Campus", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "CampusID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private string _Address1;
		 public string Address1
		 { 
			 get { return  _Address1; } 
			 set {  _Address1 = value; } 
		 } 

		 private string _Address2;
		 public string Address2
		 { 
			 get { return  _Address2; } 
			 set {  _Address2 = value; } 
		 } 

		 private string _Address3;
		 public string Address3
		 { 
			 get { return  _Address3; } 
			 set {  _Address3 = value; } 
		 } 

		 private string _PostCode;
		 public string PostCode
		 { 
			 get { return  _PostCode; } 
			 set {  _PostCode = value; } 
		 } 

		 private string _City;
		 public string City
		 { 
			 get { return  _City; } 
			 set {  _City = value; } 
		 } 

		 private string _State;
		 public string State
		 { 
			 get { return  _State; } 
			 set {  _State = value; } 
		 } 

		 private string _CountryID;
		 public string CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

		 private string _Email;
		 public string Email
		 { 
			 get { return  _Email; } 
			 set {  _Email = value; } 
		 } 

		 private string _PhoneCountryCode;
		 public string PhoneCountryCode
		 { 
			 get { return  _PhoneCountryCode; } 
			 set {  _PhoneCountryCode = value; } 
		 } 

		 private string _PhoneStateCode;
		 public string PhoneStateCode
		 { 
			 get { return  _PhoneStateCode; } 
			 set {  _PhoneStateCode = value; } 
		 } 

		 private string _Phone;
		 public string Phone
		 { 
			 get { return  _Phone; } 
			 set {  _Phone = value; } 
		 }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
