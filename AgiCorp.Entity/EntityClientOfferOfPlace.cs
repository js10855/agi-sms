using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientOfferOfPlace
	 {
		 public EntityClientOfferOfPlace() 
		 {
		 }

		 private int _ClientOfferOfPlaceID;
		 public int ClientOfferOfPlaceID
		 { 
			 get { return  _ClientOfferOfPlaceID; } 
			 set {  _ClientOfferOfPlaceID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _DocumentDescription;
		 public string DocumentDescription
		 { 
			 get { return  _DocumentDescription; } 
			 set {  _DocumentDescription = value; } 
		 } 

		 private int _ClientDocumentTypeID;
		 public int ClientDocumentTypeID
        { 
			 get { return _ClientDocumentTypeID; } 
			 set { _ClientDocumentTypeID = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
