using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientDocument
	 {
		 public EntityClientDocument() 
		 {
		 }

		 private int _ClientDocumentID;
		 public int ClientDocumentID
		 { 
			 get { return  _ClientDocumentID; } 
			 set {  _ClientDocumentID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _DocumentDescription;
		 public string DocumentDescription
		 { 
			 get { return  _DocumentDescription; } 
			 set {  _DocumentDescription = value; } 
		 } 

		 private string _DocsFileName;
		 public string DocsFileName
        { 
			 get { return _DocsFileName; } 
			 set { _DocsFileName = value; } 
		 } 

		 private byte[] _File;
		 public byte[] File
		 { 
			 get { return  _File; } 
			 set {  _File = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
