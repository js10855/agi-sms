using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityStaffModule
	 {
		 public EntityStaffModule() 
		 {
		 }

		 private int _StaffModuleID;
		 public int StaffModuleID
		 { 
			 get { return  _StaffModuleID; } 
			 set {  _StaffModuleID = value; } 
		 } 

		 private int _StaffID;
		 public int StaffID
		 { 
			 get { return  _StaffID; } 
			 set {  _StaffID = value; } 
		 } 

		 private int _ModuleID;
		 public int ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

	}
}
