using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityBuildingClass
	 {
		 public EntityBuildingClass() 
		 {
		 }

		 private int _BuildingClassID;
		 public int BuildingClassID
		 { 
			 get { return  _BuildingClassID; } 
			 set {  _BuildingClassID = value; } 
		 } 

		 private int _BuildingID;
		 public int BuildingID
		 { 
			 get { return  _BuildingID; } 
			 set {  _BuildingID = value; } 
		 } 

		 private int _ClassID;
		 public int ClassID
		 { 
			 get { return  _ClassID; } 
			 set {  _ClassID = value; } 
		 } 

	}
}
