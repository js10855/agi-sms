using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp
{
	 public class EntityClientDocumentType
	 {
		 public EntityClientDocumentType() 
		 {
		 }

		 private int _ClientDocumentTypeID;
		 public int ClientDocumentTypeID
		 { 
			 get { return  _ClientDocumentTypeID; } 
			 set {  _ClientDocumentTypeID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
