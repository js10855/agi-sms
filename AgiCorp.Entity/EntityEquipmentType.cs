using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityEquipmentType
	 {
		 public EntityEquipmentType() 
		 {
		 }

		 private int? _EquipmentTypeID;
		 public int? EquipmentTypeID
		 { 
			 get { return  _EquipmentTypeID; } 
			 set {  _EquipmentTypeID = value; } 
		 } 

		 private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "EquipmentType", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "EquipmentTypeID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
