using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityMonth
	 {
		 public EntityMonth() 
		 {
		 }

		 private int _MonthID;
		 public int MonthID
		 { 
			 get { return  _MonthID; } 
			 set {  _MonthID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
