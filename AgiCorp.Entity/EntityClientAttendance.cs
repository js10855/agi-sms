using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientAttendance
	 {
		 public EntityClientAttendance() 
		 {
		 }

		 private int _ClientAttendanceID;
		 public int ClientAttendanceID
		 { 
			 get { return  _ClientAttendanceID; } 
			 set {  _ClientAttendanceID = value; } 
		 } 

		 private int _ModuleID;
		 public int ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

		 private float _AttendancePercentage;
		 public float AttendancePercentage
		 { 
			 get { return  _AttendancePercentage; } 
			 set {  _AttendancePercentage = value; } 
		 } 

		 private float _OverallAttendance;
		 public float OverallAttendance
		 { 
			 get { return  _OverallAttendance; } 
			 set {  _OverallAttendance = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
