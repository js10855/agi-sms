using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityOffer
	 {
		 public EntityOffer() 
		 {
		 }

		 private int _OfferID;
		 public int OfferID
		 { 
			 get { return  _OfferID; } 
			 set {  _OfferID = value; } 
		 } 

		 private string _ClientID;
		 public string ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

		 private string _OfferType;
		 public string OfferType
		 { 
			 get { return  _OfferType; } 
			 set {  _OfferType = value; } 
		 } 

		 private string _ProgrammeID1;
		 public string ProgrammeID1
		 { 
			 get { return  _ProgrammeID1; } 
			 set {  _ProgrammeID1 = value; } 
		 }

        private string _ProgrammeID2;
        public string ProgrammeID2
        {
            get { return _ProgrammeID2; }
            set { _ProgrammeID2 = value; }
        }

        private string _IntakeID1;
		 public string IntakeID1
		 { 
			 get { return  _IntakeID1; } 
			 set {  _IntakeID1 = value; } 
		 }

        private string _IntakeID2;
        public string IntakeID2
        {
            get { return _IntakeID2; }
            set { _IntakeID2 = value; }
        }

        private byte[] _File;
        public byte[] File
        {
            get { return _File; }
            set { _File = value; }
        }

        private string _DocsFileName;
        public string DocsFileName
        {
            get { return _DocsFileName; }
            set { _DocsFileName = value; }
        }

        private string _OfferYearID;
		 public string OfferYearID
		 { 
			 get { return  _OfferYearID; } 
			 set {  _OfferYearID = value; } 
		 } 

		 private DateTime _OfferDate;
		 public DateTime OfferDate
		 { 
			 get { return  _OfferDate; } 
			 set {  _OfferDate = value; } 
		 } 

		 private DateTime _OfferCreatedDate;
		 public DateTime OfferCreatedDate
		 { 
			 get { return  _OfferCreatedDate; } 
			 set {  _OfferCreatedDate = value; } 
		 } 

		 private DateTime? _OfferLastEditedDate;
		 public DateTime? OfferLastEditedDate
		 { 
			 get { return  _OfferLastEditedDate; } 
			 set {  _OfferLastEditedDate = value; } 
		 } 

		 private DateTime? _OfferCancelledDate;
		 public DateTime? OfferCancelledDate
		 { 
			 get { return  _OfferCancelledDate; } 
			 set {  _OfferCancelledDate = value; } 
		 } 

		 private DateTime? _OfferWithdrawnDate;
		 public DateTime? OfferWithdrawnDate
		 { 
			 get { return  _OfferWithdrawnDate; } 
			 set {  _OfferWithdrawnDate = value; } 
		 } 

		 private string _OfferStatusID;
		 public string OfferStatusID
		 { 
			 get { return  _OfferStatusID; } 
			 set {  _OfferStatusID = value; } 
		 }

        //private string _InstitutionID;
        //public string InstitutionID
        //{
        //    get { return _InstitutionID; }
        //    set { _InstitutionID = value; }
        //}

        private string _SchoolID;
        public string SchoolID
        {
            get { return _SchoolID; }
            set { _SchoolID = value; }
        }

        //private string _Programme;
        //public string Programme
        //{
        //    get { return _Programme; }
        //    set { _Programme = value; }
        //}

        //private string _Intake;
        //public string Intake
        //{
        //    get { return _Intake; }
        //    set { _Intake = value; }
        //}


        //private string _OfferYear;
        //public string OfferYear
        //{
        //    get { return _OfferYear; }
        //    set { _OfferYear = value; }
        //}


        //private string _OfferStatus;
        //public string OfferStatus
        //{
        //    get { return _OfferStatus; }
        //    set { _OfferStatus = value; }
        //}


    }
}
