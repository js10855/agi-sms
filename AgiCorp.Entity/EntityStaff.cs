using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AgiCorp.Entity
{
	 public class EntityStaff
	 {
		 public EntityStaff() 
		 {
		 }

		 private int? _StaffID;
		 public int? StaffID
		 { 
			 get { return  _StaffID; } 
			 set {  _StaffID = value; } 
		 } 

		 private string _Code;

        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Staff", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "StaffID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Salutation;
		 public string Salutation
		 { 
			 get { return  _Salutation; } 
			 set {  _Salutation = value; } 
		 } 

		 private string _FirstName;
		 public string FirstName
		 { 
			 get { return  _FirstName; } 
			 set {  _FirstName = value; } 
		 } 

		 private string _MiddleName;
		 public string MiddleName
		 { 
			 get { return  _MiddleName; } 
			 set {  _MiddleName = value; } 
		 } 

		 private string _LastName;
		 public string LastName
		 { 
			 get { return  _LastName; } 
			 set {  _LastName = value; } 
		 } 

		 private DateTime _DateOfBirth;
		 public DateTime DateOfBirth
		 { 
			 get { return  _DateOfBirth; } 
			 set {  _DateOfBirth = value; } 
		 } 

		 private string _WorkEmail;
		 public string WorkEmail
		 { 
			 get { return  _WorkEmail; } 
			 set {  _WorkEmail = value; } 
		 } 

		 private string _HomeEmail;
		 public string HomeEmail
		 { 
			 get { return  _HomeEmail; } 
			 set {  _HomeEmail = value; } 
		 } 

		 private string _Phone;
		 public string Phone
		 { 
			 get { return  _Phone; } 
			 set {  _Phone = value; } 
		 } 

		 private string _Mobile;
		 public string Mobile
		 { 
			 get { return  _Mobile; } 
			 set {  _Mobile = value; } 
		 } 

		 private string _Address1;
		 public string Address1
		 { 
			 get { return  _Address1; } 
			 set {  _Address1 = value; } 
		 } 

		 private string _Address2;
		 public string Address2
		 { 
			 get { return  _Address2; } 
			 set {  _Address2 = value; } 
		 } 

		 private string _Address3;
		 public string Address3
		 { 
			 get { return  _Address3; } 
			 set {  _Address3 = value; } 
		 } 

		 private string _City;
		 public string City
		 { 
			 get { return  _City; } 
			 set {  _City = value; } 
		 } 

		 private string _State;
		 public string State
		 { 
			 get { return  _State; } 
			 set {  _State = value; } 
		 } 

		 private string _CountryID;
		 public string CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

		 private int _StaffTypeID;
		 public int StaffTypeID
		 { 
			 get { return  _StaffTypeID; } 
			 set {  _StaffTypeID = value; } 
		 } 

		 private int _StatusID;
		 public int StatusID
		 { 
			 get { return  _StatusID; } 
			 set {  _StatusID = value; } 
		 } 

		 private int _CostPerHour;
		 public int CostPerHour
		 { 
			 get { return  _CostPerHour; } 
			 set {  _CostPerHour = value; } 
		 }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
