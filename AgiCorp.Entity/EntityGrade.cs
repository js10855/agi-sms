using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AgiCorp.Entity
{
	 public class EntityGrade
	 {
		 public EntityGrade() 
		 {
		 }

		 private int? _GradeID;
		 public int? GradeID
		 { 
			 get { return  _GradeID; } 
			 set {  _GradeID = value; } 
		 } 

		 private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Grade", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "GradeID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
