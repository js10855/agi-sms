using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityQualificationGPO
	 {
		 public EntityQualificationGPO() 
		 {
		 }

		 private int _QualificationGPOID;
		 public int QualificationGPOID
		 { 
			 get { return  _QualificationGPOID; } 
			 set {  _QualificationGPOID = value; } 
		 } 

		 private int _QualificationID;
		 public int QualificationID
		 { 
			 get { return  _QualificationID; } 
			 set {  _QualificationID = value; } 
		 } 

		 private int _GPOID;
		 public int GPOID
		 { 
			 get { return  _GPOID; } 
			 set {  _GPOID = value; } 
		 } 

	}
}
