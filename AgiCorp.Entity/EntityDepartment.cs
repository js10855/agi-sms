using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityDepartment
	 {
		 public EntityDepartment() 
		 {
		 }

		 private int? _DepartmentID;
		 public int? DepartmentID
		 { 
			 get { return  _DepartmentID; } 
			 set {  _DepartmentID = value; } 
		 } 

		 private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Department", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "DepartmentID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 }

        private int _SchoolID;
        public int SchoolID
        {
            get { return _SchoolID; }
            set { _SchoolID = value; }
        }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
