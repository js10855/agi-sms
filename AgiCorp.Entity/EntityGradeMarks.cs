using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityGradeMarks
	 {
		 public EntityGradeMarks() 
		 {
		 }

		 private int _GradeMarksID;
		 public int GradeMarksID
		 { 
			 get { return  _GradeMarksID; } 
			 set {  _GradeMarksID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _MarksRangeFrom;
		 public int MarksRangeFrom
		 { 
			 get { return  _MarksRangeFrom; } 
			 set {  _MarksRangeFrom = value; } 
		 } 

		 private int _MarksRangeTo;
		 public int MarksRangeTo
		 { 
			 get { return  _MarksRangeTo; } 
			 set {  _MarksRangeTo = value; } 
		 }

        private int _GradeID;
        public int GradeID
        {
            get { return _GradeID; }
            set { _GradeID = value; }
        }

    }
}
