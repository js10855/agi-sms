using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientVisaType
	 {
		 public EntityClientVisaType() 
		 {
		 }

		 private int _ClientVisaTypeID;
		 public int ClientVisaTypeID
		 { 
			 get { return  _ClientVisaTypeID; } 
			 set {  _ClientVisaTypeID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
