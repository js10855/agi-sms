using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityDepartmentBuilding
	 {
		 public EntityDepartmentBuilding() 
		 {
		 }

		 private int _DepartmentBuildingID;
		 public int DepartmentBuildingID
		 { 
			 get { return  _DepartmentBuildingID; } 
			 set {  _DepartmentBuildingID = value; } 
		 } 

		 private int _DepartmentID;
		 public int DepartmentID
		 { 
			 get { return  _DepartmentID; } 
			 set {  _DepartmentID = value; } 
		 } 

		 private int _BuildingID;
		 public int BuildingID
		 { 
			 get { return  _BuildingID; } 
			 set {  _BuildingID = value; } 
		 } 

	}
}
