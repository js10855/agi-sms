using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityModuleAssessment
	 {
		 public EntityModuleAssessment() 
		 {
		 }

		 private int _ModuleAssessmentID;
		 public int ModuleAssessmentID
		 { 
			 get { return  _ModuleAssessmentID; } 
			 set {  _ModuleAssessmentID = value; } 
		 } 

		 private string _AssessmentNumber;
		 public string AssessmentNumber
        { 
			 get { return _AssessmentNumber; } 
			 set { _AssessmentNumber = value; } 
		 } 

		 private string _AssessmentName;
		 public string AssessmentName
		 { 
			 get { return _AssessmentName; } 
			 set { _AssessmentName = value; } 
		 } 

		 private int _ModuleAssessmentTypeID;
		 public int ModuleAssessmentTypeID
        { 
			 get { return _ModuleAssessmentTypeID; } 
			 set { _ModuleAssessmentTypeID = value; } 
		 } 

		 private int _ModuleAssessmentFormatID;
		 public int ModuleAssessmentFormatID
        { 
			 get { return _ModuleAssessmentFormatID; } 
			 set { _ModuleAssessmentFormatID = value; } 
		 }

        private int[] _LOIDs;
        public int[] LOIDs
        {
            get { return _LOIDs; }
            set { _LOIDs = value; }
        }
            
		 private string _LOID;
		 public string LOID
		 { 
			 get { return _LOID; } 
			 set { _LOID = value; } 
		 }

        private string _LOCovered;
        public string LOCovered
        {
            get { return _LOCovered; }
            set { _LOCovered = value; }
        }

        private int _TotalMarks;
		 public int TotalMarks
		 { 
			 get { return  _TotalMarks; } 
			 set {  _TotalMarks = value; } 
		 } 

		 private int _MinPassPercentage;
		 public int MinPassPercentage
		 { 
			 get { return  _MinPassPercentage; } 
			 set {  _MinPassPercentage = value; } 
		 } 

		 private int _ModuleID;
		 public int ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

	}
}
