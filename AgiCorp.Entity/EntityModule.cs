using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityModule
	 {
		 public EntityModule() 
		 {
		 }

		 private int? _ModuleID;
		 public int? ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

		 private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Module", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "ModuleID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _Credits;
		 public int Credits
		 { 
			 get { return  _Credits; } 
			 set {  _Credits = value; } 
		 } 

		 private int _HoursPerCredit;
		 public int HoursPerCredit
		 { 
			 get { return  _HoursPerCredit; } 
			 set {  _HoursPerCredit = value; } 
		 } 

		 private int _TotalHoursPerModule;
		 public int TotalHoursPerModule
		 { 
			 get { return  _TotalHoursPerModule; } 
			 set {  _TotalHoursPerModule = value; } 
		 } 

		 private int _F2FHours;
		 public int F2FHours
		 { 
			 get { return  _F2FHours; } 
			 set {  _F2FHours = value; } 
		 } 

		 private int _SDHours;
		 public int SDHours
		 { 
			 get { return  _SDHours; } 
			 set {  _SDHours = value; } 
		 }

        private int _ClassF2F;
        public int ClassF2F
        {
            get { return _ClassF2F; }
            set { _ClassF2F = value; }
        }

        private int _PlacementF2F;
        public int PlacementF2F
        {
            get { return _PlacementF2F; }
            set { _PlacementF2F = value; }
        }

        private int _MinPassPercentage;
		 public int MinPassPercentage
		 { 
			 get { return  _MinPassPercentage; } 
			 set {  _MinPassPercentage = value; } 
		 }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
