using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AgiCorp.Entity
{
	 public class EntityGPO
	 {
		 public EntityGPO() 
		 {
		 }

		 private int? _GPOID;
		 public int? GPOID
		 { 
			 get { return  _GPOID; } 
			 set {  _GPOID = value; } 
		 }


        private string _Code;

        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "GPO", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "GPOID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _Credits;
		 public int Credits
		 { 
			 get { return  _Credits; } 
			 set {  _Credits = value; } 
		 } 

		 private int _F2FHours;
		 public int F2FHours
		 { 
			 get { return  _F2FHours; } 
			 set {  _F2FHours = value; } 
		 } 

		 private int _SDHours;
		 public int SDHours
		 { 
			 get { return  _SDHours; } 
			 set {  _SDHours = value; } 
		 } 

	}
}
