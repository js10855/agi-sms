using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityProgrammeModule
	 {
		 public EntityProgrammeModule() 
		 {
		 }

		 private int _ProgrammeModuleID;
		 public int ProgrammeModuleID
		 { 
			 get { return  _ProgrammeModuleID; } 
			 set {  _ProgrammeModuleID = value; } 
		 } 

		 private int _ProgrammeID;
		 public int ProgrammeID
		 { 
			 get { return  _ProgrammeID; } 
			 set {  _ProgrammeID = value; } 
		 } 

		 private int _ModuleID;
		 public int ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

	}
}
