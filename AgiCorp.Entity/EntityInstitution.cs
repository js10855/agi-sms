﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
    public class EntityInstitution
    {
        public EntityInstitution()
        {
        }

        private int? _InstitutionID;
        public int? InstitutionID
        {
            get { return _InstitutionID; }
            set { _InstitutionID = value; }
        }

        private string _Code;

        
       [Required(ErrorMessage = "Code is required")]
       [Remote("CheckUniqueCode", "Institution", HttpMethod = "GET",ErrorMessage = "Duplicate Code!",AdditionalFields ="InstitutionID")]
       [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        private string _Address1;
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        private string _Address2;
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        private string _Address3;
        public string Address3
        {
            get { return _Address3; }
            set { _Address3 = value; }
        }

        private string _PostCode;
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        private string _City;
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

        private string _State;
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        private string _CountryID;
        public string CountryID
        {
            get { return _CountryID; }
            set { _CountryID = value; }
        }

        private string _Email;
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _PhoneCountryCode;
        public string PhoneCountryCode
        {
            get { return _PhoneCountryCode; }
            set { _PhoneCountryCode = value; }
        }

        private string _PhoneStateCode;
        public string PhoneStateCode
        {
            get { return _PhoneStateCode; }
            set { _PhoneStateCode = value; }
        }

        private string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        private bool _IsActive=true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        
        private string _AccountName;
        [Required(ErrorMessage = "Account Name is required")]
        public string AccountName
        {
            get { return _AccountName; }
            set { _AccountName = value; }
        }

        private string _AccountNumber;
        [Required(ErrorMessage = "Account Number is required")]
        public string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }

        private string _BankName;
        [Required(ErrorMessage = "Bank Name is required")]
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }

        private string _SwiftCode;
        [Required(ErrorMessage = "Swift Code is required")]
        public string SwiftCode
        {
            get { return _SwiftCode; }
            set { _SwiftCode = value; }
        }

        private string _SPOCEmail;
        [Required(ErrorMessage = "SPOC Email is required")]
        public string SPOCEmail
        {
            get { return _SPOCEmail; }
            set { _SPOCEmail = value; }
        }

    }
}