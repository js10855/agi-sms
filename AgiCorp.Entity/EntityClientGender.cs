using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientGender
	 {
		 public EntityClientGender() 
		 {
		 }

		 private int _ClientGenderID;
		 public int ClientGenderID
		 { 
			 get { return  _ClientGenderID; } 
			 set {  _ClientGenderID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
