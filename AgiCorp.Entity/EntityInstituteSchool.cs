using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityInstituteSchool
	 {
		 public EntityInstituteSchool() 
		 {
		 }

		 private int _InstituteSchoolID;
		 public int InstituteSchoolID
		 { 
			 get { return  _InstituteSchoolID; } 
			 set {  _InstituteSchoolID = value; } 
		 } 

		 private int _InstituteID;
		 public int InstituteID
		 { 
			 get { return  _InstituteID; } 
			 set {  _InstituteID = value; } 
		 } 

		 private int _SchoolID;
		 public int SchoolID
		 { 
			 get { return  _SchoolID; } 
			 set {  _SchoolID = value; } 
		 } 

	}
}
