using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientEnglishProficiency
	 {
		 public EntityClientEnglishProficiency() 
		 {
		 }

		 private int _ClientEnglishProficiencyID;
		 public int ClientEnglishProficiencyID
		 { 
			 get { return  _ClientEnglishProficiencyID; } 
			 set {  _ClientEnglishProficiencyID = value; } 
		 } 

		 private string _EnglishProficiencyType;
		 public string EnglishProficiencyType
		 { 
			 get { return  _EnglishProficiencyType; } 
			 set {  _EnglishProficiencyType = value; } 
		 } 

		 private string _EnglishProficiencyExamNumber;
		 public string EnglishProficiencyExamNumber
		 { 
			 get { return  _EnglishProficiencyExamNumber; } 
			 set {  _EnglishProficiencyExamNumber = value; } 
		 } 

		 private DateTime _EnglishProficiencyExamDate;
		 public DateTime EnglishProficiencyExamDate
		 { 
			 get { return  _EnglishProficiencyExamDate; } 
			 set {  _EnglishProficiencyExamDate = value; } 
		 } 

		 private DateTime _EnglishProficiencyExpiryDate;
		 public DateTime EnglishProficiencyExpiryDate
		 { 
			 get { return  _EnglishProficiencyExpiryDate; } 
			 set {  _EnglishProficiencyExpiryDate = value; } 
		 } 

		 private int _EnglishProficiencyOverallBand;
		 public int EnglishProficiencyOverallBand
		 { 
			 get { return  _EnglishProficiencyOverallBand; } 
			 set {  _EnglishProficiencyOverallBand = value; } 
		 } 

		 private int _IELTSReading;
		 public int IELTSReading
		 { 
			 get { return  _IELTSReading; } 
			 set {  _IELTSReading = value; } 
		 } 

		 private int _IELTSWriting;
		 public int IELTSWriting
		 { 
			 get { return  _IELTSWriting; } 
			 set {  _IELTSWriting = value; } 
		 } 

		 private int _IELTSSpeaking;
		 public int IELTSSpeaking
		 { 
			 get { return  _IELTSSpeaking; } 
			 set {  _IELTSSpeaking = value; } 
		 } 

		 private int _IELTSListening;
		 public int IELTSListening
		 { 
			 get { return  _IELTSListening; } 
			 set {  _IELTSListening = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
