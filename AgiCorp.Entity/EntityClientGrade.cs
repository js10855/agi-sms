using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientGrade
	 {
		 public EntityClientGrade() 
		 {
		 }

		 private int _ClientGradeID;
		 public int ClientGradeID
		 { 
			 get { return  _ClientGradeID; } 
			 set {  _ClientGradeID = value; } 
		 } 

		 private int _ClassID;
		 public int ClassID
		 { 
			 get { return  _ClassID; } 
			 set {  _ClassID = value; } 
		 } 

		 private int _ModuleID;
		 public int ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

		 private int _GradeID;
		 public int GradeID
		 { 
			 get { return  _GradeID; } 
			 set {  _GradeID = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
