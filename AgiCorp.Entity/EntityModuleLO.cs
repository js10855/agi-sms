using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityModuleLO
	 {
		 public EntityModuleLO() 
		 {
		 }

		 private int _ModuleLOID;
		 public int ModuleLOID
		 { 
			 get { return  _ModuleLOID; } 
			 set {  _ModuleLOID = value; } 
		 } 

		 private int _ModuleID;
		 public int ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

		 private int _LOID;
		 public int LOID
		 { 
			 get { return  _LOID; } 
			 set {  _LOID = value; } 
		 } 

	}
}
