using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityModuleAssessmentFormat
	 {
		 public EntityModuleAssessmentFormat() 
		 {
		 }

		 private int _ModuleAssessmentFormatID;
		 public int ModuleAssessmentFormatID
		 { 
			 get { return  _ModuleAssessmentFormatID; } 
			 set {  _ModuleAssessmentFormatID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
