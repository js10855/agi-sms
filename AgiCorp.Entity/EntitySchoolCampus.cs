using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntitySchoolCampus
	 {
		 public EntitySchoolCampus() 
		 {
		 }

		 private int _SchoolCampusID;
		 public int SchoolCampusID
		 { 
			 get { return  _SchoolCampusID; } 
			 set {  _SchoolCampusID = value; } 
		 } 

		 private int _SchoolID;
		 public int SchoolID
		 { 
			 get { return  _SchoolID; } 
			 set {  _SchoolID = value; } 
		 } 

		 private int _CampusID;
		 public int CampusID
		 { 
			 get { return  _CampusID; } 
			 set {  _CampusID = value; } 
		 } 

	}
}
