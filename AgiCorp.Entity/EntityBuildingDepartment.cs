using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityBuildingDepartment
	 {
		 public EntityBuildingDepartment() 
		 {
		 }

		 private int _BuildingDepartmentID;
		 public int BuildingDepartmentID
		 { 
			 get { return  _BuildingDepartmentID; } 
			 set {  _BuildingDepartmentID = value; } 
		 } 

		 private int _BuildingID;
		 public int BuildingID
		 { 
			 get { return  _BuildingID; } 
			 set {  _BuildingID = value; } 
		 } 

		 private int _DepartmentID;
		 public int DepartmentID
		 { 
			 get { return  _DepartmentID; } 
			 set {  _DepartmentID = value; } 
		 } 

	}
}
