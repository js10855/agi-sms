using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityEquipment
	 {
		 public EntityEquipment() 
		 {
		 }

		 private int _EquipmentID;
		 public int EquipmentID
		 { 
			 get { return  _EquipmentID; } 
			 set {  _EquipmentID = value; } 
		 }

        private int _Quantity;
        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Equipment", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "EquipmentID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 }

        private int _ClassID;
        public int ClassID
        {
            get { return _ClassID; }
            set { _ClassID = value; }
        }

        private int _EquipmentTypeID;
        public int EquipmentTypeID
        {
            get { return _EquipmentTypeID; }
            set { _EquipmentTypeID = value; }
        }

    }
}
