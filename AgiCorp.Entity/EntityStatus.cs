using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityStatus
	 {
		 public EntityStatus() 
		 {
		 }

		 private int _StatusID;
		 public int StatusID
		 { 
			 get { return  _StatusID; } 
			 set {  _StatusID = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
