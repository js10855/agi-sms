using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityAssessments
	 {
		 public EntityAssessments() 
		 {
		 }

		 private int _AssessmentsID;
		 public int AssessmentsID
		 { 
			 get { return  _AssessmentsID; } 
			 set {  _AssessmentsID = value; } 
		 } 

		 private string _Name;
		 public string Name
		 { 
			 get { return  _Name; } 
			 set {  _Name = value; } 
		 } 

		 private string _Type;
		 public string Type
		 { 
			 get { return  _Type; } 
			 set {  _Type = value; } 
		 } 

		 private string _Format;
		 public string Format
		 { 
			 get { return  _Format; } 
			 set {  _Format = value; } 
		 } 

		 private string _LastName;
		 public string LastName
		 { 
			 get { return  _LastName; } 
			 set {  _LastName = value; } 
		 } 

		 private int _TotalMarks;
		 public int TotalMarks
		 { 
			 get { return  _TotalMarks; } 
			 set {  _TotalMarks = value; } 
		 } 

		 private int _MinPassPercentage;
		 public int MinPassPercentage
		 { 
			 get { return  _MinPassPercentage; } 
			 set {  _MinPassPercentage = value; } 
		 } 

	}
}
