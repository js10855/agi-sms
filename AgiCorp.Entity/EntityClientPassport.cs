using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
    public class EntityClientPassport
    {
        public EntityClientPassport()
        {
        }

        private int _ClientPassportID;
        public int ClientPassportID
        {
            get { return _ClientPassportID; }
            set { _ClientPassportID = value; }
        }

        private string _PassportNumber;
        public string PassportNumber
        {
            get { return _PassportNumber; }
            set { _PassportNumber = value; }
        }

        private DateTime _PassportIssueDate;
        public DateTime PassportIssueDate
        {
            get { return _PassportIssueDate; }
            set { _PassportIssueDate = value; }
        }

        private DateTime _PassportExpiryDate;
        public DateTime PassportExpiryDate
        {
            get { return _PassportExpiryDate; }
            set { _PassportExpiryDate = value; }
        }

        private int _PassportIssueCountryID;
        public int PassportIssueCountryID
        {
            get { return _PassportIssueCountryID; }
            set { _PassportIssueCountryID = value; }
        }

        private string _VisaNumber;
        public string VisaNumber
        {
            get { return _VisaNumber; }
            set { _VisaNumber = value; }
        }

        private string _VisaType;
        public string VisaType
        {
            get { return _VisaType; }
            set { _VisaType = value; }
        }

        private DateTime _VisaIssueDate;
        public DateTime VisaIssueDate
        {
            get { return _VisaIssueDate; }
            set { _VisaIssueDate = value; }
        }

        private DateTime _VisaExpiryDate;
        public DateTime VisaExpiryDate
        {
            get { return _VisaExpiryDate; }
            set { _VisaExpiryDate = value; }
        }

        private string _InsuranceProvider;
        public string InsuranceProvider
        {
            get { return _InsuranceProvider; }
            set { _InsuranceProvider = value; }
        }

        private string _InsuranceNumber;
        public string InsuranceNumber
        {
            get { return _InsuranceNumber; }
            set { _InsuranceNumber = value; }
        }

        private DateTime _InsuranceExpiryDate;
        public DateTime InsuranceExpiryDate
        {
            get { return _InsuranceExpiryDate; }
            set { _InsuranceExpiryDate = value; }
        }

        private int _ClientID;
        public int ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

    }
}

