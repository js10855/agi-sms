using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClassDepartment
	 {
		 public EntityClassDepartment() 
		 {
		 }

		 private int _ClassDepartmentID;
		 public int ClassDepartmentID
		 { 
			 get { return  _ClassDepartmentID; } 
			 set {  _ClassDepartmentID = value; } 
		 } 

		 private int _ClassID;
		 public int ClassID
		 { 
			 get { return  _ClassID; } 
			 set {  _ClassID = value; } 
		 } 

		 private int _DepartmentID;
		 public int DepartmentID
		 { 
			 get { return  _DepartmentID; } 
			 set {  _DepartmentID = value; } 
		 } 

	}
}
