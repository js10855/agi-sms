using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityProgrammeRating
	 {
		 public EntityProgrammeRating() 
		 {
		 }

		 private int _ProgrammeRatingID;
		 public int ProgrammeRatingID
		 { 
			 get { return  _ProgrammeRatingID; } 
			 set {  _ProgrammeRatingID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
