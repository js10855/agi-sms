using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AgiCorp.Entity
{
	 public class EntityQualificationType
	 {
		 public EntityQualificationType() 
		 {
		 }

		 private int? _QualificationTypeID;
		 public int? QualificationTypeID
		 { 
			 get { return  _QualificationTypeID; } 
			 set {  _QualificationTypeID = value; } 
		 } 

		 private string _Code;

        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "QualificationType", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "QualificationTypeID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
