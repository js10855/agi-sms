using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientWorkExperience
	 {
		 public EntityClientWorkExperience() 
		 {
		 }

		 private int _ClientWorkExperienceID;
		 public int ClientWorkExperienceID
		 { 
			 get { return  _ClientWorkExperienceID; } 
			 set {  _ClientWorkExperienceID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _EmployerName;
		 public string EmployerName
		 { 
			 get { return  _EmployerName; } 
			 set {  _EmployerName = value; } 
		 } 

		 private string _EmployerEmail;
		 public string EmployerEmail
		 { 
			 get { return  _EmployerEmail; } 
			 set {  _EmployerEmail = value; } 
		 } 

		 private string _EmployerWebsite;
		 public string EmployerWebsite
		 { 
			 get { return  _EmployerWebsite; } 
			 set {  _EmployerWebsite = value; } 
		 } 

		 private string _PhoneCountryCode;
		 public string PhoneCountryCode
		 { 
			 get { return  _PhoneCountryCode; } 
			 set {  _PhoneCountryCode = value; } 
		 } 

		 private string _PhoneStateCode;
		 public string PhoneStateCode
		 { 
			 get { return  _PhoneStateCode; } 
			 set {  _PhoneStateCode = value; } 
		 } 

		 private string _Phone;
		 public string Phone
		 { 
			 get { return  _Phone; } 
			 set {  _Phone = value; } 
		 } 

		 private string _JobTitle;
		 public string JobTitle
		 { 
			 get { return  _JobTitle; } 
			 set {  _JobTitle = value; } 
		 } 

		 private bool _CurrentlyWorking;
		 public bool CurrentlyWorking
		 { 
			 get { return  _CurrentlyWorking; } 
			 set {  _CurrentlyWorking = value; } 
		 } 

		 private DateTime _FromDate;
		 public DateTime FromDate
		 { 
			 get { return  _FromDate; } 
			 set {  _FromDate = value; } 
		 } 

		 private DateTime _ToDate;
		 public DateTime ToDate
		 { 
			 get { return  _ToDate; } 
			 set {  _ToDate = value; } 
		 } 

		 private string _PostCode;
		 public string PostCode
		 { 
			 get { return  _PostCode; } 
			 set {  _PostCode = value; } 
		 } 

		 private string _City;
		 public string City
		 { 
			 get { return  _City; } 
			 set {  _City = value; } 
		 } 

		 private string _State;
		 public string State
		 { 
			 get { return  _State; } 
			 set {  _State = value; } 
		 } 

		 private string _CountryID;
		 public string CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
