using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClassEquipment
	 {
		 public EntityClassEquipment() 
		 {
		 }

		 private int _ClassEquipmentID;
		 public int ClassEquipmentID
		 { 
			 get { return  _ClassEquipmentID; } 
			 set {  _ClassEquipmentID = value; } 
		 } 

		 private int _ClassID;
		 public int ClassID
		 { 
			 get { return  _ClassID; } 
			 set {  _ClassID = value; } 
		 } 

		 private int _EquipmentID;
		 public int EquipmentID
		 { 
			 get { return  _EquipmentID; } 
			 set {  _EquipmentID = value; } 
		 } 

	}
}
