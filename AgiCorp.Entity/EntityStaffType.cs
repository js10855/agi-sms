using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AgiCorp.Entity
{
	 public class EntityStaffType
	 {
		 public EntityStaffType() 
		 {
		 }

		 private int? _StaffTypeID;
		 public int? StaffTypeID
		 { 
			 get { return  _StaffTypeID; } 
			 set {  _StaffTypeID = value; } 
		 }


        private string _Code;

        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "StaffType", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "StaffTypeID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
