using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityIntake
	 {
		 public EntityIntake() 
		 {
		 }

		 private int? _IntakeID;
		 public int? IntakeID
		 { 
			 get { return  _IntakeID; } 
			 set {  _IntakeID = value; } 
		 } 

		 private string _Code;

        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Intake", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "IntakeID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _ProgrammeID;
		 public int ProgrammeID
		 { 
			 get { return  _ProgrammeID; } 
			 set {  _ProgrammeID = value; } 
		 } 

		 private int _FeeID;
		 public int FeeID
		 { 
			 get { return  _FeeID; } 
			 set {  _FeeID = value; } 
		 } 

		 private int _Capacity;
		 public int Capacity
		 { 
			 get { return  _Capacity; } 
			 set {  _Capacity = value; } 
		 } 

		 private int? _Month;
		 public int? Month
		 { 
			 get { return  _Month; } 
			 set {  _Month = value; } 
		 } 

		 private int? _Year;
		 public int? Year
		 { 
			 get { return  _Year; } 
			 set {  _Year = value; } 
		 } 

		 private DateTime _StartDate;
        [Required(ErrorMessage = "Start Date is required")]
        public DateTime StartDate
		 { 
			 get { return  _StartDate; } 
			 set {  _StartDate = value; } 
		 } 

		 private DateTime _EndDate;
        [Required(ErrorMessage = "End Date is required")]
        public DateTime EndDate
		 { 
			 get { return  _EndDate; } 
			 set {  _EndDate = value; } 
		 }

        private bool _IsSemesterBreakApplicable;
        public bool IsSemesterBreakApplicable
        {
            get { return _IsSemesterBreakApplicable; }
            set { _IsSemesterBreakApplicable = value; }
        }


        private DateTime? _SemesterBreakFromDate;
        //[Required(ErrorMessage = "Semester Break From Date is required")]
        public DateTime? SemesterBreakFromDate
        {
            get { return _SemesterBreakFromDate; }
            set { _SemesterBreakFromDate = value; }
        }
        private DateTime? _SemesterBreakToDate;
        //[Required(ErrorMessage = "Semester Break To Date is required")]
        public DateTime? SemesterBreakToDate
        {
            get { return _SemesterBreakToDate; }
            set { _SemesterBreakToDate = value; }
        }

        private bool _IsSummerBreakApplicable;
        public bool IsSummerBreakApplicable
        {
            get { return _IsSummerBreakApplicable; }
            set { _IsSummerBreakApplicable = value; }
        }

        private DateTime? _SummerBreakFromDate;
        //[Required(ErrorMessage = "Summer Break From Date is required")]
        public DateTime? SummerBreakFromDate
        {
            get { return _SummerBreakFromDate; }
            set { _SummerBreakFromDate = value; }
        }

        private DateTime? _SummerBreakToDate;
        //[Required(ErrorMessage = "Summer Break To Date is required")]
        public DateTime? SummerBreakToDate
        {
            get { return _SummerBreakToDate; }
            set { _SummerBreakToDate = value; }
        }

        //private DateTime _BreakStartDate;
        //public DateTime BreakStartDate
        //{ 
        // get { return  _BreakStartDate; } 
        // set {  _BreakStartDate = value; } 
        //} 

        //private DateTime _BreakEndDate;
        //public DateTime BreakEndDate
        //{ 
        // get { return  _BreakEndDate; } 
        // set {  _BreakEndDate = value; } 
        //}

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
