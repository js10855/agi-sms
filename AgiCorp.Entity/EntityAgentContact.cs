using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityAgentContact
	 {
		 public EntityAgentContact() 
		 {
		 }

		 private int _AgentContactID;
		 public int AgentContactID
		 { 
			 get { return  _AgentContactID; } 
			 set {  _AgentContactID = value; } 
		 } 

		 private int _AgentID;
		 public int AgentID
		 { 
			 get { return  _AgentID; } 
			 set {  _AgentID = value; } 
		 } 

		 private string _FirstName;
		 public string FirstName
		 { 
			 get { return  _FirstName; } 
			 set {  _FirstName = value; } 
		 } 

		 private string _MiddleName;
		 public string MiddleName
		 { 
			 get { return  _MiddleName; } 
			 set {  _MiddleName = value; } 
		 } 

		 private string _LastName;
		 public string LastName
		 { 
			 get { return  _LastName; } 
			 set {  _LastName = value; } 
		 } 

		 private string _Email;
		 public string Email
		 { 
			 get { return  _Email; } 
			 set {  _Email = value; } 
		 } 

		 private string _PhoneCountryCode;
		 public string PhoneCountryCode
		 { 
			 get { return  _PhoneCountryCode; } 
			 set {  _PhoneCountryCode = value; } 
		 } 

		 private string _PhoneStateCode;
		 public string PhoneStateCode
		 { 
			 get { return  _PhoneStateCode; } 
			 set {  _PhoneStateCode = value; } 
		 } 

		 private string _Phone;
		 public string Phone
		 { 
			 get { return  _Phone; } 
			 set {  _Phone = value; } 
		 } 

		 private string _MobileCountryCode;
		 public string MobileCountryCode
		 { 
			 get { return  _MobileCountryCode; } 
			 set {  _MobileCountryCode = value; } 
		 } 

		 private string _Mobile;
		 public string Mobile
		 { 
			 get { return  _Mobile; } 
			 set {  _Mobile = value; } 
		 } 

		 private bool? _IsPrimaryContact;
		 public bool? IsPrimaryContact
		 { 
			 get { return  _IsPrimaryContact; } 
			 set {  _IsPrimaryContact = value; } 
		 }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
