using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientNationality
	 {
		 public EntityClientNationality() 
		 {
		 }

		 private int _ClientNationalityID;
		 public int ClientNationalityID
		 { 
			 get { return  _ClientNationalityID; } 
			 set {  _ClientNationalityID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
