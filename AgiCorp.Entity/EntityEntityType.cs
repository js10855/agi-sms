using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityEntityType
	 {
		 public EntityEntityType() 
		 {
		 }

		 private int _EntityTypeID;
		 public int EntityTypeID
		 { 
			 get { return  _EntityTypeID; } 
			 set {  _EntityTypeID = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
