using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityStaffQualificationType
	 {
		 public EntityStaffQualificationType() 
		 {
		 }

		 private int _StaffQualificationTypeID;
		 public int StaffQualificationTypeID
		 { 
			 get { return  _StaffQualificationTypeID; } 
			 set {  _StaffQualificationTypeID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
