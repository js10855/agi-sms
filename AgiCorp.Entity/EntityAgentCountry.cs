using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityAgentCountry
	 {
		 public EntityAgentCountry() 
		 {
		 }

		 private int _AgentCountryID;
		 public int AgentCountryID
		 { 
			 get { return  _AgentCountryID; } 
			 set {  _AgentCountryID = value; } 
		 } 

		 private int _AgentID;
		 public int AgentID
		 { 
			 get { return  _AgentID; } 
			 set {  _AgentID = value; } 
		 } 

		 private int _CountryID;
		 public int CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

	}
}
