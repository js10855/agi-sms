using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DevExpress.Web;

namespace AgiCorp.Entity
{
	 public class EntityClient
	 {
		 public EntityClient() 
		 {
		 }

		 private int _ClientID;
        
        public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

		 private string _StudentReference;
        [Required(ErrorMessage = "Student Reference is required")]
        [Remote("CheckUniqueCode", "Client", HttpMethod = "GET", ErrorMessage = "Duplicate Student Reference!", AdditionalFields = "ClientID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string StudentReference
		 { 
			 get { return  _StudentReference; } 
			 set {  _StudentReference = value; } 
		 } 

		 private string _FirstName;
		 public string FirstName
		 { 
			 get { return  _FirstName; } 
			 set {  _FirstName = value; } 
		 } 

		 private string _MiddleName;
		 public string MiddleName
		 { 
			 get { return  _MiddleName; } 
			 set {  _MiddleName = value; } 
		 } 

		 private string _LastName;
		 public string LastName
		 { 
			 get { return  _LastName; } 
			 set {  _LastName = value; } 
		 } 

		 private DateTime _DateOfBirth;
		 public DateTime DateOfBirth
		 { 
			 get { return  _DateOfBirth; } 
			 set {  _DateOfBirth = value; } 
		 } 

		 private byte[] _Photo;
		 public byte[] Photo
		 { 
			 get { return  _Photo; } 
			 set {  _Photo = value; } 
		 }

        private string _PhotoFileName;
        public string PhotoFileName
        {
            get { return _PhotoFileName; }
            set { _PhotoFileName = value; }
        }

        private string _PassportNumber;
		 public string PassportNumber
		 { 
			 get { return  _PassportNumber; } 
			 set {  _PassportNumber = value; } 
		 } 

		 private int _ResidenceID;
		 public int ResidenceID
		 { 
			 get { return  _ResidenceID; } 
			 set {  _ResidenceID = value; } 
		 } 

		 private int _ClientTypeID;
		 public int ClientTypeID
		 { 
			 get { return _ClientTypeID; } 
			 set { _ClientTypeID = value; } 
		 } 

		 private string _NSNNumber;
		 public string NSNNumber
		 { 
			 get { return  _NSNNumber; } 
			 set {  _NSNNumber = value; } 
		 } 

		 private string _WorkNumber;
		 public string WorkNumber
		 { 
			 get { return  _WorkNumber; } 
			 set {  _WorkNumber = value; } 
		 } 

		 private string _PublicTrustNumber;
		 public string PublicTrustNumber
		 { 
			 get { return  _PublicTrustNumber; } 
			 set {  _PublicTrustNumber = value; } 
		 } 

		 private string _ExternalRef1;
		 public string ExternalRef1
		 { 
			 get { return  _ExternalRef1; } 
			 set {  _ExternalRef1 = value; } 
		 } 

		 private string _ExternalRef2;
		 public string ExternalRef2
		 { 
			 get { return  _ExternalRef2; } 
			 set {  _ExternalRef2 = value; } 
		 } 

		 private string _Address1;
		 public string Address1
		 { 
			 get { return  _Address1; } 
			 set {  _Address1 = value; } 
		 } 

		 private string _Address2;
		 public string Address2
		 { 
			 get { return  _Address2; } 
			 set {  _Address2 = value; } 
		 } 

		 private string _Address3;
		 public string Address3
		 { 
			 get { return  _Address3; } 
			 set {  _Address3 = value; } 
		 } 

		 private string _PostCode;
		 public string PostCode
		 { 
			 get { return  _PostCode; } 
			 set {  _PostCode = value; } 
		 } 

		 private string _City;
		 public string City
		 { 
			 get { return  _City; } 
			 set {  _City = value; } 
		 } 

		 private string _State;
		 public string State
		 { 
			 get { return  _State; } 
			 set {  _State = value; } 
		 } 

		 private string _CountryID;
		 public string CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

		 private string _Email;
		 public string Email
		 { 
			 get { return  _Email; } 
			 set {  _Email = value; } 
		 } 

		 private string _PhoneCountryCode;
		 public string PhoneCountryCode
		 { 
			 get { return  _PhoneCountryCode; } 
			 set {  _PhoneCountryCode = value; } 
		 } 

		 private string _PhoneStateCode;
		 public string PhoneStateCode
		 { 
			 get { return  _PhoneStateCode; } 
			 set {  _PhoneStateCode = value; } 
		 } 

		 private string _Phone;
		 public string Phone
		 { 
			 get { return  _Phone; } 
			 set {  _Phone = value; } 
		 } 

		 private int _EthnicityID;
		 public int EthnicityID
		 { 
			 get { return  _EthnicityID; } 
			 set {  _EthnicityID = value; } 
		 } 

		 private string _IwiAffiliation1;
		 public string IwiAffiliation1
		 { 
			 get { return  _IwiAffiliation1; } 
			 set {  _IwiAffiliation1 = value; } 
		 } 

		 private string _IwiAffiliation2;
		 public string IwiAffiliation2
		 { 
			 get { return  _IwiAffiliation2; } 
			 set {  _IwiAffiliation2 = value; } 
		 } 

		 private string _IwiAffiliation3;
		 public string IwiAffiliation3
		 { 
			 get { return  _IwiAffiliation3; } 
			 set {  _IwiAffiliation3 = value; } 
		 } 

		 private string _IwiAffiliation4;
		 public string IwiAffiliation4
		 { 
			 get { return  _IwiAffiliation4; } 
			 set {  _IwiAffiliation4 = value; } 
		 } 

		 private string _IwiAffiliation5;
		 public string IwiAffiliation5
		 { 
			 get { return  _IwiAffiliation5; } 
			 set {  _IwiAffiliation5 = value; } 
		 } 

		 private int _AgentID;
		 public int AgentID
		 { 
			 get { return  _AgentID; } 
			 set {  _AgentID = value; } 
		 }

        private string _ClientGenderID;
        [Required(ErrorMessage = "Gender is required")]
        public string ClientGenderID
        {
            get { return _ClientGenderID; }
            set { _ClientGenderID = value; }
        }
        
        private string _ClientNationalityID;
        [Required(ErrorMessage = "Nationality is required")]
        public string ClientNationalityID
        {
            get { return _ClientNationalityID; }
            set { _ClientNationalityID = value; }
        }

        private string _ClientVisaTypeID;
        [Required(ErrorMessage = "Visa Type is required")]
        public string ClientVisaTypeID
        {
            get { return _ClientVisaTypeID; }
            set { _ClientVisaTypeID = value; }
        }
        
        private string _StudentType;
        [Required(ErrorMessage = "Student Type is required")]
        public string StudentType
        {
            get { return _StudentType; }
            set { _StudentType = value; }
        }

        private string _IRDNumber;
        //[Required(ErrorMessage = "IRD Number is required")]
        public string IRDNumber
        {
            get { return _IRDNumber; }
            set { _IRDNumber = value; }
        }

        private bool _NZPermanantResidence;
        //[Required(ErrorMessage = "NZ Permanant Residence is required")]
        public bool NZPermanantResidence
        {
            get { return _NZPermanantResidence; }
            set { _NZPermanantResidence = value; }
        }
    }
}
