using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityAgentCommission
	 {
		 public EntityAgentCommission() 
		 {
		 }

		 private int _AgentCommissionID;
		 public int AgentCommissionID
		 { 
			 get { return  _AgentCommissionID; } 
			 set {  _AgentCommissionID = value; } 
		 }

        private int _AgentID;
        public int AgentID
        {
            get { return _AgentID; }
            set { _AgentID = value; }
        }

        private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private int _CountryID;
		 public int CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

		 private string _ProgrammeID;
		 public string ProgrammeID
		 { 
			 get { return  _ProgrammeID; } 
			 set {  _ProgrammeID = value; } 
		 } 

		 private int _CommissionTypeID;
		 public int CommissionTypeID
		 { 
			 get { return  _CommissionTypeID; } 
			 set {  _CommissionTypeID = value; } 
		 } 

		 private string _CommissionPer;
		 public string CommissionPer
		 { 
			 get { return  _CommissionPer; } 
			 set {  _CommissionPer = value; } 
		 } 

		 private string _CommissionFixed;
		 public string CommissionFixed
		 { 
			 get { return  _CommissionFixed; } 
			 set {  _CommissionFixed = value; } 
		 } 

		 private DateTime _StartDate;
		 public DateTime StartDate
		 { 
			 get { return  _StartDate; } 
			 set {  _StartDate = value; } 
		 } 

		 private DateTime _EndDate;
		 public DateTime EndDate
		 { 
			 get { return  _EndDate; } 
			 set {  _EndDate = value; } 
		 } 

	}
}
