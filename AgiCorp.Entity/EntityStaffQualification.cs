using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityStaffQualification
	 {
		 public EntityStaffQualification() 
		 {
		 }

		 private int _StaffQualificationID;
		 public int StaffQualificationID
		 { 
			 get { return  _StaffQualificationID; } 
			 set {  _StaffQualificationID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _StaffQualificationLevelID;
		 public int StaffQualificationLevelID
		 { 
			 get { return  _StaffQualificationLevelID; } 
			 set {  _StaffQualificationLevelID = value; } 
		 } 

		 private int _StaffQualificationTypeID;
		 public int StaffQualificationTypeID
		 { 
			 get { return  _StaffQualificationTypeID; } 
			 set {  _StaffQualificationTypeID = value; } 
		 } 

		 private int _StaffID;
		 public int StaffID
		 { 
			 get { return  _StaffID; } 
			 set {  _StaffID = value; } 
		 } 

	}
}
