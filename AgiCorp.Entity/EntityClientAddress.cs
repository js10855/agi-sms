using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientAddress
	 {
		 public EntityClientAddress() 
		 {
		 }

		 private int _ClientAddressID;
		 public int ClientAddressID
		 { 
			 get { return  _ClientAddressID; } 
			 set {  _ClientAddressID = value; } 
		 } 

		 private string _Address1;
		 public string Address1
		 { 
			 get { return  _Address1; } 
			 set {  _Address1 = value; } 
		 } 

		 private string _Address2;
		 public string Address2
		 { 
			 get { return  _Address2; } 
			 set {  _Address2 = value; } 
		 } 

		 private string _Address3;
		 public string Address3
		 { 
			 get { return  _Address3; } 
			 set {  _Address3 = value; } 
		 } 

		 private string _PostCode;
		 public string PostCode
		 { 
			 get { return  _PostCode; } 
			 set {  _PostCode = value; } 
		 } 

		 private string _City;
		 public string City
		 { 
			 get { return  _City; } 
			 set {  _City = value; } 
		 } 

		 private string _State;
		 public string State
		 { 
			 get { return  _State; } 
			 set {  _State = value; } 
		 } 

		 private string _CountryID;
		 public string CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

		 private string _Email;
		 public string Email
		 { 
			 get { return  _Email; } 
			 set {  _Email = value; } 
		 } 

		 private string _PhoneCountryCode;
		 public string PhoneCountryCode
		 { 
			 get { return  _PhoneCountryCode; } 
			 set {  _PhoneCountryCode = value; } 
		 } 

		 private string _PhoneStateCode;
		 public string PhoneStateCode
		 { 
			 get { return  _PhoneStateCode; } 
			 set {  _PhoneStateCode = value; } 
		 } 

		 private string _Phone;
		 public string Phone
		 { 
			 get { return  _Phone; } 
			 set {  _Phone = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
