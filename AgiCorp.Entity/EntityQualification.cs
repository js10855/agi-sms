using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityQualification
	 {
		 public EntityQualification() 
		 {
		 }

		 private int? _QualificationID;
		 public int? QualificationID
		 { 
			 get { return  _QualificationID; } 
			 set {  _QualificationID = value; } 
		 } 

		 private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Qualification", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "QualificationID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private int _NZQACode;
		 public int NZQACode
		 { 
			 get { return  _NZQACode; } 
			 set {  _NZQACode = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _Credits;
		 public int Credits
		 { 
			 get { return  _Credits; } 
			 set {  _Credits = value; } 
		 } 

		 private int _F2FHours;
		 public int F2FHours
		 { 
			 get { return  _F2FHours; } 
			 set {  _F2FHours = value; } 
		 } 

		 private int _SDHours;
		 public int SDHours
		 { 
			 get { return  _SDHours; } 
			 set {  _SDHours = value; } 
		 }

        private int _HoursPerCredit;
        public int HoursPerCredit
        {
            get { return _HoursPerCredit; }
            set { _HoursPerCredit = value; }
        }

        private int _TotalHours;
        public int TotalHours
        {
            get { return _TotalHours; }
            set { _TotalHours = value; }
        }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 }

        private int _QualificationTypeID;
        public int QualificationTypeID
        {
            get { return _QualificationTypeID; }
            set { _QualificationTypeID = value; }
        }

        private int _QualificationLevelID;
        public int QualificationLevelID
        {
            get { return _QualificationLevelID; }
            set { _QualificationLevelID = value; }
        }

    }
}
