using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityBuilding
	 {
		 public EntityBuilding() 
		 {
		 }

		 private int? _BuildingID;
		 public int? BuildingID
		 { 
			 get { return  _BuildingID; } 
			 set {  _BuildingID = value; } 
		 }

        private int _CampusID;
        public int CampusID
        {
            get { return _CampusID; }
            set { _CampusID = value; }
        }

        private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Building", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "BuildingID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]

        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private int _Capacity;
		 public int Capacity
		 { 
			 get { return  _Capacity; } 
			 set {  _Capacity = value; } 
		 }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
