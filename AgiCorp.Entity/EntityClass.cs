using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityClass
	 {
		 public EntityClass() 
		 {
		 }

		 private int? _ClassID;
		 public int? ClassID
		 { 
			 get { return  _ClassID; } 
			 set {  _ClassID = value; } 
		 } 

		 private string _Code;
        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Class", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "ClassID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private string _BuildingID;
		 public string BuildingID
		 { 
			 get { return  _BuildingID; } 
			 set {  _BuildingID = value; } 
		 } 

		 private int _StudentCapacity;
		 public int StudentCapacity
		 { 
			 get { return  _StudentCapacity; } 
			 set {  _StudentCapacity = value; } 
		 }

        private bool _IsActive = true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

	}
}
