using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientAssessments
	 {
		 public EntityClientAssessments() 
		 {
		 }

		 private int _ClientAssessmentID;
		 public int ClientAssessmentID
		 { 
			 get { return  _ClientAssessmentID; } 
			 set {  _ClientAssessmentID = value; } 
		 } 

		 private int _ClassID;
		 public int ClassID
		 { 
			 get { return  _ClassID; } 
			 set {  _ClassID = value; } 
		 } 

		 private int _ModuleID;
		 public int ModuleID
		 { 
			 get { return  _ModuleID; } 
			 set {  _ModuleID = value; } 
		 } 

		 private int _ModuleAssessmentID;
		 public int ModuleAssessmentID
        { 
			 get { return _ModuleAssessmentID; } 
			 set { _ModuleAssessmentID = value; } 
		 } 

		 private int _TotalMarks;
		 public int TotalMarks
		 { 
			 get { return  _TotalMarks; } 
			 set {  _TotalMarks = value; } 
		 } 

		 private float _MarksObtained;
		 public float MarksObtained
		 { 
			 get { return  _MarksObtained; } 
			 set {  _MarksObtained = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
