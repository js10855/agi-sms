using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityQualificationLevel
	 {
		 public EntityQualificationLevel() 
		 {
		 }

		 private int _QualificationLevelID;
		 public int QualificationLevelID
		 { 
			 get { return  _QualificationLevelID; } 
			 set {  _QualificationLevelID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

	}
}
