using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityClientPlacement
	 {
		 public EntityClientPlacement() 
		 {
		 }

		 private int _ClientPlacementID;
		 public int ClientPlacementID
		 { 
			 get { return  _ClientPlacementID; } 
			 set {  _ClientPlacementID = value; } 
		 } 

		 private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _PlacementID;
		 public string PlacementID
		 { 
			 get { return _PlacementID; } 
			 set { _PlacementID = value; } 
		 } 

		 private bool _CurrentlyPlaced;
		 public bool CurrentlyPlaced
		 { 
			 get { return  _CurrentlyPlaced; } 
			 set {  _CurrentlyPlaced = value; } 
		 } 

		 private DateTime _FromDate;
		 public DateTime FromDate
		 { 
			 get { return  _FromDate; } 
			 set {  _FromDate = value; } 
		 } 

		 private DateTime _ToDate;
		 public DateTime ToDate
		 { 
			 get { return  _ToDate; } 
			 set {  _ToDate = value; } 
		 } 

		 private int _ClientID;
		 public int ClientID
		 { 
			 get { return  _ClientID; } 
			 set {  _ClientID = value; } 
		 } 

	}
}
