using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AgiCorp.Entity
{
	 public class EntityAgentDocument
	 {
		 public EntityAgentDocument() 
		 {
		 }

		 private int _AgentDocumentID;
		 public int AgentDocumentID
		 { 
			 get { return  _AgentDocumentID; } 
			 set {  _AgentDocumentID = value; } 
		 }

        private int _AgentID;
        public int AgentID
        {
            get { return _AgentID; }
            set { _AgentID = value; }
        }

        private string _Code;
		 public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _Description;
		 public string Description
		 { 
			 get { return  _Description; } 
			 set {  _Description = value; } 
		 } 

		 private byte[] _File;
		 public byte[] File
		 { 
			 get { return  _File; } 
			 set {  _File = value; } 
		 }

        private string _DocsFileName;
        public string DocsFileName
        {
            get { return _DocsFileName; }
            set { _DocsFileName = value; }
        }


    }
}
