using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgiCorp.Entity
{
	 public class EntityAgent
	 {
		 public EntityAgent() 
		 {
		 }

		 private int? _AgentID;
		 public int? AgentID
		 { 
			 get { return  _AgentID; } 
			 set {  _AgentID = value; } 
		 }

        
        private string _Code;

        [Required(ErrorMessage = "Code is required")]
        [Remote("CheckUniqueCode", "Agent", HttpMethod = "GET", ErrorMessage = "Duplicate Code!", AdditionalFields = "AgentID")]
        [StringLength(10, ErrorMessage = "Must be under 10 characters")]
        public string Code
		 { 
			 get { return  _Code; } 
			 set {  _Code = value; } 
		 } 

		 private string _EntityName;
		 public string EntityName
		 { 
			 get { return  _EntityName; } 
			 set {  _EntityName = value; } 
		 } 

		 private string _EntityTypeID;
		 public string EntityTypeID
		 { 
			 get { return  _EntityTypeID; } 
			 set {  _EntityTypeID = value; } 
		 } 

		 private string _Address1;
		 public string Address1
		 { 
			 get { return  _Address1; } 
			 set {  _Address1 = value; } 
		 } 

		 private string _Address2;
		 public string Address2
		 { 
			 get { return  _Address2; } 
			 set {  _Address2 = value; } 
		 } 

		 private string _Address3;
		 public string Address3
		 { 
			 get { return  _Address3; } 
			 set {  _Address3 = value; } 
		 } 

		 private string _PostCode;
		 public string PostCode
		 { 
			 get { return  _PostCode; } 
			 set {  _PostCode = value; } 
		 } 

		 private string _City;
		 public string City
		 { 
			 get { return  _City; } 
			 set {  _City = value; } 
		 } 

		 private string _State;
		 public string State
		 { 
			 get { return  _State; } 
			 set {  _State = value; } 
		 } 

		 private string _CountryID;
		 public string CountryID
		 { 
			 get { return  _CountryID; } 
			 set {  _CountryID = value; } 
		 } 

		 private string _Email;
		 public string Email
		 { 
			 get { return  _Email; } 
			 set {  _Email = value; } 
		 } 

		 private string _PhoneCountryCode;
		 public string PhoneCountryCode
		 { 
			 get { return  _PhoneCountryCode; } 
			 set {  _PhoneCountryCode = value; } 
		 } 

		 private string _PhoneStateCode;
		 public string PhoneStateCode
		 { 
			 get { return  _PhoneStateCode; } 
			 set {  _PhoneStateCode = value; } 
		 } 

		 private string _Phone;
		 public string Phone
		 { 
			 get { return  _Phone; } 
			 set {  _Phone = value; } 
		 } 

		 private string _MobileCountryCode;
		 public string MobileCountryCode
		 { 
			 get { return  _MobileCountryCode; } 
			 set {  _MobileCountryCode = value; } 
		 } 

		 private string _Mobile;
		 public string Mobile
		 { 
			 get { return  _Mobile; } 
			 set {  _Mobile = value; } 
		 } 

		 private DateTime _ContractStartDate;
		 public DateTime ContractStartDate
		 { 
			 get { return  _ContractStartDate; } 
			 set {  _ContractStartDate = value; } 
		 } 

		 private DateTime _ContractEndDate;
		 public DateTime ContractEndDate
		 { 
			 get { return  _ContractEndDate; } 
			 set {  _ContractEndDate = value; } 
		 } 

		private bool _IsActive=true;
        [System.ComponentModel.DefaultValue(true)]
        public bool IsActive
		 { 
			 get { return  _IsActive; } 
			 set {  _IsActive = value; } 
		 } 

		 private string _WebSite;
		 public string WebSite
		 { 
			 get { return  _WebSite; } 
			 set {  _WebSite = value; } 
		 } 

	}
}
