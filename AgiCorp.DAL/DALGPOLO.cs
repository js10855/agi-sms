using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALGPOLO
	{
		 public DALGPOLO() 
		 {
		 }

        public static DataSet Get_By_GPOID(int GPOID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_GPOID_tblGPOLO";
            _database.AddInParameter(_sqlcommand, "@GPOID", DbType.Int32, GPOID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }
        public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblGPOLO";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_GPOLOID(int GPOLOID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_GPOLOID_tblGPOLO";
		 	 _database.AddInParameter(_sqlcommand, "@GPOLOID", DbType.Int32, GPOLOID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityGPOLO _entity, out string errorMsg) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblGPOLO";
		 	 _database.AddInParameter(_sqlcommand, "@GPOID", DbType.Int32, _entity.GPOID); 
		 	 _database.AddInParameter(_sqlcommand, "@LOID", DbType.Int32, _entity.LOID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.AddOutParameter(_sqlcommand, "@ERROR_MSG", DbType.String, 200);
            _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            errorMsg = Convert.ToString(_database.GetParameterValue(_sqlcommand, "@ERROR_MSG"));
            return returnResult;
		 }



		 public static int Update(EntityGPOLO _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblGPOLO";
		 	 _database.AddInParameter(_sqlcommand, "@GPOLOID", DbType.Int32, _entity.GPOLOID); 
		 	 _database.AddInParameter(_sqlcommand, "@GPOID", DbType.Int32, _entity.GPOID); 
		 	 _database.AddInParameter(_sqlcommand, "@LOID", DbType.Int32, _entity.LOID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_GPOLOID(int GPOLOID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_GPOLOID_tblGPOLO";
		 	 _database.AddInParameter(_sqlcommand, "@GPOLOID", DbType.Int32, GPOLOID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
