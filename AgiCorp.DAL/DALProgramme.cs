using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALProgramme
	{
		 public DALProgramme() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblProgramme";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_ProgrammeID(int ProgrammeID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_ProgrammeID_tblProgramme";
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, ProgrammeID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityProgramme _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblProgramme";
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@NZQACode", DbType.Int32, _entity.NZQACode); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@Credits", DbType.Int32, _entity.Credits); 
		 	 _database.AddInParameter(_sqlcommand, "@F2FHours", DbType.Int32, _entity.F2FHours); 
		 	 _database.AddInParameter(_sqlcommand, "@SDHours", DbType.Int32, _entity.SDHours);
            _database.AddInParameter(_sqlcommand, "@ClassF2F", DbType.Int32, _entity.ClassF2F);
            _database.AddInParameter(_sqlcommand, "@PlacementF2F", DbType.Int32, _entity.PlacementF2F);

            _database.AddInParameter(_sqlcommand, "@ProgrammeLevelID", DbType.Int32, _entity.ProgrammeLevelID);
            _database.AddInParameter(_sqlcommand, "@ProgrammeLength", DbType.Int32, _entity.ProgrammeLength);
            //_database.AddInParameter(_sqlcommand, "@IsSemesterBreakApplicable", DbType.Boolean, _entity.IsSemesterBreakApplicable);
            //if(_entity.IsSemesterBreakApplicable)
            //{
            //    _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, null);
            //    _database.AddInParameter(_sqlcommand, "@SemesterBreakToDate", DbType.Date, null);
            //}
            //else
            //{
            //    _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, _entity.SemesterBreakFromDate);
            //    _database.AddInParameter(_sqlcommand, "@SemesterBreakToDate", DbType.Date, _entity.SemesterBreakToDate);
            //}
            //_database.AddInParameter(_sqlcommand, "@SummerBreakFromDate", DbType.Date, _entity.SummerBreakFromDate);
            //_database.AddInParameter(_sqlcommand, "@SummerBreakToDate", DbType.Date, _entity.SummerBreakToDate);
            _database.AddInParameter(_sqlcommand, "@ProgrammeRatingID", DbType.Int32, _entity.ProgrammeRatingID);
            //_database.AddInParameter(_sqlcommand, "@StartDate", DbType.Date, _entity.StartDate);
            //_database.AddInParameter(_sqlcommand, "@EndDate", DbType.Date, _entity.EndDate);

            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityProgramme _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblProgramme";
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, _entity.ProgrammeID); 
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@NZQACode", DbType.Int32, _entity.NZQACode); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@Credits", DbType.Int32, _entity.Credits); 
		 	 _database.AddInParameter(_sqlcommand, "@F2FHours", DbType.Int32, _entity.F2FHours); 
		 	 _database.AddInParameter(_sqlcommand, "@SDHours", DbType.Int32, _entity.SDHours);
            _database.AddInParameter(_sqlcommand, "@ClassF2F", DbType.Int32, _entity.ClassF2F);
            _database.AddInParameter(_sqlcommand, "@PlacementF2F", DbType.Int32, _entity.PlacementF2F);
            _database.AddInParameter(_sqlcommand, "@ProgrammeLevelID", DbType.Int32, _entity.ProgrammeLevelID);
            _database.AddInParameter(_sqlcommand, "@ProgrammeLength", DbType.Int32, _entity.ProgrammeLength);
                  
            //_database.AddInParameter(_sqlcommand, "@IsSemesterBreakApplicable", DbType.Boolean, _entity.IsSemesterBreakApplicable);
            //if (_entity.IsSemesterBreakApplicable)
            //{
            //    _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, null);
            //    _database.AddInParameter (_sqlcommand, "@SemesterBreakToDate", DbType.Date, null);
            //}
            //else
            //{
            //    _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, _entity.SemesterBreakFromDate);
            //    _database.AddInParameter(_sqlcommand, "@SemesterBreakToDate", DbType.Date, _entity.SemesterBreakToDate);
            //}

            //_database.AddInParameter(_sqlcommand, "@SummerBreakFromDate", DbType.Date, _entity.SummerBreakFromDate);
            //_database.AddInParameter(_sqlcommand, "@SummerBreakToDate", DbType.Date, _entity.SummerBreakToDate);
            _database.AddInParameter(_sqlcommand, "@ProgrammeRatingID", DbType.Int32, _entity.ProgrammeRatingID);
            //_database.AddInParameter(_sqlcommand, "@StartDate", DbType.Date, _entity.StartDate);
            //_database.AddInParameter(_sqlcommand, "@EndDate", DbType.Date, _entity.EndDate);

            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_ProgrammeID(int ProgrammeID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_ProgrammeID_tblProgramme";
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, ProgrammeID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static Boolean Check_For_UniqueCode(string Code, string ProgrammeID)
        {
            Boolean returnResult = false;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_CHECK_FOR_UniqueCode_tblProgramme";
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, Code);
            _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.String, ProgrammeID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToBoolean(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }

    }
}
