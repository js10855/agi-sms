using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALClientAddress
	{
		 public DALClientAddress() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblClientAddress";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }

        public static DataSet Get_By_ClientID(int ClientID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ClientID_tblClientAddress";
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, ClientID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }
        

         public static DataSet Get_By_ClientAddressID(int ClientAddressID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_ClientAddressID_tblClientAddress";
		 	 _database.AddInParameter(_sqlcommand, "@ClientAddressID", DbType.Int32, ClientAddressID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityClientAddress _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblClientAddress";
		 	 _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1); 
		 	 _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3); 
		 	 _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityClientAddress _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblClientAddress";
		 	 _database.AddInParameter(_sqlcommand, "@ClientAddressID", DbType.Int32, _entity.ClientAddressID); 
		 	 _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1); 
		 	 _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3); 
		 	 _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_ClientAddressID(int ClientAddressID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_ClientAddressID_tblClientAddress";
		 	 _database.AddInParameter(_sqlcommand, "@ClientAddressID", DbType.Int32, ClientAddressID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
