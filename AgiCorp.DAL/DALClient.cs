using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;


namespace AgiCorp.DAL
{
	public class DALClient
	{
		 public DALClient() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblClient";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_ClientID(int ClientID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_ClientID_tblClient";
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, ClientID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityClient _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblClient";
		 	 _database.AddInParameter(_sqlcommand, "@StudentReference", DbType.String, _entity.StudentReference); 
		 	 _database.AddInParameter(_sqlcommand, "@FirstName", DbType.String, _entity.FirstName); 
		 	 _database.AddInParameter(_sqlcommand, "@MiddleName", DbType.String, _entity.MiddleName); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@DateOfBirth", DbType.DateTime, _entity.DateOfBirth); 
		 	 _database.AddInParameter(_sqlcommand, "@Photo", DbType.Binary, _entity.Photo); 
		 	 _database.AddInParameter(_sqlcommand, "@PassportNumber", DbType.String, _entity.PassportNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@ResidenceID", DbType.Int32, _entity.ResidenceID); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientTypeID", DbType.Int32, _entity.ClientTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@NSNNumber", DbType.String, _entity.NSNNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@WorkNumber", DbType.String, _entity.WorkNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@PublicTrustNumber", DbType.String, _entity.PublicTrustNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@ExternalRef1", DbType.String, _entity.ExternalRef1); 
		 	 _database.AddInParameter(_sqlcommand, "@ExternalRef2", DbType.String, _entity.ExternalRef2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1); 
		 	 _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3); 
		 	 _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@EthnicityID", DbType.Int32, _entity.EthnicityID); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation1", DbType.String, _entity.IwiAffiliation1); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation2", DbType.String, _entity.IwiAffiliation2); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation3", DbType.String, _entity.IwiAffiliation3); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation4", DbType.String, _entity.IwiAffiliation4); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation5", DbType.String, _entity.IwiAffiliation5); 
		 	 _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, _entity.AgentID);
            _database.AddInParameter(_sqlcommand, "@PhotoFileName", DbType.String, _entity.PhotoFileName);
            _database.AddInParameter(_sqlcommand, "@ClientGenderID", DbType.Int32, _entity.ClientGenderID);
            _database.AddInParameter(_sqlcommand, "@ClientNationalityID", DbType.Int32, _entity.ClientNationalityID);
            _database.AddInParameter(_sqlcommand, "@ClientVisaTypeID", DbType.Int32, _entity.ClientVisaTypeID);
            _database.AddInParameter(_sqlcommand, "@StudentType", DbType.String, _entity.StudentType);
            _database.AddInParameter(_sqlcommand, "@IRDNumber", DbType.String, _entity.IRDNumber);
            _database.AddInParameter(_sqlcommand, "@NZPermanantResidence", DbType.Boolean, _entity.NZPermanantResidence);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityClient _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblClient";
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddInParameter(_sqlcommand, "@StudentReference", DbType.String, _entity.StudentReference); 
		 	 _database.AddInParameter(_sqlcommand, "@FirstName", DbType.String, _entity.FirstName); 
		 	 _database.AddInParameter(_sqlcommand, "@MiddleName", DbType.String, _entity.MiddleName); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@DateOfBirth", DbType.DateTime, _entity.DateOfBirth); 
		 	 _database.AddInParameter(_sqlcommand, "@Photo", DbType.Binary, _entity.Photo); 
		 	 _database.AddInParameter(_sqlcommand, "@PassportNumber", DbType.String, _entity.PassportNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@ResidenceID", DbType.Int32, _entity.ResidenceID); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientTypeID", DbType.Int32, _entity.ClientTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@NSNNumber", DbType.String, _entity.NSNNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@WorkNumber", DbType.String, _entity.WorkNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@PublicTrustNumber", DbType.String, _entity.PublicTrustNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@ExternalRef1", DbType.String, _entity.ExternalRef1); 
		 	 _database.AddInParameter(_sqlcommand, "@ExternalRef2", DbType.String, _entity.ExternalRef2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1); 
		 	 _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3); 
		 	 _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@EthnicityID", DbType.Int32, _entity.EthnicityID); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation1", DbType.String, _entity.IwiAffiliation1); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation2", DbType.String, _entity.IwiAffiliation2); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation3", DbType.String, _entity.IwiAffiliation3); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation4", DbType.String, _entity.IwiAffiliation4); 
		 	 _database.AddInParameter(_sqlcommand, "@IwiAffiliation5", DbType.String, _entity.IwiAffiliation5); 
		 	 _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, _entity.AgentID);
            _database.AddInParameter(_sqlcommand, "@PhotoFileName", DbType.String, _entity.PhotoFileName);
            _database.AddInParameter(_sqlcommand, "@ClientGenderID", DbType.Int32, _entity.ClientGenderID);
            _database.AddInParameter(_sqlcommand, "@ClientNationalityID", DbType.Int32, _entity.ClientNationalityID);
            _database.AddInParameter(_sqlcommand, "@ClientVisaTypeID", DbType.Int32, _entity.ClientVisaTypeID);
            _database.AddInParameter(_sqlcommand, "@StudentType", DbType.String, _entity.StudentType);
            _database.AddInParameter(_sqlcommand, "@IRDNumber", DbType.String, _entity.IRDNumber);
            _database.AddInParameter(_sqlcommand, "@NZPermanantResidence", DbType.Boolean, _entity.NZPermanantResidence);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_ClientID(int ClientID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_ClientID_tblClient";
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static    DataSet BALClient_GetStudentType()
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_Get_StudentType";
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
            
            
            
        }
        
        public static Boolean Check_For_UniqueCode(string StudentReference, string ClientID)
        {
            Boolean returnResult = false;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_CHECK_FOR_UniqueStudentReference_tblClient";
            _database.AddInParameter(_sqlcommand, "@StudentReference", DbType.String, StudentReference);
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.String, ClientID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToBoolean(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }

    }
}
