﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;

namespace AgiCorp.DAL
{
    public static class Common
    {
        public static DateTime? EnsureSQLSafe(this DateTime? datetime)
        {
            if (datetime.HasValue && (datetime.Value < (DateTime)SqlDateTime.MinValue || datetime.Value > (DateTime)SqlDateTime.MaxValue))
                return null;

            return datetime;
        }
    }
}
