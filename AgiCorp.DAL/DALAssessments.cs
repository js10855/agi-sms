using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALAssessments
	{
		 public DALAssessments() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblAssessments";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_AssessmentsID(int AssessmentsID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_AssessmentsID_tblAssessments";
		 	 _database.AddInParameter(_sqlcommand, "@AssessmentsID", DbType.Int32, AssessmentsID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityAssessments _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblAssessments";
		 	 _database.AddInParameter(_sqlcommand, "@Name", DbType.String, _entity.Name); 
		 	 _database.AddInParameter(_sqlcommand, "@Type", DbType.String, _entity.Type); 
		 	 _database.AddInParameter(_sqlcommand, "@Format", DbType.String, _entity.Format); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@TotalMarks", DbType.Int32, _entity.TotalMarks); 
		 	 _database.AddInParameter(_sqlcommand, "@MinPassPercentage", DbType.Int32, _entity.MinPassPercentage); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityAssessments _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblAssessments";
		 	 _database.AddInParameter(_sqlcommand, "@AssessmentsID", DbType.Int32, _entity.AssessmentsID); 
		 	 _database.AddInParameter(_sqlcommand, "@Name", DbType.String, _entity.Name); 
		 	 _database.AddInParameter(_sqlcommand, "@Type", DbType.String, _entity.Type); 
		 	 _database.AddInParameter(_sqlcommand, "@Format", DbType.String, _entity.Format); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@TotalMarks", DbType.Int32, _entity.TotalMarks); 
		 	 _database.AddInParameter(_sqlcommand, "@MinPassPercentage", DbType.Int32, _entity.MinPassPercentage); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_AssessmentsID(int AssessmentsID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_AssessmentsID_tblAssessments";
		 	 _database.AddInParameter(_sqlcommand, "@AssessmentsID", DbType.Int32, AssessmentsID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
