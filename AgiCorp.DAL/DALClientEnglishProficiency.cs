using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALClientEnglishProficiency
	{
		 public DALClientEnglishProficiency() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblClientEnglishProficiency";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_ClientEnglishProficiencyID(int ClientEnglishProficiencyID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_ClientEnglishProficiencyID_tblClientEnglishProficiency";
		 	 _database.AddInParameter(_sqlcommand, "@ClientEnglishProficiencyID", DbType.Int32, ClientEnglishProficiencyID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityClientEnglishProficiency _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblClientEnglishProficiency";
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyType", DbType.String, _entity.EnglishProficiencyType); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyExamNumber", DbType.String, _entity.EnglishProficiencyExamNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyExamDate", DbType.DateTime, _entity.EnglishProficiencyExamDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyExpiryDate", DbType.DateTime, _entity.EnglishProficiencyExpiryDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyOverallBand", DbType.Int32, _entity.EnglishProficiencyOverallBand); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSReading", DbType.Int32, _entity.IELTSReading); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSWriting", DbType.Int32, _entity.IELTSWriting); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSSpeaking", DbType.Int32, _entity.IELTSSpeaking); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSListening", DbType.Int32, _entity.IELTSListening); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityClientEnglishProficiency _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblClientEnglishProficiency";
		 	 _database.AddInParameter(_sqlcommand, "@ClientEnglishProficiencyID", DbType.Int32, _entity.ClientEnglishProficiencyID); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyType", DbType.String, _entity.EnglishProficiencyType); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyExamNumber", DbType.String, _entity.EnglishProficiencyExamNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyExamDate", DbType.DateTime, _entity.EnglishProficiencyExamDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyExpiryDate", DbType.DateTime, _entity.EnglishProficiencyExpiryDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EnglishProficiencyOverallBand", DbType.Int32, _entity.EnglishProficiencyOverallBand); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSReading", DbType.Int32, _entity.IELTSReading); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSWriting", DbType.Int32, _entity.IELTSWriting); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSSpeaking", DbType.Int32, _entity.IELTSSpeaking); 
		 	 _database.AddInParameter(_sqlcommand, "@IELTSListening", DbType.Int32, _entity.IELTSListening); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_ClientEnglishProficiencyID(int ClientEnglishProficiencyID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_ClientEnglishProficiencyID_tblClientEnglishProficiency";
		 	 _database.AddInParameter(_sqlcommand, "@ClientEnglishProficiencyID", DbType.Int32, ClientEnglishProficiencyID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }
        public static DataSet Get_By_ClientID(int ClientID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ClientID_tblClientEnglishProficiency";
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, ClientID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }

    }
}
