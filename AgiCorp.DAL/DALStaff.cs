using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALStaff
	{
		 public DALStaff() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblStaff";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_StaffID(int StaffID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_StaffID_tblStaff";
		 	 _database.AddInParameter(_sqlcommand, "@StaffID", DbType.Int32, StaffID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityStaff _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblStaff";
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@Salutation", DbType.String, _entity.Salutation); 
		 	 _database.AddInParameter(_sqlcommand, "@FirstName", DbType.String, _entity.FirstName); 
		 	 _database.AddInParameter(_sqlcommand, "@MiddleName", DbType.String, _entity.MiddleName); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@DateOfBirth", DbType.DateTime, _entity.DateOfBirth); 
		 	 _database.AddInParameter(_sqlcommand, "@WorkEmail", DbType.String, _entity.WorkEmail); 
		 	 _database.AddInParameter(_sqlcommand, "@HomeEmail", DbType.String, _entity.HomeEmail); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@Mobile", DbType.String, _entity.Mobile); 
		 	 _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1); 
		 	 _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@StaffTypeID", DbType.Int32, _entity.StaffTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@StatusID", DbType.Int32, _entity.StatusID); 
		 	 _database.AddInParameter(_sqlcommand, "@CostPerHour", DbType.Int32, _entity.CostPerHour); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityStaff _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblStaff";
		 	 _database.AddInParameter(_sqlcommand, "@StaffID", DbType.Int32, _entity.StaffID); 
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@Salutation", DbType.String, _entity.Salutation); 
		 	 _database.AddInParameter(_sqlcommand, "@FirstName", DbType.String, _entity.FirstName); 
		 	 _database.AddInParameter(_sqlcommand, "@MiddleName", DbType.String, _entity.MiddleName); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@DateOfBirth", DbType.DateTime, _entity.DateOfBirth); 
		 	 _database.AddInParameter(_sqlcommand, "@WorkEmail", DbType.String, _entity.WorkEmail); 
		 	 _database.AddInParameter(_sqlcommand, "@HomeEmail", DbType.String, _entity.HomeEmail); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@Mobile", DbType.String, _entity.Mobile); 
		 	 _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1); 
		 	 _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2); 
		 	 _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@StaffTypeID", DbType.Int32, _entity.StaffTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@StatusID", DbType.Int32, _entity.StatusID); 
		 	 _database.AddInParameter(_sqlcommand, "@CostPerHour", DbType.Int32, _entity.CostPerHour); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_StaffID(int StaffID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_StaffID_tblStaff";
		 	 _database.AddInParameter(_sqlcommand, "@StaffID", DbType.Int32, StaffID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static Boolean Check_For_UniqueCode(string Code, string StaffID)
        {
            Boolean returnResult = false;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_CHECK_FOR_UniqueCode_tblStaff";
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, Code);
            _database.AddInParameter(_sqlcommand, "@StaffID", DbType.String, StaffID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToBoolean(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }


    }
}
