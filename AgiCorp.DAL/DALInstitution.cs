﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
    public class DALInstitution
    {
        public DALInstitution()
        {
        }

        public static DataSet Get_All()
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_ALL_tblInstitution";
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }



        public static DataSet Get_By_InstitutionID(int InstitutionID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_InstitutionID_tblInstitution";
            _database.AddInParameter(_sqlcommand, "@InstitutionID", DbType.Int32, InstitutionID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }



        public static int SET(EntityInstitution _entity)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_SET_tblInstitution";
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code);
            _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description);
            _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1);
            _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2);
            _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3);
            _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode);
            _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City);
            _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State);
            _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID);
            _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email);
            _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode);
            _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode);
            _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone);
            _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive);
            _database.AddInParameter(_sqlcommand, "@AccountName", DbType.String, _entity.AccountName);
            _database.AddInParameter(_sqlcommand, "@AccountNumber", DbType.String, _entity.AccountNumber);
            _database.AddInParameter(_sqlcommand, "@BankName", DbType.String, _entity.BankName);
            _database.AddInParameter(_sqlcommand, "@SwiftCode", DbType.String, _entity.SwiftCode);
            _database.AddInParameter(_sqlcommand, "@SPOCEmail", DbType.String, _entity.SPOCEmail);
            
                        _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }



        public static int Update(EntityInstitution _entity)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_UPDATE_tblInstitution";
            _database.AddInParameter(_sqlcommand, "@InstitutionID", DbType.Int32, _entity.InstitutionID);
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code);
            _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description);
            _database.AddInParameter(_sqlcommand, "@Address1", DbType.String, _entity.Address1);
            _database.AddInParameter(_sqlcommand, "@Address2", DbType.String, _entity.Address2);
            _database.AddInParameter(_sqlcommand, "@Address3", DbType.String, _entity.Address3);
            _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode);
            _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City);
            _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State);
            _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID);
            _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email);
            _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode);
            _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode);
            _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone);
            _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.AddInParameter(_sqlcommand, "@AccountName", DbType.String, _entity.AccountName);
            _database.AddInParameter(_sqlcommand, "@AccountNumber", DbType.String, _entity.AccountNumber);
            _database.AddInParameter(_sqlcommand, "@BankName", DbType.String, _entity.BankName);
            _database.AddInParameter(_sqlcommand, "@SwiftCode", DbType.String, _entity.SwiftCode);
            _database.AddInParameter(_sqlcommand, "@SPOCEmail", DbType.String, _entity.SPOCEmail);
            _database.ExecuteNonQuery(_sqlcommand);

            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }



        public static int Delete_By_InstitutionID(int InstitutionID)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_DELETE_BY_InstitutionID_tblInstitution";
            _database.AddInParameter(_sqlcommand, "@InstitutionID", DbType.Int32, InstitutionID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }

        public static Boolean Check_For_UniqueCode(string Code,string InstitutionID)
        {
            Boolean returnResult = false;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_CHECK_FOR_UniqueCode_tblInstitution";
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, Code);
            _database.AddInParameter(_sqlcommand, "@InstitutionID", DbType.String, InstitutionID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToBoolean(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }

    }
}
