using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALInstituteSchool
	{
		 public DALInstituteSchool() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblInstituteSchool";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }

        public static DataSet Get_By_InstituteID(int InstituteID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_InstituteID_tblInstituteSchool";
            _database.AddInParameter(_sqlcommand, "@InstituteID", DbType.Int32, InstituteID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }


        public static DataSet Get_By_InstituteSchoolID(int InstituteSchoolID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_InstituteSchoolID_tblInstituteSchool";
		 	 _database.AddInParameter(_sqlcommand, "@InstituteSchoolID", DbType.Int32, InstituteSchoolID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityInstituteSchool _entity,out string errorMsg) 
		 {
		 	 int returnResult = -1;
            //string returnError = "";
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblInstituteSchool";
		 	 _database.AddInParameter(_sqlcommand, "@InstituteID", DbType.Int32, _entity.InstituteID); 
		 	 _database.AddInParameter(_sqlcommand, "@SchoolID", DbType.Int32, _entity.SchoolID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.AddOutParameter(_sqlcommand, "@ERROR_MSG", DbType.String,200);
            _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            errorMsg = Convert.ToString(_database.GetParameterValue(_sqlcommand, "@ERROR_MSG"));
            return returnResult;
		 }



		 public static int Update(EntityInstituteSchool _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblInstituteSchool";
		 	 _database.AddInParameter(_sqlcommand, "@InstituteSchoolID", DbType.Int32, _entity.InstituteSchoolID); 
		 	 _database.AddInParameter(_sqlcommand, "@InstituteID", DbType.Int32, _entity.InstituteID); 
		 	 _database.AddInParameter(_sqlcommand, "@SchoolID", DbType.Int32, _entity.SchoolID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_InstituteSchoolID(int InstituteSchoolID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_InstituteSchoolID_tblInstituteSchool";
		 	 _database.AddInParameter(_sqlcommand, "@InstituteSchoolID", DbType.Int32, InstituteSchoolID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
