using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALOffer
	{
		 public DALOffer() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblOffer";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }

        public static DataSet Get_By_IntakeID(int IntakeID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_IntakeID_tblOffer";
            _database.AddInParameter(_sqlcommand, "@IntakeID", DbType.Int32, IntakeID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }

        public static DataSet Get_By_OfferID(int OfferID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_OfferID_tblOffer";
		 	 _database.AddInParameter(_sqlcommand, "@OfferID", DbType.Int32, OfferID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityOffer _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblOffer";
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferType", DbType.String, _entity.OfferType); 
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID1", DbType.Int32, _entity.ProgrammeID1);
            _database.AddInParameter(_sqlcommand, "@ProgrammeID2", DbType.Int32, _entity.ProgrammeID2);
            _database.AddInParameter(_sqlcommand, "@IntakeID1", DbType.Int32, _entity.IntakeID1);
            _database.AddInParameter(_sqlcommand, "@IntakeID2", DbType.Int32, _entity.IntakeID2);
            _database.AddInParameter(_sqlcommand, "@OfferYearID", DbType.Int32, _entity.OfferYearID); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferDate", DbType.Date, _entity.OfferDate); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferCreatedDate", DbType.Date, _entity.OfferCreatedDate); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferLastEditedDate", DbType.Date, _entity.OfferLastEditedDate); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferCancelledDate", DbType.Date, _entity.OfferCancelledDate); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferWithdrawnDate", DbType.Date, _entity.OfferWithdrawnDate); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferStatusID", DbType.Int32, _entity.OfferStatusID);
            _database.AddInParameter(_sqlcommand, "@DocsFileName", DbType.String, _entity.DocsFileName);
            _database.AddInParameter(_sqlcommand, "@File", DbType.Binary, _entity.File);
            _database.AddInParameter(_sqlcommand, "@SchoolID", DbType.Int32, _entity.SchoolID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityOffer _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblOffer";
		 	 _database.AddInParameter(_sqlcommand, "@OfferID", DbType.Int32, _entity.OfferID); 
		 	 //_database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 //_database.AddInParameter(_sqlcommand, "@OfferType", DbType.String, _entity.OfferType); 
		 	 //_database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, _entity.ProgrammeID); 
		 	 //_database.AddInParameter(_sqlcommand, "@IntakeID", DbType.Int32, _entity.IntakeID); 
		 	 //_database.AddInParameter(_sqlcommand, "@OfferYearID", DbType.Int32, _entity.OfferYearID); 
		 	 //_database.AddInParameter(_sqlcommand, "@OfferDate", DbType.Date, _entity.OfferDate); 
		 	 //_database.AddInParameter(_sqlcommand, "@OfferCreatedDate", DbType.Date, _entity.OfferCreatedDate); 
		 	 //_database.AddInParameter(_sqlcommand, "@OfferLastEditedDate", DbType.Date, _entity.OfferLastEditedDate); 
		 	 //_database.AddInParameter(_sqlcommand, "@OfferCancelledDate", DbType.Date, _entity.OfferCancelledDate); 
		 	 //_database.AddInParameter(_sqlcommand, "@OfferWithdrawnDate", DbType.Date, _entity.OfferWithdrawnDate); 
		 	 _database.AddInParameter(_sqlcommand, "@OfferStatusID", DbType.Int32, _entity.OfferStatusID);
            //_database.AddInParameter(_sqlcommand, "@DocsFileName", DbType.String, _entity.DocsFileName);
            //_database.AddInParameter(_sqlcommand, "@File", DbType.Binary, _entity.File);
            //_database.AddInParameter(_sqlcommand, "@SchoolID", DbType.Int32, _entity.SchoolID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static int DoCancel(int OfferID)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_DoCancel_tblOffer";
            _database.AddInParameter(_sqlcommand, "@OfferID", DbType.Int32, OfferID);           
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }

        public static int DoWithdraw(int OfferID)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_DoWithdraw_tblOffer";
            _database.AddInParameter(_sqlcommand, "@OfferID", DbType.Int32, OfferID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }



        public static int Delete_By_OfferID(int OfferID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_OfferID_tblOffer";
		 	 _database.AddInParameter(_sqlcommand, "@OfferID", DbType.Int32, OfferID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
