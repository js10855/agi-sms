using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALClientWorkExperience
	{
		 public DALClientWorkExperience() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblClientWorkExperience";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_ClientWorkExperienceID(int ClientWorkExperienceID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_ClientWorkExperienceID_tblClientWorkExperience";
		 	 _database.AddInParameter(_sqlcommand, "@ClientWorkExperienceID", DbType.Int32, ClientWorkExperienceID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityClientWorkExperience _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblClientWorkExperience";
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@EmployerName", DbType.String, _entity.EmployerName); 
		 	 _database.AddInParameter(_sqlcommand, "@EmployerEmail", DbType.String, _entity.EmployerEmail); 
		 	 _database.AddInParameter(_sqlcommand, "@EmployerWebsite", DbType.String, _entity.EmployerWebsite); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@JobTitle", DbType.String, _entity.JobTitle); 
		 	 _database.AddInParameter(_sqlcommand, "@CurrentlyWorking", DbType.Boolean, _entity.CurrentlyWorking); 
		 	 _database.AddInParameter(_sqlcommand, "@FromDate", DbType.DateTime, _entity.FromDate); 
		 	 _database.AddInParameter(_sqlcommand, "@ToDate", DbType.DateTime, _entity.ToDate); 
		 	 _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityClientWorkExperience _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblClientWorkExperience";
		 	 _database.AddInParameter(_sqlcommand, "@ClientWorkExperienceID", DbType.Int32, _entity.ClientWorkExperienceID); 
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@EmployerName", DbType.String, _entity.EmployerName); 
		 	 _database.AddInParameter(_sqlcommand, "@EmployerEmail", DbType.String, _entity.EmployerEmail); 
		 	 _database.AddInParameter(_sqlcommand, "@EmployerWebsite", DbType.String, _entity.EmployerWebsite); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@JobTitle", DbType.String, _entity.JobTitle); 
		 	 _database.AddInParameter(_sqlcommand, "@CurrentlyWorking", DbType.Boolean, _entity.CurrentlyWorking); 
		 	 _database.AddInParameter(_sqlcommand, "@FromDate", DbType.DateTime, _entity.FromDate); 
		 	 _database.AddInParameter(_sqlcommand, "@ToDate", DbType.DateTime, _entity.ToDate); 
		 	 _database.AddInParameter(_sqlcommand, "@PostCode", DbType.String, _entity.PostCode); 
		 	 _database.AddInParameter(_sqlcommand, "@City", DbType.String, _entity.City); 
		 	 _database.AddInParameter(_sqlcommand, "@State", DbType.String, _entity.State); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.String, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_ClientWorkExperienceID(int ClientWorkExperienceID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_ClientWorkExperienceID_tblClientWorkExperience";
		 	 _database.AddInParameter(_sqlcommand, "@ClientWorkExperienceID", DbType.Int32, ClientWorkExperienceID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }
        public static DataSet Get_By_ClientID(int ClientID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ClientID_tblClientWorkExperience";
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, ClientID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }


    }
}
