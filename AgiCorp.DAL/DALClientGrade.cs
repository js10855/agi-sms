using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALClientGrade
	{
		 public DALClientGrade() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblClientGrade";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_ClientGradeID(int ClientGradeID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_ClientGradeID_tblClientGrade";
		 	 _database.AddInParameter(_sqlcommand, "@ClientGradeID", DbType.Int32, ClientGradeID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityClientGrade _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblClientGrade";
		 	 _database.AddInParameter(_sqlcommand, "@ClassID", DbType.Int32, _entity.ClassID); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleID", DbType.Int32, _entity.ModuleID); 
		 	 _database.AddInParameter(_sqlcommand, "@GradeID", DbType.Int32, _entity.GradeID); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityClientGrade _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblClientGrade";
		 	 _database.AddInParameter(_sqlcommand, "@ClientGradeID", DbType.Int32, _entity.ClientGradeID); 
		 	 _database.AddInParameter(_sqlcommand, "@ClassID", DbType.Int32, _entity.ClassID); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleID", DbType.Int32, _entity.ModuleID); 
		 	 _database.AddInParameter(_sqlcommand, "@GradeID", DbType.Int32, _entity.GradeID); 
		 	 _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_ClientGradeID(int ClientGradeID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_ClientGradeID_tblClientGrade";
		 	 _database.AddInParameter(_sqlcommand, "@ClientGradeID", DbType.Int32, ClientGradeID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static DataSet Get_By_ClientID(int ClientID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ClientID_tblClientGrade";
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, ClientID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }



    }
}
