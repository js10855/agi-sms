using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALAgentCommission
	{
		 public DALAgentCommission() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblAgentCommission";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }

        public static DataSet Get_By_AgentID(int AgentID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_AgentID_tblAgentCommission";
            _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, AgentID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }

        public static DataSet Get_By_AgentCommissionID(int AgentCommissionID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_AgentCommissionID_tblAgentCommission";
		 	 _database.AddInParameter(_sqlcommand, "@AgentCommissionID", DbType.Int32, AgentCommissionID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityAgentCommission _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblAgentCommission";
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.Int32, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.String, _entity.ProgrammeID); 
		 	 _database.AddInParameter(_sqlcommand, "@CommissionTypeID", DbType.Int32, _entity.CommissionTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@CommissionPer", DbType.String, _entity.CommissionPer); 
		 	 _database.AddInParameter(_sqlcommand, "@CommissionFixed", DbType.String, _entity.CommissionFixed); 
		 	 _database.AddInParameter(_sqlcommand, "@StartDate", DbType.Date, _entity.StartDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EndDate", DbType.Date, _entity.EndDate);
            _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, _entity.AgentID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityAgentCommission _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblAgentCommission";
		 	 _database.AddInParameter(_sqlcommand, "@AgentCommissionID", DbType.Int32, _entity.AgentCommissionID); 
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.Int32, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.String, _entity.ProgrammeID); 
		 	 _database.AddInParameter(_sqlcommand, "@CommissionTypeID", DbType.Int32, _entity.CommissionTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@CommissionPer", DbType.String, _entity.CommissionPer); 
		 	 _database.AddInParameter(_sqlcommand, "@CommissionFixed", DbType.String, _entity.CommissionFixed); 
		 	 _database.AddInParameter(_sqlcommand, "@StartDate", DbType.Date, _entity.StartDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EndDate", DbType.Date, _entity.EndDate);
            _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, _entity.AgentID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_AgentCommissionID(int AgentCommissionID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_AgentCommissionID_tblAgentCommission";
		 	 _database.AddInParameter(_sqlcommand, "@AgentCommissionID", DbType.Int32, AgentCommissionID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
