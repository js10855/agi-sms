using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALAgentContact
	{
		 public DALAgentContact() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblAgentContact";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }

        public static DataSet Get_By_AgentID(int AgentID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_AgentID_tblAgentContact";
            _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, AgentID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }


        public static DataSet Get_By_AgentContactID(int AgentContactID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_AgentContactID_tblAgentContact";
		 	 _database.AddInParameter(_sqlcommand, "@AgentContactID", DbType.Int32, AgentContactID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityAgentContact _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblAgentContact";
		 	 _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, _entity.AgentID); 
		 	 _database.AddInParameter(_sqlcommand, "@FirstName", DbType.String, _entity.FirstName); 
		 	 _database.AddInParameter(_sqlcommand, "@MiddleName", DbType.String, _entity.MiddleName); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@MobileCountryCode", DbType.String, _entity.MobileCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Mobile", DbType.String, _entity.Mobile); 
		 	 _database.AddInParameter(_sqlcommand, "@IsPrimaryContact", DbType.Boolean, _entity.IsPrimaryContact); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityAgentContact _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblAgentContact";
		 	 _database.AddInParameter(_sqlcommand, "@AgentContactID", DbType.Int32, _entity.AgentContactID); 
		 	 _database.AddInParameter(_sqlcommand, "@AgentID", DbType.Int32, _entity.AgentID); 
		 	 _database.AddInParameter(_sqlcommand, "@FirstName", DbType.String, _entity.FirstName); 
		 	 _database.AddInParameter(_sqlcommand, "@MiddleName", DbType.String, _entity.MiddleName); 
		 	 _database.AddInParameter(_sqlcommand, "@LastName", DbType.String, _entity.LastName); 
		 	 _database.AddInParameter(_sqlcommand, "@Email", DbType.String, _entity.Email); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneCountryCode", DbType.String, _entity.PhoneCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@PhoneStateCode", DbType.String, _entity.PhoneStateCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Phone", DbType.String, _entity.Phone); 
		 	 _database.AddInParameter(_sqlcommand, "@MobileCountryCode", DbType.String, _entity.MobileCountryCode); 
		 	 _database.AddInParameter(_sqlcommand, "@Mobile", DbType.String, _entity.Mobile); 
		 	 _database.AddInParameter(_sqlcommand, "@IsPrimaryContact", DbType.Boolean, _entity.IsPrimaryContact); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_AgentContactID(int AgentContactID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_AgentContactID_tblAgentContact";
		 	 _database.AddInParameter(_sqlcommand, "@AgentContactID", DbType.Int32, AgentContactID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
