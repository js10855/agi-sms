using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALFees
	{
		 public DALFees() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblFees";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }

        public static DataSet Get_Fee_Desc_Country()
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_Fee_Desc_Country_tblFees";
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }



        public static DataSet Get_By_FeeID(int FeeID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_FeeID_tblFees";
		 	 _database.AddInParameter(_sqlcommand, "@FeeID", DbType.Int32, FeeID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }

        public static DataSet Get_By_ProgrammeID(int ProgrammeID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ProgrammeID_tblFees";
            _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, ProgrammeID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }

        public static DataSet Get_By_ProgrammeID_CountryID(int ProgrammeID,int CountryID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ProgrammeID_CountryID_tblFees";
            _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, ProgrammeID);
            _database.AddInParameter(_sqlcommand, "@CountryID", DbType.Int32, CountryID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }



        public static int SET(EntityFees _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblFees";
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, _entity.ProgrammeID); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.Int32, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddInParameter(_sqlcommand, "@TutionFees", DbType.Double, _entity.TutionFees); 
		 	 _database.AddInParameter(_sqlcommand, "@MaterialFees", DbType.Double, _entity.MaterialFees); 
		 	 _database.AddInParameter(_sqlcommand, "@Insurance", DbType.Double, _entity.Insurance); 
		 	 _database.AddInParameter(_sqlcommand, "@RegistrationFees", DbType.Double, _entity.RegistrationFees); 
		 	 _database.AddInParameter(_sqlcommand, "@AdministrationFees", DbType.Double, _entity.AdministrationFees); 
		 	 _database.AddInParameter(_sqlcommand, "@OtherFees", DbType.Double, _entity.OtherFees);
            _database.AddInParameter(_sqlcommand, "@TotalFees", DbType.Double, _entity.TotalFees);
            _database.AddInParameter(_sqlcommand, "@AirportPickupFees", DbType.Double, _entity.AirportPickupFees);
            _database.AddInParameter(_sqlcommand, "@HomeStayPlacementFees", DbType.Double, _entity.HomeStayPlacementFees);
            _database.AddInParameter(_sqlcommand, "@HomeStayFeesPerWeek", DbType.Double, _entity.HomeStayFeesPerWeek);
            _database.AddInParameter(_sqlcommand, "@StartDate", DbType.DateTime, _entity.StartDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EndDate", DbType.DateTime, _entity.EndDate); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityFees _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblFees";
		 	 _database.AddInParameter(_sqlcommand, "@FeeID", DbType.Int32, _entity.FeeID); 
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, _entity.ProgrammeID); 
		 	 _database.AddInParameter(_sqlcommand, "@CountryID", DbType.Int32, _entity.CountryID); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddInParameter(_sqlcommand, "@TutionFees", DbType.Double, _entity.TutionFees); 
		 	 _database.AddInParameter(_sqlcommand, "@MaterialFees", DbType.Double, _entity.MaterialFees); 
		 	 _database.AddInParameter(_sqlcommand, "@Insurance", DbType.Double, _entity.Insurance); 
		 	 _database.AddInParameter(_sqlcommand, "@RegistrationFees", DbType.Double, _entity.RegistrationFees); 
		 	 _database.AddInParameter(_sqlcommand, "@AdministrationFees", DbType.Double, _entity.AdministrationFees); 
		 	 _database.AddInParameter(_sqlcommand, "@OtherFees", DbType.Double, _entity.OtherFees);
            _database.AddInParameter(_sqlcommand, "@TotalFees", DbType.Double, _entity.TotalFees);
            _database.AddInParameter(_sqlcommand, "@AirportPickupFees", DbType.Double, _entity.AirportPickupFees);
            _database.AddInParameter(_sqlcommand, "@HomeStayPlacementFees", DbType.Double, _entity.HomeStayPlacementFees);
            _database.AddInParameter(_sqlcommand, "@HomeStayFeesPerWeek", DbType.Double, _entity.HomeStayFeesPerWeek);
            _database.AddInParameter(_sqlcommand, "@StartDate", DbType.DateTime, _entity.StartDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EndDate", DbType.DateTime, _entity.EndDate); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_FeesID(int FeeID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_FeeID_tblFees";
		 	 _database.AddInParameter(_sqlcommand, "@FeeID", DbType.Int32, FeeID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static Boolean Check_For_UniqueCode(string Code, string FeeID)
        {
            Boolean returnResult = false;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_CHECK_FOR_UniqueCode_tblFees";
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, Code);
            _database.AddInParameter(_sqlcommand, "@FeeID", DbType.String, FeeID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToBoolean(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }


    }
}
