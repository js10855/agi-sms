using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
    public class DALClientPassport
    {
        public DALClientPassport()
        {
        }

        public static DataSet Get_All()
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_ALL_tblClientPassport";
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }

        public static DataSet Get_By_ClientID(int ClientID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ClientID_tblClientPassport";
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, ClientID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }


        public static DataSet Get_By_ClientPassportID(int ClientPassportID)
        {
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_GET_BY_ClientPassportID_tblClientPassport";
            _database.AddInParameter(_sqlcommand, "@ClientPassportID", DbType.Int32, ClientPassportID);
            DataSet ds = new DataSet();
            ds = _database.ExecuteDataSet(_sqlcommand);
            if (ds != null)
                return ds;
            return null;
        }



        public static int SET(EntityClientPassport _entity)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_SET_tblClientPassport";
            _database.AddInParameter(_sqlcommand, "@PassportNumber", DbType.String, _entity.PassportNumber);
            _database.AddInParameter(_sqlcommand, "@PassportIssueDate", DbType.DateTime, _entity.PassportIssueDate);
            _database.AddInParameter(_sqlcommand, "@PassportExpiryDate", DbType.DateTime, _entity.PassportExpiryDate);
            _database.AddInParameter(_sqlcommand, "@PassportIssueCountryID", DbType.Int32, _entity.PassportIssueCountryID);
            _database.AddInParameter(_sqlcommand, "@VisaNumber", DbType.String, _entity.VisaNumber);
            _database.AddInParameter(_sqlcommand, "@VisaType", DbType.String, _entity.VisaType);
            _database.AddInParameter(_sqlcommand, "@VisaIssueDate", DbType.DateTime, _entity.VisaIssueDate);
            _database.AddInParameter(_sqlcommand, "@VisaExpiryDate", DbType.DateTime, _entity.VisaExpiryDate);
            _database.AddInParameter(_sqlcommand, "@InsuranceProvider", DbType.String, _entity.InsuranceProvider);
            _database.AddInParameter(_sqlcommand, "@InsuranceNumber", DbType.String, _entity.InsuranceNumber);
            _database.AddInParameter(_sqlcommand, "@InsuranceExpiryDate", DbType.DateTime, _entity.InsuranceExpiryDate);
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }



        public static int Update(EntityClientPassport _entity)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_UPDATE_tblClientPassport";
            _database.AddInParameter(_sqlcommand, "@ClientPassportID", DbType.Int32, _entity.ClientPassportID);
            _database.AddInParameter(_sqlcommand, "@PassportNumber", DbType.String, _entity.PassportNumber);
            _database.AddInParameter(_sqlcommand, "@PassportIssueDate", DbType.DateTime, _entity.PassportIssueDate);
            _database.AddInParameter(_sqlcommand, "@PassportExpiryDate", DbType.DateTime, _entity.PassportExpiryDate);
            _database.AddInParameter(_sqlcommand, "@PassportIssueCountryID", DbType.Int32, _entity.PassportIssueCountryID);
            _database.AddInParameter(_sqlcommand, "@VisaNumber", DbType.String, _entity.VisaNumber);
            _database.AddInParameter(_sqlcommand, "@VisaType", DbType.String, _entity.VisaType);
            _database.AddInParameter(_sqlcommand, "@VisaIssueDate", DbType.DateTime, _entity.VisaIssueDate);
            _database.AddInParameter(_sqlcommand, "@VisaExpiryDate", DbType.DateTime, _entity.VisaExpiryDate);
            _database.AddInParameter(_sqlcommand, "@InsuranceProvider", DbType.String, _entity.InsuranceProvider);
            _database.AddInParameter(_sqlcommand, "@InsuranceNumber", DbType.String, _entity.InsuranceNumber);
            _database.AddInParameter(_sqlcommand, "@InsuranceExpiryDate", DbType.DateTime, _entity.InsuranceExpiryDate);
            _database.AddInParameter(_sqlcommand, "@ClientID", DbType.Int32, _entity.ClientID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }



        public static int Delete_By_ClientPassportID(int ClientPassportID)
        {
            int returnResult = -1;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_DELETE_BY_ClientPassportID_tblClientPassport";
            _database.AddInParameter(_sqlcommand, "@ClientPassportID", DbType.Int32, ClientPassportID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }


    }
}
