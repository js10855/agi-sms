using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALQualification
	{
		 public DALQualification() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblQualification";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_QualificationID(int QualificationID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_QualificationID_tblQualification";
		 	 _database.AddInParameter(_sqlcommand, "@QualificationID", DbType.Int32, QualificationID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityQualification _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblQualification";
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@NZQACode", DbType.Int32, _entity.NZQACode); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@Credits", DbType.Int32, _entity.Credits);
            _database.AddInParameter(_sqlcommand, "@HoursPerCredit", DbType.Int32, _entity.HoursPerCredit);
            _database.AddInParameter(_sqlcommand, "@TotalHours", DbType.Int32, _entity.TotalHours);
            _database.AddInParameter(_sqlcommand, "@F2FHours", DbType.Int32, _entity.F2FHours); 
		 	 _database.AddInParameter(_sqlcommand, "@SDHours", DbType.Int32, _entity.SDHours); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive);
            _database.AddInParameter(_sqlcommand, "@QualificationTypeID", DbType.Int32, _entity.QualificationTypeID);
            _database.AddInParameter(_sqlcommand, "@QualificationLevelID", DbType.Int32, _entity.QualificationLevelID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityQualification _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblQualification";
		 	 _database.AddInParameter(_sqlcommand, "@QualificationID", DbType.Int32, _entity.QualificationID); 
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@NZQACode", DbType.Int32, _entity.NZQACode); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@Credits", DbType.Int32, _entity.Credits);
            _database.AddInParameter(_sqlcommand, "@HoursPerCredit", DbType.Int32, _entity.HoursPerCredit);
            _database.AddInParameter(_sqlcommand, "@TotalHours", DbType.Int32, _entity.TotalHours);
            _database.AddInParameter(_sqlcommand, "@F2FHours", DbType.Int32, _entity.F2FHours); 
		 	 _database.AddInParameter(_sqlcommand, "@SDHours", DbType.Int32, _entity.SDHours); 
		 	 _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive);
            _database.AddInParameter(_sqlcommand, "@QualificationTypeID", DbType.Int32, _entity.QualificationTypeID);
            _database.AddInParameter(_sqlcommand, "@QualificationLevelID", DbType.Int32, _entity.QualificationLevelID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_QualificationID(int QualificationID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_QualificationID_tblQualification";
		 	 _database.AddInParameter(_sqlcommand, "@QualificationID", DbType.Int32, QualificationID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static Boolean Check_For_UniqueCode(string Code, string QualificationID)
        {
            Boolean returnResult = false;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_CHECK_FOR_UniqueCode_tblQualification";
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, Code);
            _database.AddInParameter(_sqlcommand, "@QualificationID", DbType.String, QualificationID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToBoolean(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }

    }
}
