using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALModuleAssessment
	{
		 public DALModuleAssessment() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblModuleAssessment";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_ModuleID(int ModuleID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_ModuleID_tblModuleAssessment";
		 	 _database.AddInParameter(_sqlcommand, "@ModuleID", DbType.Int32, ModuleID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityModuleAssessment _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblModuleAssessment";
		 	 _database.AddInParameter(_sqlcommand, "@AssessmentNumber", DbType.String, _entity.AssessmentNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@AssessmentName", DbType.String, _entity.AssessmentName); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleAssessmentTypeID", DbType.Int32, _entity.ModuleAssessmentTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleAssessmentFormatID", DbType.Int32, _entity.ModuleAssessmentFormatID); 
		 	 _database.AddInParameter(_sqlcommand, "@LOCovered", DbType.String, _entity.LOCovered); 
		 	 _database.AddInParameter(_sqlcommand, "@TotalMarks", DbType.Int32, _entity.TotalMarks); 
		 	 _database.AddInParameter(_sqlcommand, "@MinPassPercentage", DbType.Int32, _entity.MinPassPercentage); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleID", DbType.Int32, _entity.ModuleID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityModuleAssessment _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblModuleAssessment";
		 	 _database.AddInParameter(_sqlcommand, "@ModuleAssessmentID", DbType.Int32, _entity.ModuleAssessmentID); 
		 	 _database.AddInParameter(_sqlcommand, "@AssessmentNumber", DbType.String, _entity.AssessmentNumber); 
		 	 _database.AddInParameter(_sqlcommand, "@AssessmentName", DbType.String, _entity.AssessmentName); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleAssessmentTypeID", DbType.Int32, _entity.ModuleAssessmentTypeID); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleAssessmentFormatID", DbType.Int32, _entity.ModuleAssessmentFormatID); 
		 	 _database.AddInParameter(_sqlcommand, "@LOCovered", DbType.String, _entity.LOCovered); 
		 	 _database.AddInParameter(_sqlcommand, "@TotalMarks", DbType.Int32, _entity.TotalMarks); 
		 	 _database.AddInParameter(_sqlcommand, "@MinPassPercentage", DbType.Int32, _entity.MinPassPercentage); 
		 	 _database.AddInParameter(_sqlcommand, "@ModuleID", DbType.Int32, _entity.ModuleID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_ModuleAssessmentID(int ModuleAssessmentID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_ModuleAssessmentID_tblModuleAssessment";
		 	 _database.AddInParameter(_sqlcommand, "@ModuleAssessmentID", DbType.Int32, ModuleAssessmentID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }


	}
}
