using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using AgiCorp.Entity;

namespace AgiCorp.DAL
{
	public class DALIntake
	{
		 public DALIntake() 
		 {
		 }

		 public static DataSet Get_All()
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_ALL_tblIntake";
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static DataSet Get_By_IntakeID(int IntakeID) 
		 {
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_GET_BY_IntakeID_tblIntake";
		 	 _database.AddInParameter(_sqlcommand, "@IntakeID", DbType.Int32, IntakeID); 
		 	 DataSet ds = new DataSet();
		 	 ds = _database.ExecuteDataSet(_sqlcommand);
		 	 if (ds != null)
		 	 	 	 return ds;
		 	 return null;
		 }



		 public static int SET(EntityIntake _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_SET_tblIntake";
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, _entity.ProgrammeID); 
		 	 _database.AddInParameter(_sqlcommand, "@FeeID", DbType.Int32, _entity.FeeID); 
		 	 _database.AddInParameter(_sqlcommand, "@Capacity", DbType.Int32, _entity.Capacity); 
		 	 _database.AddInParameter(_sqlcommand, "@Month", DbType.Int32, _entity.Month); 
		 	 _database.AddInParameter(_sqlcommand, "@Year", DbType.Int32, _entity.Year); 
		 	 _database.AddInParameter(_sqlcommand, "@StartDate", DbType.DateTime, _entity.StartDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EndDate", DbType.DateTime, _entity.EndDate);
            //_database.AddInParameter(_sqlcommand, "@BreakStartDate", DbType.DateTime, _entity.BreakStartDate); 
            //_database.AddInParameter(_sqlcommand, "@BreakEndDate", DbType.DateTime, _entity.BreakEndDate); 
            _database.AddInParameter(_sqlcommand, "@IsSemesterBreakApplicable", DbType.Boolean, _entity.IsSemesterBreakApplicable);
            _database.AddInParameter(_sqlcommand, "@IsSummerBreakApplicable", DbType.Boolean, _entity.IsSemesterBreakApplicable);
            if (!_entity.IsSemesterBreakApplicable)
            {
                _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, null);
                _database.AddInParameter(_sqlcommand, "@SemesterBreakToDate", DbType.Date, null);
            }
            else
            {
                _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, _entity.SemesterBreakFromDate);
                _database.AddInParameter(_sqlcommand, "@SemesterBreakToDate", DbType.Date, _entity.SemesterBreakToDate);
            }
            if (!_entity.IsSummerBreakApplicable)
            {
                _database.AddInParameter(_sqlcommand, "@SummerBreakFromDate", DbType.Date, null);
                _database.AddInParameter(_sqlcommand, "@SummerBreakToDate", DbType.Date, null);
            }
            else
            {
                _database.AddInParameter(_sqlcommand, "@SummerBreakFromDate", DbType.Date, _entity.SummerBreakFromDate);
                _database.AddInParameter(_sqlcommand, "@SummerBreakToDate", DbType.Date, _entity.SummerBreakToDate);
            }

            _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Update(EntityIntake _entity) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_UPDATE_tblIntake";
		 	 _database.AddInParameter(_sqlcommand, "@IntakeID", DbType.Int32, _entity.IntakeID); 
		 	 _database.AddInParameter(_sqlcommand, "@Code", DbType.String, _entity.Code); 
		 	 _database.AddInParameter(_sqlcommand, "@Description", DbType.String, _entity.Description); 
		 	 _database.AddInParameter(_sqlcommand, "@ProgrammeID", DbType.Int32, _entity.ProgrammeID); 
		 	 _database.AddInParameter(_sqlcommand, "@FeeID", DbType.Int32, _entity.FeeID); 
		 	 _database.AddInParameter(_sqlcommand, "@Capacity", DbType.Int32, _entity.Capacity); 
		 	 _database.AddInParameter(_sqlcommand, "@Month", DbType.Int32, _entity.Month); 
		 	 _database.AddInParameter(_sqlcommand, "@Year", DbType.Int32, _entity.Year); 
		 	 _database.AddInParameter(_sqlcommand, "@StartDate", DbType.DateTime, _entity.StartDate); 
		 	 _database.AddInParameter(_sqlcommand, "@EndDate", DbType.DateTime, _entity.EndDate);
            //_database.AddInParameter(_sqlcommand, "@BreakStartDate", DbType.DateTime, _entity.BreakStartDate); 
            //_database.AddInParameter(_sqlcommand, "@BreakEndDate", DbType.DateTime, _entity.BreakEndDate); 
            _database.AddInParameter(_sqlcommand, "@IsSemesterBreakApplicable", DbType.Boolean, _entity.IsSemesterBreakApplicable);
            _database.AddInParameter(_sqlcommand, "@IsSummerBreakApplicable", DbType.Boolean, _entity.IsSemesterBreakApplicable);
            if (!_entity.IsSemesterBreakApplicable)
            {
                _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, null);
                _database.AddInParameter(_sqlcommand, "@SemesterBreakToDate", DbType.Date, null);
            }
            else
            {
                _database.AddInParameter(_sqlcommand, "@SemesterBreakFromDate", DbType.Date, _entity.SemesterBreakFromDate);
                _database.AddInParameter(_sqlcommand, "@SemesterBreakToDate", DbType.Date, _entity.SemesterBreakToDate);
            }
            if (!_entity.IsSummerBreakApplicable)
            {
                _database.AddInParameter(_sqlcommand, "@SummerBreakFromDate", DbType.Date, null);
                _database.AddInParameter(_sqlcommand, "@SummerBreakToDate", DbType.Date, null);
            }
            else
            {
                _database.AddInParameter(_sqlcommand, "@SummerBreakFromDate", DbType.Date, _entity.SummerBreakFromDate);
                _database.AddInParameter(_sqlcommand, "@SummerBreakToDate", DbType.Date, _entity.SummerBreakToDate);
            }
            _database.AddInParameter(_sqlcommand, "@IsActive", DbType.Boolean, _entity.IsActive); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }



		 public static int Delete_By_IntakeID(int IntakeID) 
		 {
		 	 int returnResult = -1;
		 	 Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
		 	 SqlCommand _sqlcommand = new SqlCommand();
		 	 _sqlcommand.CommandType = CommandType.StoredProcedure;
		 	 _sqlcommand.CommandText = "SCP_DELETE_BY_IntakeID_tblIntake";
		 	 _database.AddInParameter(_sqlcommand, "@IntakeID", DbType.Int32, IntakeID); 
		 	 _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
		 	 _database.ExecuteNonQuery(_sqlcommand);
		 	 returnResult = Convert.ToInt32(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
		 	 return returnResult;
		 }

        public static Boolean Check_For_UniqueCode(string Code, string IntakeID)
        {
            Boolean returnResult = false;
            Database _database = DatabaseFactory.CreateDatabase(ConfigurationManager.ConnectionStrings["CDB"].Name);
            SqlCommand _sqlcommand = new SqlCommand();
            _sqlcommand.CommandType = CommandType.StoredProcedure;
            _sqlcommand.CommandText = "SCP_CHECK_FOR_UniqueCode_tblIntake";
            _database.AddInParameter(_sqlcommand, "@Code", DbType.String, Code);
            _database.AddInParameter(_sqlcommand, "@IntakeID", DbType.String, IntakeID);
            _database.AddOutParameter(_sqlcommand, "@RETURN_VALUE", DbType.Int32, 0);
            _database.ExecuteNonQuery(_sqlcommand);
            returnResult = Convert.ToBoolean(_database.GetParameterValue(_sqlcommand, "@RETURN_VALUE"));
            return returnResult;
        }

    }
}
